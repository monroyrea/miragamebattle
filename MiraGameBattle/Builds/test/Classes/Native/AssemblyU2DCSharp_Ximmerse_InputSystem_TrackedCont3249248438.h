﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerI1382602808.h"

// Ximmerse.InputSystem.IInputTracking
struct IInputTracking_t561171318;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.InputSystem.TrackedControllerInput
struct  TrackedControllerInput_t3249248438  : public ControllerInput_t1382602808
{
public:
	// Ximmerse.InputSystem.IInputTracking Ximmerse.InputSystem.TrackedControllerInput::inputTracking
	Il2CppObject * ___inputTracking_9;
	// System.Int32 Ximmerse.InputSystem.TrackedControllerInput::node
	int32_t ___node_10;

public:
	inline static int32_t get_offset_of_inputTracking_9() { return static_cast<int32_t>(offsetof(TrackedControllerInput_t3249248438, ___inputTracking_9)); }
	inline Il2CppObject * get_inputTracking_9() const { return ___inputTracking_9; }
	inline Il2CppObject ** get_address_of_inputTracking_9() { return &___inputTracking_9; }
	inline void set_inputTracking_9(Il2CppObject * value)
	{
		___inputTracking_9 = value;
		Il2CppCodeGenWriteBarrier(&___inputTracking_9, value);
	}

	inline static int32_t get_offset_of_node_10() { return static_cast<int32_t>(offsetof(TrackedControllerInput_t3249248438, ___node_10)); }
	inline int32_t get_node_10() const { return ___node_10; }
	inline int32_t* get_address_of_node_10() { return &___node_10; }
	inline void set_node_10(int32_t value)
	{
		___node_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
