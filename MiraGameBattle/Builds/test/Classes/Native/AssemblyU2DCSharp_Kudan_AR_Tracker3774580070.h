﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Kudan_AR_TrackerBase1411547817.h"
#include "UnityEngine_UnityEngine_ScreenOrientation4019489636.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"

// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kudan.AR.Tracker
struct  Tracker_t3774580070  : public TrackerBase_t1411547817
{
public:
	// UnityEngine.Renderer Kudan.AR.Tracker::_background
	Renderer_t257310565 * ____background_14;
	// UnityEngine.MeshFilter Kudan.AR.Tracker::_cameraBackgroundMeshFilter
	MeshFilter_t3026937449 * ____cameraBackgroundMeshFilter_15;
	// UnityEngine.Texture2D Kudan.AR.Tracker::_textureYp
	Texture2D_t3542995729 * ____textureYp_16;
	// System.Int32 Kudan.AR.Tracker::_textureYpID
	int32_t ____textureYpID_17;
	// UnityEngine.Texture2D Kudan.AR.Tracker::_textureCbCr
	Texture2D_t3542995729 * ____textureCbCr_18;
	// System.Int32 Kudan.AR.Tracker::_textureCbCrID
	int32_t ____textureCbCrID_19;
	// System.Single Kudan.AR.Tracker::_cameraAspect
	float ____cameraAspect_20;
	// UnityEngine.ScreenOrientation Kudan.AR.Tracker::_prevScreenOrientation
	int32_t ____prevScreenOrientation_21;
	// UnityEngine.Matrix4x4 Kudan.AR.Tracker::_projectionRotation
	Matrix4x4_t2933234003  ____projectionRotation_22;
	// System.Int32 Kudan.AR.Tracker::_numFramesGrabbed
	int32_t ____numFramesGrabbed_24;
	// System.Int32 Kudan.AR.Tracker::_numFramesTracked
	int32_t ____numFramesTracked_25;
	// System.Int32 Kudan.AR.Tracker::_numFramesProcessed
	int32_t ____numFramesProcessed_26;
	// System.Int32 Kudan.AR.Tracker::_numFramesRendered
	int32_t ____numFramesRendered_27;
	// System.Single Kudan.AR.Tracker::_rateTimer
	float ____rateTimer_28;
	// System.Boolean Kudan.AR.Tracker::receivingInput
	bool ___receivingInput_29;

public:
	inline static int32_t get_offset_of__background_14() { return static_cast<int32_t>(offsetof(Tracker_t3774580070, ____background_14)); }
	inline Renderer_t257310565 * get__background_14() const { return ____background_14; }
	inline Renderer_t257310565 ** get_address_of__background_14() { return &____background_14; }
	inline void set__background_14(Renderer_t257310565 * value)
	{
		____background_14 = value;
		Il2CppCodeGenWriteBarrier(&____background_14, value);
	}

	inline static int32_t get_offset_of__cameraBackgroundMeshFilter_15() { return static_cast<int32_t>(offsetof(Tracker_t3774580070, ____cameraBackgroundMeshFilter_15)); }
	inline MeshFilter_t3026937449 * get__cameraBackgroundMeshFilter_15() const { return ____cameraBackgroundMeshFilter_15; }
	inline MeshFilter_t3026937449 ** get_address_of__cameraBackgroundMeshFilter_15() { return &____cameraBackgroundMeshFilter_15; }
	inline void set__cameraBackgroundMeshFilter_15(MeshFilter_t3026937449 * value)
	{
		____cameraBackgroundMeshFilter_15 = value;
		Il2CppCodeGenWriteBarrier(&____cameraBackgroundMeshFilter_15, value);
	}

	inline static int32_t get_offset_of__textureYp_16() { return static_cast<int32_t>(offsetof(Tracker_t3774580070, ____textureYp_16)); }
	inline Texture2D_t3542995729 * get__textureYp_16() const { return ____textureYp_16; }
	inline Texture2D_t3542995729 ** get_address_of__textureYp_16() { return &____textureYp_16; }
	inline void set__textureYp_16(Texture2D_t3542995729 * value)
	{
		____textureYp_16 = value;
		Il2CppCodeGenWriteBarrier(&____textureYp_16, value);
	}

	inline static int32_t get_offset_of__textureYpID_17() { return static_cast<int32_t>(offsetof(Tracker_t3774580070, ____textureYpID_17)); }
	inline int32_t get__textureYpID_17() const { return ____textureYpID_17; }
	inline int32_t* get_address_of__textureYpID_17() { return &____textureYpID_17; }
	inline void set__textureYpID_17(int32_t value)
	{
		____textureYpID_17 = value;
	}

	inline static int32_t get_offset_of__textureCbCr_18() { return static_cast<int32_t>(offsetof(Tracker_t3774580070, ____textureCbCr_18)); }
	inline Texture2D_t3542995729 * get__textureCbCr_18() const { return ____textureCbCr_18; }
	inline Texture2D_t3542995729 ** get_address_of__textureCbCr_18() { return &____textureCbCr_18; }
	inline void set__textureCbCr_18(Texture2D_t3542995729 * value)
	{
		____textureCbCr_18 = value;
		Il2CppCodeGenWriteBarrier(&____textureCbCr_18, value);
	}

	inline static int32_t get_offset_of__textureCbCrID_19() { return static_cast<int32_t>(offsetof(Tracker_t3774580070, ____textureCbCrID_19)); }
	inline int32_t get__textureCbCrID_19() const { return ____textureCbCrID_19; }
	inline int32_t* get_address_of__textureCbCrID_19() { return &____textureCbCrID_19; }
	inline void set__textureCbCrID_19(int32_t value)
	{
		____textureCbCrID_19 = value;
	}

	inline static int32_t get_offset_of__cameraAspect_20() { return static_cast<int32_t>(offsetof(Tracker_t3774580070, ____cameraAspect_20)); }
	inline float get__cameraAspect_20() const { return ____cameraAspect_20; }
	inline float* get_address_of__cameraAspect_20() { return &____cameraAspect_20; }
	inline void set__cameraAspect_20(float value)
	{
		____cameraAspect_20 = value;
	}

	inline static int32_t get_offset_of__prevScreenOrientation_21() { return static_cast<int32_t>(offsetof(Tracker_t3774580070, ____prevScreenOrientation_21)); }
	inline int32_t get__prevScreenOrientation_21() const { return ____prevScreenOrientation_21; }
	inline int32_t* get_address_of__prevScreenOrientation_21() { return &____prevScreenOrientation_21; }
	inline void set__prevScreenOrientation_21(int32_t value)
	{
		____prevScreenOrientation_21 = value;
	}

	inline static int32_t get_offset_of__projectionRotation_22() { return static_cast<int32_t>(offsetof(Tracker_t3774580070, ____projectionRotation_22)); }
	inline Matrix4x4_t2933234003  get__projectionRotation_22() const { return ____projectionRotation_22; }
	inline Matrix4x4_t2933234003 * get_address_of__projectionRotation_22() { return &____projectionRotation_22; }
	inline void set__projectionRotation_22(Matrix4x4_t2933234003  value)
	{
		____projectionRotation_22 = value;
	}

	inline static int32_t get_offset_of__numFramesGrabbed_24() { return static_cast<int32_t>(offsetof(Tracker_t3774580070, ____numFramesGrabbed_24)); }
	inline int32_t get__numFramesGrabbed_24() const { return ____numFramesGrabbed_24; }
	inline int32_t* get_address_of__numFramesGrabbed_24() { return &____numFramesGrabbed_24; }
	inline void set__numFramesGrabbed_24(int32_t value)
	{
		____numFramesGrabbed_24 = value;
	}

	inline static int32_t get_offset_of__numFramesTracked_25() { return static_cast<int32_t>(offsetof(Tracker_t3774580070, ____numFramesTracked_25)); }
	inline int32_t get__numFramesTracked_25() const { return ____numFramesTracked_25; }
	inline int32_t* get_address_of__numFramesTracked_25() { return &____numFramesTracked_25; }
	inline void set__numFramesTracked_25(int32_t value)
	{
		____numFramesTracked_25 = value;
	}

	inline static int32_t get_offset_of__numFramesProcessed_26() { return static_cast<int32_t>(offsetof(Tracker_t3774580070, ____numFramesProcessed_26)); }
	inline int32_t get__numFramesProcessed_26() const { return ____numFramesProcessed_26; }
	inline int32_t* get_address_of__numFramesProcessed_26() { return &____numFramesProcessed_26; }
	inline void set__numFramesProcessed_26(int32_t value)
	{
		____numFramesProcessed_26 = value;
	}

	inline static int32_t get_offset_of__numFramesRendered_27() { return static_cast<int32_t>(offsetof(Tracker_t3774580070, ____numFramesRendered_27)); }
	inline int32_t get__numFramesRendered_27() const { return ____numFramesRendered_27; }
	inline int32_t* get_address_of__numFramesRendered_27() { return &____numFramesRendered_27; }
	inline void set__numFramesRendered_27(int32_t value)
	{
		____numFramesRendered_27 = value;
	}

	inline static int32_t get_offset_of__rateTimer_28() { return static_cast<int32_t>(offsetof(Tracker_t3774580070, ____rateTimer_28)); }
	inline float get__rateTimer_28() const { return ____rateTimer_28; }
	inline float* get_address_of__rateTimer_28() { return &____rateTimer_28; }
	inline void set__rateTimer_28(float value)
	{
		____rateTimer_28 = value;
	}

	inline static int32_t get_offset_of_receivingInput_29() { return static_cast<int32_t>(offsetof(Tracker_t3774580070, ___receivingInput_29)); }
	inline bool get_receivingInput_29() const { return ___receivingInput_29; }
	inline bool* get_address_of_receivingInput_29() { return &___receivingInput_29; }
	inline void set_receivingInput_29(bool value)
	{
		___receivingInput_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
