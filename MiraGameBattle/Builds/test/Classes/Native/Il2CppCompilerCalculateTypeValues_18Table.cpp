﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CollisionStateController705611967.h"
#include "AssemblyU2DCSharp_EnemyAIController937801982.h"
#include "AssemblyU2DCSharp_GameController3607102586.h"
#include "AssemblyU2DCSharp_PlayerController4148409433.h"
#include "AssemblyU2DCSharp_SelectionController1038360966.h"
#include "AssemblyU2DCSharp_colliderDetector794139730.h"
#include "AssemblyU2DCSharp_scriptInput3566377219.h"
#include "AssemblyU2DCSharp_ClassPlayerSensorsData400201692.h"
#include "AssemblyU2DCSharp_scriptMechanics3538609330.h"
#include "AssemblyU2DCSharp_scriptMechanics_EnumPlayerType985826215.h"
#include "AssemblyU2DCSharp_scriptMechanics_EnumAttackCollide537956954.h"
#include "AssemblyU2DCSharp_scriptSensor1009192331.h"
#include "AssemblyU2DCSharp_scriptSensor_PlayerHittingCollid3394089924.h"
#include "AssemblyU2DCSharp_scriptSensorHitValues1173922324.h"
#include "AssemblyU2DCSharp_ControllerInputGUI2571972535.h"
#include "AssemblyU2DCSharp_ControllerInputGUI_AxisType1230240043.h"
#include "AssemblyU2DCSharp_ControllerInputGUI_AxisEntry2979998719.h"
#include "AssemblyU2DCSharp_ControllerInputGUI_ButtonEntry1083102552.h"
#include "AssemblyU2DCSharp_ControllerInputGUI_SliderGroup396385140.h"
#include "AssemblyU2DCSharp_ControllerInputGUI_VibrationEntr1830347994.h"
#include "AssemblyU2DCSharp_ControllerInputGUI_ControllerEnt1998352496.h"
#include "AssemblyU2DCSharp_FPSCounter921841495.h"
#include "AssemblyU2DCSharp_TrackerInputTest1154877504.h"
#include "AssemblyU2DCSharp_Ximmerse_UI_UIVector3Field3536787880.h"
#include "AssemblyU2DCSharp_Ximmerse_UI_UIVector3Field_Vecto3319516927.h"
#include "AssemblyU2DCSharp_CubeGridCreator2967326991.h"
#include "AssemblyU2DCSharp_SimplePicker288740920.h"
#include "AssemblyU2DCSharp_SimplePicker_PickObject2598897045.h"
#include "AssemblyU2DCSharp_Ximmerse_Log2973118932.h"
#include "AssemblyU2DCSharp_Ximmerse_Logger3681606150.h"
#include "AssemblyU2DCSharp_PlayerPrefsEx1459650680.h"
#include "AssemblyU2DCSharp_PlayerPrefsEx_StringPlayerPrefsE1449231856.h"
#include "AssemblyU2DCSharp_PlayerPrefsEx_PlayerPrefsExDicti3924552273.h"
#include "AssemblyU2DCSharp_PlayerPrefsEx_StringIntPair1691506285.h"
#include "AssemblyU2DCSharp_PlayerPrefsEx_IntDictionary405243266.h"
#include "AssemblyU2DCSharp_PlayerPrefsEx_StringFloatPair3908173074.h"
#include "AssemblyU2DCSharp_PlayerPrefsEx_FloatDictionary1539784753.h"
#include "AssemblyU2DCSharp_PlayerPrefsEx_StringStringPair1896496907.h"
#include "AssemblyU2DCSharp_PlayerPrefsEx_StringDictionary1060055032.h"
#include "AssemblyU2DCSharp_PlayerPrefsEx_StringVector3Pair4251321744.h"
#include "AssemblyU2DCSharp_PlayerPrefsEx_Vector3Dictionary4020395255.h"
#include "AssemblyU2DCSharp_PlayerPrefsEx_StringColorPair408398259.h"
#include "AssemblyU2DCSharp_PlayerPrefsEx_ColorDictionary1385420584.h"
#include "AssemblyU2DCSharp_PlayerPrefsEx_StringObjectPair2353987591.h"
#include "AssemblyU2DCSharp_PlayerPrefsEx_ObjectDictionary1252187966.h"
#include "AssemblyU2DCSharp_ReflectUtil752396129.h"
#include "AssemblyU2DCSharp_ReflectUtil_MemberEntry200158728.h"
#include "AssemblyU2DCSharp_ReflectUtil_U3CParseMemberU3Ec__2113297536.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ArmModel1335532495.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ArmModel_Ga1258145080.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ArmModel_On3934451428.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ArmModelNod3478863847.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ArmModelInpu162341459.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_TrackedCont3249248438.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_TrackerInpu4065713356.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_TrackerInpu3172144093.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XHawkInput3103630643.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XHawkInput_3675692101.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XHawkInput_I598830048.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XHawkInput_Ou46940832.h"
#include "AssemblyU2DCSharp_Ximmerse_VR_VRNode1238762130.h"
#include "AssemblyU2DCSharp_Ximmerse_VR_TrackingOrigin2567001781.h"
#include "AssemblyU2DCSharp_Ximmerse_VR_VRContext3921406953.h"
#include "AssemblyU2DCSharp_Ximmerse_VR_VRContext_NodeTransfo746410958.h"
#include "AssemblyU2DCSharp_Ximmerse_VR_VRContext_OnHmdUpdat3424778990.h"
#include "AssemblyU2DCSharp_Ximmerse_VR_VRContext_OnHmdMessa1010896196.h"
#include "AssemblyU2DCSharp_Ximmerse_VR_Pose3D1800640744.h"
#include "AssemblyU2DCSharp_Ximmerse_VR_VRDevice903186564.h"
#include "AssemblyU2DCSharp_Ximmerse_VR_VRDevice_U3CInitDevi4209420940.h"
#include "AssemblyU2DCSharp_Ximmerse_UI_UIFade1631582502.h"
#include "AssemblyU2DCSharp_Ximmerse_UI_UIFade_U3CFadeInDela3821834713.h"
#include "AssemblyU2DCSharp_Ximmerse_UI_UIFade_U3CFadeOutDel3724933873.h"
#include "AssemblyU2DCSharp_Ximmerse_UI_VRPointerEventData2223506075.h"
#include "AssemblyU2DCSharp_Ximmerse_UI_VRInputModule523376602.h"
#include "AssemblyU2DCSharp_Ximmerse_UI_VRInputModule_SubInp2062164065.h"
#include "AssemblyU2DCSharp_Ximmerse_UI_VRRaycaster653539202.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerTy123719274.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerR3322258777.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerR2024485020.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerA3753021729.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerB2189630288.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerI1382602808.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerIn354289143.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerI1382684775.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerI3037329813.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_InputManage3388780835.h"
#include "AssemblyU2DCSharp_ControllerVisual293503452.h"
#include "AssemblyU2DCSharp_ControllerVisual_AxisType3855017368.h"
#include "AssemblyU2DCSharp_ControllerVisual_AxisListener1876418532.h"
#include "AssemblyU2DCSharp_ControllerVisual_ButtonListener2279732829.h"
#include "AssemblyU2DCSharp_DeviceStatus381343064.h"
#include "AssemblyU2DCSharp_DeviceStatus_DeviceUI3797029069.h"
#include "AssemblyU2DCSharp_HapticsLibrary1701333201.h"
#include "AssemblyU2DCSharp_HapticsLibrary_HapticsDesc3497052495.h"
#include "AssemblyU2DCSharp_PlayAreaHelper1795592975.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (CollisionStateController_t705611967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[4] = 
{
	CollisionStateController_t705611967::get_offset_of__startTime_2(),
	CollisionStateController_t705611967::get_offset_of__endTime_3(),
	CollisionStateController_t705611967::get_offset_of__tempCollider_4(),
	CollisionStateController_t705611967::get_offset_of__attackType_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (EnemyAIController_t937801982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[17] = 
{
	0,
	0,
	0,
	0,
	0,
	EnemyAIController_t937801982::get_offset_of_speed_7(),
	EnemyAIController_t937801982::get_offset_of_player_8(),
	EnemyAIController_t937801982::get_offset_of_leftLimit_9(),
	EnemyAIController_t937801982::get_offset_of_rightLimit_10(),
	EnemyAIController_t937801982::get_offset_of_rotate_11(),
	EnemyAIController_t937801982::get_offset_of_walkSpeed_12(),
	EnemyAIController_t937801982::get_offset_of_anim_13(),
	EnemyAIController_t937801982::get_offset_of_rb_14(),
	EnemyAIController_t937801982::get_offset_of_a_15(),
	EnemyAIController_t937801982::get_offset_of_startAI_16(),
	EnemyAIController_t937801982::get_offset_of_delay_17(),
	EnemyAIController_t937801982::get_offset_of_nextUsage_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (GameController_t3607102586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[19] = 
{
	GameController_t3607102586::get_offset_of_playerToSpawn_2(),
	GameController_t3607102586::get_offset_of_enemies_3(),
	GameController_t3607102586::get_offset_of_players_4(),
	GameController_t3607102586::get_offset_of_m_frameCounter_5(),
	GameController_t3607102586::get_offset_of_m_timeCounter_6(),
	GameController_t3607102586::get_offset_of_m_refreshTime_7(),
	GameController_t3607102586::get_offset_of_FPS_8(),
	GameController_t3607102586::get_offset_of_selectCharacter_9(),
	GameController_t3607102586::get_offset_of_ct_10(),
	GameController_t3607102586::get_offset_of_laser_11(),
	GameController_t3607102586::get_offset_of_setupUI_12(),
	GameController_t3607102586::get_offset_of_battleUI_13(),
	GameController_t3607102586::get_offset_of_boardObjects_14(),
	GameController_t3607102586::get_offset_of_playerSelection_15(),
	GameController_t3607102586::get_offset_of_count_16(),
	GameController_t3607102586::get_offset_of_battleBoard_17(),
	GameController_t3607102586::get_offset_of_rend_18(),
	GameController_t3607102586::get_offset_of_moveBattle_19(),
	GameController_t3607102586::get_offset_of_lastCast_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (PlayerController_t4148409433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[13] = 
{
	PlayerController_t4148409433::get_offset_of_gesture_2(),
	PlayerController_t4148409433::get_offset_of_enemy_3(),
	PlayerController_t4148409433::get_offset_of_verticalVelocity_4(),
	PlayerController_t4148409433::get_offset_of_gravity_5(),
	PlayerController_t4148409433::get_offset_of_jumpForce_6(),
	PlayerController_t4148409433::get_offset_of_canJump_7(),
	PlayerController_t4148409433::get_offset_of_defending_8(),
	PlayerController_t4148409433::get_offset_of_leftLimit_9(),
	PlayerController_t4148409433::get_offset_of_rightLimit_10(),
	PlayerController_t4148409433::get_offset_of_anim_11(),
	PlayerController_t4148409433::get_offset_of_charController_12(),
	PlayerController_t4148409433::get_offset_of_playerMovementX_13(),
	PlayerController_t4148409433::get_offset_of_time_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (SelectionController_t1038360966), -1, sizeof(SelectionController_t1038360966_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1804[12] = 
{
	SelectionController_t1038360966_StaticFields::get_offset_of_selectedPlayer_2(),
	SelectionController_t1038360966_StaticFields::get_offset_of_selectedEnemy_3(),
	SelectionController_t1038360966_StaticFields::get_offset_of_boardPosition_4(),
	SelectionController_t1038360966_StaticFields::get_offset_of_boardRotation_5(),
	SelectionController_t1038360966::get_offset_of_ct_6(),
	SelectionController_t1038360966::get_offset_of_laser_7(),
	SelectionController_t1038360966::get_offset_of_startSetupUI_8(),
	SelectionController_t1038360966::get_offset_of_characterSelectionUI_9(),
	SelectionController_t1038360966::get_offset_of_characterSelection_10(),
	SelectionController_t1038360966::get_offset_of_battleBoard_11(),
	SelectionController_t1038360966::get_offset_of_battleBoardRenderer_12(),
	SelectionController_t1038360966::get_offset_of_count_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (colliderDetector_t794139730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[1] = 
{
	colliderDetector_t794139730::get_offset_of_gc_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (scriptInput_t3566377219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[1] = 
{
	scriptInput_t3566377219::get_offset_of_mechScript_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (ClassPlayerSensorsData_t400201692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[2] = 
{
	ClassPlayerSensorsData_t400201692::get_offset_of__sensors_0(),
	ClassPlayerSensorsData_t400201692::get_offset_of__attackCollider_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (scriptMechanics_t3538609330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[6] = 
{
	scriptMechanics_t3538609330::get_offset_of__playerSensorList_2(),
	scriptMechanics_t3538609330::get_offset_of_anim_3(),
	scriptMechanics_t3538609330::get_offset_of__value_4(),
	scriptMechanics_t3538609330::get_offset_of__maximumHealth_5(),
	scriptMechanics_t3538609330::get_offset_of__currentHealth_6(),
	scriptMechanics_t3538609330::get_offset_of__instancePlayerType_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (EnumPlayerType_t985826215)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1809[3] = 
{
	EnumPlayerType_t985826215::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (EnumAttackCollider_t537956954)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1810[5] = 
{
	EnumAttackCollider_t537956954::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (scriptSensor_t1009192331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1811[14] = 
{
	scriptSensor_t1009192331::get_offset_of_enemies_4(),
	scriptSensor_t1009192331::get_offset_of_sprites_5(),
	scriptSensor_t1009192331::get_offset_of_enemyImage_6(),
	scriptSensor_t1009192331::get_offset_of_Enemy_7(),
	scriptSensor_t1009192331::get_offset_of_Player_8(),
	scriptSensor_t1009192331::get_offset_of_gameOver_9(),
	scriptSensor_t1009192331::get_offset_of_battleBoard_10(),
	scriptSensor_t1009192331::get_offset_of_uiVS_11(),
	scriptSensor_t1009192331::get_offset_of_battleUI_12(),
	scriptSensor_t1009192331::get_offset_of_pController_13(),
	scriptSensor_t1009192331::get_offset_of_eAIController_14(),
	scriptSensor_t1009192331::get_offset_of__getHit_15(),
	scriptSensor_t1009192331::get_offset_of__parent_16(),
	scriptSensor_t1009192331::get_offset_of__instance_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (PlayerHittingCollider_t3394089924)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1812[4] = 
{
	PlayerHittingCollider_t3394089924::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (scriptSensorHitValues_t1173922324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[2] = 
{
	scriptSensorHitValues_t1173922324::get_offset_of__hitPower_2(),
	scriptSensorHitValues_t1173922324::get_offset_of__speed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (ControllerInputGUI_t2571972535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1814[1] = 
{
	ControllerInputGUI_t2571972535::get_offset_of_m_Controllers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (AxisType_t1230240043)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1815[4] = 
{
	AxisType_t1230240043::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (AxisEntry_t2979998719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1816[8] = 
{
	AxisEntry_t2979998719::get_offset_of_context_0(),
	AxisEntry_t2979998719::get_offset_of_type_1(),
	AxisEntry_t2979998719::get_offset_of_keys_2(),
	AxisEntry_t2979998719::get_offset_of_m_GameObject_3(),
	AxisEntry_t2979998719::get_offset_of_m_Image_4(),
	AxisEntry_t2979998719::get_offset_of_m_Thumb_5(),
	AxisEntry_t2979998719::get_offset_of_m_Center_6(),
	AxisEntry_t2979998719::get_offset_of_m_Radius_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (ButtonEntry_t1083102552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[6] = 
{
	ButtonEntry_t1083102552::get_offset_of_context_0(),
	ButtonEntry_t1083102552::get_offset_of_key_1(),
	ButtonEntry_t1083102552::get_offset_of_m_GameObject_2(),
	ButtonEntry_t1083102552::get_offset_of_m_Transform_3(),
	ButtonEntry_t1083102552::get_offset_of_m_Graphic_4(),
	ButtonEntry_t1083102552::get_offset_of_m_DefaultScale_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (SliderGroup_t396385140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[4] = 
{
	SliderGroup_t396385140::get_offset_of_image_0(),
	SliderGroup_t396385140::get_offset_of_max_1(),
	SliderGroup_t396385140::get_offset_of_text_2(),
	SliderGroup_t396385140::get_offset_of_format_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (VibrationEntry_t1830347994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[2] = 
{
	VibrationEntry_t1830347994::get_offset_of_type_2(),
	VibrationEntry_t1830347994::get_offset_of_context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (ControllerEntry_t1998352496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[17] = 
{
	ControllerEntry_t1998352496::get_offset_of_controllerInput_0(),
	ControllerEntry_t1998352496::get_offset_of_m_NameText_1(),
	ControllerEntry_t1998352496::get_offset_of_m_Name_2(),
	ControllerEntry_t1998352496::get_offset_of_m_EnableGo_3(),
	ControllerEntry_t1998352496::get_offset_of_m_DisableGo_4(),
	ControllerEntry_t1998352496::get_offset_of_m_FPSCounter_5(),
	ControllerEntry_t1998352496::get_offset_of_m_ConnectionText_6(),
	ControllerEntry_t1998352496::get_offset_of_m_Axes_7(),
	ControllerEntry_t1998352496::get_offset_of_m_Buttons_8(),
	ControllerEntry_t1998352496::get_offset_of_m_Vibrations_9(),
	ControllerEntry_t1998352496::get_offset_of_m_Position_10(),
	ControllerEntry_t1998352496::get_offset_of_m_PositionGo_11(),
	ControllerEntry_t1998352496::get_offset_of_m_Rotation_12(),
	ControllerEntry_t1998352496::get_offset_of_m_Accelerometer_13(),
	ControllerEntry_t1998352496::get_offset_of_m_Gyroscope_14(),
	ControllerEntry_t1998352496::get_offset_of_m_AccelerometerMag_15(),
	ControllerEntry_t1998352496::get_offset_of_m_GyroscopeMag_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (FPSCounter_t921841495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[8] = 
{
	FPSCounter_t921841495::get_offset_of_fpsMeasurePeriod_2(),
	FPSCounter_t921841495::get_offset_of_display_3(),
	FPSCounter_t921841495::get_offset_of_getFrameCount_4(),
	FPSCounter_t921841495::get_offset_of_m_FpsAccumulator_5(),
	FPSCounter_t921841495::get_offset_of_m_PrevFpsAccumulator_6(),
	FPSCounter_t921841495::get_offset_of_m_FpsNextPeriod_7(),
	FPSCounter_t921841495::get_offset_of_m_CurrentFps_8(),
	FPSCounter_t921841495::get_offset_of_m_Text_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (TrackerInputTest_t1154877504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[4] = 
{
	TrackerInputTest_t1154877504::get_offset_of_m_FPSCounter_2(),
	TrackerInputTest_t1154877504::get_offset_of_m_Text_3(),
	TrackerInputTest_t1154877504::get_offset_of_m_Nodes_4(),
	TrackerInputTest_t1154877504::get_offset_of_m_TrackerInput_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (UIVector3Field_t3536787880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[7] = 
{
	UIVector3Field_t3536787880::get_offset_of_m_Field_2(),
	UIVector3Field_t3536787880::get_offset_of_m_Format_3(),
	UIVector3Field_t3536787880::get_offset_of_m_Value_4(),
	UIVector3Field_t3536787880::get_offset_of_m_Label_5(),
	UIVector3Field_t3536787880::get_offset_of_m_Texts_6(),
	UIVector3Field_t3536787880::get_offset_of_m_OnValueChanged_7(),
	UIVector3Field_t3536787880::get_offset_of_m_IsRefreshing_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (Vector3Event_t3319516927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (CubeGridCreator_t2967326991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[7] = 
{
	CubeGridCreator_t2967326991::get_offset_of_root_2(),
	CubeGridCreator_t2967326991::get_offset_of_defaultPrefab_3(),
	CubeGridCreator_t2967326991::get_offset_of_createOnAwake_4(),
	CubeGridCreator_t2967326991::get_offset_of_count_x_5(),
	CubeGridCreator_t2967326991::get_offset_of_count_y_6(),
	CubeGridCreator_t2967326991::get_offset_of_count_z_7(),
	CubeGridCreator_t2967326991::get_offset_of_offset_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (SimplePicker_t288740920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1826[10] = 
{
	SimplePicker_t288740920::get_offset_of_m_ControllerType_2(),
	SimplePicker_t288740920::get_offset_of_m_ControllerInput_3(),
	SimplePicker_t288740920::get_offset_of_m_Container_4(),
	SimplePicker_t288740920::get_offset_of_m_Point_5(),
	SimplePicker_t288740920::get_offset_of_m_LayerMask_6(),
	SimplePicker_t288740920::get_offset_of_layerMask_7(),
	SimplePicker_t288740920::get_offset_of_m_Radius_8(),
	SimplePicker_t288740920::get_offset_of_m_MaxObjects_9(),
	SimplePicker_t288740920::get_offset_of_m_Colliders_10(),
	SimplePicker_t288740920::get_offset_of_m_PickObjects_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (PickObject_t2598897045), -1, sizeof(PickObject_t2598897045_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1827[7] = 
{
	PickObject_t2598897045_StaticFields::get_offset_of_s_InstancePool_0(),
	PickObject_t2598897045_StaticFields::get_offset_of_s_NumInstancePool_1(),
	PickObject_t2598897045::get_offset_of_transform_2(),
	PickObject_t2598897045::get_offset_of_parent_3(),
	PickObject_t2598897045::get_offset_of_rigidbody_4(),
	PickObject_t2598897045::get_offset_of_isKinematic_5(),
	PickObject_t2598897045::get_offset_of_useGravity_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (Log_t2973118932), -1, sizeof(Log_t2973118932_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1828[10] = 
{
	Log_t2973118932_StaticFields::get_offset_of_s_Logger_0(),
	Log_t2973118932_StaticFields::get_offset_of_s_Filter_1(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (Logger_t3681606150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[1] = 
{
	Logger_t3681606150::get_offset_of_format_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (PlayerPrefsEx_t1459650680), -1, sizeof(PlayerPrefsEx_t1459650680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1831[10] = 
{
	PlayerPrefsEx_t1459650680_StaticFields::get_offset_of_s_MainCached_2(),
	PlayerPrefsEx_t1459650680_StaticFields::get_offset_of_s_Main_3(),
	PlayerPrefsEx_t1459650680::get_offset_of_mapPlayerPrefsEx_4(),
	PlayerPrefsEx_t1459650680::get_offset_of_mapInt_5(),
	PlayerPrefsEx_t1459650680::get_offset_of_mapFloat_6(),
	PlayerPrefsEx_t1459650680::get_offset_of_mapString_7(),
	PlayerPrefsEx_t1459650680::get_offset_of_mapVector3_8(),
	PlayerPrefsEx_t1459650680::get_offset_of_mapColor_9(),
	PlayerPrefsEx_t1459650680::get_offset_of_mapObject_10(),
	PlayerPrefsEx_t1459650680::get_offset_of_parent_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1832[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (StringPlayerPrefsExPair_t1449231856), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (PlayerPrefsExDictionary_t3924552273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (StringIntPair_t1691506285), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (IntDictionary_t405243266), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (StringFloatPair_t3908173074), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (FloatDictionary_t1539784753), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (StringStringPair_t1896496907), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (StringDictionary_t1060055032), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (StringVector3Pair_t4251321744), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (Vector3Dictionary_t4020395255), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (StringColorPair_t408398259), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (ColorDictionary_t1385420584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (StringObjectPair_t2353987591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (ObjectDictionary_t1252187966), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (ReflectUtil_t752396129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1847[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (MemberEntry_t200158728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1848[6] = 
{
	MemberEntry_t200158728::get_offset_of_type_0(),
	MemberEntry_t200158728::get_offset_of_memberType_1(),
	MemberEntry_t200158728::get_offset_of_objectValue_2(),
	MemberEntry_t200158728::get_offset_of_methodValue_3(),
	MemberEntry_t200158728::get_offset_of_propertyValue_4(),
	MemberEntry_t200158728::get_offset_of_delegateValue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (U3CParseMemberU3Ec__AnonStorey0_t2113297536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1849[3] = 
{
	U3CParseMemberU3Ec__AnonStorey0_t2113297536::get_offset_of_currentMethod_0(),
	U3CParseMemberU3Ec__AnonStorey0_t2113297536::get_offset_of_currentObject_1(),
	U3CParseMemberU3Ec__AnonStorey0_t2113297536::get_offset_of_currentProperty_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1850[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (ArmModel_t1335532495), -1, sizeof(ArmModel_t1335532495_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1851[46] = 
{
	ArmModel_t1335532495_StaticFields::get_offset_of_DEFAULT_SHOULDER_RIGHT_0(),
	ArmModel_t1335532495_StaticFields::get_offset_of_ELBOW_MIN_RANGE_1(),
	ArmModel_t1335532495_StaticFields::get_offset_of_ELBOW_MAX_RANGE_2(),
	ArmModel_t1335532495_StaticFields::get_offset_of_POINTER_OFFSET_3(),
	ArmModel_t1335532495_StaticFields::get_offset_of_ELBOW_POSITION_4(),
	ArmModel_t1335532495_StaticFields::get_offset_of_WRIST_POSITION_5(),
	ArmModel_t1335532495_StaticFields::get_offset_of_ARM_EXTENSION_OFFSET_6(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ArmModel_t1335532495::get_offset_of_elbowOffset_17(),
	ArmModel_t1335532495::get_offset_of_torsoDirection_18(),
	ArmModel_t1335532495::get_offset_of_filteredVelocity_19(),
	ArmModel_t1335532495::get_offset_of_filteredAccel_20(),
	ArmModel_t1335532495::get_offset_of_zeroAccel_21(),
	ArmModel_t1335532495::get_offset_of_firstUpdate_22(),
	ArmModel_t1335532495::get_offset_of_handedMultiplier_23(),
	ArmModel_t1335532495::get_offset_of_addedElbowHeight_24(),
	ArmModel_t1335532495::get_offset_of_addedElbowDepth_25(),
	ArmModel_t1335532495::get_offset_of_pointerTiltAngle_26(),
	ArmModel_t1335532495::get_offset_of_fadeDistanceFromFace_27(),
	ArmModel_t1335532495::get_offset_of_tooltipMinDistanceFromFace_28(),
	ArmModel_t1335532495::get_offset_of_followGaze_29(),
	ArmModel_t1335532495::get_offset_of_useAccelerometer_30(),
	ArmModel_t1335532495::get_offset_of_U3CpointerPositionU3Ek__BackingField_31(),
	ArmModel_t1335532495::get_offset_of_U3CpointerRotationU3Ek__BackingField_32(),
	ArmModel_t1335532495::get_offset_of_U3CwristPositionU3Ek__BackingField_33(),
	ArmModel_t1335532495::get_offset_of_U3CwristRotationU3Ek__BackingField_34(),
	ArmModel_t1335532495::get_offset_of_U3CelbowPositionU3Ek__BackingField_35(),
	ArmModel_t1335532495::get_offset_of_U3CelbowRotationU3Ek__BackingField_36(),
	ArmModel_t1335532495::get_offset_of_U3CshoulderPositionU3Ek__BackingField_37(),
	ArmModel_t1335532495::get_offset_of_U3CshoulderRotationU3Ek__BackingField_38(),
	ArmModel_t1335532495::get_offset_of_U3CalphaValueU3Ek__BackingField_39(),
	ArmModel_t1335532495::get_offset_of_U3CtooltipAlphaValueU3Ek__BackingField_40(),
	ArmModel_t1335532495::get_offset_of_OnArmModelUpdate_41(),
	ArmModel_t1335532495::get_offset_of_name_42(),
	ArmModel_t1335532495::get_offset_of_Controller_43(),
	ArmModel_t1335532495::get_offset_of_defaultNode_44(),
	ArmModel_t1335532495::get_offset_of_handedness_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (GazeBehavior_t1258145080)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1852[4] = 
{
	GazeBehavior_t1258145080::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (OnArmModelUpdateEvent_t3934451428), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (ArmModelNode_t3478863847)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1854[5] = 
{
	ArmModelNode_t3478863847::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (ArmModelInput_t162341459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1855[1] = 
{
	ArmModelInput_t162341459::get_offset_of_m_Controllers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (TrackedControllerInput_t3249248438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1857[2] = 
{
	TrackedControllerInput_t3249248438::get_offset_of_inputTracking_9(),
	TrackedControllerInput_t3249248438::get_offset_of_node_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (TrackerInput_t4065713356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[12] = 
{
	TrackerInput_t4065713356::get_offset_of_deviceName_2(),
	TrackerInput_t4065713356::get_offset_of_sensitivity_3(),
	TrackerInput_t4065713356::get_offset_of_trackingSpace_4(),
	TrackerInput_t4065713356::get_offset_of_anchor_5(),
	TrackerInput_t4065713356::get_offset_of_onRecenter_6(),
	TrackerInput_t4065713356::get_offset_of_m_Handle_7(),
	TrackerInput_t4065713356::get_offset_of_m_State_8(),
	TrackerInput_t4065713356::get_offset_of_m_PrevFrameCount_9(),
	TrackerInput_t4065713356::get_offset_of_m_PrevState_frameCount_10(),
	TrackerInput_t4065713356::get_offset_of_m_KeepFrames_11(),
	TrackerInput_t4065713356::get_offset_of_m_UseAnchorProjection_12(),
	TrackerInput_t4065713356::get_offset_of_m_AnchorMatrix_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (U3CFindAllU3Ec__AnonStorey0_t3172144093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1859[1] = 
{
	U3CFindAllU3Ec__AnonStorey0_t3172144093::get_offset_of_deviceName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (XHawkInput_t3103630643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1860[5] = 
{
	XHawkInput_t3103630643::get_offset_of_m_Controllers_14(),
	XHawkInput_t3103630643::get_offset_of_controllers_15(),
	XHawkInput_t3103630643::get_offset_of_m_IsInited_16(),
	XHawkInput_t3103630643::get_offset_of_m_IsRequestVR_17(),
	XHawkInput_t3103630643::get_offset_of_m_HmdInput_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (StringIntPair_t3675692101), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (XHawkInput_InsideOut_t598830048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1862[4] = 
{
	XHawkInput_InsideOut_t598830048::get_offset_of_makeSmooth_19(),
	XHawkInput_InsideOut_t598830048::get_offset_of_m_CenterEye_20(),
	XHawkInput_InsideOut_t598830048::get_offset_of_m_AnchorPosition_21(),
	XHawkInput_InsideOut_t598830048::get_offset_of_m_AnchorRotation_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (XHawkInput_OutsideIn_t46940832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1863[6] = 
{
	XHawkInput_OutsideIn_t46940832::get_offset_of_anchorPrefab_19(),
	XHawkInput_OutsideIn_t46940832::get_offset_of_makeTrackerForward_20(),
	XHawkInput_OutsideIn_t46940832::get_offset_of_m_CenterEye_21(),
	XHawkInput_OutsideIn_t46940832::get_offset_of_m_VRDevice_22(),
	XHawkInput_OutsideIn_t46940832::get_offset_of_m_PlayAreaHelper_23(),
	XHawkInput_OutsideIn_t46940832::get_offset_of_m_UpdateAnchorFromPluginTRS_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (VRNode_t1238762130)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1864[11] = 
{
	VRNode_t1238762130::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (TrackingOrigin_t2567001781)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1865[5] = 
{
	TrackingOrigin_t2567001781::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (VRContext_t3921406953), -1, sizeof(VRContext_t3921406953_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1866[15] = 
{
	VRContext_t3921406953_StaticFields::get_offset_of_s_Main_2(),
	VRContext_t3921406953_StaticFields::get_offset_of_s_MainCached_3(),
	VRContext_t3921406953_StaticFields::get_offset_of_s_OnHmdUpdate_4(),
	VRContext_t3921406953_StaticFields::get_offset_of_s_OnHmdMessage_5(),
	VRContext_t3921406953::get_offset_of_trackingOriginType_6(),
	VRContext_t3921406953::get_offset_of_vrDevice_7(),
	VRContext_t3921406953::get_offset_of_m_HomeIsDownLastTime_8(),
	VRContext_t3921406953::get_offset_of_m_Anchors_9(),
	VRContext_t3921406953::get_offset_of_canRecenter_10(),
	VRContext_t3921406953::get_offset_of_onRecenter_11(),
	VRContext_t3921406953::get_offset_of_m_IsInited_12(),
	VRContext_t3921406953::get_offset_of_m_UiRootVR_13(),
	VRContext_t3921406953::get_offset_of_m_UIFade_14(),
	VRContext_t3921406953_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_15(),
	VRContext_t3921406953_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (NodeTransformPair_t746410958), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (OnHmdUpdateDelegate_t3424778990), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (OnHmdMessageDelegate_t1010896196), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (Pose3D_t1800640744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1870[2] = 
{
	Pose3D_t1800640744::get_offset_of_position_0(),
	Pose3D_t1800640744::get_offset_of_eulerAngles_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (VRDevice_t903186564), -1, sizeof(VRDevice_t903186564_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1871[20] = 
{
	VRDevice_t903186564::get_offset_of_family_2(),
	VRDevice_t903186564::get_offset_of_trackingOriginType_3(),
	VRDevice_t903186564::get_offset_of_neckPosition_4(),
	VRDevice_t903186564::get_offset_of_neckToEye_5(),
	VRDevice_t903186564::get_offset_of_isCenterEye_6(),
	VRDevice_t903186564::get_offset_of_methodIsPresent_7(),
	VRDevice_t903186564::get_offset_of_m_MethodIsPresent_8(),
	VRDevice_t903186564::get_offset_of_methodRefreshRate_9(),
	VRDevice_t903186564::get_offset_of_m_MethodRefreshRate_10(),
	VRDevice_t903186564::get_offset_of_methodRecenter_11(),
	VRDevice_t903186564::get_offset_of_m_MethodRecenter_12(),
	VRDevice_t903186564::get_offset_of_m_MethodRecenterCached_13(),
	VRDevice_t903186564::get_offset_of_androidTargetName_14(),
	VRDevice_t903186564::get_offset_of_androidManifest_15(),
	VRDevice_t903186564::get_offset_of_useUnityVR_16(),
	VRDevice_t903186564::get_offset_of_inOutsideMarkPose_17(),
	VRDevice_t903186564::get_offset_of_outsideInMarkPose_18(),
	VRDevice_t903186564::get_offset_of_m_CenterEyeAnchor_19(),
	VRDevice_t903186564_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_20(),
	VRDevice_t903186564_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (U3CInitDeviceU3Ec__AnonStorey0_t4209420940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1872[1] = 
{
	U3CInitDeviceU3Ec__AnonStorey0_t4209420940::get_offset_of_f_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (UIFade_t1631582502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1873[15] = 
{
	UIFade_t1631582502::get_offset_of_autoPlay_2(),
	UIFade_t1631582502::get_offset_of_delay_3(),
	UIFade_t1631582502::get_offset_of_durationIn_4(),
	UIFade_t1631582502::get_offset_of_durationKeep_5(),
	UIFade_t1631582502::get_offset_of_durationOut_6(),
	UIFade_t1631582502::get_offset_of_onBecameVisible_7(),
	UIFade_t1631582502::get_offset_of_onBecameInvisible_8(),
	UIFade_t1631582502::get_offset_of_m_GameObject_9(),
	UIFade_t1631582502::get_offset_of_m_IsPlaying_10(),
	UIFade_t1631582502::get_offset_of_m_IsFadeOut_11(),
	UIFade_t1631582502::get_offset_of_m_Duration_12(),
	UIFade_t1631582502::get_offset_of_m_Alpha_13(),
	UIFade_t1631582502::get_offset_of_m_Graphics_14(),
	UIFade_t1631582502::get_offset_of_m_Alphas_15(),
	UIFade_t1631582502::get_offset_of_m_DelayCount_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (U3CFadeInDelayedU3Ec__Iterator0_t3821834713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1874[6] = 
{
	U3CFadeInDelayedU3Ec__Iterator0_t3821834713::get_offset_of_U3CdcU3E__0_0(),
	U3CFadeInDelayedU3Ec__Iterator0_t3821834713::get_offset_of_duration_1(),
	U3CFadeInDelayedU3Ec__Iterator0_t3821834713::get_offset_of_U24this_2(),
	U3CFadeInDelayedU3Ec__Iterator0_t3821834713::get_offset_of_U24current_3(),
	U3CFadeInDelayedU3Ec__Iterator0_t3821834713::get_offset_of_U24disposing_4(),
	U3CFadeInDelayedU3Ec__Iterator0_t3821834713::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (U3CFadeOutDelayedU3Ec__Iterator1_t3724933873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1875[6] = 
{
	U3CFadeOutDelayedU3Ec__Iterator1_t3724933873::get_offset_of_U3CdcU3E__0_0(),
	U3CFadeOutDelayedU3Ec__Iterator1_t3724933873::get_offset_of_duration_1(),
	U3CFadeOutDelayedU3Ec__Iterator1_t3724933873::get_offset_of_U24this_2(),
	U3CFadeOutDelayedU3Ec__Iterator1_t3724933873::get_offset_of_U24current_3(),
	U3CFadeOutDelayedU3Ec__Iterator1_t3724933873::get_offset_of_U24disposing_4(),
	U3CFadeOutDelayedU3Ec__Iterator1_t3724933873::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (VRPointerEventData_t2223506075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1876[1] = 
{
	VRPointerEventData_t2223506075::get_offset_of_controller_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (VRInputModule_t523376602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1877[2] = 
{
	VRInputModule_t523376602::get_offset_of_subInputModules_8(),
	VRInputModule_t523376602::get_offset_of_isActive_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (SubInputModule_t2062164065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1878[10] = 
{
	SubInputModule_t2062164065::get_offset_of_controller_0(),
	SubInputModule_t2062164065::get_offset_of_m_Controller_1(),
	SubInputModule_t2062164065::get_offset_of_m_ControllerName_2(),
	SubInputModule_t2062164065::get_offset_of_clickTime_3(),
	SubInputModule_t2062164065::get_offset_of_buttonClick_4(),
	SubInputModule_t2062164065::get_offset_of_eventSystem_5(),
	SubInputModule_t2062164065::get_offset_of_main_6(),
	SubInputModule_t2062164065::get_offset_of_m_RaycastResultCache_7(),
	SubInputModule_t2062164065::get_offset_of_hotspot_8(),
	SubInputModule_t2062164065::get_offset_of_pointerData_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (VRRaycaster_t653539202), -1, sizeof(VRRaycaster_t653539202_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1879[11] = 
{
	VRRaycaster_t653539202_StaticFields::get_offset_of_s_InstanceMap_10(),
	VRRaycaster_t653539202_StaticFields::get_offset_of_s_AllCanvases_11(),
	VRRaycaster_t653539202::get_offset_of_controller_12(),
	VRRaycaster_t653539202::get_offset_of_laserLine_13(),
	VRRaycaster_t653539202::get_offset_of_usePhysics_14(),
	VRRaycaster_t653539202::get_offset_of_eventMask_15(),
	VRRaycaster_t653539202::get_offset_of_m_EventCamera_16(),
	VRRaycaster_t653539202::get_offset_of_m_Transform_17(),
	VRRaycaster_t653539202::get_offset_of_m_CanvasRef_18(),
	VRRaycaster_t653539202_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_19(),
	VRRaycaster_t653539202_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (ControllerType_t123719274)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1880[8] = 
{
	ControllerType_t123719274::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (ControllerRawAxis_t3322258777)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1881[8] = 
{
	ControllerRawAxis_t3322258777::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (ControllerRawButton_t2024485020)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1882[30] = 
{
	ControllerRawButton_t2024485020::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (ControllerAxis_t3753021729)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1883[8] = 
{
	ControllerAxis_t3753021729::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (ControllerButton_t2189630288)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1884[30] = 
{
	ControllerButton_t2189630288::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (ControllerInput_t1382602808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1885[9] = 
{
	ControllerInput_t1382602808::get_offset_of_type_0(),
	ControllerInput_t1382602808::get_offset_of_handle_1(),
	ControllerInput_t1382602808::get_offset_of_name_2(),
	ControllerInput_t1382602808::get_offset_of_priority_3(),
	ControllerInput_t1382602808::get_offset_of_m_PrevFrameCount_4(),
	ControllerInput_t1382602808::get_offset_of_m_State_5(),
	ControllerInput_t1382602808::get_offset_of_m_PrevState_6(),
	ControllerInput_t1382602808::get_offset_of_m_TouchPos_7(),
	ControllerInput_t1382602808::get_offset_of_m_HapticsDC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (U3CStopHapticsDelayedU3Ec__Iterator0_t354289143), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1886[6] = 
{
	U3CStopHapticsDelayedU3Ec__Iterator0_t354289143::get_offset_of_U3CdcU3E__0_0(),
	U3CStopHapticsDelayedU3Ec__Iterator0_t354289143::get_offset_of_duration_1(),
	U3CStopHapticsDelayedU3Ec__Iterator0_t354289143::get_offset_of_U24this_2(),
	U3CStopHapticsDelayedU3Ec__Iterator0_t354289143::get_offset_of_U24current_3(),
	U3CStopHapticsDelayedU3Ec__Iterator0_t354289143::get_offset_of_U24disposing_4(),
	U3CStopHapticsDelayedU3Ec__Iterator0_t354289143::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (ControllerInputManager_t1382684775), -1, sizeof(ControllerInputManager_t1382684775_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1887[9] = 
{
	ControllerInputManager_t1382684775_StaticFields::get_offset_of_s_ControllerDatabase_6(),
	ControllerInputManager_t1382684775_StaticFields::get_offset_of_s_EmptyInput_7(),
	ControllerInputManager_t1382684775::get_offset_of_dontDestroyOnLoad_8(),
	ControllerInputManager_t1382684775::get_offset_of_m_ControllerDataTxt_9(),
	ControllerInputManager_t1382684775::get_offset_of_controllerInputs_10(),
	ControllerInputManager_t1382684775::get_offset_of_m_OnInitialized_11(),
	ControllerInputManager_t1382684775::get_offset_of_onUpdate_12(),
	ControllerInputManager_t1382684775::get_offset_of_onDestroy_13(),
	ControllerInputManager_t1382684775::get_offset_of_m_GetInputByTypeCaches_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (ControllerInfo_t3037329813), -1, sizeof(ControllerInfo_t3037329813_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1888[5] = 
{
	ControllerInfo_t3037329813_StaticFields::get_offset_of_SPLIT_NEW_LINE_0(),
	ControllerInfo_t3037329813_StaticFields::get_offset_of_SPLIT_CSV_1(),
	ControllerInfo_t3037329813::get_offset_of_name_2(),
	ControllerInfo_t3037329813::get_offset_of_type_3(),
	ControllerInfo_t3037329813::get_offset_of_priority_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (InputManager_t3388780835), -1, sizeof(InputManager_t3388780835_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1890[4] = 
{
	InputManager_t3388780835_StaticFields::get_offset_of_s_Instance_2(),
	InputManager_t3388780835_StaticFields::get_offset_of_s_InstanceCached_3(),
	InputManager_t3388780835::get_offset_of_monoInputModules_4(),
	InputManager_t3388780835::get_offset_of_inputModules_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (ControllerVisual_t293503452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1891[4] = 
{
	ControllerVisual_t293503452::get_offset_of_controller_2(),
	ControllerVisual_t293503452::get_offset_of_axes_3(),
	ControllerVisual_t293503452::get_offset_of_buttons_4(),
	ControllerVisual_t293503452::get_offset_of_m_Input_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (AxisType_t3855017368)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1892[3] = 
{
	AxisType_t3855017368::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (AxisListener_t1876418532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1893[6] = 
{
	AxisListener_t1876418532::get_offset_of_name_0(),
	AxisListener_t1876418532::get_offset_of_type_1(),
	AxisListener_t1876418532::get_offset_of_axes_2(),
	AxisListener_t1876418532::get_offset_of_target_3(),
	AxisListener_t1876418532::get_offset_of_maxValue_4(),
	AxisListener_t1876418532::get_offset_of_m_CachedVector3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (ButtonListener_t2279732829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[8] = 
{
	ButtonListener_t2279732829::get_offset_of_name_0(),
	ButtonListener_t2279732829::get_offset_of_unacceptables_1(),
	ButtonListener_t2279732829::get_offset_of_acceptables_2(),
	ButtonListener_t2279732829::get_offset_of_target_3(),
	ButtonListener_t2279732829::get_offset_of_onPressed_4(),
	ButtonListener_t2279732829::get_offset_of_onReleased_5(),
	ButtonListener_t2279732829::get_offset_of_value_6(),
	ButtonListener_t2279732829::get_offset_of_buttonMasks_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (DeviceStatus_t381343064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1895[4] = 
{
	DeviceStatus_t381343064::get_offset_of_alwaysShowUI_2(),
	DeviceStatus_t381343064::get_offset_of_uiRoot_3(),
	DeviceStatus_t381343064::get_offset_of_devices_4(),
	DeviceStatus_t381343064::get_offset_of_m_FadeUiRoot_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (DeviceUI_t3797029069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1896[7] = 
{
	DeviceUI_t3797029069::get_offset_of_deviceName_0(),
	DeviceUI_t3797029069::get_offset_of_connectUI_1(),
	DeviceUI_t3797029069::get_offset_of_battImage_2(),
	DeviceUI_t3797029069::get_offset_of_battText_3(),
	DeviceUI_t3797029069::get_offset_of_battSprites_4(),
	DeviceUI_t3797029069::get_offset_of_handle_5(),
	DeviceUI_t3797029069::get_offset_of_connectionState_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (HapticsLibrary_t1701333201), -1, sizeof(HapticsLibrary_t1701333201_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1897[4] = 
{
	HapticsLibrary_t1701333201_StaticFields::get_offset_of_s_Instance_2(),
	HapticsLibrary_t1701333201_StaticFields::get_offset_of_s_InstanceCached_3(),
	HapticsLibrary_t1701333201::get_offset_of_m_HapticsDescs_4(),
	HapticsLibrary_t1701333201::get_offset_of_hapticsDescs_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (HapticsDesc_t3497052495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1898[4] = 
{
	HapticsDesc_t3497052495::get_offset_of_key_0(),
	HapticsDesc_t3497052495::get_offset_of_intValue0_1(),
	HapticsDesc_t3497052495::get_offset_of_intValue1_2(),
	HapticsDesc_t3497052495::get_offset_of_objValue_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (PlayAreaHelper_t1795592975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1899[16] = 
{
	PlayAreaHelper_t1795592975::get_offset_of_warningDistance_2(),
	PlayAreaHelper_t1795592975::get_offset_of_controlPoints_3(),
	PlayAreaHelper_t1795592975::get_offset_of_showCameraModel_4(),
	PlayAreaHelper_t1795592975::get_offset_of_m_cameraModel_5(),
	PlayAreaHelper_t1795592975::get_offset_of_autoCreatePlayArea_6(),
	PlayAreaHelper_t1795592975::get_offset_of_m_PlayArea_7(),
	PlayAreaHelper_t1795592975::get_offset_of_m_Corners_8(),
	PlayAreaHelper_t1795592975::get_offset_of_trackerName_9(),
	PlayAreaHelper_t1795592975::get_offset_of_m_TrackerHandle_10(),
	PlayAreaHelper_t1795592975::get_offset_of_showOnTrackingLost_11(),
	PlayAreaHelper_t1795592975::get_offset_of_trackedNodes_12(),
	PlayAreaHelper_t1795592975::get_offset_of_m_Transform_13(),
	PlayAreaHelper_t1795592975::get_offset_of_m_ReadyForModel_14(),
	PlayAreaHelper_t1795592975::get_offset_of_m_GroundAlpha_15(),
	PlayAreaHelper_t1795592975::get_offset_of_m_WallAlpha_16(),
	PlayAreaHelper_t1795592975::get_offset_of_m_CachedCorners_17(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
