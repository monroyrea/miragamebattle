﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputM1295781545.h"

// Ximmerse.UI.VRInputModule/SubInputModule[]
struct SubInputModuleU5BU5D_t2306938108;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.UI.VRInputModule
struct  VRInputModule_t523376602  : public BaseInputModule_t1295781545
{
public:
	// Ximmerse.UI.VRInputModule/SubInputModule[] Ximmerse.UI.VRInputModule::subInputModules
	SubInputModuleU5BU5D_t2306938108* ___subInputModules_8;
	// System.Boolean Ximmerse.UI.VRInputModule::isActive
	bool ___isActive_9;

public:
	inline static int32_t get_offset_of_subInputModules_8() { return static_cast<int32_t>(offsetof(VRInputModule_t523376602, ___subInputModules_8)); }
	inline SubInputModuleU5BU5D_t2306938108* get_subInputModules_8() const { return ___subInputModules_8; }
	inline SubInputModuleU5BU5D_t2306938108** get_address_of_subInputModules_8() { return &___subInputModules_8; }
	inline void set_subInputModules_8(SubInputModuleU5BU5D_t2306938108* value)
	{
		___subInputModules_8 = value;
		Il2CppCodeGenWriteBarrier(&___subInputModules_8, value);
	}

	inline static int32_t get_offset_of_isActive_9() { return static_cast<int32_t>(offsetof(VRInputModule_t523376602, ___isActive_9)); }
	inline bool get_isActive_9() const { return ___isActive_9; }
	inline bool* get_address_of_isActive_9() { return &___isActive_9; }
	inline void set_isActive_9(bool value)
	{
		___isActive_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
