﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Ximmerse.UI.UIFade
struct UIFade_t1631582502;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.UI.UIFade/<FadeOutDelayed>c__Iterator1
struct  U3CFadeOutDelayedU3Ec__Iterator1_t3724933873  : public Il2CppObject
{
public:
	// System.Int32 Ximmerse.UI.UIFade/<FadeOutDelayed>c__Iterator1::<dc>__0
	int32_t ___U3CdcU3E__0_0;
	// System.Single Ximmerse.UI.UIFade/<FadeOutDelayed>c__Iterator1::duration
	float ___duration_1;
	// Ximmerse.UI.UIFade Ximmerse.UI.UIFade/<FadeOutDelayed>c__Iterator1::$this
	UIFade_t1631582502 * ___U24this_2;
	// System.Object Ximmerse.UI.UIFade/<FadeOutDelayed>c__Iterator1::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean Ximmerse.UI.UIFade/<FadeOutDelayed>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 Ximmerse.UI.UIFade/<FadeOutDelayed>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CdcU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFadeOutDelayedU3Ec__Iterator1_t3724933873, ___U3CdcU3E__0_0)); }
	inline int32_t get_U3CdcU3E__0_0() const { return ___U3CdcU3E__0_0; }
	inline int32_t* get_address_of_U3CdcU3E__0_0() { return &___U3CdcU3E__0_0; }
	inline void set_U3CdcU3E__0_0(int32_t value)
	{
		___U3CdcU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_duration_1() { return static_cast<int32_t>(offsetof(U3CFadeOutDelayedU3Ec__Iterator1_t3724933873, ___duration_1)); }
	inline float get_duration_1() const { return ___duration_1; }
	inline float* get_address_of_duration_1() { return &___duration_1; }
	inline void set_duration_1(float value)
	{
		___duration_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CFadeOutDelayedU3Ec__Iterator1_t3724933873, ___U24this_2)); }
	inline UIFade_t1631582502 * get_U24this_2() const { return ___U24this_2; }
	inline UIFade_t1631582502 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(UIFade_t1631582502 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CFadeOutDelayedU3Ec__Iterator1_t3724933873, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CFadeOutDelayedU3Ec__Iterator1_t3724933873, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CFadeOutDelayedU3Ec__Iterator1_t3724933873, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
