﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// buttons
struct  buttons_t3159989035  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject buttons::go
	GameObject_t1756533147 * ___go_2;
	// System.Collections.Generic.List`1<System.String> buttons::AniList
	List_1_t1398341365 * ___AniList_3;
	// System.Int32 buttons::count
	int32_t ___count_4;

public:
	inline static int32_t get_offset_of_go_2() { return static_cast<int32_t>(offsetof(buttons_t3159989035, ___go_2)); }
	inline GameObject_t1756533147 * get_go_2() const { return ___go_2; }
	inline GameObject_t1756533147 ** get_address_of_go_2() { return &___go_2; }
	inline void set_go_2(GameObject_t1756533147 * value)
	{
		___go_2 = value;
		Il2CppCodeGenWriteBarrier(&___go_2, value);
	}

	inline static int32_t get_offset_of_AniList_3() { return static_cast<int32_t>(offsetof(buttons_t3159989035, ___AniList_3)); }
	inline List_1_t1398341365 * get_AniList_3() const { return ___AniList_3; }
	inline List_1_t1398341365 ** get_address_of_AniList_3() { return &___AniList_3; }
	inline void set_AniList_3(List_1_t1398341365 * value)
	{
		___AniList_3 = value;
		Il2CppCodeGenWriteBarrier(&___AniList_3, value);
	}

	inline static int32_t get_offset_of_count_4() { return static_cast<int32_t>(offsetof(buttons_t3159989035, ___count_4)); }
	inline int32_t get_count_4() const { return ___count_4; }
	inline int32_t* get_address_of_count_4() { return &___count_4; }
	inline void set_count_4(int32_t value)
	{
		___count_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
