﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.InputField[]
struct InputFieldU5BU5D_t1225584527;
// Ximmerse.UI.UIVector3Field/Vector3Event
struct Vector3Event_t3319516927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.UI.UIVector3Field
struct  UIVector3Field_t3536787880  : public MonoBehaviour_t1158329972
{
public:
	// System.String Ximmerse.UI.UIVector3Field::m_Field
	String_t* ___m_Field_2;
	// System.String Ximmerse.UI.UIVector3Field::m_Format
	String_t* ___m_Format_3;
	// UnityEngine.Vector3 Ximmerse.UI.UIVector3Field::m_Value
	Vector3_t2243707580  ___m_Value_4;
	// UnityEngine.UI.Text Ximmerse.UI.UIVector3Field::m_Label
	Text_t356221433 * ___m_Label_5;
	// UnityEngine.UI.InputField[] Ximmerse.UI.UIVector3Field::m_Texts
	InputFieldU5BU5D_t1225584527* ___m_Texts_6;
	// Ximmerse.UI.UIVector3Field/Vector3Event Ximmerse.UI.UIVector3Field::m_OnValueChanged
	Vector3Event_t3319516927 * ___m_OnValueChanged_7;
	// System.Boolean Ximmerse.UI.UIVector3Field::m_IsRefreshing
	bool ___m_IsRefreshing_8;

public:
	inline static int32_t get_offset_of_m_Field_2() { return static_cast<int32_t>(offsetof(UIVector3Field_t3536787880, ___m_Field_2)); }
	inline String_t* get_m_Field_2() const { return ___m_Field_2; }
	inline String_t** get_address_of_m_Field_2() { return &___m_Field_2; }
	inline void set_m_Field_2(String_t* value)
	{
		___m_Field_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Field_2, value);
	}

	inline static int32_t get_offset_of_m_Format_3() { return static_cast<int32_t>(offsetof(UIVector3Field_t3536787880, ___m_Format_3)); }
	inline String_t* get_m_Format_3() const { return ___m_Format_3; }
	inline String_t** get_address_of_m_Format_3() { return &___m_Format_3; }
	inline void set_m_Format_3(String_t* value)
	{
		___m_Format_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_Format_3, value);
	}

	inline static int32_t get_offset_of_m_Value_4() { return static_cast<int32_t>(offsetof(UIVector3Field_t3536787880, ___m_Value_4)); }
	inline Vector3_t2243707580  get_m_Value_4() const { return ___m_Value_4; }
	inline Vector3_t2243707580 * get_address_of_m_Value_4() { return &___m_Value_4; }
	inline void set_m_Value_4(Vector3_t2243707580  value)
	{
		___m_Value_4 = value;
	}

	inline static int32_t get_offset_of_m_Label_5() { return static_cast<int32_t>(offsetof(UIVector3Field_t3536787880, ___m_Label_5)); }
	inline Text_t356221433 * get_m_Label_5() const { return ___m_Label_5; }
	inline Text_t356221433 ** get_address_of_m_Label_5() { return &___m_Label_5; }
	inline void set_m_Label_5(Text_t356221433 * value)
	{
		___m_Label_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_Label_5, value);
	}

	inline static int32_t get_offset_of_m_Texts_6() { return static_cast<int32_t>(offsetof(UIVector3Field_t3536787880, ___m_Texts_6)); }
	inline InputFieldU5BU5D_t1225584527* get_m_Texts_6() const { return ___m_Texts_6; }
	inline InputFieldU5BU5D_t1225584527** get_address_of_m_Texts_6() { return &___m_Texts_6; }
	inline void set_m_Texts_6(InputFieldU5BU5D_t1225584527* value)
	{
		___m_Texts_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_Texts_6, value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_7() { return static_cast<int32_t>(offsetof(UIVector3Field_t3536787880, ___m_OnValueChanged_7)); }
	inline Vector3Event_t3319516927 * get_m_OnValueChanged_7() const { return ___m_OnValueChanged_7; }
	inline Vector3Event_t3319516927 ** get_address_of_m_OnValueChanged_7() { return &___m_OnValueChanged_7; }
	inline void set_m_OnValueChanged_7(Vector3Event_t3319516927 * value)
	{
		___m_OnValueChanged_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_OnValueChanged_7, value);
	}

	inline static int32_t get_offset_of_m_IsRefreshing_8() { return static_cast<int32_t>(offsetof(UIVector3Field_t3536787880, ___m_IsRefreshing_8)); }
	inline bool get_m_IsRefreshing_8() const { return ___m_IsRefreshing_8; }
	inline bool* get_address_of_m_IsRefreshing_8() { return &___m_IsRefreshing_8; }
	inline void set_m_IsRefreshing_8(bool value)
	{
		___m_IsRefreshing_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
