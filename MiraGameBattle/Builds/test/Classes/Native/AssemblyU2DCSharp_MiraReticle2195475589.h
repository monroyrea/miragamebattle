﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// ControllerTarget
struct ControllerTarget_t3193765035;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraReticle
struct  MiraReticle_t2195475589  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform MiraReticle::ParentFacing
	Transform_t3275118058 * ___ParentFacing_2;
	// System.Single MiraReticle::scaleMultiplier
	float ___scaleMultiplier_3;
	// UnityEngine.Color MiraReticle::reticleHoverColor
	Color_t2020392075  ___reticleHoverColor_4;
	// UnityEngine.SpriteRenderer MiraReticle::reticleRenderer
	SpriteRenderer_t1209076198 * ___reticleRenderer_5;
	// UnityEngine.Vector3 MiraReticle::reticleOriginalScale
	Vector3_t2243707580  ___reticleOriginalScale_6;
	// UnityEngine.RaycastHit MiraReticle::raycastHit
	RaycastHit_t87180320  ___raycastHit_7;
	// UnityEngine.GameObject MiraReticle::gameObjectHit
	GameObject_t1756533147 * ___gameObjectHit_8;
	// ControllerTarget MiraReticle::ControllerTargetHit
	ControllerTarget_t3193765035 * ___ControllerTargetHit_9;
	// System.Single MiraReticle::reticuleOffsetZ
	float ___reticuleOffsetZ_10;
	// System.Single MiraReticle::camAngle
	float ___camAngle_11;
	// System.Single MiraReticle::camRadians
	float ___camRadians_12;
	// UnityEngine.Vector3 MiraReticle::nullPos
	Vector3_t2243707580  ___nullPos_13;

public:
	inline static int32_t get_offset_of_ParentFacing_2() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___ParentFacing_2)); }
	inline Transform_t3275118058 * get_ParentFacing_2() const { return ___ParentFacing_2; }
	inline Transform_t3275118058 ** get_address_of_ParentFacing_2() { return &___ParentFacing_2; }
	inline void set_ParentFacing_2(Transform_t3275118058 * value)
	{
		___ParentFacing_2 = value;
		Il2CppCodeGenWriteBarrier(&___ParentFacing_2, value);
	}

	inline static int32_t get_offset_of_scaleMultiplier_3() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___scaleMultiplier_3)); }
	inline float get_scaleMultiplier_3() const { return ___scaleMultiplier_3; }
	inline float* get_address_of_scaleMultiplier_3() { return &___scaleMultiplier_3; }
	inline void set_scaleMultiplier_3(float value)
	{
		___scaleMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_reticleHoverColor_4() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___reticleHoverColor_4)); }
	inline Color_t2020392075  get_reticleHoverColor_4() const { return ___reticleHoverColor_4; }
	inline Color_t2020392075 * get_address_of_reticleHoverColor_4() { return &___reticleHoverColor_4; }
	inline void set_reticleHoverColor_4(Color_t2020392075  value)
	{
		___reticleHoverColor_4 = value;
	}

	inline static int32_t get_offset_of_reticleRenderer_5() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___reticleRenderer_5)); }
	inline SpriteRenderer_t1209076198 * get_reticleRenderer_5() const { return ___reticleRenderer_5; }
	inline SpriteRenderer_t1209076198 ** get_address_of_reticleRenderer_5() { return &___reticleRenderer_5; }
	inline void set_reticleRenderer_5(SpriteRenderer_t1209076198 * value)
	{
		___reticleRenderer_5 = value;
		Il2CppCodeGenWriteBarrier(&___reticleRenderer_5, value);
	}

	inline static int32_t get_offset_of_reticleOriginalScale_6() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___reticleOriginalScale_6)); }
	inline Vector3_t2243707580  get_reticleOriginalScale_6() const { return ___reticleOriginalScale_6; }
	inline Vector3_t2243707580 * get_address_of_reticleOriginalScale_6() { return &___reticleOriginalScale_6; }
	inline void set_reticleOriginalScale_6(Vector3_t2243707580  value)
	{
		___reticleOriginalScale_6 = value;
	}

	inline static int32_t get_offset_of_raycastHit_7() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___raycastHit_7)); }
	inline RaycastHit_t87180320  get_raycastHit_7() const { return ___raycastHit_7; }
	inline RaycastHit_t87180320 * get_address_of_raycastHit_7() { return &___raycastHit_7; }
	inline void set_raycastHit_7(RaycastHit_t87180320  value)
	{
		___raycastHit_7 = value;
	}

	inline static int32_t get_offset_of_gameObjectHit_8() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___gameObjectHit_8)); }
	inline GameObject_t1756533147 * get_gameObjectHit_8() const { return ___gameObjectHit_8; }
	inline GameObject_t1756533147 ** get_address_of_gameObjectHit_8() { return &___gameObjectHit_8; }
	inline void set_gameObjectHit_8(GameObject_t1756533147 * value)
	{
		___gameObjectHit_8 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectHit_8, value);
	}

	inline static int32_t get_offset_of_ControllerTargetHit_9() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___ControllerTargetHit_9)); }
	inline ControllerTarget_t3193765035 * get_ControllerTargetHit_9() const { return ___ControllerTargetHit_9; }
	inline ControllerTarget_t3193765035 ** get_address_of_ControllerTargetHit_9() { return &___ControllerTargetHit_9; }
	inline void set_ControllerTargetHit_9(ControllerTarget_t3193765035 * value)
	{
		___ControllerTargetHit_9 = value;
		Il2CppCodeGenWriteBarrier(&___ControllerTargetHit_9, value);
	}

	inline static int32_t get_offset_of_reticuleOffsetZ_10() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___reticuleOffsetZ_10)); }
	inline float get_reticuleOffsetZ_10() const { return ___reticuleOffsetZ_10; }
	inline float* get_address_of_reticuleOffsetZ_10() { return &___reticuleOffsetZ_10; }
	inline void set_reticuleOffsetZ_10(float value)
	{
		___reticuleOffsetZ_10 = value;
	}

	inline static int32_t get_offset_of_camAngle_11() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___camAngle_11)); }
	inline float get_camAngle_11() const { return ___camAngle_11; }
	inline float* get_address_of_camAngle_11() { return &___camAngle_11; }
	inline void set_camAngle_11(float value)
	{
		___camAngle_11 = value;
	}

	inline static int32_t get_offset_of_camRadians_12() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___camRadians_12)); }
	inline float get_camRadians_12() const { return ___camRadians_12; }
	inline float* get_address_of_camRadians_12() { return &___camRadians_12; }
	inline void set_camRadians_12(float value)
	{
		___camRadians_12 = value;
	}

	inline static int32_t get_offset_of_nullPos_13() { return static_cast<int32_t>(offsetof(MiraReticle_t2195475589, ___nullPos_13)); }
	inline Vector3_t2243707580  get_nullPos_13() const { return ___nullPos_13; }
	inline Vector3_t2243707580 * get_address_of_nullPos_13() { return &___nullPos_13; }
	inline void set_nullPos_13(Vector3_t2243707580  value)
	{
		___nullPos_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
