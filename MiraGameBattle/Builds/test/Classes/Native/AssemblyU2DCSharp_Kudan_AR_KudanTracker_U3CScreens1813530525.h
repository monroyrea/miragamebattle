﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// Kudan.AR.KudanTracker
struct KudanTracker_t2134143885;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kudan.AR.KudanTracker/<Screenshot>c__Iterator0
struct  U3CScreenshotU3Ec__Iterator0_t1813530525  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Kudan.AR.KudanTracker/<Screenshot>c__Iterator0::<uiObjects>__0
	List_1_t1125654279 * ___U3CuiObjectsU3E__0_0;
	// System.Boolean Kudan.AR.KudanTracker/<Screenshot>c__Iterator0::<wasDebug>__0
	bool ___U3CwasDebugU3E__0_1;
	// UnityEngine.RenderTexture Kudan.AR.KudanTracker/<Screenshot>c__Iterator0::<RT>__0
	RenderTexture_t2666733923 * ___U3CRTU3E__0_2;
	// UnityEngine.Texture2D Kudan.AR.KudanTracker/<Screenshot>c__Iterator0::<screen>__0
	Texture2D_t3542995729 * ___U3CscreenU3E__0_3;
	// System.Byte[] Kudan.AR.KudanTracker/<Screenshot>c__Iterator0::<bytes>__0
	ByteU5BU5D_t3397334013* ___U3CbytesU3E__0_4;
	// System.String Kudan.AR.KudanTracker/<Screenshot>c__Iterator0::<filePath>__0
	String_t* ___U3CfilePathU3E__0_5;
	// Kudan.AR.KudanTracker Kudan.AR.KudanTracker/<Screenshot>c__Iterator0::$this
	KudanTracker_t2134143885 * ___U24this_6;
	// System.Object Kudan.AR.KudanTracker/<Screenshot>c__Iterator0::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean Kudan.AR.KudanTracker/<Screenshot>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 Kudan.AR.KudanTracker/<Screenshot>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CuiObjectsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CScreenshotU3Ec__Iterator0_t1813530525, ___U3CuiObjectsU3E__0_0)); }
	inline List_1_t1125654279 * get_U3CuiObjectsU3E__0_0() const { return ___U3CuiObjectsU3E__0_0; }
	inline List_1_t1125654279 ** get_address_of_U3CuiObjectsU3E__0_0() { return &___U3CuiObjectsU3E__0_0; }
	inline void set_U3CuiObjectsU3E__0_0(List_1_t1125654279 * value)
	{
		___U3CuiObjectsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuiObjectsU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CwasDebugU3E__0_1() { return static_cast<int32_t>(offsetof(U3CScreenshotU3Ec__Iterator0_t1813530525, ___U3CwasDebugU3E__0_1)); }
	inline bool get_U3CwasDebugU3E__0_1() const { return ___U3CwasDebugU3E__0_1; }
	inline bool* get_address_of_U3CwasDebugU3E__0_1() { return &___U3CwasDebugU3E__0_1; }
	inline void set_U3CwasDebugU3E__0_1(bool value)
	{
		___U3CwasDebugU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CRTU3E__0_2() { return static_cast<int32_t>(offsetof(U3CScreenshotU3Ec__Iterator0_t1813530525, ___U3CRTU3E__0_2)); }
	inline RenderTexture_t2666733923 * get_U3CRTU3E__0_2() const { return ___U3CRTU3E__0_2; }
	inline RenderTexture_t2666733923 ** get_address_of_U3CRTU3E__0_2() { return &___U3CRTU3E__0_2; }
	inline void set_U3CRTU3E__0_2(RenderTexture_t2666733923 * value)
	{
		___U3CRTU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRTU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CscreenU3E__0_3() { return static_cast<int32_t>(offsetof(U3CScreenshotU3Ec__Iterator0_t1813530525, ___U3CscreenU3E__0_3)); }
	inline Texture2D_t3542995729 * get_U3CscreenU3E__0_3() const { return ___U3CscreenU3E__0_3; }
	inline Texture2D_t3542995729 ** get_address_of_U3CscreenU3E__0_3() { return &___U3CscreenU3E__0_3; }
	inline void set_U3CscreenU3E__0_3(Texture2D_t3542995729 * value)
	{
		___U3CscreenU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CscreenU3E__0_3, value);
	}

	inline static int32_t get_offset_of_U3CbytesU3E__0_4() { return static_cast<int32_t>(offsetof(U3CScreenshotU3Ec__Iterator0_t1813530525, ___U3CbytesU3E__0_4)); }
	inline ByteU5BU5D_t3397334013* get_U3CbytesU3E__0_4() const { return ___U3CbytesU3E__0_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CbytesU3E__0_4() { return &___U3CbytesU3E__0_4; }
	inline void set_U3CbytesU3E__0_4(ByteU5BU5D_t3397334013* value)
	{
		___U3CbytesU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbytesU3E__0_4, value);
	}

	inline static int32_t get_offset_of_U3CfilePathU3E__0_5() { return static_cast<int32_t>(offsetof(U3CScreenshotU3Ec__Iterator0_t1813530525, ___U3CfilePathU3E__0_5)); }
	inline String_t* get_U3CfilePathU3E__0_5() const { return ___U3CfilePathU3E__0_5; }
	inline String_t** get_address_of_U3CfilePathU3E__0_5() { return &___U3CfilePathU3E__0_5; }
	inline void set_U3CfilePathU3E__0_5(String_t* value)
	{
		___U3CfilePathU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfilePathU3E__0_5, value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CScreenshotU3Ec__Iterator0_t1813530525, ___U24this_6)); }
	inline KudanTracker_t2134143885 * get_U24this_6() const { return ___U24this_6; }
	inline KudanTracker_t2134143885 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(KudanTracker_t2134143885 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CScreenshotU3Ec__Iterator0_t1813530525, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CScreenshotU3Ec__Iterator0_t1813530525, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CScreenshotU3Ec__Iterator0_t1813530525, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
