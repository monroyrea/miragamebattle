﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControllerInputGUI/SliderGroup
struct  SliderGroup_t396385140  : public Il2CppObject
{
public:
	// UnityEngine.UI.Image ControllerInputGUI/SliderGroup::image
	Image_t2042527209 * ___image_0;
	// System.Single ControllerInputGUI/SliderGroup::max
	float ___max_1;
	// UnityEngine.UI.Text ControllerInputGUI/SliderGroup::text
	Text_t356221433 * ___text_2;
	// System.String ControllerInputGUI/SliderGroup::format
	String_t* ___format_3;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(SliderGroup_t396385140, ___image_0)); }
	inline Image_t2042527209 * get_image_0() const { return ___image_0; }
	inline Image_t2042527209 ** get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(Image_t2042527209 * value)
	{
		___image_0 = value;
		Il2CppCodeGenWriteBarrier(&___image_0, value);
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(SliderGroup_t396385140, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}

	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(SliderGroup_t396385140, ___text_2)); }
	inline Text_t356221433 * get_text_2() const { return ___text_2; }
	inline Text_t356221433 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(Text_t356221433 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier(&___text_2, value);
	}

	inline static int32_t get_offset_of_format_3() { return static_cast<int32_t>(offsetof(SliderGroup_t396385140, ___format_3)); }
	inline String_t* get_format_3() const { return ___format_3; }
	inline String_t** get_address_of_format_3() { return &___format_3; }
	inline void set_format_3(String_t* value)
	{
		___format_3 = value;
		Il2CppCodeGenWriteBarrier(&___format_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
