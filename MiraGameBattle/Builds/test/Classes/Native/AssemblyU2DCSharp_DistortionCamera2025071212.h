﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// DistortionCamera
struct DistortionCamera_t2025071212;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DistortionCamera
struct  DistortionCamera_t2025071212  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct DistortionCamera_t2025071212_StaticFields
{
public:
	// DistortionCamera DistortionCamera::instance
	DistortionCamera_t2025071212 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(DistortionCamera_t2025071212_StaticFields, ___instance_2)); }
	inline DistortionCamera_t2025071212 * get_instance_2() const { return ___instance_2; }
	inline DistortionCamera_t2025071212 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(DistortionCamera_t2025071212 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
