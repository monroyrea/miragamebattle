﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<SimplePicker/PickObject>
struct List_1_t1968018177;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplePicker/PickObject
struct  PickObject_t2598897045  : public Il2CppObject
{
public:
	// UnityEngine.Transform SimplePicker/PickObject::transform
	Transform_t3275118058 * ___transform_2;
	// UnityEngine.Transform SimplePicker/PickObject::parent
	Transform_t3275118058 * ___parent_3;
	// UnityEngine.Rigidbody SimplePicker/PickObject::rigidbody
	Rigidbody_t4233889191 * ___rigidbody_4;
	// System.Boolean SimplePicker/PickObject::isKinematic
	bool ___isKinematic_5;
	// System.Boolean SimplePicker/PickObject::useGravity
	bool ___useGravity_6;

public:
	inline static int32_t get_offset_of_transform_2() { return static_cast<int32_t>(offsetof(PickObject_t2598897045, ___transform_2)); }
	inline Transform_t3275118058 * get_transform_2() const { return ___transform_2; }
	inline Transform_t3275118058 ** get_address_of_transform_2() { return &___transform_2; }
	inline void set_transform_2(Transform_t3275118058 * value)
	{
		___transform_2 = value;
		Il2CppCodeGenWriteBarrier(&___transform_2, value);
	}

	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(PickObject_t2598897045, ___parent_3)); }
	inline Transform_t3275118058 * get_parent_3() const { return ___parent_3; }
	inline Transform_t3275118058 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(Transform_t3275118058 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier(&___parent_3, value);
	}

	inline static int32_t get_offset_of_rigidbody_4() { return static_cast<int32_t>(offsetof(PickObject_t2598897045, ___rigidbody_4)); }
	inline Rigidbody_t4233889191 * get_rigidbody_4() const { return ___rigidbody_4; }
	inline Rigidbody_t4233889191 ** get_address_of_rigidbody_4() { return &___rigidbody_4; }
	inline void set_rigidbody_4(Rigidbody_t4233889191 * value)
	{
		___rigidbody_4 = value;
		Il2CppCodeGenWriteBarrier(&___rigidbody_4, value);
	}

	inline static int32_t get_offset_of_isKinematic_5() { return static_cast<int32_t>(offsetof(PickObject_t2598897045, ___isKinematic_5)); }
	inline bool get_isKinematic_5() const { return ___isKinematic_5; }
	inline bool* get_address_of_isKinematic_5() { return &___isKinematic_5; }
	inline void set_isKinematic_5(bool value)
	{
		___isKinematic_5 = value;
	}

	inline static int32_t get_offset_of_useGravity_6() { return static_cast<int32_t>(offsetof(PickObject_t2598897045, ___useGravity_6)); }
	inline bool get_useGravity_6() const { return ___useGravity_6; }
	inline bool* get_address_of_useGravity_6() { return &___useGravity_6; }
	inline void set_useGravity_6(bool value)
	{
		___useGravity_6 = value;
	}
};

struct PickObject_t2598897045_StaticFields
{
public:
	// System.Collections.Generic.List`1<SimplePicker/PickObject> SimplePicker/PickObject::s_InstancePool
	List_1_t1968018177 * ___s_InstancePool_0;
	// System.Int32 SimplePicker/PickObject::s_NumInstancePool
	int32_t ___s_NumInstancePool_1;

public:
	inline static int32_t get_offset_of_s_InstancePool_0() { return static_cast<int32_t>(offsetof(PickObject_t2598897045_StaticFields, ___s_InstancePool_0)); }
	inline List_1_t1968018177 * get_s_InstancePool_0() const { return ___s_InstancePool_0; }
	inline List_1_t1968018177 ** get_address_of_s_InstancePool_0() { return &___s_InstancePool_0; }
	inline void set_s_InstancePool_0(List_1_t1968018177 * value)
	{
		___s_InstancePool_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_InstancePool_0, value);
	}

	inline static int32_t get_offset_of_s_NumInstancePool_1() { return static_cast<int32_t>(offsetof(PickObject_t2598897045_StaticFields, ___s_NumInstancePool_1)); }
	inline int32_t get_s_NumInstancePool_1() const { return ___s_NumInstancePool_1; }
	inline int32_t* get_address_of_s_NumInstancePool_1() { return &___s_NumInstancePool_1; }
	inline void set_s_NumInstancePool_1(int32_t value)
	{
		___s_NumInstancePool_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
