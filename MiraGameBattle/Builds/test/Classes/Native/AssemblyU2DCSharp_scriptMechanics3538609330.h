﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_scriptMechanics_EnumPlayerType985826215.h"

// System.Collections.Generic.List`1<ClassPlayerSensorsData>
struct List_1_t4064290120;
// UnityEngine.Animator
struct Animator_t69676727;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// scriptMechanics
struct  scriptMechanics_t3538609330  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<ClassPlayerSensorsData> scriptMechanics::_playerSensorList
	List_1_t4064290120 * ____playerSensorList_2;
	// UnityEngine.Animator scriptMechanics::anim
	Animator_t69676727 * ___anim_3;
	// System.Single scriptMechanics::_value
	float ____value_4;
	// System.Single scriptMechanics::_maximumHealth
	float ____maximumHealth_5;
	// System.Single scriptMechanics::_currentHealth
	float ____currentHealth_6;
	// scriptMechanics/EnumPlayerType scriptMechanics::_instancePlayerType
	int32_t ____instancePlayerType_7;

public:
	inline static int32_t get_offset_of__playerSensorList_2() { return static_cast<int32_t>(offsetof(scriptMechanics_t3538609330, ____playerSensorList_2)); }
	inline List_1_t4064290120 * get__playerSensorList_2() const { return ____playerSensorList_2; }
	inline List_1_t4064290120 ** get_address_of__playerSensorList_2() { return &____playerSensorList_2; }
	inline void set__playerSensorList_2(List_1_t4064290120 * value)
	{
		____playerSensorList_2 = value;
		Il2CppCodeGenWriteBarrier(&____playerSensorList_2, value);
	}

	inline static int32_t get_offset_of_anim_3() { return static_cast<int32_t>(offsetof(scriptMechanics_t3538609330, ___anim_3)); }
	inline Animator_t69676727 * get_anim_3() const { return ___anim_3; }
	inline Animator_t69676727 ** get_address_of_anim_3() { return &___anim_3; }
	inline void set_anim_3(Animator_t69676727 * value)
	{
		___anim_3 = value;
		Il2CppCodeGenWriteBarrier(&___anim_3, value);
	}

	inline static int32_t get_offset_of__value_4() { return static_cast<int32_t>(offsetof(scriptMechanics_t3538609330, ____value_4)); }
	inline float get__value_4() const { return ____value_4; }
	inline float* get_address_of__value_4() { return &____value_4; }
	inline void set__value_4(float value)
	{
		____value_4 = value;
	}

	inline static int32_t get_offset_of__maximumHealth_5() { return static_cast<int32_t>(offsetof(scriptMechanics_t3538609330, ____maximumHealth_5)); }
	inline float get__maximumHealth_5() const { return ____maximumHealth_5; }
	inline float* get_address_of__maximumHealth_5() { return &____maximumHealth_5; }
	inline void set__maximumHealth_5(float value)
	{
		____maximumHealth_5 = value;
	}

	inline static int32_t get_offset_of__currentHealth_6() { return static_cast<int32_t>(offsetof(scriptMechanics_t3538609330, ____currentHealth_6)); }
	inline float get__currentHealth_6() const { return ____currentHealth_6; }
	inline float* get_address_of__currentHealth_6() { return &____currentHealth_6; }
	inline void set__currentHealth_6(float value)
	{
		____currentHealth_6 = value;
	}

	inline static int32_t get_offset_of__instancePlayerType_7() { return static_cast<int32_t>(offsetof(scriptMechanics_t3538609330, ____instancePlayerType_7)); }
	inline int32_t get__instancePlayerType_7() const { return ____instancePlayerType_7; }
	inline int32_t* get_address_of__instancePlayerType_7() { return &____instancePlayerType_7; }
	inline void set__instancePlayerType_7(int32_t value)
	{
		____instancePlayerType_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
