﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayAreaHelper/BoundaryTestResult
struct  BoundaryTestResult_t993820647 
{
public:
	// System.Boolean PlayAreaHelper/BoundaryTestResult::isTriggering
	bool ___isTriggering_0;
	// System.Int32 PlayAreaHelper/BoundaryTestResult::cornerId0
	int32_t ___cornerId0_1;
	// System.Int32 PlayAreaHelper/BoundaryTestResult::cornerId1
	int32_t ___cornerId1_2;
	// System.Single PlayAreaHelper/BoundaryTestResult::distance
	float ___distance_3;
	// UnityEngine.Vector3 PlayAreaHelper/BoundaryTestResult::point
	Vector3_t2243707580  ___point_4;
	// UnityEngine.Vector3 PlayAreaHelper/BoundaryTestResult::normal
	Vector3_t2243707580  ___normal_5;

public:
	inline static int32_t get_offset_of_isTriggering_0() { return static_cast<int32_t>(offsetof(BoundaryTestResult_t993820647, ___isTriggering_0)); }
	inline bool get_isTriggering_0() const { return ___isTriggering_0; }
	inline bool* get_address_of_isTriggering_0() { return &___isTriggering_0; }
	inline void set_isTriggering_0(bool value)
	{
		___isTriggering_0 = value;
	}

	inline static int32_t get_offset_of_cornerId0_1() { return static_cast<int32_t>(offsetof(BoundaryTestResult_t993820647, ___cornerId0_1)); }
	inline int32_t get_cornerId0_1() const { return ___cornerId0_1; }
	inline int32_t* get_address_of_cornerId0_1() { return &___cornerId0_1; }
	inline void set_cornerId0_1(int32_t value)
	{
		___cornerId0_1 = value;
	}

	inline static int32_t get_offset_of_cornerId1_2() { return static_cast<int32_t>(offsetof(BoundaryTestResult_t993820647, ___cornerId1_2)); }
	inline int32_t get_cornerId1_2() const { return ___cornerId1_2; }
	inline int32_t* get_address_of_cornerId1_2() { return &___cornerId1_2; }
	inline void set_cornerId1_2(int32_t value)
	{
		___cornerId1_2 = value;
	}

	inline static int32_t get_offset_of_distance_3() { return static_cast<int32_t>(offsetof(BoundaryTestResult_t993820647, ___distance_3)); }
	inline float get_distance_3() const { return ___distance_3; }
	inline float* get_address_of_distance_3() { return &___distance_3; }
	inline void set_distance_3(float value)
	{
		___distance_3 = value;
	}

	inline static int32_t get_offset_of_point_4() { return static_cast<int32_t>(offsetof(BoundaryTestResult_t993820647, ___point_4)); }
	inline Vector3_t2243707580  get_point_4() const { return ___point_4; }
	inline Vector3_t2243707580 * get_address_of_point_4() { return &___point_4; }
	inline void set_point_4(Vector3_t2243707580  value)
	{
		___point_4 = value;
	}

	inline static int32_t get_offset_of_normal_5() { return static_cast<int32_t>(offsetof(BoundaryTestResult_t993820647, ___normal_5)); }
	inline Vector3_t2243707580  get_normal_5() const { return ___normal_5; }
	inline Vector3_t2243707580 * get_address_of_normal_5() { return &___normal_5; }
	inline void set_normal_5(Vector3_t2243707580  value)
	{
		___normal_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PlayAreaHelper/BoundaryTestResult
struct BoundaryTestResult_t993820647_marshaled_pinvoke
{
	int32_t ___isTriggering_0;
	int32_t ___cornerId0_1;
	int32_t ___cornerId1_2;
	float ___distance_3;
	Vector3_t2243707580  ___point_4;
	Vector3_t2243707580  ___normal_5;
};
// Native definition for COM marshalling of PlayAreaHelper/BoundaryTestResult
struct BoundaryTestResult_t993820647_marshaled_com
{
	int32_t ___isTriggering_0;
	int32_t ___cornerId0_1;
	int32_t ___cornerId1_2;
	float ___distance_3;
	Vector3_t2243707580  ___point_4;
	Vector3_t2243707580  ___normal_5;
};
