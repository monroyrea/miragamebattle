﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraHUD
struct  MiraHUD_t2762852056  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text MiraHUD::trackingText
	Text_t356221433 * ___trackingText_2;
	// UnityEngine.Color MiraHUD::trackingColor
	Color_t2020392075  ___trackingColor_3;
	// UnityEngine.Color MiraHUD::notTrackingColor
	Color_t2020392075  ___notTrackingColor_4;

public:
	inline static int32_t get_offset_of_trackingText_2() { return static_cast<int32_t>(offsetof(MiraHUD_t2762852056, ___trackingText_2)); }
	inline Text_t356221433 * get_trackingText_2() const { return ___trackingText_2; }
	inline Text_t356221433 ** get_address_of_trackingText_2() { return &___trackingText_2; }
	inline void set_trackingText_2(Text_t356221433 * value)
	{
		___trackingText_2 = value;
		Il2CppCodeGenWriteBarrier(&___trackingText_2, value);
	}

	inline static int32_t get_offset_of_trackingColor_3() { return static_cast<int32_t>(offsetof(MiraHUD_t2762852056, ___trackingColor_3)); }
	inline Color_t2020392075  get_trackingColor_3() const { return ___trackingColor_3; }
	inline Color_t2020392075 * get_address_of_trackingColor_3() { return &___trackingColor_3; }
	inline void set_trackingColor_3(Color_t2020392075  value)
	{
		___trackingColor_3 = value;
	}

	inline static int32_t get_offset_of_notTrackingColor_4() { return static_cast<int32_t>(offsetof(MiraHUD_t2762852056, ___notTrackingColor_4)); }
	inline Color_t2020392075  get_notTrackingColor_4() const { return ___notTrackingColor_4; }
	inline Color_t2020392075 * get_address_of_notTrackingColor_4() { return &___notTrackingColor_4; }
	inline void set_notTrackingColor_4(Color_t2020392075  value)
	{
		___notTrackingColor_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
