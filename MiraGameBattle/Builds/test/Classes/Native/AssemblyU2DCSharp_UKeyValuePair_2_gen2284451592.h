﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.UI.Button
struct Button_t2872111280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UKeyValuePair`2<UnityEngine.UI.Button,System.Single>
struct  UKeyValuePair_2_t2284451592  : public Il2CppObject
{
public:
	// TKey UKeyValuePair`2::key
	Button_t2872111280 * ___key_0;
	// TValue UKeyValuePair`2::value
	float ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(UKeyValuePair_2_t2284451592, ___key_0)); }
	inline Button_t2872111280 * get_key_0() const { return ___key_0; }
	inline Button_t2872111280 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Button_t2872111280 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(UKeyValuePair_2_t2284451592, ___value_1)); }
	inline float get_value_1() const { return ___value_1; }
	inline float* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(float value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
