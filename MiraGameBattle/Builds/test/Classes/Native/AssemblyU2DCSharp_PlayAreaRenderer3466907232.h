﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayAreaRenderer
struct  PlayAreaRenderer_t3466907232  : public MonoBehaviour_t1158329972
{
public:
	// System.Single PlayAreaRenderer::borderThickness
	float ___borderThickness_2;
	// UnityEngine.Material PlayAreaRenderer::groundMaterial
	Material_t193706927 * ___groundMaterial_3;
	// UnityEngine.Color PlayAreaRenderer::groundColor
	Color_t2020392075  ___groundColor_4;
	// System.Single PlayAreaRenderer::thickness
	float ___thickness_5;
	// System.Single PlayAreaRenderer::cellSize
	float ___cellSize_6;
	// System.Single PlayAreaRenderer::emptySize
	float ___emptySize_7;
	// UnityEngine.Material PlayAreaRenderer::wallMaterial
	Material_t193706927 * ___wallMaterial_8;
	// UnityEngine.Color PlayAreaRenderer::wallColor
	Color_t2020392075  ___wallColor_9;
	// System.Single PlayAreaRenderer::height
	float ___height_10;
	// System.Int32 PlayAreaRenderer::handedness
	int32_t ___handedness_11;
	// UnityEngine.Vector3[] PlayAreaRenderer::corners
	Vector3U5BU5D_t1172311765* ___corners_12;
	// UnityEngine.GameObject PlayAreaRenderer::m_GroundRoot
	GameObject_t1756533147 * ___m_GroundRoot_13;
	// UnityEngine.GameObject PlayAreaRenderer::m_WallRoot
	GameObject_t1756533147 * ___m_WallRoot_14;
	// UnityEngine.GameObject[] PlayAreaRenderer::m_Walls
	GameObjectU5BU5D_t3057952154* ___m_Walls_15;

public:
	inline static int32_t get_offset_of_borderThickness_2() { return static_cast<int32_t>(offsetof(PlayAreaRenderer_t3466907232, ___borderThickness_2)); }
	inline float get_borderThickness_2() const { return ___borderThickness_2; }
	inline float* get_address_of_borderThickness_2() { return &___borderThickness_2; }
	inline void set_borderThickness_2(float value)
	{
		___borderThickness_2 = value;
	}

	inline static int32_t get_offset_of_groundMaterial_3() { return static_cast<int32_t>(offsetof(PlayAreaRenderer_t3466907232, ___groundMaterial_3)); }
	inline Material_t193706927 * get_groundMaterial_3() const { return ___groundMaterial_3; }
	inline Material_t193706927 ** get_address_of_groundMaterial_3() { return &___groundMaterial_3; }
	inline void set_groundMaterial_3(Material_t193706927 * value)
	{
		___groundMaterial_3 = value;
		Il2CppCodeGenWriteBarrier(&___groundMaterial_3, value);
	}

	inline static int32_t get_offset_of_groundColor_4() { return static_cast<int32_t>(offsetof(PlayAreaRenderer_t3466907232, ___groundColor_4)); }
	inline Color_t2020392075  get_groundColor_4() const { return ___groundColor_4; }
	inline Color_t2020392075 * get_address_of_groundColor_4() { return &___groundColor_4; }
	inline void set_groundColor_4(Color_t2020392075  value)
	{
		___groundColor_4 = value;
	}

	inline static int32_t get_offset_of_thickness_5() { return static_cast<int32_t>(offsetof(PlayAreaRenderer_t3466907232, ___thickness_5)); }
	inline float get_thickness_5() const { return ___thickness_5; }
	inline float* get_address_of_thickness_5() { return &___thickness_5; }
	inline void set_thickness_5(float value)
	{
		___thickness_5 = value;
	}

	inline static int32_t get_offset_of_cellSize_6() { return static_cast<int32_t>(offsetof(PlayAreaRenderer_t3466907232, ___cellSize_6)); }
	inline float get_cellSize_6() const { return ___cellSize_6; }
	inline float* get_address_of_cellSize_6() { return &___cellSize_6; }
	inline void set_cellSize_6(float value)
	{
		___cellSize_6 = value;
	}

	inline static int32_t get_offset_of_emptySize_7() { return static_cast<int32_t>(offsetof(PlayAreaRenderer_t3466907232, ___emptySize_7)); }
	inline float get_emptySize_7() const { return ___emptySize_7; }
	inline float* get_address_of_emptySize_7() { return &___emptySize_7; }
	inline void set_emptySize_7(float value)
	{
		___emptySize_7 = value;
	}

	inline static int32_t get_offset_of_wallMaterial_8() { return static_cast<int32_t>(offsetof(PlayAreaRenderer_t3466907232, ___wallMaterial_8)); }
	inline Material_t193706927 * get_wallMaterial_8() const { return ___wallMaterial_8; }
	inline Material_t193706927 ** get_address_of_wallMaterial_8() { return &___wallMaterial_8; }
	inline void set_wallMaterial_8(Material_t193706927 * value)
	{
		___wallMaterial_8 = value;
		Il2CppCodeGenWriteBarrier(&___wallMaterial_8, value);
	}

	inline static int32_t get_offset_of_wallColor_9() { return static_cast<int32_t>(offsetof(PlayAreaRenderer_t3466907232, ___wallColor_9)); }
	inline Color_t2020392075  get_wallColor_9() const { return ___wallColor_9; }
	inline Color_t2020392075 * get_address_of_wallColor_9() { return &___wallColor_9; }
	inline void set_wallColor_9(Color_t2020392075  value)
	{
		___wallColor_9 = value;
	}

	inline static int32_t get_offset_of_height_10() { return static_cast<int32_t>(offsetof(PlayAreaRenderer_t3466907232, ___height_10)); }
	inline float get_height_10() const { return ___height_10; }
	inline float* get_address_of_height_10() { return &___height_10; }
	inline void set_height_10(float value)
	{
		___height_10 = value;
	}

	inline static int32_t get_offset_of_handedness_11() { return static_cast<int32_t>(offsetof(PlayAreaRenderer_t3466907232, ___handedness_11)); }
	inline int32_t get_handedness_11() const { return ___handedness_11; }
	inline int32_t* get_address_of_handedness_11() { return &___handedness_11; }
	inline void set_handedness_11(int32_t value)
	{
		___handedness_11 = value;
	}

	inline static int32_t get_offset_of_corners_12() { return static_cast<int32_t>(offsetof(PlayAreaRenderer_t3466907232, ___corners_12)); }
	inline Vector3U5BU5D_t1172311765* get_corners_12() const { return ___corners_12; }
	inline Vector3U5BU5D_t1172311765** get_address_of_corners_12() { return &___corners_12; }
	inline void set_corners_12(Vector3U5BU5D_t1172311765* value)
	{
		___corners_12 = value;
		Il2CppCodeGenWriteBarrier(&___corners_12, value);
	}

	inline static int32_t get_offset_of_m_GroundRoot_13() { return static_cast<int32_t>(offsetof(PlayAreaRenderer_t3466907232, ___m_GroundRoot_13)); }
	inline GameObject_t1756533147 * get_m_GroundRoot_13() const { return ___m_GroundRoot_13; }
	inline GameObject_t1756533147 ** get_address_of_m_GroundRoot_13() { return &___m_GroundRoot_13; }
	inline void set_m_GroundRoot_13(GameObject_t1756533147 * value)
	{
		___m_GroundRoot_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_GroundRoot_13, value);
	}

	inline static int32_t get_offset_of_m_WallRoot_14() { return static_cast<int32_t>(offsetof(PlayAreaRenderer_t3466907232, ___m_WallRoot_14)); }
	inline GameObject_t1756533147 * get_m_WallRoot_14() const { return ___m_WallRoot_14; }
	inline GameObject_t1756533147 ** get_address_of_m_WallRoot_14() { return &___m_WallRoot_14; }
	inline void set_m_WallRoot_14(GameObject_t1756533147 * value)
	{
		___m_WallRoot_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_WallRoot_14, value);
	}

	inline static int32_t get_offset_of_m_Walls_15() { return static_cast<int32_t>(offsetof(PlayAreaRenderer_t3466907232, ___m_Walls_15)); }
	inline GameObjectU5BU5D_t3057952154* get_m_Walls_15() const { return ___m_Walls_15; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_Walls_15() { return &___m_Walls_15; }
	inline void set_m_Walls_15(GameObjectU5BU5D_t3057952154* value)
	{
		___m_Walls_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_Walls_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
