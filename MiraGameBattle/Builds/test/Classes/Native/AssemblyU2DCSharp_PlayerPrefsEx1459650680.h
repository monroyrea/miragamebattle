﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// PlayerPrefsEx
struct PlayerPrefsEx_t1459650680;
// PlayerPrefsEx/PlayerPrefsExDictionary
struct PlayerPrefsExDictionary_t3924552273;
// PlayerPrefsEx/IntDictionary
struct IntDictionary_t405243266;
// PlayerPrefsEx/FloatDictionary
struct FloatDictionary_t1539784753;
// PlayerPrefsEx/StringDictionary
struct StringDictionary_t1060055032;
// PlayerPrefsEx/Vector3Dictionary
struct Vector3Dictionary_t4020395255;
// PlayerPrefsEx/ColorDictionary
struct ColorDictionary_t1385420584;
// PlayerPrefsEx/ObjectDictionary
struct ObjectDictionary_t1252187966;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerPrefsEx
struct  PlayerPrefsEx_t1459650680  : public MonoBehaviour_t1158329972
{
public:
	// PlayerPrefsEx/PlayerPrefsExDictionary PlayerPrefsEx::mapPlayerPrefsEx
	PlayerPrefsExDictionary_t3924552273 * ___mapPlayerPrefsEx_4;
	// PlayerPrefsEx/IntDictionary PlayerPrefsEx::mapInt
	IntDictionary_t405243266 * ___mapInt_5;
	// PlayerPrefsEx/FloatDictionary PlayerPrefsEx::mapFloat
	FloatDictionary_t1539784753 * ___mapFloat_6;
	// PlayerPrefsEx/StringDictionary PlayerPrefsEx::mapString
	StringDictionary_t1060055032 * ___mapString_7;
	// PlayerPrefsEx/Vector3Dictionary PlayerPrefsEx::mapVector3
	Vector3Dictionary_t4020395255 * ___mapVector3_8;
	// PlayerPrefsEx/ColorDictionary PlayerPrefsEx::mapColor
	ColorDictionary_t1385420584 * ___mapColor_9;
	// PlayerPrefsEx/ObjectDictionary PlayerPrefsEx::mapObject
	ObjectDictionary_t1252187966 * ___mapObject_10;
	// PlayerPrefsEx PlayerPrefsEx::parent
	PlayerPrefsEx_t1459650680 * ___parent_11;

public:
	inline static int32_t get_offset_of_mapPlayerPrefsEx_4() { return static_cast<int32_t>(offsetof(PlayerPrefsEx_t1459650680, ___mapPlayerPrefsEx_4)); }
	inline PlayerPrefsExDictionary_t3924552273 * get_mapPlayerPrefsEx_4() const { return ___mapPlayerPrefsEx_4; }
	inline PlayerPrefsExDictionary_t3924552273 ** get_address_of_mapPlayerPrefsEx_4() { return &___mapPlayerPrefsEx_4; }
	inline void set_mapPlayerPrefsEx_4(PlayerPrefsExDictionary_t3924552273 * value)
	{
		___mapPlayerPrefsEx_4 = value;
		Il2CppCodeGenWriteBarrier(&___mapPlayerPrefsEx_4, value);
	}

	inline static int32_t get_offset_of_mapInt_5() { return static_cast<int32_t>(offsetof(PlayerPrefsEx_t1459650680, ___mapInt_5)); }
	inline IntDictionary_t405243266 * get_mapInt_5() const { return ___mapInt_5; }
	inline IntDictionary_t405243266 ** get_address_of_mapInt_5() { return &___mapInt_5; }
	inline void set_mapInt_5(IntDictionary_t405243266 * value)
	{
		___mapInt_5 = value;
		Il2CppCodeGenWriteBarrier(&___mapInt_5, value);
	}

	inline static int32_t get_offset_of_mapFloat_6() { return static_cast<int32_t>(offsetof(PlayerPrefsEx_t1459650680, ___mapFloat_6)); }
	inline FloatDictionary_t1539784753 * get_mapFloat_6() const { return ___mapFloat_6; }
	inline FloatDictionary_t1539784753 ** get_address_of_mapFloat_6() { return &___mapFloat_6; }
	inline void set_mapFloat_6(FloatDictionary_t1539784753 * value)
	{
		___mapFloat_6 = value;
		Il2CppCodeGenWriteBarrier(&___mapFloat_6, value);
	}

	inline static int32_t get_offset_of_mapString_7() { return static_cast<int32_t>(offsetof(PlayerPrefsEx_t1459650680, ___mapString_7)); }
	inline StringDictionary_t1060055032 * get_mapString_7() const { return ___mapString_7; }
	inline StringDictionary_t1060055032 ** get_address_of_mapString_7() { return &___mapString_7; }
	inline void set_mapString_7(StringDictionary_t1060055032 * value)
	{
		___mapString_7 = value;
		Il2CppCodeGenWriteBarrier(&___mapString_7, value);
	}

	inline static int32_t get_offset_of_mapVector3_8() { return static_cast<int32_t>(offsetof(PlayerPrefsEx_t1459650680, ___mapVector3_8)); }
	inline Vector3Dictionary_t4020395255 * get_mapVector3_8() const { return ___mapVector3_8; }
	inline Vector3Dictionary_t4020395255 ** get_address_of_mapVector3_8() { return &___mapVector3_8; }
	inline void set_mapVector3_8(Vector3Dictionary_t4020395255 * value)
	{
		___mapVector3_8 = value;
		Il2CppCodeGenWriteBarrier(&___mapVector3_8, value);
	}

	inline static int32_t get_offset_of_mapColor_9() { return static_cast<int32_t>(offsetof(PlayerPrefsEx_t1459650680, ___mapColor_9)); }
	inline ColorDictionary_t1385420584 * get_mapColor_9() const { return ___mapColor_9; }
	inline ColorDictionary_t1385420584 ** get_address_of_mapColor_9() { return &___mapColor_9; }
	inline void set_mapColor_9(ColorDictionary_t1385420584 * value)
	{
		___mapColor_9 = value;
		Il2CppCodeGenWriteBarrier(&___mapColor_9, value);
	}

	inline static int32_t get_offset_of_mapObject_10() { return static_cast<int32_t>(offsetof(PlayerPrefsEx_t1459650680, ___mapObject_10)); }
	inline ObjectDictionary_t1252187966 * get_mapObject_10() const { return ___mapObject_10; }
	inline ObjectDictionary_t1252187966 ** get_address_of_mapObject_10() { return &___mapObject_10; }
	inline void set_mapObject_10(ObjectDictionary_t1252187966 * value)
	{
		___mapObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___mapObject_10, value);
	}

	inline static int32_t get_offset_of_parent_11() { return static_cast<int32_t>(offsetof(PlayerPrefsEx_t1459650680, ___parent_11)); }
	inline PlayerPrefsEx_t1459650680 * get_parent_11() const { return ___parent_11; }
	inline PlayerPrefsEx_t1459650680 ** get_address_of_parent_11() { return &___parent_11; }
	inline void set_parent_11(PlayerPrefsEx_t1459650680 * value)
	{
		___parent_11 = value;
		Il2CppCodeGenWriteBarrier(&___parent_11, value);
	}
};

struct PlayerPrefsEx_t1459650680_StaticFields
{
public:
	// System.Boolean PlayerPrefsEx::s_MainCached
	bool ___s_MainCached_2;
	// PlayerPrefsEx PlayerPrefsEx::s_Main
	PlayerPrefsEx_t1459650680 * ___s_Main_3;

public:
	inline static int32_t get_offset_of_s_MainCached_2() { return static_cast<int32_t>(offsetof(PlayerPrefsEx_t1459650680_StaticFields, ___s_MainCached_2)); }
	inline bool get_s_MainCached_2() const { return ___s_MainCached_2; }
	inline bool* get_address_of_s_MainCached_2() { return &___s_MainCached_2; }
	inline void set_s_MainCached_2(bool value)
	{
		___s_MainCached_2 = value;
	}

	inline static int32_t get_offset_of_s_Main_3() { return static_cast<int32_t>(offsetof(PlayerPrefsEx_t1459650680_StaticFields, ___s_Main_3)); }
	inline PlayerPrefsEx_t1459650680 * get_s_Main_3() const { return ___s_Main_3; }
	inline PlayerPrefsEx_t1459650680 ** get_address_of_s_Main_3() { return &___s_Main_3; }
	inline void set_s_Main_3(PlayerPrefsEx_t1459650680 * value)
	{
		___s_Main_3 = value;
		Il2CppCodeGenWriteBarrier(&___s_Main_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
