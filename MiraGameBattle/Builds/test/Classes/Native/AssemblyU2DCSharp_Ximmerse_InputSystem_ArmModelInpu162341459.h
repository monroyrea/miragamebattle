﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Ximmerse.InputSystem.ArmModel[]
struct ArmModelU5BU5D_t3154266134;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.InputSystem.ArmModelInput
struct  ArmModelInput_t162341459  : public MonoBehaviour_t1158329972
{
public:
	// Ximmerse.InputSystem.ArmModel[] Ximmerse.InputSystem.ArmModelInput::m_Controllers
	ArmModelU5BU5D_t3154266134* ___m_Controllers_2;

public:
	inline static int32_t get_offset_of_m_Controllers_2() { return static_cast<int32_t>(offsetof(ArmModelInput_t162341459, ___m_Controllers_2)); }
	inline ArmModelU5BU5D_t3154266134* get_m_Controllers_2() const { return ___m_Controllers_2; }
	inline ArmModelU5BU5D_t3154266134** get_address_of_m_Controllers_2() { return &___m_Controllers_2; }
	inline void set_m_Controllers_2(ArmModelU5BU5D_t3154266134* value)
	{
		___m_Controllers_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Controllers_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
