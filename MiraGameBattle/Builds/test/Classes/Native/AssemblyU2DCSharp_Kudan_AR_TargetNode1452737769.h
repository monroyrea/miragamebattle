﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Kudan.AR.TrackingMethodMarkerless
struct TrackingMethodMarkerless_t432022491;
// Kudan.AR.KudanTracker
struct KudanTracker_t2134143885;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kudan.AR.TargetNode
struct  TargetNode_t1452737769  : public MonoBehaviour_t1158329972
{
public:
	// Kudan.AR.TrackingMethodMarkerless Kudan.AR.TargetNode::markerless
	TrackingMethodMarkerless_t432022491 * ___markerless_2;
	// Kudan.AR.KudanTracker Kudan.AR.TargetNode::tracker
	KudanTracker_t2134143885 * ___tracker_3;
	// UnityEngine.GameObject Kudan.AR.TargetNode::activeObj
	GameObject_t1756533147 * ___activeObj_4;
	// UnityEngine.GameObject Kudan.AR.TargetNode::target
	GameObject_t1756533147 * ___target_5;

public:
	inline static int32_t get_offset_of_markerless_2() { return static_cast<int32_t>(offsetof(TargetNode_t1452737769, ___markerless_2)); }
	inline TrackingMethodMarkerless_t432022491 * get_markerless_2() const { return ___markerless_2; }
	inline TrackingMethodMarkerless_t432022491 ** get_address_of_markerless_2() { return &___markerless_2; }
	inline void set_markerless_2(TrackingMethodMarkerless_t432022491 * value)
	{
		___markerless_2 = value;
		Il2CppCodeGenWriteBarrier(&___markerless_2, value);
	}

	inline static int32_t get_offset_of_tracker_3() { return static_cast<int32_t>(offsetof(TargetNode_t1452737769, ___tracker_3)); }
	inline KudanTracker_t2134143885 * get_tracker_3() const { return ___tracker_3; }
	inline KudanTracker_t2134143885 ** get_address_of_tracker_3() { return &___tracker_3; }
	inline void set_tracker_3(KudanTracker_t2134143885 * value)
	{
		___tracker_3 = value;
		Il2CppCodeGenWriteBarrier(&___tracker_3, value);
	}

	inline static int32_t get_offset_of_activeObj_4() { return static_cast<int32_t>(offsetof(TargetNode_t1452737769, ___activeObj_4)); }
	inline GameObject_t1756533147 * get_activeObj_4() const { return ___activeObj_4; }
	inline GameObject_t1756533147 ** get_address_of_activeObj_4() { return &___activeObj_4; }
	inline void set_activeObj_4(GameObject_t1756533147 * value)
	{
		___activeObj_4 = value;
		Il2CppCodeGenWriteBarrier(&___activeObj_4, value);
	}

	inline static int32_t get_offset_of_target_5() { return static_cast<int32_t>(offsetof(TargetNode_t1452737769, ___target_5)); }
	inline GameObject_t1756533147 * get_target_5() const { return ___target_5; }
	inline GameObject_t1756533147 ** get_address_of_target_5() { return &___target_5; }
	inline void set_target_5(GameObject_t1756533147 * value)
	{
		___target_5 = value;
		Il2CppCodeGenWriteBarrier(&___target_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
