﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// UnityEngine.Object
struct Object_t1021602117;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HapticsLibrary/HapticsDesc
struct  HapticsDesc_t3497052495  : public Il2CppObject
{
public:
	// System.String HapticsLibrary/HapticsDesc::key
	String_t* ___key_0;
	// System.Int32 HapticsLibrary/HapticsDesc::intValue0
	int32_t ___intValue0_1;
	// System.Int32 HapticsLibrary/HapticsDesc::intValue1
	int32_t ___intValue1_2;
	// UnityEngine.Object HapticsLibrary/HapticsDesc::objValue
	Object_t1021602117 * ___objValue_3;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(HapticsDesc_t3497052495, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_intValue0_1() { return static_cast<int32_t>(offsetof(HapticsDesc_t3497052495, ___intValue0_1)); }
	inline int32_t get_intValue0_1() const { return ___intValue0_1; }
	inline int32_t* get_address_of_intValue0_1() { return &___intValue0_1; }
	inline void set_intValue0_1(int32_t value)
	{
		___intValue0_1 = value;
	}

	inline static int32_t get_offset_of_intValue1_2() { return static_cast<int32_t>(offsetof(HapticsDesc_t3497052495, ___intValue1_2)); }
	inline int32_t get_intValue1_2() const { return ___intValue1_2; }
	inline int32_t* get_address_of_intValue1_2() { return &___intValue1_2; }
	inline void set_intValue1_2(int32_t value)
	{
		___intValue1_2 = value;
	}

	inline static int32_t get_offset_of_objValue_3() { return static_cast<int32_t>(offsetof(HapticsDesc_t3497052495, ___objValue_3)); }
	inline Object_t1021602117 * get_objValue_3() const { return ___objValue_3; }
	inline Object_t1021602117 ** get_address_of_objValue_3() { return &___objValue_3; }
	inline void set_objValue_3(Object_t1021602117 * value)
	{
		___objValue_3 = value;
		Il2CppCodeGenWriteBarrier(&___objValue_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
