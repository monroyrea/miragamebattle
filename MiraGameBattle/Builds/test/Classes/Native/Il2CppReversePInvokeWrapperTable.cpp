﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

struct ControllerState_t472304880_marshaled_pinvoke;




extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_VRContext_OnHmdUpdate_m3833304590(int32_t ___which0, ControllerState_t472304880_marshaled_pinvoke* ___state1);
extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_VRContext_OnHmdMessage_m1164292673(int32_t ___which0, int32_t ___Msg1, int32_t ___wParam2, int32_t ___lParam3);
extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[2] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_VRContext_OnHmdUpdate_m3833304590),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_VRContext_OnHmdMessage_m1164292673),
};
