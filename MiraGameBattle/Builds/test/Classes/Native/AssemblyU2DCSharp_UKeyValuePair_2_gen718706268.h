﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Ximmerse_VR_VRNode1238762130.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UKeyValuePair`2<Ximmerse.VR.VRNode,UnityEngine.Transform>
struct  UKeyValuePair_2_t718706268  : public Il2CppObject
{
public:
	// TKey UKeyValuePair`2::key
	int32_t ___key_0;
	// TValue UKeyValuePair`2::value
	Transform_t3275118058 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(UKeyValuePair_2_t718706268, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(UKeyValuePair_2_t718706268, ___value_1)); }
	inline Transform_t3275118058 * get_value_1() const { return ___value_1; }
	inline Transform_t3275118058 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Transform_t3275118058 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
