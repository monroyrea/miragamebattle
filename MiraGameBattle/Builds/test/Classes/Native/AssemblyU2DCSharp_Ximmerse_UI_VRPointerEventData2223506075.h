﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// Ximmerse.InputSystem.ControllerInput
struct ControllerInput_t1382602808;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.UI.VRPointerEventData
struct  VRPointerEventData_t2223506075  : public PointerEventData_t1599784723
{
public:
	// Ximmerse.InputSystem.ControllerInput Ximmerse.UI.VRPointerEventData::controller
	ControllerInput_t1382602808 * ___controller_23;

public:
	inline static int32_t get_offset_of_controller_23() { return static_cast<int32_t>(offsetof(VRPointerEventData_t2223506075, ___controller_23)); }
	inline ControllerInput_t1382602808 * get_controller_23() const { return ___controller_23; }
	inline ControllerInput_t1382602808 ** get_address_of_controller_23() { return &___controller_23; }
	inline void set_controller_23(ControllerInput_t1382602808 * value)
	{
		___controller_23 = value;
		Il2CppCodeGenWriteBarrier(&___controller_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
