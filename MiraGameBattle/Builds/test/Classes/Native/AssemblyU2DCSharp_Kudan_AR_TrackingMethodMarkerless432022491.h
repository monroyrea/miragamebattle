﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Kudan_AR_TrackingMethodBase2810511357.h"

// Kudan.AR.MarkerUpdateEvent
struct MarkerUpdateEvent_t2836667359;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kudan.AR.TrackingMethodMarkerless
struct  TrackingMethodMarkerless_t432022491  : public TrackingMethodBase_t2810511357
{
public:
	// Kudan.AR.MarkerUpdateEvent Kudan.AR.TrackingMethodMarkerless::_updateMarkerEvent
	MarkerUpdateEvent_t2836667359 * ____updateMarkerEvent_4;
	// System.Single Kudan.AR.TrackingMethodMarkerless::_floorDepth
	float ____floorDepth_5;

public:
	inline static int32_t get_offset_of__updateMarkerEvent_4() { return static_cast<int32_t>(offsetof(TrackingMethodMarkerless_t432022491, ____updateMarkerEvent_4)); }
	inline MarkerUpdateEvent_t2836667359 * get__updateMarkerEvent_4() const { return ____updateMarkerEvent_4; }
	inline MarkerUpdateEvent_t2836667359 ** get_address_of__updateMarkerEvent_4() { return &____updateMarkerEvent_4; }
	inline void set__updateMarkerEvent_4(MarkerUpdateEvent_t2836667359 * value)
	{
		____updateMarkerEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&____updateMarkerEvent_4, value);
	}

	inline static int32_t get_offset_of__floorDepth_5() { return static_cast<int32_t>(offsetof(TrackingMethodMarkerless_t432022491, ____floorDepth_5)); }
	inline float get__floorDepth_5() const { return ____floorDepth_5; }
	inline float* get_address_of__floorDepth_5() { return &____floorDepth_5; }
	inline void set__floorDepth_5(float value)
	{
		____floorDepth_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
