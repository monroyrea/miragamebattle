﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_Single2076509932.h"

// System.Single[]
struct SingleU5BU5D_t577127397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.InputSystem.XDevicePlugin/ControllerState
struct  ControllerState_t472304880 
{
public:
	// System.Int32 Ximmerse.InputSystem.XDevicePlugin/ControllerState::handle
	int32_t ___handle_0;
	// System.Int32 Ximmerse.InputSystem.XDevicePlugin/ControllerState::timestamp
	int32_t ___timestamp_1;
	// System.Int32 Ximmerse.InputSystem.XDevicePlugin/ControllerState::state_mask
	int32_t ___state_mask_2;
	// System.Single[] Ximmerse.InputSystem.XDevicePlugin/ControllerState::axes
	SingleU5BU5D_t577127397* ___axes_3;
	// System.UInt32 Ximmerse.InputSystem.XDevicePlugin/ControllerState::buttons
	uint32_t ___buttons_4;
	// System.Single[] Ximmerse.InputSystem.XDevicePlugin/ControllerState::position
	SingleU5BU5D_t577127397* ___position_5;
	// System.Single[] Ximmerse.InputSystem.XDevicePlugin/ControllerState::rotation
	SingleU5BU5D_t577127397* ___rotation_6;
	// System.Single[] Ximmerse.InputSystem.XDevicePlugin/ControllerState::accelerometer
	SingleU5BU5D_t577127397* ___accelerometer_7;
	// System.Single[] Ximmerse.InputSystem.XDevicePlugin/ControllerState::gyroscope
	SingleU5BU5D_t577127397* ___gyroscope_8;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(ControllerState_t472304880, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}

	inline static int32_t get_offset_of_timestamp_1() { return static_cast<int32_t>(offsetof(ControllerState_t472304880, ___timestamp_1)); }
	inline int32_t get_timestamp_1() const { return ___timestamp_1; }
	inline int32_t* get_address_of_timestamp_1() { return &___timestamp_1; }
	inline void set_timestamp_1(int32_t value)
	{
		___timestamp_1 = value;
	}

	inline static int32_t get_offset_of_state_mask_2() { return static_cast<int32_t>(offsetof(ControllerState_t472304880, ___state_mask_2)); }
	inline int32_t get_state_mask_2() const { return ___state_mask_2; }
	inline int32_t* get_address_of_state_mask_2() { return &___state_mask_2; }
	inline void set_state_mask_2(int32_t value)
	{
		___state_mask_2 = value;
	}

	inline static int32_t get_offset_of_axes_3() { return static_cast<int32_t>(offsetof(ControllerState_t472304880, ___axes_3)); }
	inline SingleU5BU5D_t577127397* get_axes_3() const { return ___axes_3; }
	inline SingleU5BU5D_t577127397** get_address_of_axes_3() { return &___axes_3; }
	inline void set_axes_3(SingleU5BU5D_t577127397* value)
	{
		___axes_3 = value;
		Il2CppCodeGenWriteBarrier(&___axes_3, value);
	}

	inline static int32_t get_offset_of_buttons_4() { return static_cast<int32_t>(offsetof(ControllerState_t472304880, ___buttons_4)); }
	inline uint32_t get_buttons_4() const { return ___buttons_4; }
	inline uint32_t* get_address_of_buttons_4() { return &___buttons_4; }
	inline void set_buttons_4(uint32_t value)
	{
		___buttons_4 = value;
	}

	inline static int32_t get_offset_of_position_5() { return static_cast<int32_t>(offsetof(ControllerState_t472304880, ___position_5)); }
	inline SingleU5BU5D_t577127397* get_position_5() const { return ___position_5; }
	inline SingleU5BU5D_t577127397** get_address_of_position_5() { return &___position_5; }
	inline void set_position_5(SingleU5BU5D_t577127397* value)
	{
		___position_5 = value;
		Il2CppCodeGenWriteBarrier(&___position_5, value);
	}

	inline static int32_t get_offset_of_rotation_6() { return static_cast<int32_t>(offsetof(ControllerState_t472304880, ___rotation_6)); }
	inline SingleU5BU5D_t577127397* get_rotation_6() const { return ___rotation_6; }
	inline SingleU5BU5D_t577127397** get_address_of_rotation_6() { return &___rotation_6; }
	inline void set_rotation_6(SingleU5BU5D_t577127397* value)
	{
		___rotation_6 = value;
		Il2CppCodeGenWriteBarrier(&___rotation_6, value);
	}

	inline static int32_t get_offset_of_accelerometer_7() { return static_cast<int32_t>(offsetof(ControllerState_t472304880, ___accelerometer_7)); }
	inline SingleU5BU5D_t577127397* get_accelerometer_7() const { return ___accelerometer_7; }
	inline SingleU5BU5D_t577127397** get_address_of_accelerometer_7() { return &___accelerometer_7; }
	inline void set_accelerometer_7(SingleU5BU5D_t577127397* value)
	{
		___accelerometer_7 = value;
		Il2CppCodeGenWriteBarrier(&___accelerometer_7, value);
	}

	inline static int32_t get_offset_of_gyroscope_8() { return static_cast<int32_t>(offsetof(ControllerState_t472304880, ___gyroscope_8)); }
	inline SingleU5BU5D_t577127397* get_gyroscope_8() const { return ___gyroscope_8; }
	inline SingleU5BU5D_t577127397** get_address_of_gyroscope_8() { return &___gyroscope_8; }
	inline void set_gyroscope_8(SingleU5BU5D_t577127397* value)
	{
		___gyroscope_8 = value;
		Il2CppCodeGenWriteBarrier(&___gyroscope_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Ximmerse.InputSystem.XDevicePlugin/ControllerState
struct ControllerState_t472304880_marshaled_pinvoke
{
	int32_t ___handle_0;
	int32_t ___timestamp_1;
	int32_t ___state_mask_2;
	float ___axes_3[6];
	uint32_t ___buttons_4;
	float ___position_5[3];
	float ___rotation_6[4];
	float ___accelerometer_7[3];
	float ___gyroscope_8[3];
};
// Native definition for COM marshalling of Ximmerse.InputSystem.XDevicePlugin/ControllerState
struct ControllerState_t472304880_marshaled_com
{
	int32_t ___handle_0;
	int32_t ___timestamp_1;
	int32_t ___state_mask_2;
	float ___axes_3[6];
	uint32_t ___buttons_4;
	float ___position_5[3];
	float ___rotation_6[4];
	float ___accelerometer_7[3];
	float ___gyroscope_8[3];
};
