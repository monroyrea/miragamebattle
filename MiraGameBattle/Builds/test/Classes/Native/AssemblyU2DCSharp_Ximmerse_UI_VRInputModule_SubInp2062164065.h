﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerTy123719274.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerB2189630288.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// Ximmerse.InputSystem.ControllerInput
struct ControllerInput_t1382602808;
// System.String
struct String_t;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// Ximmerse.UI.VRInputModule
struct VRInputModule_t523376602;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// Ximmerse.UI.VRPointerEventData
struct VRPointerEventData_t2223506075;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.UI.VRInputModule/SubInputModule
struct  SubInputModule_t2062164065  : public Il2CppObject
{
public:
	// Ximmerse.InputSystem.ControllerType Ximmerse.UI.VRInputModule/SubInputModule::controller
	int32_t ___controller_0;
	// Ximmerse.InputSystem.ControllerInput Ximmerse.UI.VRInputModule/SubInputModule::m_Controller
	ControllerInput_t1382602808 * ___m_Controller_1;
	// System.String Ximmerse.UI.VRInputModule/SubInputModule::m_ControllerName
	String_t* ___m_ControllerName_2;
	// System.Single Ximmerse.UI.VRInputModule/SubInputModule::clickTime
	float ___clickTime_3;
	// Ximmerse.InputSystem.ControllerButton Ximmerse.UI.VRInputModule/SubInputModule::buttonClick
	int32_t ___buttonClick_4;
	// UnityEngine.EventSystems.EventSystem Ximmerse.UI.VRInputModule/SubInputModule::eventSystem
	EventSystem_t3466835263 * ___eventSystem_5;
	// Ximmerse.UI.VRInputModule Ximmerse.UI.VRInputModule/SubInputModule::main
	VRInputModule_t523376602 * ___main_6;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> Ximmerse.UI.VRInputModule/SubInputModule::m_RaycastResultCache
	List_1_t3685274804 * ___m_RaycastResultCache_7;
	// UnityEngine.Vector2 Ximmerse.UI.VRInputModule/SubInputModule::hotspot
	Vector2_t2243707579  ___hotspot_8;
	// Ximmerse.UI.VRPointerEventData Ximmerse.UI.VRInputModule/SubInputModule::pointerData
	VRPointerEventData_t2223506075 * ___pointerData_9;

public:
	inline static int32_t get_offset_of_controller_0() { return static_cast<int32_t>(offsetof(SubInputModule_t2062164065, ___controller_0)); }
	inline int32_t get_controller_0() const { return ___controller_0; }
	inline int32_t* get_address_of_controller_0() { return &___controller_0; }
	inline void set_controller_0(int32_t value)
	{
		___controller_0 = value;
	}

	inline static int32_t get_offset_of_m_Controller_1() { return static_cast<int32_t>(offsetof(SubInputModule_t2062164065, ___m_Controller_1)); }
	inline ControllerInput_t1382602808 * get_m_Controller_1() const { return ___m_Controller_1; }
	inline ControllerInput_t1382602808 ** get_address_of_m_Controller_1() { return &___m_Controller_1; }
	inline void set_m_Controller_1(ControllerInput_t1382602808 * value)
	{
		___m_Controller_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Controller_1, value);
	}

	inline static int32_t get_offset_of_m_ControllerName_2() { return static_cast<int32_t>(offsetof(SubInputModule_t2062164065, ___m_ControllerName_2)); }
	inline String_t* get_m_ControllerName_2() const { return ___m_ControllerName_2; }
	inline String_t** get_address_of_m_ControllerName_2() { return &___m_ControllerName_2; }
	inline void set_m_ControllerName_2(String_t* value)
	{
		___m_ControllerName_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_ControllerName_2, value);
	}

	inline static int32_t get_offset_of_clickTime_3() { return static_cast<int32_t>(offsetof(SubInputModule_t2062164065, ___clickTime_3)); }
	inline float get_clickTime_3() const { return ___clickTime_3; }
	inline float* get_address_of_clickTime_3() { return &___clickTime_3; }
	inline void set_clickTime_3(float value)
	{
		___clickTime_3 = value;
	}

	inline static int32_t get_offset_of_buttonClick_4() { return static_cast<int32_t>(offsetof(SubInputModule_t2062164065, ___buttonClick_4)); }
	inline int32_t get_buttonClick_4() const { return ___buttonClick_4; }
	inline int32_t* get_address_of_buttonClick_4() { return &___buttonClick_4; }
	inline void set_buttonClick_4(int32_t value)
	{
		___buttonClick_4 = value;
	}

	inline static int32_t get_offset_of_eventSystem_5() { return static_cast<int32_t>(offsetof(SubInputModule_t2062164065, ___eventSystem_5)); }
	inline EventSystem_t3466835263 * get_eventSystem_5() const { return ___eventSystem_5; }
	inline EventSystem_t3466835263 ** get_address_of_eventSystem_5() { return &___eventSystem_5; }
	inline void set_eventSystem_5(EventSystem_t3466835263 * value)
	{
		___eventSystem_5 = value;
		Il2CppCodeGenWriteBarrier(&___eventSystem_5, value);
	}

	inline static int32_t get_offset_of_main_6() { return static_cast<int32_t>(offsetof(SubInputModule_t2062164065, ___main_6)); }
	inline VRInputModule_t523376602 * get_main_6() const { return ___main_6; }
	inline VRInputModule_t523376602 ** get_address_of_main_6() { return &___main_6; }
	inline void set_main_6(VRInputModule_t523376602 * value)
	{
		___main_6 = value;
		Il2CppCodeGenWriteBarrier(&___main_6, value);
	}

	inline static int32_t get_offset_of_m_RaycastResultCache_7() { return static_cast<int32_t>(offsetof(SubInputModule_t2062164065, ___m_RaycastResultCache_7)); }
	inline List_1_t3685274804 * get_m_RaycastResultCache_7() const { return ___m_RaycastResultCache_7; }
	inline List_1_t3685274804 ** get_address_of_m_RaycastResultCache_7() { return &___m_RaycastResultCache_7; }
	inline void set_m_RaycastResultCache_7(List_1_t3685274804 * value)
	{
		___m_RaycastResultCache_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_RaycastResultCache_7, value);
	}

	inline static int32_t get_offset_of_hotspot_8() { return static_cast<int32_t>(offsetof(SubInputModule_t2062164065, ___hotspot_8)); }
	inline Vector2_t2243707579  get_hotspot_8() const { return ___hotspot_8; }
	inline Vector2_t2243707579 * get_address_of_hotspot_8() { return &___hotspot_8; }
	inline void set_hotspot_8(Vector2_t2243707579  value)
	{
		___hotspot_8 = value;
	}

	inline static int32_t get_offset_of_pointerData_9() { return static_cast<int32_t>(offsetof(SubInputModule_t2062164065, ___pointerData_9)); }
	inline VRPointerEventData_t2223506075 * get_pointerData_9() const { return ___pointerData_9; }
	inline VRPointerEventData_t2223506075 ** get_address_of_pointerData_9() { return &___pointerData_9; }
	inline void set_pointerData_9(VRPointerEventData_t2223506075 * value)
	{
		___pointerData_9 = value;
		Il2CppCodeGenWriteBarrier(&___pointerData_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
