﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_TrackedObject16303305.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackedHead
struct  TrackedHead_t623849920  : public TrackedObject_t16303305
{
public:
	// UnityEngine.Transform TrackedHead::eyeContainer
	Transform_t3275118058 * ___eyeContainer_11;
	// UnityEngine.Transform TrackedHead::markTransform
	Transform_t3275118058 * ___markTransform_12;
	// UnityEngine.GameObject[] TrackedHead::m_Gizmos
	GameObjectU5BU5D_t3057952154* ___m_Gizmos_13;

public:
	inline static int32_t get_offset_of_eyeContainer_11() { return static_cast<int32_t>(offsetof(TrackedHead_t623849920, ___eyeContainer_11)); }
	inline Transform_t3275118058 * get_eyeContainer_11() const { return ___eyeContainer_11; }
	inline Transform_t3275118058 ** get_address_of_eyeContainer_11() { return &___eyeContainer_11; }
	inline void set_eyeContainer_11(Transform_t3275118058 * value)
	{
		___eyeContainer_11 = value;
		Il2CppCodeGenWriteBarrier(&___eyeContainer_11, value);
	}

	inline static int32_t get_offset_of_markTransform_12() { return static_cast<int32_t>(offsetof(TrackedHead_t623849920, ___markTransform_12)); }
	inline Transform_t3275118058 * get_markTransform_12() const { return ___markTransform_12; }
	inline Transform_t3275118058 ** get_address_of_markTransform_12() { return &___markTransform_12; }
	inline void set_markTransform_12(Transform_t3275118058 * value)
	{
		___markTransform_12 = value;
		Il2CppCodeGenWriteBarrier(&___markTransform_12, value);
	}

	inline static int32_t get_offset_of_m_Gizmos_13() { return static_cast<int32_t>(offsetof(TrackedHead_t623849920, ___m_Gizmos_13)); }
	inline GameObjectU5BU5D_t3057952154* get_m_Gizmos_13() const { return ___m_Gizmos_13; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_Gizmos_13() { return &___m_Gizmos_13; }
	inline void set_m_Gizmos_13(GameObjectU5BU5D_t3057952154* value)
	{
		___m_Gizmos_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_Gizmos_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
