﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerTy123719274.h"

// System.String[]
struct StringU5BU5D_t1642385972;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.InputSystem.ControllerInputManager/ControllerInfo
struct  ControllerInfo_t3037329813  : public Il2CppObject
{
public:
	// System.String Ximmerse.InputSystem.ControllerInputManager/ControllerInfo::name
	String_t* ___name_2;
	// Ximmerse.InputSystem.ControllerType Ximmerse.InputSystem.ControllerInputManager/ControllerInfo::type
	int32_t ___type_3;
	// System.Int32 Ximmerse.InputSystem.ControllerInputManager/ControllerInfo::priority
	int32_t ___priority_4;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(ControllerInfo_t3037329813, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ControllerInfo_t3037329813, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_priority_4() { return static_cast<int32_t>(offsetof(ControllerInfo_t3037329813, ___priority_4)); }
	inline int32_t get_priority_4() const { return ___priority_4; }
	inline int32_t* get_address_of_priority_4() { return &___priority_4; }
	inline void set_priority_4(int32_t value)
	{
		___priority_4 = value;
	}
};

struct ControllerInfo_t3037329813_StaticFields
{
public:
	// System.String[] Ximmerse.InputSystem.ControllerInputManager/ControllerInfo::SPLIT_NEW_LINE
	StringU5BU5D_t1642385972* ___SPLIT_NEW_LINE_0;
	// System.Char[] Ximmerse.InputSystem.ControllerInputManager/ControllerInfo::SPLIT_CSV
	CharU5BU5D_t1328083999* ___SPLIT_CSV_1;

public:
	inline static int32_t get_offset_of_SPLIT_NEW_LINE_0() { return static_cast<int32_t>(offsetof(ControllerInfo_t3037329813_StaticFields, ___SPLIT_NEW_LINE_0)); }
	inline StringU5BU5D_t1642385972* get_SPLIT_NEW_LINE_0() const { return ___SPLIT_NEW_LINE_0; }
	inline StringU5BU5D_t1642385972** get_address_of_SPLIT_NEW_LINE_0() { return &___SPLIT_NEW_LINE_0; }
	inline void set_SPLIT_NEW_LINE_0(StringU5BU5D_t1642385972* value)
	{
		___SPLIT_NEW_LINE_0 = value;
		Il2CppCodeGenWriteBarrier(&___SPLIT_NEW_LINE_0, value);
	}

	inline static int32_t get_offset_of_SPLIT_CSV_1() { return static_cast<int32_t>(offsetof(ControllerInfo_t3037329813_StaticFields, ___SPLIT_CSV_1)); }
	inline CharU5BU5D_t1328083999* get_SPLIT_CSV_1() const { return ___SPLIT_CSV_1; }
	inline CharU5BU5D_t1328083999** get_address_of_SPLIT_CSV_1() { return &___SPLIT_CSV_1; }
	inline void set_SPLIT_CSV_1(CharU5BU5D_t1328083999* value)
	{
		___SPLIT_CSV_1 = value;
		Il2CppCodeGenWriteBarrier(&___SPLIT_CSV_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
