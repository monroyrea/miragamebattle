﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Ximmerse.InputSystem.InputManager
struct InputManager_t3388780835;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t3035069757;
// Ximmerse.InputSystem.IInputModule[]
struct IInputModuleU5BU5D_t2105656152;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.InputSystem.InputManager
struct  InputManager_t3388780835  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.MonoBehaviour[] Ximmerse.InputSystem.InputManager::monoInputModules
	MonoBehaviourU5BU5D_t3035069757* ___monoInputModules_4;
	// Ximmerse.InputSystem.IInputModule[] Ximmerse.InputSystem.InputManager::inputModules
	IInputModuleU5BU5D_t2105656152* ___inputModules_5;

public:
	inline static int32_t get_offset_of_monoInputModules_4() { return static_cast<int32_t>(offsetof(InputManager_t3388780835, ___monoInputModules_4)); }
	inline MonoBehaviourU5BU5D_t3035069757* get_monoInputModules_4() const { return ___monoInputModules_4; }
	inline MonoBehaviourU5BU5D_t3035069757** get_address_of_monoInputModules_4() { return &___monoInputModules_4; }
	inline void set_monoInputModules_4(MonoBehaviourU5BU5D_t3035069757* value)
	{
		___monoInputModules_4 = value;
		Il2CppCodeGenWriteBarrier(&___monoInputModules_4, value);
	}

	inline static int32_t get_offset_of_inputModules_5() { return static_cast<int32_t>(offsetof(InputManager_t3388780835, ___inputModules_5)); }
	inline IInputModuleU5BU5D_t2105656152* get_inputModules_5() const { return ___inputModules_5; }
	inline IInputModuleU5BU5D_t2105656152** get_address_of_inputModules_5() { return &___inputModules_5; }
	inline void set_inputModules_5(IInputModuleU5BU5D_t2105656152* value)
	{
		___inputModules_5 = value;
		Il2CppCodeGenWriteBarrier(&___inputModules_5, value);
	}
};

struct InputManager_t3388780835_StaticFields
{
public:
	// Ximmerse.InputSystem.InputManager Ximmerse.InputSystem.InputManager::s_Instance
	InputManager_t3388780835 * ___s_Instance_2;
	// System.Boolean Ximmerse.InputSystem.InputManager::s_InstanceCached
	bool ___s_InstanceCached_3;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(InputManager_t3388780835_StaticFields, ___s_Instance_2)); }
	inline InputManager_t3388780835 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline InputManager_t3388780835 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(InputManager_t3388780835 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___s_Instance_2, value);
	}

	inline static int32_t get_offset_of_s_InstanceCached_3() { return static_cast<int32_t>(offsetof(InputManager_t3388780835_StaticFields, ___s_InstanceCached_3)); }
	inline bool get_s_InstanceCached_3() const { return ___s_InstanceCached_3; }
	inline bool* get_address_of_s_InstanceCached_3() { return &___s_InstanceCached_3; }
	inline void set_s_InstanceCached_3(bool value)
	{
		___s_InstanceCached_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
