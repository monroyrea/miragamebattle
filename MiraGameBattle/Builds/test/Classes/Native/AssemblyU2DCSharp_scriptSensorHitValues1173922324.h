﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// scriptSensorHitValues
struct  scriptSensorHitValues_t1173922324  : public MonoBehaviour_t1158329972
{
public:
	// System.Single scriptSensorHitValues::_hitPower
	float ____hitPower_2;
	// System.Single scriptSensorHitValues::_speed
	float ____speed_3;

public:
	inline static int32_t get_offset_of__hitPower_2() { return static_cast<int32_t>(offsetof(scriptSensorHitValues_t1173922324, ____hitPower_2)); }
	inline float get__hitPower_2() const { return ____hitPower_2; }
	inline float* get_address_of__hitPower_2() { return &____hitPower_2; }
	inline void set__hitPower_2(float value)
	{
		____hitPower_2 = value;
	}

	inline static int32_t get_offset_of__speed_3() { return static_cast<int32_t>(offsetof(scriptSensorHitValues_t1173922324, ____speed_3)); }
	inline float get__speed_3() const { return ____speed_3; }
	inline float* get_address_of__speed_3() { return &____speed_3; }
	inline void set__speed_3(float value)
	{
		____speed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
