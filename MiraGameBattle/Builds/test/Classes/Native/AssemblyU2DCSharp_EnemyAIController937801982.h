﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Animator
struct Animator_t69676727;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyAIController
struct  EnemyAIController_t937801982  : public MonoBehaviour_t1158329972
{
public:
	// System.Single EnemyAIController::speed
	float ___speed_7;
	// UnityEngine.GameObject EnemyAIController::player
	GameObject_t1756533147 * ___player_8;
	// UnityEngine.GameObject EnemyAIController::leftLimit
	GameObject_t1756533147 * ___leftLimit_9;
	// UnityEngine.GameObject EnemyAIController::rightLimit
	GameObject_t1756533147 * ___rightLimit_10;
	// System.Boolean EnemyAIController::rotate
	bool ___rotate_11;
	// System.Single EnemyAIController::walkSpeed
	float ___walkSpeed_12;
	// UnityEngine.Animator EnemyAIController::anim
	Animator_t69676727 * ___anim_13;
	// UnityEngine.Rigidbody EnemyAIController::rb
	Rigidbody_t4233889191 * ___rb_14;
	// System.Int32 EnemyAIController::a
	int32_t ___a_15;
	// System.Int32 EnemyAIController::startAI
	int32_t ___startAI_16;
	// System.Single EnemyAIController::delay
	float ___delay_17;
	// System.Single EnemyAIController::nextUsage
	float ___nextUsage_18;

public:
	inline static int32_t get_offset_of_speed_7() { return static_cast<int32_t>(offsetof(EnemyAIController_t937801982, ___speed_7)); }
	inline float get_speed_7() const { return ___speed_7; }
	inline float* get_address_of_speed_7() { return &___speed_7; }
	inline void set_speed_7(float value)
	{
		___speed_7 = value;
	}

	inline static int32_t get_offset_of_player_8() { return static_cast<int32_t>(offsetof(EnemyAIController_t937801982, ___player_8)); }
	inline GameObject_t1756533147 * get_player_8() const { return ___player_8; }
	inline GameObject_t1756533147 ** get_address_of_player_8() { return &___player_8; }
	inline void set_player_8(GameObject_t1756533147 * value)
	{
		___player_8 = value;
		Il2CppCodeGenWriteBarrier(&___player_8, value);
	}

	inline static int32_t get_offset_of_leftLimit_9() { return static_cast<int32_t>(offsetof(EnemyAIController_t937801982, ___leftLimit_9)); }
	inline GameObject_t1756533147 * get_leftLimit_9() const { return ___leftLimit_9; }
	inline GameObject_t1756533147 ** get_address_of_leftLimit_9() { return &___leftLimit_9; }
	inline void set_leftLimit_9(GameObject_t1756533147 * value)
	{
		___leftLimit_9 = value;
		Il2CppCodeGenWriteBarrier(&___leftLimit_9, value);
	}

	inline static int32_t get_offset_of_rightLimit_10() { return static_cast<int32_t>(offsetof(EnemyAIController_t937801982, ___rightLimit_10)); }
	inline GameObject_t1756533147 * get_rightLimit_10() const { return ___rightLimit_10; }
	inline GameObject_t1756533147 ** get_address_of_rightLimit_10() { return &___rightLimit_10; }
	inline void set_rightLimit_10(GameObject_t1756533147 * value)
	{
		___rightLimit_10 = value;
		Il2CppCodeGenWriteBarrier(&___rightLimit_10, value);
	}

	inline static int32_t get_offset_of_rotate_11() { return static_cast<int32_t>(offsetof(EnemyAIController_t937801982, ___rotate_11)); }
	inline bool get_rotate_11() const { return ___rotate_11; }
	inline bool* get_address_of_rotate_11() { return &___rotate_11; }
	inline void set_rotate_11(bool value)
	{
		___rotate_11 = value;
	}

	inline static int32_t get_offset_of_walkSpeed_12() { return static_cast<int32_t>(offsetof(EnemyAIController_t937801982, ___walkSpeed_12)); }
	inline float get_walkSpeed_12() const { return ___walkSpeed_12; }
	inline float* get_address_of_walkSpeed_12() { return &___walkSpeed_12; }
	inline void set_walkSpeed_12(float value)
	{
		___walkSpeed_12 = value;
	}

	inline static int32_t get_offset_of_anim_13() { return static_cast<int32_t>(offsetof(EnemyAIController_t937801982, ___anim_13)); }
	inline Animator_t69676727 * get_anim_13() const { return ___anim_13; }
	inline Animator_t69676727 ** get_address_of_anim_13() { return &___anim_13; }
	inline void set_anim_13(Animator_t69676727 * value)
	{
		___anim_13 = value;
		Il2CppCodeGenWriteBarrier(&___anim_13, value);
	}

	inline static int32_t get_offset_of_rb_14() { return static_cast<int32_t>(offsetof(EnemyAIController_t937801982, ___rb_14)); }
	inline Rigidbody_t4233889191 * get_rb_14() const { return ___rb_14; }
	inline Rigidbody_t4233889191 ** get_address_of_rb_14() { return &___rb_14; }
	inline void set_rb_14(Rigidbody_t4233889191 * value)
	{
		___rb_14 = value;
		Il2CppCodeGenWriteBarrier(&___rb_14, value);
	}

	inline static int32_t get_offset_of_a_15() { return static_cast<int32_t>(offsetof(EnemyAIController_t937801982, ___a_15)); }
	inline int32_t get_a_15() const { return ___a_15; }
	inline int32_t* get_address_of_a_15() { return &___a_15; }
	inline void set_a_15(int32_t value)
	{
		___a_15 = value;
	}

	inline static int32_t get_offset_of_startAI_16() { return static_cast<int32_t>(offsetof(EnemyAIController_t937801982, ___startAI_16)); }
	inline int32_t get_startAI_16() const { return ___startAI_16; }
	inline int32_t* get_address_of_startAI_16() { return &___startAI_16; }
	inline void set_startAI_16(int32_t value)
	{
		___startAI_16 = value;
	}

	inline static int32_t get_offset_of_delay_17() { return static_cast<int32_t>(offsetof(EnemyAIController_t937801982, ___delay_17)); }
	inline float get_delay_17() const { return ___delay_17; }
	inline float* get_address_of_delay_17() { return &___delay_17; }
	inline void set_delay_17(float value)
	{
		___delay_17 = value;
	}

	inline static int32_t get_offset_of_nextUsage_18() { return static_cast<int32_t>(offsetof(EnemyAIController_t937801982, ___nextUsage_18)); }
	inline float get_nextUsage_18() const { return ___nextUsage_18; }
	inline float* get_address_of_nextUsage_18() { return &___nextUsage_18; }
	inline void set_nextUsage_18(float value)
	{
		___nextUsage_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
