﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerB2189630288.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// Ximmerse.InputSystem.ControllerInput
struct ControllerInput_t1382602808;
// GyroController
struct GyroController_t3857203917;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControllerTransform
struct  ControllerTransform_t2066672932  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean ControllerTransform::allowControllerClick
	bool ___allowControllerClick_2;
	// Ximmerse.InputSystem.ControllerButton ControllerTransform::recenterButton
	int32_t ___recenterButton_3;
	// Ximmerse.InputSystem.ControllerButton ControllerTransform::rotateToCamButton
	int32_t ___rotateToCamButton_4;
	// System.Boolean ControllerTransform::horizontalOnly
	bool ___horizontalOnly_5;
	// Ximmerse.InputSystem.ControllerInput ControllerTransform::controller
	ControllerInput_t1382602808 * ___controller_6;
	// UnityEngine.Quaternion ControllerTransform::offsetRotation
	Quaternion_t4030073918  ___offsetRotation_7;
	// System.Boolean ControllerTransform::trackPosition
	bool ___trackPosition_8;
	// GyroController ControllerTransform::gyro
	GyroController_t3857203917 * ___gyro_9;
	// System.Single ControllerTransform::frameRate
	float ___frameRate_10;
	// System.Single ControllerTransform::sceneScaleMult
	float ___sceneScaleMult_11;
	// System.Single ControllerTransform::sceneOffsetX
	float ___sceneOffsetX_12;
	// System.Single ControllerTransform::sceneOffsetY
	float ___sceneOffsetY_13;
	// System.Single ControllerTransform::sceneOffsetZ
	float ___sceneOffsetZ_14;

public:
	inline static int32_t get_offset_of_allowControllerClick_2() { return static_cast<int32_t>(offsetof(ControllerTransform_t2066672932, ___allowControllerClick_2)); }
	inline bool get_allowControllerClick_2() const { return ___allowControllerClick_2; }
	inline bool* get_address_of_allowControllerClick_2() { return &___allowControllerClick_2; }
	inline void set_allowControllerClick_2(bool value)
	{
		___allowControllerClick_2 = value;
	}

	inline static int32_t get_offset_of_recenterButton_3() { return static_cast<int32_t>(offsetof(ControllerTransform_t2066672932, ___recenterButton_3)); }
	inline int32_t get_recenterButton_3() const { return ___recenterButton_3; }
	inline int32_t* get_address_of_recenterButton_3() { return &___recenterButton_3; }
	inline void set_recenterButton_3(int32_t value)
	{
		___recenterButton_3 = value;
	}

	inline static int32_t get_offset_of_rotateToCamButton_4() { return static_cast<int32_t>(offsetof(ControllerTransform_t2066672932, ___rotateToCamButton_4)); }
	inline int32_t get_rotateToCamButton_4() const { return ___rotateToCamButton_4; }
	inline int32_t* get_address_of_rotateToCamButton_4() { return &___rotateToCamButton_4; }
	inline void set_rotateToCamButton_4(int32_t value)
	{
		___rotateToCamButton_4 = value;
	}

	inline static int32_t get_offset_of_horizontalOnly_5() { return static_cast<int32_t>(offsetof(ControllerTransform_t2066672932, ___horizontalOnly_5)); }
	inline bool get_horizontalOnly_5() const { return ___horizontalOnly_5; }
	inline bool* get_address_of_horizontalOnly_5() { return &___horizontalOnly_5; }
	inline void set_horizontalOnly_5(bool value)
	{
		___horizontalOnly_5 = value;
	}

	inline static int32_t get_offset_of_controller_6() { return static_cast<int32_t>(offsetof(ControllerTransform_t2066672932, ___controller_6)); }
	inline ControllerInput_t1382602808 * get_controller_6() const { return ___controller_6; }
	inline ControllerInput_t1382602808 ** get_address_of_controller_6() { return &___controller_6; }
	inline void set_controller_6(ControllerInput_t1382602808 * value)
	{
		___controller_6 = value;
		Il2CppCodeGenWriteBarrier(&___controller_6, value);
	}

	inline static int32_t get_offset_of_offsetRotation_7() { return static_cast<int32_t>(offsetof(ControllerTransform_t2066672932, ___offsetRotation_7)); }
	inline Quaternion_t4030073918  get_offsetRotation_7() const { return ___offsetRotation_7; }
	inline Quaternion_t4030073918 * get_address_of_offsetRotation_7() { return &___offsetRotation_7; }
	inline void set_offsetRotation_7(Quaternion_t4030073918  value)
	{
		___offsetRotation_7 = value;
	}

	inline static int32_t get_offset_of_trackPosition_8() { return static_cast<int32_t>(offsetof(ControllerTransform_t2066672932, ___trackPosition_8)); }
	inline bool get_trackPosition_8() const { return ___trackPosition_8; }
	inline bool* get_address_of_trackPosition_8() { return &___trackPosition_8; }
	inline void set_trackPosition_8(bool value)
	{
		___trackPosition_8 = value;
	}

	inline static int32_t get_offset_of_gyro_9() { return static_cast<int32_t>(offsetof(ControllerTransform_t2066672932, ___gyro_9)); }
	inline GyroController_t3857203917 * get_gyro_9() const { return ___gyro_9; }
	inline GyroController_t3857203917 ** get_address_of_gyro_9() { return &___gyro_9; }
	inline void set_gyro_9(GyroController_t3857203917 * value)
	{
		___gyro_9 = value;
		Il2CppCodeGenWriteBarrier(&___gyro_9, value);
	}

	inline static int32_t get_offset_of_frameRate_10() { return static_cast<int32_t>(offsetof(ControllerTransform_t2066672932, ___frameRate_10)); }
	inline float get_frameRate_10() const { return ___frameRate_10; }
	inline float* get_address_of_frameRate_10() { return &___frameRate_10; }
	inline void set_frameRate_10(float value)
	{
		___frameRate_10 = value;
	}

	inline static int32_t get_offset_of_sceneScaleMult_11() { return static_cast<int32_t>(offsetof(ControllerTransform_t2066672932, ___sceneScaleMult_11)); }
	inline float get_sceneScaleMult_11() const { return ___sceneScaleMult_11; }
	inline float* get_address_of_sceneScaleMult_11() { return &___sceneScaleMult_11; }
	inline void set_sceneScaleMult_11(float value)
	{
		___sceneScaleMult_11 = value;
	}

	inline static int32_t get_offset_of_sceneOffsetX_12() { return static_cast<int32_t>(offsetof(ControllerTransform_t2066672932, ___sceneOffsetX_12)); }
	inline float get_sceneOffsetX_12() const { return ___sceneOffsetX_12; }
	inline float* get_address_of_sceneOffsetX_12() { return &___sceneOffsetX_12; }
	inline void set_sceneOffsetX_12(float value)
	{
		___sceneOffsetX_12 = value;
	}

	inline static int32_t get_offset_of_sceneOffsetY_13() { return static_cast<int32_t>(offsetof(ControllerTransform_t2066672932, ___sceneOffsetY_13)); }
	inline float get_sceneOffsetY_13() const { return ___sceneOffsetY_13; }
	inline float* get_address_of_sceneOffsetY_13() { return &___sceneOffsetY_13; }
	inline void set_sceneOffsetY_13(float value)
	{
		___sceneOffsetY_13 = value;
	}

	inline static int32_t get_offset_of_sceneOffsetZ_14() { return static_cast<int32_t>(offsetof(ControllerTransform_t2066672932, ___sceneOffsetZ_14)); }
	inline float get_sceneOffsetZ_14() const { return ___sceneOffsetZ_14; }
	inline float* get_address_of_sceneOffsetZ_14() { return &___sceneOffsetZ_14; }
	inline void set_sceneOffsetZ_14(float value)
	{
		___sceneOffsetZ_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
