﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_MiraArController_MarkerState2127725244.h"

// System.String
struct String_t;
// Kudan.AR.TrackableData
struct TrackableData_t3748763085;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraArController/MarkerList
struct  MarkerList_t3622290753 
{
public:
	// MiraArController/MarkerState MiraArController/MarkerList::currentState
	int32_t ___currentState_0;
	// System.String MiraArController/MarkerList::id
	String_t* ___id_1;
	// Kudan.AR.TrackableData MiraArController/MarkerList::tracker
	TrackableData_t3748763085 * ___tracker_2;
	// UnityEngine.Events.UnityEvent MiraArController/MarkerList::OnMarkerFound
	UnityEvent_t408735097 * ___OnMarkerFound_3;
	// UnityEngine.Events.UnityEvent MiraArController/MarkerList::OnMarkerLost
	UnityEvent_t408735097 * ___OnMarkerLost_4;
	// UnityEngine.Events.UnityEvent MiraArController/MarkerList::OnMarkerUpdate
	UnityEvent_t408735097 * ___OnMarkerUpdate_5;

public:
	inline static int32_t get_offset_of_currentState_0() { return static_cast<int32_t>(offsetof(MarkerList_t3622290753, ___currentState_0)); }
	inline int32_t get_currentState_0() const { return ___currentState_0; }
	inline int32_t* get_address_of_currentState_0() { return &___currentState_0; }
	inline void set_currentState_0(int32_t value)
	{
		___currentState_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(MarkerList_t3622290753, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier(&___id_1, value);
	}

	inline static int32_t get_offset_of_tracker_2() { return static_cast<int32_t>(offsetof(MarkerList_t3622290753, ___tracker_2)); }
	inline TrackableData_t3748763085 * get_tracker_2() const { return ___tracker_2; }
	inline TrackableData_t3748763085 ** get_address_of_tracker_2() { return &___tracker_2; }
	inline void set_tracker_2(TrackableData_t3748763085 * value)
	{
		___tracker_2 = value;
		Il2CppCodeGenWriteBarrier(&___tracker_2, value);
	}

	inline static int32_t get_offset_of_OnMarkerFound_3() { return static_cast<int32_t>(offsetof(MarkerList_t3622290753, ___OnMarkerFound_3)); }
	inline UnityEvent_t408735097 * get_OnMarkerFound_3() const { return ___OnMarkerFound_3; }
	inline UnityEvent_t408735097 ** get_address_of_OnMarkerFound_3() { return &___OnMarkerFound_3; }
	inline void set_OnMarkerFound_3(UnityEvent_t408735097 * value)
	{
		___OnMarkerFound_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnMarkerFound_3, value);
	}

	inline static int32_t get_offset_of_OnMarkerLost_4() { return static_cast<int32_t>(offsetof(MarkerList_t3622290753, ___OnMarkerLost_4)); }
	inline UnityEvent_t408735097 * get_OnMarkerLost_4() const { return ___OnMarkerLost_4; }
	inline UnityEvent_t408735097 ** get_address_of_OnMarkerLost_4() { return &___OnMarkerLost_4; }
	inline void set_OnMarkerLost_4(UnityEvent_t408735097 * value)
	{
		___OnMarkerLost_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnMarkerLost_4, value);
	}

	inline static int32_t get_offset_of_OnMarkerUpdate_5() { return static_cast<int32_t>(offsetof(MarkerList_t3622290753, ___OnMarkerUpdate_5)); }
	inline UnityEvent_t408735097 * get_OnMarkerUpdate_5() const { return ___OnMarkerUpdate_5; }
	inline UnityEvent_t408735097 ** get_address_of_OnMarkerUpdate_5() { return &___OnMarkerUpdate_5; }
	inline void set_OnMarkerUpdate_5(UnityEvent_t408735097 * value)
	{
		___OnMarkerUpdate_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnMarkerUpdate_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MiraArController/MarkerList
struct MarkerList_t3622290753_marshaled_pinvoke
{
	int32_t ___currentState_0;
	char* ___id_1;
	TrackableData_t3748763085 * ___tracker_2;
	UnityEvent_t408735097 * ___OnMarkerFound_3;
	UnityEvent_t408735097 * ___OnMarkerLost_4;
	UnityEvent_t408735097 * ___OnMarkerUpdate_5;
};
// Native definition for COM marshalling of MiraArController/MarkerList
struct MarkerList_t3622290753_marshaled_com
{
	int32_t ___currentState_0;
	Il2CppChar* ___id_1;
	TrackableData_t3748763085 * ___tracker_2;
	UnityEvent_t408735097 * ___OnMarkerFound_3;
	UnityEvent_t408735097 * ___OnMarkerLost_4;
	UnityEvent_t408735097 * ___OnMarkerUpdate_5;
};
