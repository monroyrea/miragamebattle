﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_UKeyValuePair_2_gen2284451592.h"

// ControllerInputGUI/ControllerEntry
struct ControllerEntry_t1998352496;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControllerInputGUI/VibrationEntry
struct  VibrationEntry_t1830347994  : public UKeyValuePair_2_t2284451592
{
public:
	// System.Int32 ControllerInputGUI/VibrationEntry::type
	int32_t ___type_2;
	// ControllerInputGUI/ControllerEntry ControllerInputGUI/VibrationEntry::context
	ControllerEntry_t1998352496 * ___context_3;

public:
	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(VibrationEntry_t1830347994, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_context_3() { return static_cast<int32_t>(offsetof(VibrationEntry_t1830347994, ___context_3)); }
	inline ControllerEntry_t1998352496 * get_context_3() const { return ___context_3; }
	inline ControllerEntry_t1998352496 ** get_address_of_context_3() { return &___context_3; }
	inline void set_context_3(ControllerEntry_t1998352496 * value)
	{
		___context_3 = value;
		Il2CppCodeGenWriteBarrier(&___context_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
