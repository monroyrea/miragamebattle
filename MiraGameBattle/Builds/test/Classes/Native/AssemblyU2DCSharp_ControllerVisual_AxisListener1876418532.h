﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_ControllerVisual_AxisType3855017368.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.String
struct String_t;
// Ximmerse.InputSystem.ControllerAxis[]
struct ControllerAxisU5BU5D_t3016952124;
// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControllerVisual/AxisListener
struct  AxisListener_t1876418532  : public Il2CppObject
{
public:
	// System.String ControllerVisual/AxisListener::name
	String_t* ___name_0;
	// ControllerVisual/AxisType ControllerVisual/AxisListener::type
	int32_t ___type_1;
	// Ximmerse.InputSystem.ControllerAxis[] ControllerVisual/AxisListener::axes
	ControllerAxisU5BU5D_t3016952124* ___axes_2;
	// UnityEngine.Transform ControllerVisual/AxisListener::target
	Transform_t3275118058 * ___target_3;
	// System.Single ControllerVisual/AxisListener::maxValue
	float ___maxValue_4;
	// UnityEngine.Vector3 ControllerVisual/AxisListener::m_CachedVector3
	Vector3_t2243707580  ___m_CachedVector3_5;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AxisListener_t1876418532, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(AxisListener_t1876418532, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_axes_2() { return static_cast<int32_t>(offsetof(AxisListener_t1876418532, ___axes_2)); }
	inline ControllerAxisU5BU5D_t3016952124* get_axes_2() const { return ___axes_2; }
	inline ControllerAxisU5BU5D_t3016952124** get_address_of_axes_2() { return &___axes_2; }
	inline void set_axes_2(ControllerAxisU5BU5D_t3016952124* value)
	{
		___axes_2 = value;
		Il2CppCodeGenWriteBarrier(&___axes_2, value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(AxisListener_t1876418532, ___target_3)); }
	inline Transform_t3275118058 * get_target_3() const { return ___target_3; }
	inline Transform_t3275118058 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Transform_t3275118058 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier(&___target_3, value);
	}

	inline static int32_t get_offset_of_maxValue_4() { return static_cast<int32_t>(offsetof(AxisListener_t1876418532, ___maxValue_4)); }
	inline float get_maxValue_4() const { return ___maxValue_4; }
	inline float* get_address_of_maxValue_4() { return &___maxValue_4; }
	inline void set_maxValue_4(float value)
	{
		___maxValue_4 = value;
	}

	inline static int32_t get_offset_of_m_CachedVector3_5() { return static_cast<int32_t>(offsetof(AxisListener_t1876418532, ___m_CachedVector3_5)); }
	inline Vector3_t2243707580  get_m_CachedVector3_5() const { return ___m_CachedVector3_5; }
	inline Vector3_t2243707580 * get_address_of_m_CachedVector3_5() { return &___m_CachedVector3_5; }
	inline void set_m_CachedVector3_5(Vector3_t2243707580  value)
	{
		___m_CachedVector3_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
