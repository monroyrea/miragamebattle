﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// scriptMechanics
struct scriptMechanics_t3538609330;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// scriptInput
struct  scriptInput_t3566377219  : public MonoBehaviour_t1158329972
{
public:
	// scriptMechanics scriptInput::mechScript
	scriptMechanics_t3538609330 * ___mechScript_2;

public:
	inline static int32_t get_offset_of_mechScript_2() { return static_cast<int32_t>(offsetof(scriptInput_t3566377219, ___mechScript_2)); }
	inline scriptMechanics_t3538609330 * get_mechScript_2() const { return ___mechScript_2; }
	inline scriptMechanics_t3538609330 ** get_address_of_mechScript_2() { return &___mechScript_2; }
	inline void set_mechScript_2(scriptMechanics_t3538609330 * value)
	{
		___mechScript_2 = value;
		Il2CppCodeGenWriteBarrier(&___mechScript_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
