﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// MiraViewer
struct MiraViewer_t256795227;
// GyroController
struct GyroController_t3857203917;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraViewer
struct  MiraViewer_t256795227  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct MiraViewer_t256795227_StaticFields
{
public:
	// MiraViewer MiraViewer::instance
	MiraViewer_t256795227 * ___instance_2;
	// GyroController MiraViewer::currentGyroController
	GyroController_t3857203917 * ___currentGyroController_3;
	// UnityEngine.Camera MiraViewer::currentMainCamera
	Camera_t189460977 * ___currentMainCamera_4;
	// System.Boolean MiraViewer::IsDebug
	bool ___IsDebug_5;
	// UnityEngine.GameObject MiraViewer::viewer
	GameObject_t1756533147 * ___viewer_6;
	// UnityEngine.GameObject MiraViewer::backrenderer
	GameObject_t1756533147 * ___backrenderer_7;
	// UnityEngine.GameObject MiraViewer::Left_Eye
	GameObject_t1756533147 * ___Left_Eye_8;
	// UnityEngine.GameObject MiraViewer::Right_Eye
	GameObject_t1756533147 * ___Right_Eye_9;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(MiraViewer_t256795227_StaticFields, ___instance_2)); }
	inline MiraViewer_t256795227 * get_instance_2() const { return ___instance_2; }
	inline MiraViewer_t256795227 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(MiraViewer_t256795227 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}

	inline static int32_t get_offset_of_currentGyroController_3() { return static_cast<int32_t>(offsetof(MiraViewer_t256795227_StaticFields, ___currentGyroController_3)); }
	inline GyroController_t3857203917 * get_currentGyroController_3() const { return ___currentGyroController_3; }
	inline GyroController_t3857203917 ** get_address_of_currentGyroController_3() { return &___currentGyroController_3; }
	inline void set_currentGyroController_3(GyroController_t3857203917 * value)
	{
		___currentGyroController_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentGyroController_3, value);
	}

	inline static int32_t get_offset_of_currentMainCamera_4() { return static_cast<int32_t>(offsetof(MiraViewer_t256795227_StaticFields, ___currentMainCamera_4)); }
	inline Camera_t189460977 * get_currentMainCamera_4() const { return ___currentMainCamera_4; }
	inline Camera_t189460977 ** get_address_of_currentMainCamera_4() { return &___currentMainCamera_4; }
	inline void set_currentMainCamera_4(Camera_t189460977 * value)
	{
		___currentMainCamera_4 = value;
		Il2CppCodeGenWriteBarrier(&___currentMainCamera_4, value);
	}

	inline static int32_t get_offset_of_IsDebug_5() { return static_cast<int32_t>(offsetof(MiraViewer_t256795227_StaticFields, ___IsDebug_5)); }
	inline bool get_IsDebug_5() const { return ___IsDebug_5; }
	inline bool* get_address_of_IsDebug_5() { return &___IsDebug_5; }
	inline void set_IsDebug_5(bool value)
	{
		___IsDebug_5 = value;
	}

	inline static int32_t get_offset_of_viewer_6() { return static_cast<int32_t>(offsetof(MiraViewer_t256795227_StaticFields, ___viewer_6)); }
	inline GameObject_t1756533147 * get_viewer_6() const { return ___viewer_6; }
	inline GameObject_t1756533147 ** get_address_of_viewer_6() { return &___viewer_6; }
	inline void set_viewer_6(GameObject_t1756533147 * value)
	{
		___viewer_6 = value;
		Il2CppCodeGenWriteBarrier(&___viewer_6, value);
	}

	inline static int32_t get_offset_of_backrenderer_7() { return static_cast<int32_t>(offsetof(MiraViewer_t256795227_StaticFields, ___backrenderer_7)); }
	inline GameObject_t1756533147 * get_backrenderer_7() const { return ___backrenderer_7; }
	inline GameObject_t1756533147 ** get_address_of_backrenderer_7() { return &___backrenderer_7; }
	inline void set_backrenderer_7(GameObject_t1756533147 * value)
	{
		___backrenderer_7 = value;
		Il2CppCodeGenWriteBarrier(&___backrenderer_7, value);
	}

	inline static int32_t get_offset_of_Left_Eye_8() { return static_cast<int32_t>(offsetof(MiraViewer_t256795227_StaticFields, ___Left_Eye_8)); }
	inline GameObject_t1756533147 * get_Left_Eye_8() const { return ___Left_Eye_8; }
	inline GameObject_t1756533147 ** get_address_of_Left_Eye_8() { return &___Left_Eye_8; }
	inline void set_Left_Eye_8(GameObject_t1756533147 * value)
	{
		___Left_Eye_8 = value;
		Il2CppCodeGenWriteBarrier(&___Left_Eye_8, value);
	}

	inline static int32_t get_offset_of_Right_Eye_9() { return static_cast<int32_t>(offsetof(MiraViewer_t256795227_StaticFields, ___Right_Eye_9)); }
	inline GameObject_t1756533147 * get_Right_Eye_9() const { return ___Right_Eye_9; }
	inline GameObject_t1756533147 ** get_address_of_Right_Eye_9() { return &___Right_Eye_9; }
	inline void set_Right_Eye_9(GameObject_t1756533147 * value)
	{
		___Right_Eye_9 = value;
		Il2CppCodeGenWriteBarrier(&___Right_Eye_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
