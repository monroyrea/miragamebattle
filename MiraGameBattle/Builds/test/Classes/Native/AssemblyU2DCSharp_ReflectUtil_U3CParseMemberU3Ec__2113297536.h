﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object
struct Il2CppObject;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReflectUtil/<ParseMember>c__AnonStorey0
struct  U3CParseMemberU3Ec__AnonStorey0_t2113297536  : public Il2CppObject
{
public:
	// System.Reflection.MethodInfo ReflectUtil/<ParseMember>c__AnonStorey0::currentMethod
	MethodInfo_t * ___currentMethod_0;
	// System.Object ReflectUtil/<ParseMember>c__AnonStorey0::currentObject
	Il2CppObject * ___currentObject_1;
	// System.Reflection.PropertyInfo ReflectUtil/<ParseMember>c__AnonStorey0::currentProperty
	PropertyInfo_t * ___currentProperty_2;

public:
	inline static int32_t get_offset_of_currentMethod_0() { return static_cast<int32_t>(offsetof(U3CParseMemberU3Ec__AnonStorey0_t2113297536, ___currentMethod_0)); }
	inline MethodInfo_t * get_currentMethod_0() const { return ___currentMethod_0; }
	inline MethodInfo_t ** get_address_of_currentMethod_0() { return &___currentMethod_0; }
	inline void set_currentMethod_0(MethodInfo_t * value)
	{
		___currentMethod_0 = value;
		Il2CppCodeGenWriteBarrier(&___currentMethod_0, value);
	}

	inline static int32_t get_offset_of_currentObject_1() { return static_cast<int32_t>(offsetof(U3CParseMemberU3Ec__AnonStorey0_t2113297536, ___currentObject_1)); }
	inline Il2CppObject * get_currentObject_1() const { return ___currentObject_1; }
	inline Il2CppObject ** get_address_of_currentObject_1() { return &___currentObject_1; }
	inline void set_currentObject_1(Il2CppObject * value)
	{
		___currentObject_1 = value;
		Il2CppCodeGenWriteBarrier(&___currentObject_1, value);
	}

	inline static int32_t get_offset_of_currentProperty_2() { return static_cast<int32_t>(offsetof(U3CParseMemberU3Ec__AnonStorey0_t2113297536, ___currentProperty_2)); }
	inline PropertyInfo_t * get_currentProperty_2() const { return ___currentProperty_2; }
	inline PropertyInfo_t ** get_address_of_currentProperty_2() { return &___currentProperty_2; }
	inline void set_currentProperty_2(PropertyInfo_t * value)
	{
		___currentProperty_2 = value;
		Il2CppCodeGenWriteBarrier(&___currentProperty_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
