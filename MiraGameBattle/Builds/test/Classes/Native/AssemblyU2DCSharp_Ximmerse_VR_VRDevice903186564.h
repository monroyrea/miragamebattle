﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Ximmerse_VR_TrackingOrigin2567001781.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.String
struct String_t;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;
// System.Func`1<System.Single>
struct Func_1_t4030902614;
// System.Action
struct Action_t3226471752;
// UnityEngine.TextAsset
struct TextAsset_t3973159845;
// Ximmerse.VR.Pose3D
struct Pose3D_t1800640744;
// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.VR.VRDevice
struct  VRDevice_t903186564  : public MonoBehaviour_t1158329972
{
public:
	// System.String Ximmerse.VR.VRDevice::family
	String_t* ___family_2;
	// Ximmerse.VR.TrackingOrigin Ximmerse.VR.VRDevice::trackingOriginType
	int32_t ___trackingOriginType_3;
	// UnityEngine.Vector3 Ximmerse.VR.VRDevice::neckPosition
	Vector3_t2243707580  ___neckPosition_4;
	// UnityEngine.Vector3 Ximmerse.VR.VRDevice::neckToEye
	Vector3_t2243707580  ___neckToEye_5;
	// System.Boolean Ximmerse.VR.VRDevice::isCenterEye
	bool ___isCenterEye_6;
	// System.String Ximmerse.VR.VRDevice::methodIsPresent
	String_t* ___methodIsPresent_7;
	// System.Func`1<System.Boolean> Ximmerse.VR.VRDevice::m_MethodIsPresent
	Func_1_t1485000104 * ___m_MethodIsPresent_8;
	// System.String Ximmerse.VR.VRDevice::methodRefreshRate
	String_t* ___methodRefreshRate_9;
	// System.Func`1<System.Single> Ximmerse.VR.VRDevice::m_MethodRefreshRate
	Func_1_t4030902614 * ___m_MethodRefreshRate_10;
	// System.String Ximmerse.VR.VRDevice::methodRecenter
	String_t* ___methodRecenter_11;
	// System.Action Ximmerse.VR.VRDevice::m_MethodRecenter
	Action_t3226471752 * ___m_MethodRecenter_12;
	// System.Boolean Ximmerse.VR.VRDevice::m_MethodRecenterCached
	bool ___m_MethodRecenterCached_13;
	// System.String Ximmerse.VR.VRDevice::androidTargetName
	String_t* ___androidTargetName_14;
	// UnityEngine.TextAsset Ximmerse.VR.VRDevice::androidManifest
	TextAsset_t3973159845 * ___androidManifest_15;
	// System.Boolean Ximmerse.VR.VRDevice::useUnityVR
	bool ___useUnityVR_16;
	// Ximmerse.VR.Pose3D Ximmerse.VR.VRDevice::inOutsideMarkPose
	Pose3D_t1800640744 * ___inOutsideMarkPose_17;
	// Ximmerse.VR.Pose3D Ximmerse.VR.VRDevice::outsideInMarkPose
	Pose3D_t1800640744 * ___outsideInMarkPose_18;
	// UnityEngine.Transform Ximmerse.VR.VRDevice::m_CenterEyeAnchor
	Transform_t3275118058 * ___m_CenterEyeAnchor_19;

public:
	inline static int32_t get_offset_of_family_2() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___family_2)); }
	inline String_t* get_family_2() const { return ___family_2; }
	inline String_t** get_address_of_family_2() { return &___family_2; }
	inline void set_family_2(String_t* value)
	{
		___family_2 = value;
		Il2CppCodeGenWriteBarrier(&___family_2, value);
	}

	inline static int32_t get_offset_of_trackingOriginType_3() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___trackingOriginType_3)); }
	inline int32_t get_trackingOriginType_3() const { return ___trackingOriginType_3; }
	inline int32_t* get_address_of_trackingOriginType_3() { return &___trackingOriginType_3; }
	inline void set_trackingOriginType_3(int32_t value)
	{
		___trackingOriginType_3 = value;
	}

	inline static int32_t get_offset_of_neckPosition_4() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___neckPosition_4)); }
	inline Vector3_t2243707580  get_neckPosition_4() const { return ___neckPosition_4; }
	inline Vector3_t2243707580 * get_address_of_neckPosition_4() { return &___neckPosition_4; }
	inline void set_neckPosition_4(Vector3_t2243707580  value)
	{
		___neckPosition_4 = value;
	}

	inline static int32_t get_offset_of_neckToEye_5() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___neckToEye_5)); }
	inline Vector3_t2243707580  get_neckToEye_5() const { return ___neckToEye_5; }
	inline Vector3_t2243707580 * get_address_of_neckToEye_5() { return &___neckToEye_5; }
	inline void set_neckToEye_5(Vector3_t2243707580  value)
	{
		___neckToEye_5 = value;
	}

	inline static int32_t get_offset_of_isCenterEye_6() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___isCenterEye_6)); }
	inline bool get_isCenterEye_6() const { return ___isCenterEye_6; }
	inline bool* get_address_of_isCenterEye_6() { return &___isCenterEye_6; }
	inline void set_isCenterEye_6(bool value)
	{
		___isCenterEye_6 = value;
	}

	inline static int32_t get_offset_of_methodIsPresent_7() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___methodIsPresent_7)); }
	inline String_t* get_methodIsPresent_7() const { return ___methodIsPresent_7; }
	inline String_t** get_address_of_methodIsPresent_7() { return &___methodIsPresent_7; }
	inline void set_methodIsPresent_7(String_t* value)
	{
		___methodIsPresent_7 = value;
		Il2CppCodeGenWriteBarrier(&___methodIsPresent_7, value);
	}

	inline static int32_t get_offset_of_m_MethodIsPresent_8() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___m_MethodIsPresent_8)); }
	inline Func_1_t1485000104 * get_m_MethodIsPresent_8() const { return ___m_MethodIsPresent_8; }
	inline Func_1_t1485000104 ** get_address_of_m_MethodIsPresent_8() { return &___m_MethodIsPresent_8; }
	inline void set_m_MethodIsPresent_8(Func_1_t1485000104 * value)
	{
		___m_MethodIsPresent_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_MethodIsPresent_8, value);
	}

	inline static int32_t get_offset_of_methodRefreshRate_9() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___methodRefreshRate_9)); }
	inline String_t* get_methodRefreshRate_9() const { return ___methodRefreshRate_9; }
	inline String_t** get_address_of_methodRefreshRate_9() { return &___methodRefreshRate_9; }
	inline void set_methodRefreshRate_9(String_t* value)
	{
		___methodRefreshRate_9 = value;
		Il2CppCodeGenWriteBarrier(&___methodRefreshRate_9, value);
	}

	inline static int32_t get_offset_of_m_MethodRefreshRate_10() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___m_MethodRefreshRate_10)); }
	inline Func_1_t4030902614 * get_m_MethodRefreshRate_10() const { return ___m_MethodRefreshRate_10; }
	inline Func_1_t4030902614 ** get_address_of_m_MethodRefreshRate_10() { return &___m_MethodRefreshRate_10; }
	inline void set_m_MethodRefreshRate_10(Func_1_t4030902614 * value)
	{
		___m_MethodRefreshRate_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_MethodRefreshRate_10, value);
	}

	inline static int32_t get_offset_of_methodRecenter_11() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___methodRecenter_11)); }
	inline String_t* get_methodRecenter_11() const { return ___methodRecenter_11; }
	inline String_t** get_address_of_methodRecenter_11() { return &___methodRecenter_11; }
	inline void set_methodRecenter_11(String_t* value)
	{
		___methodRecenter_11 = value;
		Il2CppCodeGenWriteBarrier(&___methodRecenter_11, value);
	}

	inline static int32_t get_offset_of_m_MethodRecenter_12() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___m_MethodRecenter_12)); }
	inline Action_t3226471752 * get_m_MethodRecenter_12() const { return ___m_MethodRecenter_12; }
	inline Action_t3226471752 ** get_address_of_m_MethodRecenter_12() { return &___m_MethodRecenter_12; }
	inline void set_m_MethodRecenter_12(Action_t3226471752 * value)
	{
		___m_MethodRecenter_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_MethodRecenter_12, value);
	}

	inline static int32_t get_offset_of_m_MethodRecenterCached_13() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___m_MethodRecenterCached_13)); }
	inline bool get_m_MethodRecenterCached_13() const { return ___m_MethodRecenterCached_13; }
	inline bool* get_address_of_m_MethodRecenterCached_13() { return &___m_MethodRecenterCached_13; }
	inline void set_m_MethodRecenterCached_13(bool value)
	{
		___m_MethodRecenterCached_13 = value;
	}

	inline static int32_t get_offset_of_androidTargetName_14() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___androidTargetName_14)); }
	inline String_t* get_androidTargetName_14() const { return ___androidTargetName_14; }
	inline String_t** get_address_of_androidTargetName_14() { return &___androidTargetName_14; }
	inline void set_androidTargetName_14(String_t* value)
	{
		___androidTargetName_14 = value;
		Il2CppCodeGenWriteBarrier(&___androidTargetName_14, value);
	}

	inline static int32_t get_offset_of_androidManifest_15() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___androidManifest_15)); }
	inline TextAsset_t3973159845 * get_androidManifest_15() const { return ___androidManifest_15; }
	inline TextAsset_t3973159845 ** get_address_of_androidManifest_15() { return &___androidManifest_15; }
	inline void set_androidManifest_15(TextAsset_t3973159845 * value)
	{
		___androidManifest_15 = value;
		Il2CppCodeGenWriteBarrier(&___androidManifest_15, value);
	}

	inline static int32_t get_offset_of_useUnityVR_16() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___useUnityVR_16)); }
	inline bool get_useUnityVR_16() const { return ___useUnityVR_16; }
	inline bool* get_address_of_useUnityVR_16() { return &___useUnityVR_16; }
	inline void set_useUnityVR_16(bool value)
	{
		___useUnityVR_16 = value;
	}

	inline static int32_t get_offset_of_inOutsideMarkPose_17() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___inOutsideMarkPose_17)); }
	inline Pose3D_t1800640744 * get_inOutsideMarkPose_17() const { return ___inOutsideMarkPose_17; }
	inline Pose3D_t1800640744 ** get_address_of_inOutsideMarkPose_17() { return &___inOutsideMarkPose_17; }
	inline void set_inOutsideMarkPose_17(Pose3D_t1800640744 * value)
	{
		___inOutsideMarkPose_17 = value;
		Il2CppCodeGenWriteBarrier(&___inOutsideMarkPose_17, value);
	}

	inline static int32_t get_offset_of_outsideInMarkPose_18() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___outsideInMarkPose_18)); }
	inline Pose3D_t1800640744 * get_outsideInMarkPose_18() const { return ___outsideInMarkPose_18; }
	inline Pose3D_t1800640744 ** get_address_of_outsideInMarkPose_18() { return &___outsideInMarkPose_18; }
	inline void set_outsideInMarkPose_18(Pose3D_t1800640744 * value)
	{
		___outsideInMarkPose_18 = value;
		Il2CppCodeGenWriteBarrier(&___outsideInMarkPose_18, value);
	}

	inline static int32_t get_offset_of_m_CenterEyeAnchor_19() { return static_cast<int32_t>(offsetof(VRDevice_t903186564, ___m_CenterEyeAnchor_19)); }
	inline Transform_t3275118058 * get_m_CenterEyeAnchor_19() const { return ___m_CenterEyeAnchor_19; }
	inline Transform_t3275118058 ** get_address_of_m_CenterEyeAnchor_19() { return &___m_CenterEyeAnchor_19; }
	inline void set_m_CenterEyeAnchor_19(Transform_t3275118058 * value)
	{
		___m_CenterEyeAnchor_19 = value;
		Il2CppCodeGenWriteBarrier(&___m_CenterEyeAnchor_19, value);
	}
};

struct VRDevice_t903186564_StaticFields
{
public:
	// System.Func`1<System.Single> Ximmerse.VR.VRDevice::<>f__am$cache0
	Func_1_t4030902614 * ___U3CU3Ef__amU24cache0_20;
	// System.Func`1<System.Single> Ximmerse.VR.VRDevice::<>f__am$cache1
	Func_1_t4030902614 * ___U3CU3Ef__amU24cache1_21;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_20() { return static_cast<int32_t>(offsetof(VRDevice_t903186564_StaticFields, ___U3CU3Ef__amU24cache0_20)); }
	inline Func_1_t4030902614 * get_U3CU3Ef__amU24cache0_20() const { return ___U3CU3Ef__amU24cache0_20; }
	inline Func_1_t4030902614 ** get_address_of_U3CU3Ef__amU24cache0_20() { return &___U3CU3Ef__amU24cache0_20; }
	inline void set_U3CU3Ef__amU24cache0_20(Func_1_t4030902614 * value)
	{
		___U3CU3Ef__amU24cache0_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_21() { return static_cast<int32_t>(offsetof(VRDevice_t903186564_StaticFields, ___U3CU3Ef__amU24cache1_21)); }
	inline Func_1_t4030902614 * get_U3CU3Ef__amU24cache1_21() const { return ___U3CU3Ef__amU24cache1_21; }
	inline Func_1_t4030902614 ** get_address_of_U3CU3Ef__amU24cache1_21() { return &___U3CU3Ef__amU24cache1_21; }
	inline void set_U3CU3Ef__amU24cache1_21(Func_1_t4030902614 * value)
	{
		___U3CU3Ef__amU24cache1_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
