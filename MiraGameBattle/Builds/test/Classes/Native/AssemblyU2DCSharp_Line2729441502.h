﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Line
struct  Line_t2729441502  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 Line::p0
	Vector3_t2243707580  ___p0_2;
	// UnityEngine.Vector3 Line::p1
	Vector3_t2243707580  ___p1_3;

public:
	inline static int32_t get_offset_of_p0_2() { return static_cast<int32_t>(offsetof(Line_t2729441502, ___p0_2)); }
	inline Vector3_t2243707580  get_p0_2() const { return ___p0_2; }
	inline Vector3_t2243707580 * get_address_of_p0_2() { return &___p0_2; }
	inline void set_p0_2(Vector3_t2243707580  value)
	{
		___p0_2 = value;
	}

	inline static int32_t get_offset_of_p1_3() { return static_cast<int32_t>(offsetof(Line_t2729441502, ___p1_3)); }
	inline Vector3_t2243707580  get_p1_3() const { return ___p1_3; }
	inline Vector3_t2243707580 * get_address_of_p1_3() { return &___p1_3; }
	inline void set_p1_3(Vector3_t2243707580  value)
	{
		___p1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
