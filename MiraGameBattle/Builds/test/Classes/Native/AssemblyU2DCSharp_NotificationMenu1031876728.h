﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_ControllerHover24802818.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<NotificationIcon>
struct List_1_t3997059074;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationMenu
struct  NotificationMenu_t1031876728  : public ControllerHover_t24802818
{
public:
	// System.Single NotificationMenu::maxAngle
	float ___maxAngle_10;
	// System.Single NotificationMenu::height
	float ___height_11;
	// UnityEngine.GameObject NotificationMenu::centerCollider
	GameObject_t1756533147 * ___centerCollider_12;
	// System.Single NotificationMenu::zRadiusLU
	float ___zRadiusLU_13;
	// System.Single NotificationMenu::padding
	float ___padding_14;
	// System.Collections.Generic.List`1<NotificationIcon> NotificationMenu::icons
	List_1_t3997059074 * ___icons_15;
	// System.Single NotificationMenu::elevationAngle
	float ___elevationAngle_16;
	// System.Single NotificationMenu::fadeTime
	float ___fadeTime_17;
	// System.Single NotificationMenu::currentFadeTimer
	float ___currentFadeTimer_18;

public:
	inline static int32_t get_offset_of_maxAngle_10() { return static_cast<int32_t>(offsetof(NotificationMenu_t1031876728, ___maxAngle_10)); }
	inline float get_maxAngle_10() const { return ___maxAngle_10; }
	inline float* get_address_of_maxAngle_10() { return &___maxAngle_10; }
	inline void set_maxAngle_10(float value)
	{
		___maxAngle_10 = value;
	}

	inline static int32_t get_offset_of_height_11() { return static_cast<int32_t>(offsetof(NotificationMenu_t1031876728, ___height_11)); }
	inline float get_height_11() const { return ___height_11; }
	inline float* get_address_of_height_11() { return &___height_11; }
	inline void set_height_11(float value)
	{
		___height_11 = value;
	}

	inline static int32_t get_offset_of_centerCollider_12() { return static_cast<int32_t>(offsetof(NotificationMenu_t1031876728, ___centerCollider_12)); }
	inline GameObject_t1756533147 * get_centerCollider_12() const { return ___centerCollider_12; }
	inline GameObject_t1756533147 ** get_address_of_centerCollider_12() { return &___centerCollider_12; }
	inline void set_centerCollider_12(GameObject_t1756533147 * value)
	{
		___centerCollider_12 = value;
		Il2CppCodeGenWriteBarrier(&___centerCollider_12, value);
	}

	inline static int32_t get_offset_of_zRadiusLU_13() { return static_cast<int32_t>(offsetof(NotificationMenu_t1031876728, ___zRadiusLU_13)); }
	inline float get_zRadiusLU_13() const { return ___zRadiusLU_13; }
	inline float* get_address_of_zRadiusLU_13() { return &___zRadiusLU_13; }
	inline void set_zRadiusLU_13(float value)
	{
		___zRadiusLU_13 = value;
	}

	inline static int32_t get_offset_of_padding_14() { return static_cast<int32_t>(offsetof(NotificationMenu_t1031876728, ___padding_14)); }
	inline float get_padding_14() const { return ___padding_14; }
	inline float* get_address_of_padding_14() { return &___padding_14; }
	inline void set_padding_14(float value)
	{
		___padding_14 = value;
	}

	inline static int32_t get_offset_of_icons_15() { return static_cast<int32_t>(offsetof(NotificationMenu_t1031876728, ___icons_15)); }
	inline List_1_t3997059074 * get_icons_15() const { return ___icons_15; }
	inline List_1_t3997059074 ** get_address_of_icons_15() { return &___icons_15; }
	inline void set_icons_15(List_1_t3997059074 * value)
	{
		___icons_15 = value;
		Il2CppCodeGenWriteBarrier(&___icons_15, value);
	}

	inline static int32_t get_offset_of_elevationAngle_16() { return static_cast<int32_t>(offsetof(NotificationMenu_t1031876728, ___elevationAngle_16)); }
	inline float get_elevationAngle_16() const { return ___elevationAngle_16; }
	inline float* get_address_of_elevationAngle_16() { return &___elevationAngle_16; }
	inline void set_elevationAngle_16(float value)
	{
		___elevationAngle_16 = value;
	}

	inline static int32_t get_offset_of_fadeTime_17() { return static_cast<int32_t>(offsetof(NotificationMenu_t1031876728, ___fadeTime_17)); }
	inline float get_fadeTime_17() const { return ___fadeTime_17; }
	inline float* get_address_of_fadeTime_17() { return &___fadeTime_17; }
	inline void set_fadeTime_17(float value)
	{
		___fadeTime_17 = value;
	}

	inline static int32_t get_offset_of_currentFadeTimer_18() { return static_cast<int32_t>(offsetof(NotificationMenu_t1031876728, ___currentFadeTimer_18)); }
	inline float get_currentFadeTimer_18() const { return ___currentFadeTimer_18; }
	inline float* get_address_of_currentFadeTimer_18() { return &___currentFadeTimer_18; }
	inline void set_currentFadeTimer_18(float value)
	{
		___currentFadeTimer_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
