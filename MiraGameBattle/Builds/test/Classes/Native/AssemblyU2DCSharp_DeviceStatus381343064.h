﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// DeviceStatus/DeviceUI[]
struct DeviceUIU5BU5D_t1594366304;
// Ximmerse.UI.UIFade
struct UIFade_t1631582502;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceStatus
struct  DeviceStatus_t381343064  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean DeviceStatus::alwaysShowUI
	bool ___alwaysShowUI_2;
	// UnityEngine.GameObject DeviceStatus::uiRoot
	GameObject_t1756533147 * ___uiRoot_3;
	// DeviceStatus/DeviceUI[] DeviceStatus::devices
	DeviceUIU5BU5D_t1594366304* ___devices_4;
	// Ximmerse.UI.UIFade DeviceStatus::m_FadeUiRoot
	UIFade_t1631582502 * ___m_FadeUiRoot_5;

public:
	inline static int32_t get_offset_of_alwaysShowUI_2() { return static_cast<int32_t>(offsetof(DeviceStatus_t381343064, ___alwaysShowUI_2)); }
	inline bool get_alwaysShowUI_2() const { return ___alwaysShowUI_2; }
	inline bool* get_address_of_alwaysShowUI_2() { return &___alwaysShowUI_2; }
	inline void set_alwaysShowUI_2(bool value)
	{
		___alwaysShowUI_2 = value;
	}

	inline static int32_t get_offset_of_uiRoot_3() { return static_cast<int32_t>(offsetof(DeviceStatus_t381343064, ___uiRoot_3)); }
	inline GameObject_t1756533147 * get_uiRoot_3() const { return ___uiRoot_3; }
	inline GameObject_t1756533147 ** get_address_of_uiRoot_3() { return &___uiRoot_3; }
	inline void set_uiRoot_3(GameObject_t1756533147 * value)
	{
		___uiRoot_3 = value;
		Il2CppCodeGenWriteBarrier(&___uiRoot_3, value);
	}

	inline static int32_t get_offset_of_devices_4() { return static_cast<int32_t>(offsetof(DeviceStatus_t381343064, ___devices_4)); }
	inline DeviceUIU5BU5D_t1594366304* get_devices_4() const { return ___devices_4; }
	inline DeviceUIU5BU5D_t1594366304** get_address_of_devices_4() { return &___devices_4; }
	inline void set_devices_4(DeviceUIU5BU5D_t1594366304* value)
	{
		___devices_4 = value;
		Il2CppCodeGenWriteBarrier(&___devices_4, value);
	}

	inline static int32_t get_offset_of_m_FadeUiRoot_5() { return static_cast<int32_t>(offsetof(DeviceStatus_t381343064, ___m_FadeUiRoot_5)); }
	inline UIFade_t1631582502 * get_m_FadeUiRoot_5() const { return ___m_FadeUiRoot_5; }
	inline UIFade_t1631582502 ** get_address_of_m_FadeUiRoot_5() { return &___m_FadeUiRoot_5; }
	inline void set_m_FadeUiRoot_5(UIFade_t1631582502 * value)
	{
		___m_FadeUiRoot_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_FadeUiRoot_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
