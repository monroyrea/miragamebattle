﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerTy123719274.h"

// ControllerVisual/AxisListener[]
struct AxisListenerU5BU5D_t3724726285;
// ControllerVisual/ButtonListener[]
struct ButtonListenerU5BU5D_t4278623120;
// Ximmerse.InputSystem.ControllerInput
struct ControllerInput_t1382602808;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControllerVisual
struct  ControllerVisual_t293503452  : public MonoBehaviour_t1158329972
{
public:
	// Ximmerse.InputSystem.ControllerType ControllerVisual::controller
	int32_t ___controller_2;
	// ControllerVisual/AxisListener[] ControllerVisual::axes
	AxisListenerU5BU5D_t3724726285* ___axes_3;
	// ControllerVisual/ButtonListener[] ControllerVisual::buttons
	ButtonListenerU5BU5D_t4278623120* ___buttons_4;
	// Ximmerse.InputSystem.ControllerInput ControllerVisual::m_Input
	ControllerInput_t1382602808 * ___m_Input_5;

public:
	inline static int32_t get_offset_of_controller_2() { return static_cast<int32_t>(offsetof(ControllerVisual_t293503452, ___controller_2)); }
	inline int32_t get_controller_2() const { return ___controller_2; }
	inline int32_t* get_address_of_controller_2() { return &___controller_2; }
	inline void set_controller_2(int32_t value)
	{
		___controller_2 = value;
	}

	inline static int32_t get_offset_of_axes_3() { return static_cast<int32_t>(offsetof(ControllerVisual_t293503452, ___axes_3)); }
	inline AxisListenerU5BU5D_t3724726285* get_axes_3() const { return ___axes_3; }
	inline AxisListenerU5BU5D_t3724726285** get_address_of_axes_3() { return &___axes_3; }
	inline void set_axes_3(AxisListenerU5BU5D_t3724726285* value)
	{
		___axes_3 = value;
		Il2CppCodeGenWriteBarrier(&___axes_3, value);
	}

	inline static int32_t get_offset_of_buttons_4() { return static_cast<int32_t>(offsetof(ControllerVisual_t293503452, ___buttons_4)); }
	inline ButtonListenerU5BU5D_t4278623120* get_buttons_4() const { return ___buttons_4; }
	inline ButtonListenerU5BU5D_t4278623120** get_address_of_buttons_4() { return &___buttons_4; }
	inline void set_buttons_4(ButtonListenerU5BU5D_t4278623120* value)
	{
		___buttons_4 = value;
		Il2CppCodeGenWriteBarrier(&___buttons_4, value);
	}

	inline static int32_t get_offset_of_m_Input_5() { return static_cast<int32_t>(offsetof(ControllerVisual_t293503452, ___m_Input_5)); }
	inline ControllerInput_t1382602808 * get_m_Input_5() const { return ___m_Input_5; }
	inline ControllerInput_t1382602808 ** get_address_of_m_Input_5() { return &___m_Input_5; }
	inline void set_m_Input_5(ControllerInput_t1382602808 * value)
	{
		___m_Input_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_Input_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
