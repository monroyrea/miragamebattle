﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_MiraArController_CommonMarkerList285872970.h"

// MiraArController/MarkerList[]
struct MarkerListU5BU5D_t462610588;
// MiraArController
struct MiraArController_t3896789088;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Kudan.AR.TrackingMethodMarker
struct TrackingMethodMarker_t3831798236;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraArController
struct  MiraArController_t3896789088  : public MonoBehaviour_t1158329972
{
public:
	// MiraArController/CommonMarkerList MiraArController::CommonMarkerEvents
	CommonMarkerList_t285872970  ___CommonMarkerEvents_2;
	// MiraArController/MarkerList[] MiraArController::ListOfMarkers
	MarkerListU5BU5D_t462610588* ___ListOfMarkers_3;
	// System.Single MiraArController::IPD
	float ___IPD_4;
	// System.Boolean MiraArController::Display_Debug_Menu
	bool ___Display_Debug_Menu_5;

public:
	inline static int32_t get_offset_of_CommonMarkerEvents_2() { return static_cast<int32_t>(offsetof(MiraArController_t3896789088, ___CommonMarkerEvents_2)); }
	inline CommonMarkerList_t285872970  get_CommonMarkerEvents_2() const { return ___CommonMarkerEvents_2; }
	inline CommonMarkerList_t285872970 * get_address_of_CommonMarkerEvents_2() { return &___CommonMarkerEvents_2; }
	inline void set_CommonMarkerEvents_2(CommonMarkerList_t285872970  value)
	{
		___CommonMarkerEvents_2 = value;
	}

	inline static int32_t get_offset_of_ListOfMarkers_3() { return static_cast<int32_t>(offsetof(MiraArController_t3896789088, ___ListOfMarkers_3)); }
	inline MarkerListU5BU5D_t462610588* get_ListOfMarkers_3() const { return ___ListOfMarkers_3; }
	inline MarkerListU5BU5D_t462610588** get_address_of_ListOfMarkers_3() { return &___ListOfMarkers_3; }
	inline void set_ListOfMarkers_3(MarkerListU5BU5D_t462610588* value)
	{
		___ListOfMarkers_3 = value;
		Il2CppCodeGenWriteBarrier(&___ListOfMarkers_3, value);
	}

	inline static int32_t get_offset_of_IPD_4() { return static_cast<int32_t>(offsetof(MiraArController_t3896789088, ___IPD_4)); }
	inline float get_IPD_4() const { return ___IPD_4; }
	inline float* get_address_of_IPD_4() { return &___IPD_4; }
	inline void set_IPD_4(float value)
	{
		___IPD_4 = value;
	}

	inline static int32_t get_offset_of_Display_Debug_Menu_5() { return static_cast<int32_t>(offsetof(MiraArController_t3896789088, ___Display_Debug_Menu_5)); }
	inline bool get_Display_Debug_Menu_5() const { return ___Display_Debug_Menu_5; }
	inline bool* get_address_of_Display_Debug_Menu_5() { return &___Display_Debug_Menu_5; }
	inline void set_Display_Debug_Menu_5(bool value)
	{
		___Display_Debug_Menu_5 = value;
	}
};

struct MiraArController_t3896789088_StaticFields
{
public:
	// MiraArController MiraArController::instance
	MiraArController_t3896789088 * ___instance_6;
	// UnityEngine.GameObject MiraArController::MiraTracking
	GameObject_t1756533147 * ___MiraTracking_7;
	// Kudan.AR.TrackingMethodMarker MiraArController::currentMiraMarkers
	TrackingMethodMarker_t3831798236 * ___currentMiraMarkers_8;

public:
	inline static int32_t get_offset_of_instance_6() { return static_cast<int32_t>(offsetof(MiraArController_t3896789088_StaticFields, ___instance_6)); }
	inline MiraArController_t3896789088 * get_instance_6() const { return ___instance_6; }
	inline MiraArController_t3896789088 ** get_address_of_instance_6() { return &___instance_6; }
	inline void set_instance_6(MiraArController_t3896789088 * value)
	{
		___instance_6 = value;
		Il2CppCodeGenWriteBarrier(&___instance_6, value);
	}

	inline static int32_t get_offset_of_MiraTracking_7() { return static_cast<int32_t>(offsetof(MiraArController_t3896789088_StaticFields, ___MiraTracking_7)); }
	inline GameObject_t1756533147 * get_MiraTracking_7() const { return ___MiraTracking_7; }
	inline GameObject_t1756533147 ** get_address_of_MiraTracking_7() { return &___MiraTracking_7; }
	inline void set_MiraTracking_7(GameObject_t1756533147 * value)
	{
		___MiraTracking_7 = value;
		Il2CppCodeGenWriteBarrier(&___MiraTracking_7, value);
	}

	inline static int32_t get_offset_of_currentMiraMarkers_8() { return static_cast<int32_t>(offsetof(MiraArController_t3896789088_StaticFields, ___currentMiraMarkers_8)); }
	inline TrackingMethodMarker_t3831798236 * get_currentMiraMarkers_8() const { return ___currentMiraMarkers_8; }
	inline TrackingMethodMarker_t3831798236 ** get_address_of_currentMiraMarkers_8() { return &___currentMiraMarkers_8; }
	inline void set_currentMiraMarkers_8(TrackingMethodMarker_t3831798236 * value)
	{
		___currentMiraMarkers_8 = value;
		Il2CppCodeGenWriteBarrier(&___currentMiraMarkers_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
