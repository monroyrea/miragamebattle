﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XHawkInput3103630643.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.InputSystem.XHawkInput_InsideOut
struct  XHawkInput_InsideOut_t598830048  : public XHawkInput_t3103630643
{
public:
	// System.Boolean Ximmerse.InputSystem.XHawkInput_InsideOut::makeSmooth
	bool ___makeSmooth_19;
	// UnityEngine.Transform Ximmerse.InputSystem.XHawkInput_InsideOut::m_CenterEye
	Transform_t3275118058 * ___m_CenterEye_20;
	// UnityEngine.Vector3 Ximmerse.InputSystem.XHawkInput_InsideOut::m_AnchorPosition
	Vector3_t2243707580  ___m_AnchorPosition_21;
	// UnityEngine.Quaternion Ximmerse.InputSystem.XHawkInput_InsideOut::m_AnchorRotation
	Quaternion_t4030073918  ___m_AnchorRotation_22;

public:
	inline static int32_t get_offset_of_makeSmooth_19() { return static_cast<int32_t>(offsetof(XHawkInput_InsideOut_t598830048, ___makeSmooth_19)); }
	inline bool get_makeSmooth_19() const { return ___makeSmooth_19; }
	inline bool* get_address_of_makeSmooth_19() { return &___makeSmooth_19; }
	inline void set_makeSmooth_19(bool value)
	{
		___makeSmooth_19 = value;
	}

	inline static int32_t get_offset_of_m_CenterEye_20() { return static_cast<int32_t>(offsetof(XHawkInput_InsideOut_t598830048, ___m_CenterEye_20)); }
	inline Transform_t3275118058 * get_m_CenterEye_20() const { return ___m_CenterEye_20; }
	inline Transform_t3275118058 ** get_address_of_m_CenterEye_20() { return &___m_CenterEye_20; }
	inline void set_m_CenterEye_20(Transform_t3275118058 * value)
	{
		___m_CenterEye_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_CenterEye_20, value);
	}

	inline static int32_t get_offset_of_m_AnchorPosition_21() { return static_cast<int32_t>(offsetof(XHawkInput_InsideOut_t598830048, ___m_AnchorPosition_21)); }
	inline Vector3_t2243707580  get_m_AnchorPosition_21() const { return ___m_AnchorPosition_21; }
	inline Vector3_t2243707580 * get_address_of_m_AnchorPosition_21() { return &___m_AnchorPosition_21; }
	inline void set_m_AnchorPosition_21(Vector3_t2243707580  value)
	{
		___m_AnchorPosition_21 = value;
	}

	inline static int32_t get_offset_of_m_AnchorRotation_22() { return static_cast<int32_t>(offsetof(XHawkInput_InsideOut_t598830048, ___m_AnchorRotation_22)); }
	inline Quaternion_t4030073918  get_m_AnchorRotation_22() const { return ___m_AnchorRotation_22; }
	inline Quaternion_t4030073918 * get_address_of_m_AnchorRotation_22() { return &___m_AnchorRotation_22; }
	inline void set_m_AnchorRotation_22(Quaternion_t4030073918  value)
	{
		___m_AnchorRotation_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
