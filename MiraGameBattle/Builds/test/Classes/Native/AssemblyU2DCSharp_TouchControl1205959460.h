﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// ActivatedScript
struct ActivatedScript_t2824515540;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchControl
struct  TouchControl_t1205959460  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TouchControl::zoomSpeed
	float ___zoomSpeed_2;
	// System.Single TouchControl::moveSpeed
	float ___moveSpeed_3;
	// System.Single TouchControl::roughDiff
	float ___roughDiff_4;
	// UnityEngine.Vector2 TouchControl::startPos
	Vector2_t2243707579  ___startPos_5;
	// UnityEngine.Vector2 TouchControl::endPos
	Vector2_t2243707579  ___endPos_6;
	// ActivatedScript TouchControl::activate
	ActivatedScript_t2824515540 * ___activate_7;

public:
	inline static int32_t get_offset_of_zoomSpeed_2() { return static_cast<int32_t>(offsetof(TouchControl_t1205959460, ___zoomSpeed_2)); }
	inline float get_zoomSpeed_2() const { return ___zoomSpeed_2; }
	inline float* get_address_of_zoomSpeed_2() { return &___zoomSpeed_2; }
	inline void set_zoomSpeed_2(float value)
	{
		___zoomSpeed_2 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_3() { return static_cast<int32_t>(offsetof(TouchControl_t1205959460, ___moveSpeed_3)); }
	inline float get_moveSpeed_3() const { return ___moveSpeed_3; }
	inline float* get_address_of_moveSpeed_3() { return &___moveSpeed_3; }
	inline void set_moveSpeed_3(float value)
	{
		___moveSpeed_3 = value;
	}

	inline static int32_t get_offset_of_roughDiff_4() { return static_cast<int32_t>(offsetof(TouchControl_t1205959460, ___roughDiff_4)); }
	inline float get_roughDiff_4() const { return ___roughDiff_4; }
	inline float* get_address_of_roughDiff_4() { return &___roughDiff_4; }
	inline void set_roughDiff_4(float value)
	{
		___roughDiff_4 = value;
	}

	inline static int32_t get_offset_of_startPos_5() { return static_cast<int32_t>(offsetof(TouchControl_t1205959460, ___startPos_5)); }
	inline Vector2_t2243707579  get_startPos_5() const { return ___startPos_5; }
	inline Vector2_t2243707579 * get_address_of_startPos_5() { return &___startPos_5; }
	inline void set_startPos_5(Vector2_t2243707579  value)
	{
		___startPos_5 = value;
	}

	inline static int32_t get_offset_of_endPos_6() { return static_cast<int32_t>(offsetof(TouchControl_t1205959460, ___endPos_6)); }
	inline Vector2_t2243707579  get_endPos_6() const { return ___endPos_6; }
	inline Vector2_t2243707579 * get_address_of_endPos_6() { return &___endPos_6; }
	inline void set_endPos_6(Vector2_t2243707579  value)
	{
		___endPos_6 = value;
	}

	inline static int32_t get_offset_of_activate_7() { return static_cast<int32_t>(offsetof(TouchControl_t1205959460, ___activate_7)); }
	inline ActivatedScript_t2824515540 * get_activate_7() const { return ___activate_7; }
	inline ActivatedScript_t2824515540 ** get_address_of_activate_7() { return &___activate_7; }
	inline void set_activate_7(ActivatedScript_t2824515540 * value)
	{
		___activate_7 = value;
		Il2CppCodeGenWriteBarrier(&___activate_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
