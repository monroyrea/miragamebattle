﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.VR.Pose3D
struct  Pose3D_t1800640744  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 Ximmerse.VR.Pose3D::position
	Vector3_t2243707580  ___position_0;
	// UnityEngine.Vector3 Ximmerse.VR.Pose3D::eulerAngles
	Vector3_t2243707580  ___eulerAngles_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(Pose3D_t1800640744, ___position_0)); }
	inline Vector3_t2243707580  get_position_0() const { return ___position_0; }
	inline Vector3_t2243707580 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t2243707580  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_eulerAngles_1() { return static_cast<int32_t>(offsetof(Pose3D_t1800640744, ___eulerAngles_1)); }
	inline Vector3_t2243707580  get_eulerAngles_1() const { return ___eulerAngles_1; }
	inline Vector3_t2243707580 * get_address_of_eulerAngles_1() { return &___eulerAngles_1; }
	inline void set_eulerAngles_1(Vector3_t2243707580  value)
	{
		___eulerAngles_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
