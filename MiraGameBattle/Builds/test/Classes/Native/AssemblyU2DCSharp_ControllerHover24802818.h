﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_ControllerTarget3193765035.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControllerHover
struct  ControllerHover_t24802818  : public ControllerTarget_t3193765035
{
public:
	// System.Single ControllerHover::scaleChange
	float ___scaleChange_2;
	// UnityEngine.Vector3 ControllerHover::ogScale
	Vector3_t2243707580  ___ogScale_3;
	// UnityEngine.Vector3 ControllerHover::finalScaleUp
	Vector3_t2243707580  ___finalScaleUp_4;
	// UnityEngine.Vector3 ControllerHover::finalScaleDown
	Vector3_t2243707580  ___finalScaleDown_5;
	// UnityEngine.Vector3 ControllerHover::currentScale
	Vector3_t2243707580  ___currentScale_6;
	// UnityEngine.Color ControllerHover::ogColor
	Color_t2020392075  ___ogColor_7;
	// UnityEngine.Color ControllerHover::holverColor
	Color_t2020392075  ___holverColor_8;
	// System.Boolean ControllerHover::hovering
	bool ___hovering_9;

public:
	inline static int32_t get_offset_of_scaleChange_2() { return static_cast<int32_t>(offsetof(ControllerHover_t24802818, ___scaleChange_2)); }
	inline float get_scaleChange_2() const { return ___scaleChange_2; }
	inline float* get_address_of_scaleChange_2() { return &___scaleChange_2; }
	inline void set_scaleChange_2(float value)
	{
		___scaleChange_2 = value;
	}

	inline static int32_t get_offset_of_ogScale_3() { return static_cast<int32_t>(offsetof(ControllerHover_t24802818, ___ogScale_3)); }
	inline Vector3_t2243707580  get_ogScale_3() const { return ___ogScale_3; }
	inline Vector3_t2243707580 * get_address_of_ogScale_3() { return &___ogScale_3; }
	inline void set_ogScale_3(Vector3_t2243707580  value)
	{
		___ogScale_3 = value;
	}

	inline static int32_t get_offset_of_finalScaleUp_4() { return static_cast<int32_t>(offsetof(ControllerHover_t24802818, ___finalScaleUp_4)); }
	inline Vector3_t2243707580  get_finalScaleUp_4() const { return ___finalScaleUp_4; }
	inline Vector3_t2243707580 * get_address_of_finalScaleUp_4() { return &___finalScaleUp_4; }
	inline void set_finalScaleUp_4(Vector3_t2243707580  value)
	{
		___finalScaleUp_4 = value;
	}

	inline static int32_t get_offset_of_finalScaleDown_5() { return static_cast<int32_t>(offsetof(ControllerHover_t24802818, ___finalScaleDown_5)); }
	inline Vector3_t2243707580  get_finalScaleDown_5() const { return ___finalScaleDown_5; }
	inline Vector3_t2243707580 * get_address_of_finalScaleDown_5() { return &___finalScaleDown_5; }
	inline void set_finalScaleDown_5(Vector3_t2243707580  value)
	{
		___finalScaleDown_5 = value;
	}

	inline static int32_t get_offset_of_currentScale_6() { return static_cast<int32_t>(offsetof(ControllerHover_t24802818, ___currentScale_6)); }
	inline Vector3_t2243707580  get_currentScale_6() const { return ___currentScale_6; }
	inline Vector3_t2243707580 * get_address_of_currentScale_6() { return &___currentScale_6; }
	inline void set_currentScale_6(Vector3_t2243707580  value)
	{
		___currentScale_6 = value;
	}

	inline static int32_t get_offset_of_ogColor_7() { return static_cast<int32_t>(offsetof(ControllerHover_t24802818, ___ogColor_7)); }
	inline Color_t2020392075  get_ogColor_7() const { return ___ogColor_7; }
	inline Color_t2020392075 * get_address_of_ogColor_7() { return &___ogColor_7; }
	inline void set_ogColor_7(Color_t2020392075  value)
	{
		___ogColor_7 = value;
	}

	inline static int32_t get_offset_of_holverColor_8() { return static_cast<int32_t>(offsetof(ControllerHover_t24802818, ___holverColor_8)); }
	inline Color_t2020392075  get_holverColor_8() const { return ___holverColor_8; }
	inline Color_t2020392075 * get_address_of_holverColor_8() { return &___holverColor_8; }
	inline void set_holverColor_8(Color_t2020392075  value)
	{
		___holverColor_8 = value;
	}

	inline static int32_t get_offset_of_hovering_9() { return static_cast<int32_t>(offsetof(ControllerHover_t24802818, ___hovering_9)); }
	inline bool get_hovering_9() const { return ___hovering_9; }
	inline bool* get_address_of_hovering_9() { return &___hovering_9; }
	inline void set_hovering_9(bool value)
	{
		___hovering_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
