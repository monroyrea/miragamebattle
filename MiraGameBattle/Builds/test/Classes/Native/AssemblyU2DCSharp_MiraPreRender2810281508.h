﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Camera
struct Camera_t189460977;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraPreRender
struct  MiraPreRender_t2810281508  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera MiraPreRender::<cam>k__BackingField
	Camera_t189460977 * ___U3CcamU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CcamU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MiraPreRender_t2810281508, ___U3CcamU3Ek__BackingField_2)); }
	inline Camera_t189460977 * get_U3CcamU3Ek__BackingField_2() const { return ___U3CcamU3Ek__BackingField_2; }
	inline Camera_t189460977 ** get_address_of_U3CcamU3Ek__BackingField_2() { return &___U3CcamU3Ek__BackingField_2; }
	inline void set_U3CcamU3Ek__BackingField_2(Camera_t189460977 * value)
	{
		___U3CcamU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcamU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
