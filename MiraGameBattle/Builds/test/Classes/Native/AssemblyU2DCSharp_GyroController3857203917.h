﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GyroController
struct  GyroController_t3857203917  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean GyroController::gyroEnabled
	bool ___gyroEnabled_2;
	// System.Single GyroController::lowPassFilterFactor
	float ___lowPassFilterFactor_3;
	// UnityEngine.Quaternion GyroController::baseIdentity
	Quaternion_t4030073918  ___baseIdentity_4;
	// UnityEngine.Quaternion GyroController::landscapeRight
	Quaternion_t4030073918  ___landscapeRight_5;
	// UnityEngine.Quaternion GyroController::landscapeLeft
	Quaternion_t4030073918  ___landscapeLeft_6;
	// UnityEngine.Quaternion GyroController::upsideDown
	Quaternion_t4030073918  ___upsideDown_7;
	// UnityEngine.Quaternion GyroController::frontCamera
	Quaternion_t4030073918  ___frontCamera_8;
	// UnityEngine.Quaternion GyroController::cameraBase
	Quaternion_t4030073918  ___cameraBase_9;
	// UnityEngine.Quaternion GyroController::calibration
	Quaternion_t4030073918  ___calibration_10;
	// UnityEngine.Quaternion GyroController::baseOrientation
	Quaternion_t4030073918  ___baseOrientation_11;
	// UnityEngine.Quaternion GyroController::baseOrientationRotationFix
	Quaternion_t4030073918  ___baseOrientationRotationFix_12;
	// UnityEngine.Quaternion GyroController::referenceRotation
	Quaternion_t4030073918  ___referenceRotation_13;
	// System.Boolean GyroController::debugGUI
	bool ___debugGUI_14;
	// System.Boolean GyroController::useFrontCamera
	bool ___useFrontCamera_15;

public:
	inline static int32_t get_offset_of_gyroEnabled_2() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___gyroEnabled_2)); }
	inline bool get_gyroEnabled_2() const { return ___gyroEnabled_2; }
	inline bool* get_address_of_gyroEnabled_2() { return &___gyroEnabled_2; }
	inline void set_gyroEnabled_2(bool value)
	{
		___gyroEnabled_2 = value;
	}

	inline static int32_t get_offset_of_lowPassFilterFactor_3() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___lowPassFilterFactor_3)); }
	inline float get_lowPassFilterFactor_3() const { return ___lowPassFilterFactor_3; }
	inline float* get_address_of_lowPassFilterFactor_3() { return &___lowPassFilterFactor_3; }
	inline void set_lowPassFilterFactor_3(float value)
	{
		___lowPassFilterFactor_3 = value;
	}

	inline static int32_t get_offset_of_baseIdentity_4() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___baseIdentity_4)); }
	inline Quaternion_t4030073918  get_baseIdentity_4() const { return ___baseIdentity_4; }
	inline Quaternion_t4030073918 * get_address_of_baseIdentity_4() { return &___baseIdentity_4; }
	inline void set_baseIdentity_4(Quaternion_t4030073918  value)
	{
		___baseIdentity_4 = value;
	}

	inline static int32_t get_offset_of_landscapeRight_5() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___landscapeRight_5)); }
	inline Quaternion_t4030073918  get_landscapeRight_5() const { return ___landscapeRight_5; }
	inline Quaternion_t4030073918 * get_address_of_landscapeRight_5() { return &___landscapeRight_5; }
	inline void set_landscapeRight_5(Quaternion_t4030073918  value)
	{
		___landscapeRight_5 = value;
	}

	inline static int32_t get_offset_of_landscapeLeft_6() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___landscapeLeft_6)); }
	inline Quaternion_t4030073918  get_landscapeLeft_6() const { return ___landscapeLeft_6; }
	inline Quaternion_t4030073918 * get_address_of_landscapeLeft_6() { return &___landscapeLeft_6; }
	inline void set_landscapeLeft_6(Quaternion_t4030073918  value)
	{
		___landscapeLeft_6 = value;
	}

	inline static int32_t get_offset_of_upsideDown_7() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___upsideDown_7)); }
	inline Quaternion_t4030073918  get_upsideDown_7() const { return ___upsideDown_7; }
	inline Quaternion_t4030073918 * get_address_of_upsideDown_7() { return &___upsideDown_7; }
	inline void set_upsideDown_7(Quaternion_t4030073918  value)
	{
		___upsideDown_7 = value;
	}

	inline static int32_t get_offset_of_frontCamera_8() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___frontCamera_8)); }
	inline Quaternion_t4030073918  get_frontCamera_8() const { return ___frontCamera_8; }
	inline Quaternion_t4030073918 * get_address_of_frontCamera_8() { return &___frontCamera_8; }
	inline void set_frontCamera_8(Quaternion_t4030073918  value)
	{
		___frontCamera_8 = value;
	}

	inline static int32_t get_offset_of_cameraBase_9() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___cameraBase_9)); }
	inline Quaternion_t4030073918  get_cameraBase_9() const { return ___cameraBase_9; }
	inline Quaternion_t4030073918 * get_address_of_cameraBase_9() { return &___cameraBase_9; }
	inline void set_cameraBase_9(Quaternion_t4030073918  value)
	{
		___cameraBase_9 = value;
	}

	inline static int32_t get_offset_of_calibration_10() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___calibration_10)); }
	inline Quaternion_t4030073918  get_calibration_10() const { return ___calibration_10; }
	inline Quaternion_t4030073918 * get_address_of_calibration_10() { return &___calibration_10; }
	inline void set_calibration_10(Quaternion_t4030073918  value)
	{
		___calibration_10 = value;
	}

	inline static int32_t get_offset_of_baseOrientation_11() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___baseOrientation_11)); }
	inline Quaternion_t4030073918  get_baseOrientation_11() const { return ___baseOrientation_11; }
	inline Quaternion_t4030073918 * get_address_of_baseOrientation_11() { return &___baseOrientation_11; }
	inline void set_baseOrientation_11(Quaternion_t4030073918  value)
	{
		___baseOrientation_11 = value;
	}

	inline static int32_t get_offset_of_baseOrientationRotationFix_12() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___baseOrientationRotationFix_12)); }
	inline Quaternion_t4030073918  get_baseOrientationRotationFix_12() const { return ___baseOrientationRotationFix_12; }
	inline Quaternion_t4030073918 * get_address_of_baseOrientationRotationFix_12() { return &___baseOrientationRotationFix_12; }
	inline void set_baseOrientationRotationFix_12(Quaternion_t4030073918  value)
	{
		___baseOrientationRotationFix_12 = value;
	}

	inline static int32_t get_offset_of_referenceRotation_13() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___referenceRotation_13)); }
	inline Quaternion_t4030073918  get_referenceRotation_13() const { return ___referenceRotation_13; }
	inline Quaternion_t4030073918 * get_address_of_referenceRotation_13() { return &___referenceRotation_13; }
	inline void set_referenceRotation_13(Quaternion_t4030073918  value)
	{
		___referenceRotation_13 = value;
	}

	inline static int32_t get_offset_of_debugGUI_14() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___debugGUI_14)); }
	inline bool get_debugGUI_14() const { return ___debugGUI_14; }
	inline bool* get_address_of_debugGUI_14() { return &___debugGUI_14; }
	inline void set_debugGUI_14(bool value)
	{
		___debugGUI_14 = value;
	}

	inline static int32_t get_offset_of_useFrontCamera_15() { return static_cast<int32_t>(offsetof(GyroController_t3857203917, ___useFrontCamera_15)); }
	inline bool get_useFrontCamera_15() const { return ___useFrontCamera_15; }
	inline bool* get_address_of_useFrontCamera_15() { return &___useFrontCamera_15; }
	inline void set_useFrontCamera_15(bool value)
	{
		___useFrontCamera_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
