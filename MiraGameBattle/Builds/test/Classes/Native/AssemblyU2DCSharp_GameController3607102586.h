﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// ControllerTransform
struct ControllerTransform_t2066672932;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController
struct  GameController_t3607102586  : public MonoBehaviour_t1158329972
{
public:
	// System.String GameController::playerToSpawn
	String_t* ___playerToSpawn_2;
	// UnityEngine.GameObject[] GameController::enemies
	GameObjectU5BU5D_t3057952154* ___enemies_3;
	// UnityEngine.GameObject[] GameController::players
	GameObjectU5BU5D_t3057952154* ___players_4;
	// System.Int32 GameController::m_frameCounter
	int32_t ___m_frameCounter_5;
	// System.Single GameController::m_timeCounter
	float ___m_timeCounter_6;
	// System.Single GameController::m_refreshTime
	float ___m_refreshTime_7;
	// UnityEngine.UI.Text GameController::FPS
	Text_t356221433 * ___FPS_8;
	// UnityEngine.GameObject GameController::selectCharacter
	GameObject_t1756533147 * ___selectCharacter_9;
	// ControllerTransform GameController::ct
	ControllerTransform_t2066672932 * ___ct_10;
	// UnityEngine.GameObject GameController::laser
	GameObject_t1756533147 * ___laser_11;
	// UnityEngine.GameObject GameController::setupUI
	GameObject_t1756533147 * ___setupUI_12;
	// UnityEngine.GameObject GameController::battleUI
	GameObject_t1756533147 * ___battleUI_13;
	// UnityEngine.GameObject GameController::boardObjects
	GameObject_t1756533147 * ___boardObjects_14;
	// UnityEngine.GameObject GameController::playerSelection
	GameObject_t1756533147 * ___playerSelection_15;
	// System.Int32 GameController::count
	int32_t ___count_16;
	// UnityEngine.GameObject GameController::battleBoard
	GameObject_t1756533147 * ___battleBoard_17;
	// UnityEngine.MeshRenderer GameController::rend
	MeshRenderer_t1268241104 * ___rend_18;
	// System.Boolean GameController::moveBattle
	bool ___moveBattle_19;
	// System.Single GameController::lastCast
	float ___lastCast_20;

public:
	inline static int32_t get_offset_of_playerToSpawn_2() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___playerToSpawn_2)); }
	inline String_t* get_playerToSpawn_2() const { return ___playerToSpawn_2; }
	inline String_t** get_address_of_playerToSpawn_2() { return &___playerToSpawn_2; }
	inline void set_playerToSpawn_2(String_t* value)
	{
		___playerToSpawn_2 = value;
		Il2CppCodeGenWriteBarrier(&___playerToSpawn_2, value);
	}

	inline static int32_t get_offset_of_enemies_3() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___enemies_3)); }
	inline GameObjectU5BU5D_t3057952154* get_enemies_3() const { return ___enemies_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_enemies_3() { return &___enemies_3; }
	inline void set_enemies_3(GameObjectU5BU5D_t3057952154* value)
	{
		___enemies_3 = value;
		Il2CppCodeGenWriteBarrier(&___enemies_3, value);
	}

	inline static int32_t get_offset_of_players_4() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___players_4)); }
	inline GameObjectU5BU5D_t3057952154* get_players_4() const { return ___players_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_players_4() { return &___players_4; }
	inline void set_players_4(GameObjectU5BU5D_t3057952154* value)
	{
		___players_4 = value;
		Il2CppCodeGenWriteBarrier(&___players_4, value);
	}

	inline static int32_t get_offset_of_m_frameCounter_5() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___m_frameCounter_5)); }
	inline int32_t get_m_frameCounter_5() const { return ___m_frameCounter_5; }
	inline int32_t* get_address_of_m_frameCounter_5() { return &___m_frameCounter_5; }
	inline void set_m_frameCounter_5(int32_t value)
	{
		___m_frameCounter_5 = value;
	}

	inline static int32_t get_offset_of_m_timeCounter_6() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___m_timeCounter_6)); }
	inline float get_m_timeCounter_6() const { return ___m_timeCounter_6; }
	inline float* get_address_of_m_timeCounter_6() { return &___m_timeCounter_6; }
	inline void set_m_timeCounter_6(float value)
	{
		___m_timeCounter_6 = value;
	}

	inline static int32_t get_offset_of_m_refreshTime_7() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___m_refreshTime_7)); }
	inline float get_m_refreshTime_7() const { return ___m_refreshTime_7; }
	inline float* get_address_of_m_refreshTime_7() { return &___m_refreshTime_7; }
	inline void set_m_refreshTime_7(float value)
	{
		___m_refreshTime_7 = value;
	}

	inline static int32_t get_offset_of_FPS_8() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___FPS_8)); }
	inline Text_t356221433 * get_FPS_8() const { return ___FPS_8; }
	inline Text_t356221433 ** get_address_of_FPS_8() { return &___FPS_8; }
	inline void set_FPS_8(Text_t356221433 * value)
	{
		___FPS_8 = value;
		Il2CppCodeGenWriteBarrier(&___FPS_8, value);
	}

	inline static int32_t get_offset_of_selectCharacter_9() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___selectCharacter_9)); }
	inline GameObject_t1756533147 * get_selectCharacter_9() const { return ___selectCharacter_9; }
	inline GameObject_t1756533147 ** get_address_of_selectCharacter_9() { return &___selectCharacter_9; }
	inline void set_selectCharacter_9(GameObject_t1756533147 * value)
	{
		___selectCharacter_9 = value;
		Il2CppCodeGenWriteBarrier(&___selectCharacter_9, value);
	}

	inline static int32_t get_offset_of_ct_10() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___ct_10)); }
	inline ControllerTransform_t2066672932 * get_ct_10() const { return ___ct_10; }
	inline ControllerTransform_t2066672932 ** get_address_of_ct_10() { return &___ct_10; }
	inline void set_ct_10(ControllerTransform_t2066672932 * value)
	{
		___ct_10 = value;
		Il2CppCodeGenWriteBarrier(&___ct_10, value);
	}

	inline static int32_t get_offset_of_laser_11() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___laser_11)); }
	inline GameObject_t1756533147 * get_laser_11() const { return ___laser_11; }
	inline GameObject_t1756533147 ** get_address_of_laser_11() { return &___laser_11; }
	inline void set_laser_11(GameObject_t1756533147 * value)
	{
		___laser_11 = value;
		Il2CppCodeGenWriteBarrier(&___laser_11, value);
	}

	inline static int32_t get_offset_of_setupUI_12() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___setupUI_12)); }
	inline GameObject_t1756533147 * get_setupUI_12() const { return ___setupUI_12; }
	inline GameObject_t1756533147 ** get_address_of_setupUI_12() { return &___setupUI_12; }
	inline void set_setupUI_12(GameObject_t1756533147 * value)
	{
		___setupUI_12 = value;
		Il2CppCodeGenWriteBarrier(&___setupUI_12, value);
	}

	inline static int32_t get_offset_of_battleUI_13() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___battleUI_13)); }
	inline GameObject_t1756533147 * get_battleUI_13() const { return ___battleUI_13; }
	inline GameObject_t1756533147 ** get_address_of_battleUI_13() { return &___battleUI_13; }
	inline void set_battleUI_13(GameObject_t1756533147 * value)
	{
		___battleUI_13 = value;
		Il2CppCodeGenWriteBarrier(&___battleUI_13, value);
	}

	inline static int32_t get_offset_of_boardObjects_14() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___boardObjects_14)); }
	inline GameObject_t1756533147 * get_boardObjects_14() const { return ___boardObjects_14; }
	inline GameObject_t1756533147 ** get_address_of_boardObjects_14() { return &___boardObjects_14; }
	inline void set_boardObjects_14(GameObject_t1756533147 * value)
	{
		___boardObjects_14 = value;
		Il2CppCodeGenWriteBarrier(&___boardObjects_14, value);
	}

	inline static int32_t get_offset_of_playerSelection_15() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___playerSelection_15)); }
	inline GameObject_t1756533147 * get_playerSelection_15() const { return ___playerSelection_15; }
	inline GameObject_t1756533147 ** get_address_of_playerSelection_15() { return &___playerSelection_15; }
	inline void set_playerSelection_15(GameObject_t1756533147 * value)
	{
		___playerSelection_15 = value;
		Il2CppCodeGenWriteBarrier(&___playerSelection_15, value);
	}

	inline static int32_t get_offset_of_count_16() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___count_16)); }
	inline int32_t get_count_16() const { return ___count_16; }
	inline int32_t* get_address_of_count_16() { return &___count_16; }
	inline void set_count_16(int32_t value)
	{
		___count_16 = value;
	}

	inline static int32_t get_offset_of_battleBoard_17() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___battleBoard_17)); }
	inline GameObject_t1756533147 * get_battleBoard_17() const { return ___battleBoard_17; }
	inline GameObject_t1756533147 ** get_address_of_battleBoard_17() { return &___battleBoard_17; }
	inline void set_battleBoard_17(GameObject_t1756533147 * value)
	{
		___battleBoard_17 = value;
		Il2CppCodeGenWriteBarrier(&___battleBoard_17, value);
	}

	inline static int32_t get_offset_of_rend_18() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___rend_18)); }
	inline MeshRenderer_t1268241104 * get_rend_18() const { return ___rend_18; }
	inline MeshRenderer_t1268241104 ** get_address_of_rend_18() { return &___rend_18; }
	inline void set_rend_18(MeshRenderer_t1268241104 * value)
	{
		___rend_18 = value;
		Il2CppCodeGenWriteBarrier(&___rend_18, value);
	}

	inline static int32_t get_offset_of_moveBattle_19() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___moveBattle_19)); }
	inline bool get_moveBattle_19() const { return ___moveBattle_19; }
	inline bool* get_address_of_moveBattle_19() { return &___moveBattle_19; }
	inline void set_moveBattle_19(bool value)
	{
		___moveBattle_19 = value;
	}

	inline static int32_t get_offset_of_lastCast_20() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___lastCast_20)); }
	inline float get_lastCast_20() const { return ___lastCast_20; }
	inline float* get_address_of_lastCast_20() { return &___lastCast_20; }
	inline void set_lastCast_20(float value)
	{
		___lastCast_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
