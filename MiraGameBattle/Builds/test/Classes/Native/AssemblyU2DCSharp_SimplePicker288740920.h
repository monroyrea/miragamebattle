﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerTy123719274.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"

// Ximmerse.InputSystem.ControllerInput
struct ControllerInput_t1382602808;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t462843629;
// System.Collections.Generic.List`1<SimplePicker/PickObject>
struct List_1_t1968018177;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplePicker
struct  SimplePicker_t288740920  : public MonoBehaviour_t1158329972
{
public:
	// Ximmerse.InputSystem.ControllerType SimplePicker::m_ControllerType
	int32_t ___m_ControllerType_2;
	// Ximmerse.InputSystem.ControllerInput SimplePicker::m_ControllerInput
	ControllerInput_t1382602808 * ___m_ControllerInput_3;
	// UnityEngine.Transform SimplePicker::m_Container
	Transform_t3275118058 * ___m_Container_4;
	// UnityEngine.Transform SimplePicker::m_Point
	Transform_t3275118058 * ___m_Point_5;
	// UnityEngine.LayerMask SimplePicker::m_LayerMask
	LayerMask_t3188175821  ___m_LayerMask_6;
	// System.Int32 SimplePicker::layerMask
	int32_t ___layerMask_7;
	// System.Single SimplePicker::m_Radius
	float ___m_Radius_8;
	// System.Int32 SimplePicker::m_MaxObjects
	int32_t ___m_MaxObjects_9;
	// UnityEngine.Collider[] SimplePicker::m_Colliders
	ColliderU5BU5D_t462843629* ___m_Colliders_10;
	// System.Collections.Generic.List`1<SimplePicker/PickObject> SimplePicker::m_PickObjects
	List_1_t1968018177 * ___m_PickObjects_11;

public:
	inline static int32_t get_offset_of_m_ControllerType_2() { return static_cast<int32_t>(offsetof(SimplePicker_t288740920, ___m_ControllerType_2)); }
	inline int32_t get_m_ControllerType_2() const { return ___m_ControllerType_2; }
	inline int32_t* get_address_of_m_ControllerType_2() { return &___m_ControllerType_2; }
	inline void set_m_ControllerType_2(int32_t value)
	{
		___m_ControllerType_2 = value;
	}

	inline static int32_t get_offset_of_m_ControllerInput_3() { return static_cast<int32_t>(offsetof(SimplePicker_t288740920, ___m_ControllerInput_3)); }
	inline ControllerInput_t1382602808 * get_m_ControllerInput_3() const { return ___m_ControllerInput_3; }
	inline ControllerInput_t1382602808 ** get_address_of_m_ControllerInput_3() { return &___m_ControllerInput_3; }
	inline void set_m_ControllerInput_3(ControllerInput_t1382602808 * value)
	{
		___m_ControllerInput_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_ControllerInput_3, value);
	}

	inline static int32_t get_offset_of_m_Container_4() { return static_cast<int32_t>(offsetof(SimplePicker_t288740920, ___m_Container_4)); }
	inline Transform_t3275118058 * get_m_Container_4() const { return ___m_Container_4; }
	inline Transform_t3275118058 ** get_address_of_m_Container_4() { return &___m_Container_4; }
	inline void set_m_Container_4(Transform_t3275118058 * value)
	{
		___m_Container_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_Container_4, value);
	}

	inline static int32_t get_offset_of_m_Point_5() { return static_cast<int32_t>(offsetof(SimplePicker_t288740920, ___m_Point_5)); }
	inline Transform_t3275118058 * get_m_Point_5() const { return ___m_Point_5; }
	inline Transform_t3275118058 ** get_address_of_m_Point_5() { return &___m_Point_5; }
	inline void set_m_Point_5(Transform_t3275118058 * value)
	{
		___m_Point_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_Point_5, value);
	}

	inline static int32_t get_offset_of_m_LayerMask_6() { return static_cast<int32_t>(offsetof(SimplePicker_t288740920, ___m_LayerMask_6)); }
	inline LayerMask_t3188175821  get_m_LayerMask_6() const { return ___m_LayerMask_6; }
	inline LayerMask_t3188175821 * get_address_of_m_LayerMask_6() { return &___m_LayerMask_6; }
	inline void set_m_LayerMask_6(LayerMask_t3188175821  value)
	{
		___m_LayerMask_6 = value;
	}

	inline static int32_t get_offset_of_layerMask_7() { return static_cast<int32_t>(offsetof(SimplePicker_t288740920, ___layerMask_7)); }
	inline int32_t get_layerMask_7() const { return ___layerMask_7; }
	inline int32_t* get_address_of_layerMask_7() { return &___layerMask_7; }
	inline void set_layerMask_7(int32_t value)
	{
		___layerMask_7 = value;
	}

	inline static int32_t get_offset_of_m_Radius_8() { return static_cast<int32_t>(offsetof(SimplePicker_t288740920, ___m_Radius_8)); }
	inline float get_m_Radius_8() const { return ___m_Radius_8; }
	inline float* get_address_of_m_Radius_8() { return &___m_Radius_8; }
	inline void set_m_Radius_8(float value)
	{
		___m_Radius_8 = value;
	}

	inline static int32_t get_offset_of_m_MaxObjects_9() { return static_cast<int32_t>(offsetof(SimplePicker_t288740920, ___m_MaxObjects_9)); }
	inline int32_t get_m_MaxObjects_9() const { return ___m_MaxObjects_9; }
	inline int32_t* get_address_of_m_MaxObjects_9() { return &___m_MaxObjects_9; }
	inline void set_m_MaxObjects_9(int32_t value)
	{
		___m_MaxObjects_9 = value;
	}

	inline static int32_t get_offset_of_m_Colliders_10() { return static_cast<int32_t>(offsetof(SimplePicker_t288740920, ___m_Colliders_10)); }
	inline ColliderU5BU5D_t462843629* get_m_Colliders_10() const { return ___m_Colliders_10; }
	inline ColliderU5BU5D_t462843629** get_address_of_m_Colliders_10() { return &___m_Colliders_10; }
	inline void set_m_Colliders_10(ColliderU5BU5D_t462843629* value)
	{
		___m_Colliders_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_Colliders_10, value);
	}

	inline static int32_t get_offset_of_m_PickObjects_11() { return static_cast<int32_t>(offsetof(SimplePicker_t288740920, ___m_PickObjects_11)); }
	inline List_1_t1968018177 * get_m_PickObjects_11() const { return ___m_PickObjects_11; }
	inline List_1_t1968018177 ** get_address_of_m_PickObjects_11() { return &___m_PickObjects_11; }
	inline void set_m_PickObjects_11(List_1_t1968018177 * value)
	{
		___m_PickObjects_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_PickObjects_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
