﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// cameraController
struct  cameraController_t4112230091  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct cameraController_t4112230091_StaticFields
{
public:
	// System.Boolean cameraController::created1
	bool ___created1_2;

public:
	inline static int32_t get_offset_of_created1_2() { return static_cast<int32_t>(offsetof(cameraController_t4112230091_StaticFields, ___created1_2)); }
	inline bool get_created1_2() const { return ___created1_2; }
	inline bool* get_address_of_created1_2() { return &___created1_2; }
	inline void set_created1_2(bool value)
	{
		___created1_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
