﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Kudan.AR.KudanTracker
struct KudanTracker_t2134143885;
// System.String
struct String_t;
// Kudan.AR.TrackerBase
struct TrackerBase_t1411547817;
// Kudan.AR.Trackable[]
struct TrackableU5BU5D_t871956176;
// Kudan.AR.TrackingMethodBase
struct TrackingMethodBase_t2810511357;
// Kudan.AR.TrackingMethodBase[]
struct TrackingMethodBaseU5BU5D_t567744368;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kudan.AR.KudanTracker
struct  KudanTracker_t2134143885  : public MonoBehaviour_t1158329972
{
public:
	// System.String Kudan.AR.KudanTracker::_EditorAPIKey
	String_t* ____EditorAPIKey_5;
	// Kudan.AR.TrackerBase Kudan.AR.KudanTracker::_trackerPlugin
	TrackerBase_t1411547817 * ____trackerPlugin_6;
	// Kudan.AR.Trackable[] Kudan.AR.KudanTracker::_lastDetectedTrackables
	TrackableU5BU5D_t871956176* ____lastDetectedTrackables_7;
	// System.String Kudan.AR.KudanTracker::_APIKey
	String_t* ____APIKey_8;
	// Kudan.AR.TrackingMethodBase Kudan.AR.KudanTracker::_defaultTrackingMethod
	TrackingMethodBase_t2810511357 * ____defaultTrackingMethod_9;
	// Kudan.AR.TrackingMethodBase[] Kudan.AR.KudanTracker::_trackingMethods
	TrackingMethodBaseU5BU5D_t567744368* ____trackingMethods_10;
	// System.Boolean Kudan.AR.KudanTracker::_useFrontFacingCameraOnMobile
	bool ____useFrontFacingCameraOnMobile_11;
	// System.Boolean Kudan.AR.KudanTracker::_markerRecoveryMode
	bool ____markerRecoveryMode_12;
	// System.Boolean Kudan.AR.KudanTracker::_markerAutoCrop
	bool ____markerAutoCrop_13;
	// System.Boolean Kudan.AR.KudanTracker::_markerExtensibility
	bool ____markerExtensibility_14;
	// System.Boolean Kudan.AR.KudanTracker::_makePersistent
	bool ____makePersistent_15;
	// System.Boolean Kudan.AR.KudanTracker::_startOnEnable
	bool ____startOnEnable_16;
	// System.Boolean Kudan.AR.KudanTracker::_applyProjection
	bool ____applyProjection_17;
	// UnityEngine.Camera Kudan.AR.KudanTracker::_renderingCamera
	Camera_t189460977 * ____renderingCamera_18;
	// UnityEngine.Renderer Kudan.AR.KudanTracker::_background
	Renderer_t257310565 * ____background_19;
	// System.Boolean Kudan.AR.KudanTracker::_displayDebugGUI
	bool ____displayDebugGUI_20;
	// System.Int32 Kudan.AR.KudanTracker::_debugGUIScale
	int32_t ____debugGUIScale_21;
	// UnityEngine.Shader Kudan.AR.KudanTracker::_debugFlatShader
	Shader_t2430389951 * ____debugFlatShader_22;
	// Kudan.AR.TrackingMethodBase Kudan.AR.KudanTracker::_currentTrackingMethod
	TrackingMethodBase_t2810511357 * ____currentTrackingMethod_23;
	// UnityEngine.Material Kudan.AR.KudanTracker::_lineMaterial
	Material_t193706927 * ____lineMaterial_24;

public:
	inline static int32_t get_offset_of__EditorAPIKey_5() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____EditorAPIKey_5)); }
	inline String_t* get__EditorAPIKey_5() const { return ____EditorAPIKey_5; }
	inline String_t** get_address_of__EditorAPIKey_5() { return &____EditorAPIKey_5; }
	inline void set__EditorAPIKey_5(String_t* value)
	{
		____EditorAPIKey_5 = value;
		Il2CppCodeGenWriteBarrier(&____EditorAPIKey_5, value);
	}

	inline static int32_t get_offset_of__trackerPlugin_6() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____trackerPlugin_6)); }
	inline TrackerBase_t1411547817 * get__trackerPlugin_6() const { return ____trackerPlugin_6; }
	inline TrackerBase_t1411547817 ** get_address_of__trackerPlugin_6() { return &____trackerPlugin_6; }
	inline void set__trackerPlugin_6(TrackerBase_t1411547817 * value)
	{
		____trackerPlugin_6 = value;
		Il2CppCodeGenWriteBarrier(&____trackerPlugin_6, value);
	}

	inline static int32_t get_offset_of__lastDetectedTrackables_7() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____lastDetectedTrackables_7)); }
	inline TrackableU5BU5D_t871956176* get__lastDetectedTrackables_7() const { return ____lastDetectedTrackables_7; }
	inline TrackableU5BU5D_t871956176** get_address_of__lastDetectedTrackables_7() { return &____lastDetectedTrackables_7; }
	inline void set__lastDetectedTrackables_7(TrackableU5BU5D_t871956176* value)
	{
		____lastDetectedTrackables_7 = value;
		Il2CppCodeGenWriteBarrier(&____lastDetectedTrackables_7, value);
	}

	inline static int32_t get_offset_of__APIKey_8() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____APIKey_8)); }
	inline String_t* get__APIKey_8() const { return ____APIKey_8; }
	inline String_t** get_address_of__APIKey_8() { return &____APIKey_8; }
	inline void set__APIKey_8(String_t* value)
	{
		____APIKey_8 = value;
		Il2CppCodeGenWriteBarrier(&____APIKey_8, value);
	}

	inline static int32_t get_offset_of__defaultTrackingMethod_9() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____defaultTrackingMethod_9)); }
	inline TrackingMethodBase_t2810511357 * get__defaultTrackingMethod_9() const { return ____defaultTrackingMethod_9; }
	inline TrackingMethodBase_t2810511357 ** get_address_of__defaultTrackingMethod_9() { return &____defaultTrackingMethod_9; }
	inline void set__defaultTrackingMethod_9(TrackingMethodBase_t2810511357 * value)
	{
		____defaultTrackingMethod_9 = value;
		Il2CppCodeGenWriteBarrier(&____defaultTrackingMethod_9, value);
	}

	inline static int32_t get_offset_of__trackingMethods_10() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____trackingMethods_10)); }
	inline TrackingMethodBaseU5BU5D_t567744368* get__trackingMethods_10() const { return ____trackingMethods_10; }
	inline TrackingMethodBaseU5BU5D_t567744368** get_address_of__trackingMethods_10() { return &____trackingMethods_10; }
	inline void set__trackingMethods_10(TrackingMethodBaseU5BU5D_t567744368* value)
	{
		____trackingMethods_10 = value;
		Il2CppCodeGenWriteBarrier(&____trackingMethods_10, value);
	}

	inline static int32_t get_offset_of__useFrontFacingCameraOnMobile_11() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____useFrontFacingCameraOnMobile_11)); }
	inline bool get__useFrontFacingCameraOnMobile_11() const { return ____useFrontFacingCameraOnMobile_11; }
	inline bool* get_address_of__useFrontFacingCameraOnMobile_11() { return &____useFrontFacingCameraOnMobile_11; }
	inline void set__useFrontFacingCameraOnMobile_11(bool value)
	{
		____useFrontFacingCameraOnMobile_11 = value;
	}

	inline static int32_t get_offset_of__markerRecoveryMode_12() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____markerRecoveryMode_12)); }
	inline bool get__markerRecoveryMode_12() const { return ____markerRecoveryMode_12; }
	inline bool* get_address_of__markerRecoveryMode_12() { return &____markerRecoveryMode_12; }
	inline void set__markerRecoveryMode_12(bool value)
	{
		____markerRecoveryMode_12 = value;
	}

	inline static int32_t get_offset_of__markerAutoCrop_13() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____markerAutoCrop_13)); }
	inline bool get__markerAutoCrop_13() const { return ____markerAutoCrop_13; }
	inline bool* get_address_of__markerAutoCrop_13() { return &____markerAutoCrop_13; }
	inline void set__markerAutoCrop_13(bool value)
	{
		____markerAutoCrop_13 = value;
	}

	inline static int32_t get_offset_of__markerExtensibility_14() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____markerExtensibility_14)); }
	inline bool get__markerExtensibility_14() const { return ____markerExtensibility_14; }
	inline bool* get_address_of__markerExtensibility_14() { return &____markerExtensibility_14; }
	inline void set__markerExtensibility_14(bool value)
	{
		____markerExtensibility_14 = value;
	}

	inline static int32_t get_offset_of__makePersistent_15() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____makePersistent_15)); }
	inline bool get__makePersistent_15() const { return ____makePersistent_15; }
	inline bool* get_address_of__makePersistent_15() { return &____makePersistent_15; }
	inline void set__makePersistent_15(bool value)
	{
		____makePersistent_15 = value;
	}

	inline static int32_t get_offset_of__startOnEnable_16() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____startOnEnable_16)); }
	inline bool get__startOnEnable_16() const { return ____startOnEnable_16; }
	inline bool* get_address_of__startOnEnable_16() { return &____startOnEnable_16; }
	inline void set__startOnEnable_16(bool value)
	{
		____startOnEnable_16 = value;
	}

	inline static int32_t get_offset_of__applyProjection_17() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____applyProjection_17)); }
	inline bool get__applyProjection_17() const { return ____applyProjection_17; }
	inline bool* get_address_of__applyProjection_17() { return &____applyProjection_17; }
	inline void set__applyProjection_17(bool value)
	{
		____applyProjection_17 = value;
	}

	inline static int32_t get_offset_of__renderingCamera_18() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____renderingCamera_18)); }
	inline Camera_t189460977 * get__renderingCamera_18() const { return ____renderingCamera_18; }
	inline Camera_t189460977 ** get_address_of__renderingCamera_18() { return &____renderingCamera_18; }
	inline void set__renderingCamera_18(Camera_t189460977 * value)
	{
		____renderingCamera_18 = value;
		Il2CppCodeGenWriteBarrier(&____renderingCamera_18, value);
	}

	inline static int32_t get_offset_of__background_19() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____background_19)); }
	inline Renderer_t257310565 * get__background_19() const { return ____background_19; }
	inline Renderer_t257310565 ** get_address_of__background_19() { return &____background_19; }
	inline void set__background_19(Renderer_t257310565 * value)
	{
		____background_19 = value;
		Il2CppCodeGenWriteBarrier(&____background_19, value);
	}

	inline static int32_t get_offset_of__displayDebugGUI_20() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____displayDebugGUI_20)); }
	inline bool get__displayDebugGUI_20() const { return ____displayDebugGUI_20; }
	inline bool* get_address_of__displayDebugGUI_20() { return &____displayDebugGUI_20; }
	inline void set__displayDebugGUI_20(bool value)
	{
		____displayDebugGUI_20 = value;
	}

	inline static int32_t get_offset_of__debugGUIScale_21() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____debugGUIScale_21)); }
	inline int32_t get__debugGUIScale_21() const { return ____debugGUIScale_21; }
	inline int32_t* get_address_of__debugGUIScale_21() { return &____debugGUIScale_21; }
	inline void set__debugGUIScale_21(int32_t value)
	{
		____debugGUIScale_21 = value;
	}

	inline static int32_t get_offset_of__debugFlatShader_22() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____debugFlatShader_22)); }
	inline Shader_t2430389951 * get__debugFlatShader_22() const { return ____debugFlatShader_22; }
	inline Shader_t2430389951 ** get_address_of__debugFlatShader_22() { return &____debugFlatShader_22; }
	inline void set__debugFlatShader_22(Shader_t2430389951 * value)
	{
		____debugFlatShader_22 = value;
		Il2CppCodeGenWriteBarrier(&____debugFlatShader_22, value);
	}

	inline static int32_t get_offset_of__currentTrackingMethod_23() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____currentTrackingMethod_23)); }
	inline TrackingMethodBase_t2810511357 * get__currentTrackingMethod_23() const { return ____currentTrackingMethod_23; }
	inline TrackingMethodBase_t2810511357 ** get_address_of__currentTrackingMethod_23() { return &____currentTrackingMethod_23; }
	inline void set__currentTrackingMethod_23(TrackingMethodBase_t2810511357 * value)
	{
		____currentTrackingMethod_23 = value;
		Il2CppCodeGenWriteBarrier(&____currentTrackingMethod_23, value);
	}

	inline static int32_t get_offset_of__lineMaterial_24() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885, ____lineMaterial_24)); }
	inline Material_t193706927 * get__lineMaterial_24() const { return ____lineMaterial_24; }
	inline Material_t193706927 ** get_address_of__lineMaterial_24() { return &____lineMaterial_24; }
	inline void set__lineMaterial_24(Material_t193706927 * value)
	{
		____lineMaterial_24 = value;
		Il2CppCodeGenWriteBarrier(&____lineMaterial_24, value);
	}
};

struct KudanTracker_t2134143885_StaticFields
{
public:
	// Kudan.AR.KudanTracker Kudan.AR.KudanTracker::kudanTracker
	KudanTracker_t2134143885 * ___kudanTracker_2;

public:
	inline static int32_t get_offset_of_kudanTracker_2() { return static_cast<int32_t>(offsetof(KudanTracker_t2134143885_StaticFields, ___kudanTracker_2)); }
	inline KudanTracker_t2134143885 * get_kudanTracker_2() const { return ___kudanTracker_2; }
	inline KudanTracker_t2134143885 ** get_address_of_kudanTracker_2() { return &___kudanTracker_2; }
	inline void set_kudanTracker_2(KudanTracker_t2134143885 * value)
	{
		___kudanTracker_2 = value;
		Il2CppCodeGenWriteBarrier(&___kudanTracker_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
