﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleController
struct  BattleController_t3050984398  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image BattleController::playerImage
	Image_t2042527209 * ___playerImage_2;
	// UnityEngine.UI.Image BattleController::enemyImage
	Image_t2042527209 * ___enemyImage_3;
	// UnityEngine.Sprite[] BattleController::sprites
	SpriteU5BU5D_t3359083662* ___sprites_4;
	// UnityEngine.GameObject[] BattleController::enemies
	GameObjectU5BU5D_t3057952154* ___enemies_5;
	// UnityEngine.GameObject[] BattleController::players
	GameObjectU5BU5D_t3057952154* ___players_6;
	// UnityEngine.GameObject BattleController::battleBoard
	GameObject_t1756533147 * ___battleBoard_7;
	// UnityEngine.GameObject BattleController::battleUI
	GameObject_t1756533147 * ___battleUI_8;
	// UnityEngine.GameObject BattleController::VSUI
	GameObject_t1756533147 * ___VSUI_9;
	// UnityEngine.MeshRenderer BattleController::laser
	MeshRenderer_t1268241104 * ___laser_10;
	// UnityEngine.MeshRenderer BattleController::rend
	MeshRenderer_t1268241104 * ___rend_11;

public:
	inline static int32_t get_offset_of_playerImage_2() { return static_cast<int32_t>(offsetof(BattleController_t3050984398, ___playerImage_2)); }
	inline Image_t2042527209 * get_playerImage_2() const { return ___playerImage_2; }
	inline Image_t2042527209 ** get_address_of_playerImage_2() { return &___playerImage_2; }
	inline void set_playerImage_2(Image_t2042527209 * value)
	{
		___playerImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___playerImage_2, value);
	}

	inline static int32_t get_offset_of_enemyImage_3() { return static_cast<int32_t>(offsetof(BattleController_t3050984398, ___enemyImage_3)); }
	inline Image_t2042527209 * get_enemyImage_3() const { return ___enemyImage_3; }
	inline Image_t2042527209 ** get_address_of_enemyImage_3() { return &___enemyImage_3; }
	inline void set_enemyImage_3(Image_t2042527209 * value)
	{
		___enemyImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___enemyImage_3, value);
	}

	inline static int32_t get_offset_of_sprites_4() { return static_cast<int32_t>(offsetof(BattleController_t3050984398, ___sprites_4)); }
	inline SpriteU5BU5D_t3359083662* get_sprites_4() const { return ___sprites_4; }
	inline SpriteU5BU5D_t3359083662** get_address_of_sprites_4() { return &___sprites_4; }
	inline void set_sprites_4(SpriteU5BU5D_t3359083662* value)
	{
		___sprites_4 = value;
		Il2CppCodeGenWriteBarrier(&___sprites_4, value);
	}

	inline static int32_t get_offset_of_enemies_5() { return static_cast<int32_t>(offsetof(BattleController_t3050984398, ___enemies_5)); }
	inline GameObjectU5BU5D_t3057952154* get_enemies_5() const { return ___enemies_5; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_enemies_5() { return &___enemies_5; }
	inline void set_enemies_5(GameObjectU5BU5D_t3057952154* value)
	{
		___enemies_5 = value;
		Il2CppCodeGenWriteBarrier(&___enemies_5, value);
	}

	inline static int32_t get_offset_of_players_6() { return static_cast<int32_t>(offsetof(BattleController_t3050984398, ___players_6)); }
	inline GameObjectU5BU5D_t3057952154* get_players_6() const { return ___players_6; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_players_6() { return &___players_6; }
	inline void set_players_6(GameObjectU5BU5D_t3057952154* value)
	{
		___players_6 = value;
		Il2CppCodeGenWriteBarrier(&___players_6, value);
	}

	inline static int32_t get_offset_of_battleBoard_7() { return static_cast<int32_t>(offsetof(BattleController_t3050984398, ___battleBoard_7)); }
	inline GameObject_t1756533147 * get_battleBoard_7() const { return ___battleBoard_7; }
	inline GameObject_t1756533147 ** get_address_of_battleBoard_7() { return &___battleBoard_7; }
	inline void set_battleBoard_7(GameObject_t1756533147 * value)
	{
		___battleBoard_7 = value;
		Il2CppCodeGenWriteBarrier(&___battleBoard_7, value);
	}

	inline static int32_t get_offset_of_battleUI_8() { return static_cast<int32_t>(offsetof(BattleController_t3050984398, ___battleUI_8)); }
	inline GameObject_t1756533147 * get_battleUI_8() const { return ___battleUI_8; }
	inline GameObject_t1756533147 ** get_address_of_battleUI_8() { return &___battleUI_8; }
	inline void set_battleUI_8(GameObject_t1756533147 * value)
	{
		___battleUI_8 = value;
		Il2CppCodeGenWriteBarrier(&___battleUI_8, value);
	}

	inline static int32_t get_offset_of_VSUI_9() { return static_cast<int32_t>(offsetof(BattleController_t3050984398, ___VSUI_9)); }
	inline GameObject_t1756533147 * get_VSUI_9() const { return ___VSUI_9; }
	inline GameObject_t1756533147 ** get_address_of_VSUI_9() { return &___VSUI_9; }
	inline void set_VSUI_9(GameObject_t1756533147 * value)
	{
		___VSUI_9 = value;
		Il2CppCodeGenWriteBarrier(&___VSUI_9, value);
	}

	inline static int32_t get_offset_of_laser_10() { return static_cast<int32_t>(offsetof(BattleController_t3050984398, ___laser_10)); }
	inline MeshRenderer_t1268241104 * get_laser_10() const { return ___laser_10; }
	inline MeshRenderer_t1268241104 ** get_address_of_laser_10() { return &___laser_10; }
	inline void set_laser_10(MeshRenderer_t1268241104 * value)
	{
		___laser_10 = value;
		Il2CppCodeGenWriteBarrier(&___laser_10, value);
	}

	inline static int32_t get_offset_of_rend_11() { return static_cast<int32_t>(offsetof(BattleController_t3050984398, ___rend_11)); }
	inline MeshRenderer_t1268241104 * get_rend_11() const { return ___rend_11; }
	inline MeshRenderer_t1268241104 ** get_address_of_rend_11() { return &___rend_11; }
	inline void set_rend_11(MeshRenderer_t1268241104 * value)
	{
		___rend_11 = value;
		Il2CppCodeGenWriteBarrier(&___rend_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
