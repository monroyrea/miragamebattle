﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayAreaHelper_BoundaryTestResult993820647.h"
#include "AssemblyU2DCSharp_PlayAreaHelper_NativeMethods3366702661.h"
#include "AssemblyU2DCSharp_PlayAreaRenderer3466907232.h"
#include "AssemblyU2DCSharp_TrackedEventListener3475880246.h"
#include "AssemblyU2DCSharp_TrackedHead623849920.h"
#include "AssemblyU2DCSharp_TrackedObject16303305.h"
#include "AssemblyU2DCSharp_UpdatePoses2248759809.h"
#include "AssemblyU2DCSharp_UpdatePoses_U3CUpdateEofU3Ec__It3423252852.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XDevicePlug4260930879.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XDevicePlugi504417158.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XDevicePlug2524073343.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XDevicePlug1922806523.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XDevicePlug2376997798.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XDevicePlug1898088789.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XDevicePlug1957920311.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XDevicePlug3864830023.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XDevicePlug4117204168.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XDevicePlugi472304880.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XDevicePlug2122967042.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XDevicePlug3291819286.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_LOGLevel1160354656.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_DeviceConne3617794665.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_TrackingRes3701089776.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XimmerseBut3352308568.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XimmerseExt3763175417.h"
#include "AssemblyU2DCSharp_buttons3159989035.h"
#include "AssemblyU2DCSharp_cameraController4112230091.h"
#include "AssemblyU2DCSharp_vsScript2154834836.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236545.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_cameraRotate2056098828.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (BoundaryTestResult_t993820647)+ sizeof (Il2CppObject), sizeof(BoundaryTestResult_t993820647_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1900[6] = 
{
	BoundaryTestResult_t993820647::get_offset_of_isTriggering_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BoundaryTestResult_t993820647::get_offset_of_cornerId0_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BoundaryTestResult_t993820647::get_offset_of_cornerId1_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BoundaryTestResult_t993820647::get_offset_of_distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BoundaryTestResult_t993820647::get_offset_of_point_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BoundaryTestResult_t993820647::get_offset_of_normal_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (NativeMethods_t3366702661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1901[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (PlayAreaRenderer_t3466907232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1902[14] = 
{
	PlayAreaRenderer_t3466907232::get_offset_of_borderThickness_2(),
	PlayAreaRenderer_t3466907232::get_offset_of_groundMaterial_3(),
	PlayAreaRenderer_t3466907232::get_offset_of_groundColor_4(),
	PlayAreaRenderer_t3466907232::get_offset_of_thickness_5(),
	PlayAreaRenderer_t3466907232::get_offset_of_cellSize_6(),
	PlayAreaRenderer_t3466907232::get_offset_of_emptySize_7(),
	PlayAreaRenderer_t3466907232::get_offset_of_wallMaterial_8(),
	PlayAreaRenderer_t3466907232::get_offset_of_wallColor_9(),
	PlayAreaRenderer_t3466907232::get_offset_of_height_10(),
	PlayAreaRenderer_t3466907232::get_offset_of_handedness_11(),
	PlayAreaRenderer_t3466907232::get_offset_of_corners_12(),
	PlayAreaRenderer_t3466907232::get_offset_of_m_GroundRoot_13(),
	PlayAreaRenderer_t3466907232::get_offset_of_m_WallRoot_14(),
	PlayAreaRenderer_t3466907232::get_offset_of_m_Walls_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (TrackedEventListener_t3475880246), -1, sizeof(TrackedEventListener_t3475880246_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1903[13] = 
{
	0,
	0,
	0,
	0,
	0,
	TrackedEventListener_t3475880246_StaticFields::get_offset_of_current_7(),
	TrackedEventListener_t3475880246::get_offset_of_m_LastResult_8(),
	TrackedEventListener_t3475880246::get_offset_of_controller_9(),
	TrackedEventListener_t3475880246::get_offset_of_m_ControllerInput_10(),
	TrackedEventListener_t3475880246::get_offset_of_uiOutOfRange_11(),
	TrackedEventListener_t3475880246::get_offset_of_m_FadeOutOfRange_12(),
	TrackedEventListener_t3475880246::get_offset_of_onBecameVisible_13(),
	TrackedEventListener_t3475880246::get_offset_of_onBecameInvisible_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (TrackedHead_t623849920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1904[3] = 
{
	TrackedHead_t623849920::get_offset_of_eyeContainer_11(),
	TrackedHead_t623849920::get_offset_of_markTransform_12(),
	TrackedHead_t623849920::get_offset_of_m_Gizmos_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (TrackedObject_t16303305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[9] = 
{
	TrackedObject_t16303305::get_offset_of_target_2(),
	TrackedObject_t16303305::get_offset_of_source_3(),
	TrackedObject_t16303305::get_offset_of_sourceName_4(),
	TrackedObject_t16303305::get_offset_of_trackPosition_5(),
	TrackedObject_t16303305::get_offset_of_trackRotation_6(),
	TrackedObject_t16303305::get_offset_of_checkParent_7(),
	TrackedObject_t16303305::get_offset_of_canRecenter_8(),
	TrackedObject_t16303305::get_offset_of_m_ControllerInput_9(),
	TrackedObject_t16303305::get_offset_of_m_PrevTrackingResult_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (UpdatePoses_t2248759809), -1, sizeof(UpdatePoses_t2248759809_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1906[4] = 
{
	UpdatePoses_t2248759809_StaticFields::get_offset_of_s_Instance_2(),
	UpdatePoses_t2248759809_StaticFields::get_offset_of_s_InstanceCached_3(),
	UpdatePoses_t2248759809::get_offset_of_onUpdatePoses_4(),
	UpdatePoses_t2248759809::get_offset_of_m_PrevFrameCount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (U3CUpdateEofU3Ec__Iterator0_t3423252852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1907[5] = 
{
	U3CUpdateEofU3Ec__Iterator0_t3423252852::get_offset_of_U3CwaitU3E__0_0(),
	U3CUpdateEofU3Ec__Iterator0_t3423252852::get_offset_of_U24this_1(),
	U3CUpdateEofU3Ec__Iterator0_t3423252852::get_offset_of_U24current_2(),
	U3CUpdateEofU3Ec__Iterator0_t3423252852::get_offset_of_U24disposing_3(),
	U3CUpdateEofU3Ec__Iterator0_t3423252852::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (XDevicePlugin_t4260930879), -1, sizeof(XDevicePlugin_t4260930879_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1908[27] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	XDevicePlugin_t4260930879_StaticFields::get_offset_of_s_IsInited_23(),
	XDevicePlugin_t4260930879_StaticFields::get_offset_of_s_Logger_24(),
	XDevicePlugin_t4260930879_StaticFields::get_offset_of_GetNodePosition_floats_25(),
	XDevicePlugin_t4260930879_StaticFields::get_offset_of_UpdateNodeRotation_floats_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (VoidDelegate_t504417158), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (AxesDelegate_t2524073343), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (KeyDelegate_t1922806523), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (Vector3Delegate_t2376997798), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (Vector4Delegate_t1898088789), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (ControllerStateDelegate_t1957920311), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (SendMessageDelegate_t3864830023), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (LogDelegate_t4117204168), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (ControllerState_t472304880)+ sizeof (Il2CppObject), sizeof(ControllerState_t472304880_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1917[9] = 
{
	ControllerState_t472304880::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ControllerState_t472304880::get_offset_of_timestamp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ControllerState_t472304880::get_offset_of_state_mask_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ControllerState_t472304880::get_offset_of_axes_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ControllerState_t472304880::get_offset_of_buttons_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ControllerState_t472304880::get_offset_of_position_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ControllerState_t472304880::get_offset_of_rotation_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ControllerState_t472304880::get_offset_of_accelerometer_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ControllerState_t472304880::get_offset_of_gyroscope_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (TrackerState_t2122967042)+ sizeof (Il2CppObject), sizeof(TrackerState_t2122967042 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1918[8] = 
{
	0,
	TrackerState_t2122967042::get_offset_of_handle_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackerState_t2122967042::get_offset_of_timestamp_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackerState_t2122967042::get_offset_of_frameCount_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackerState_t2122967042::get_offset_of_capacity_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackerState_t2122967042::get_offset_of_count_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackerState_t2122967042::get_offset_of_id_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackerState_t2122967042::get_offset_of_data_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (NativeMethods_t3291819286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1919[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (LOGLevel_t1160354656)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1920[7] = 
{
	LOGLevel_t1160354656::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (DeviceConnectionState_t3617794665)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1921[6] = 
{
	DeviceConnectionState_t3617794665::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (TrackingResult_t3701089776)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1922[5] = 
{
	TrackingResult_t3701089776::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (XimmerseButton_t3352308568)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1923[14] = 
{
	XimmerseButton_t3352308568::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (XimmerseExtension_t3763175417), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (buttons_t3159989035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1925[3] = 
{
	buttons_t3159989035::get_offset_of_go_2(),
	buttons_t3159989035::get_offset_of_AniList_3(),
	buttons_t3159989035::get_offset_of_count_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (cameraController_t4112230091), -1, sizeof(cameraController_t4112230091_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1926[1] = 
{
	cameraController_t4112230091_StaticFields::get_offset_of_created1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (vsScript_t2154834836), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1928[4] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_0(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2DFFE3A880A537C894A86B892B4F9D6FD3792ABC89_1(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D1AE3A7267294AAB35C6B28E5FD9FE7173A83A7DC_2(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D589B8B9A9B4A42014B839580060C4D75DE19D231_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (U24ArrayTypeU3D16_t3894236545)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236545 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (cameraRotate_t2056098828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1931[1] = 
{
	cameraRotate_t2056098828::get_offset_of_rotationVelocity_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
