﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Ximmerse_InputSystem_TrackerInpu4065713356.h"

// Ximmerse.InputSystem.XHawkInput/StringIntPair[]
struct StringIntPairU5BU5D_t3801962760;
// Ximmerse.InputSystem.TrackedControllerInput[]
struct TrackedControllerInputU5BU5D_t1311428531;
// Ximmerse.InputSystem.ControllerInput
struct ControllerInput_t1382602808;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.InputSystem.XHawkInput
struct  XHawkInput_t3103630643  : public TrackerInput_t4065713356
{
public:
	// Ximmerse.InputSystem.XHawkInput/StringIntPair[] Ximmerse.InputSystem.XHawkInput::m_Controllers
	StringIntPairU5BU5D_t3801962760* ___m_Controllers_14;
	// Ximmerse.InputSystem.TrackedControllerInput[] Ximmerse.InputSystem.XHawkInput::controllers
	TrackedControllerInputU5BU5D_t1311428531* ___controllers_15;
	// System.Boolean Ximmerse.InputSystem.XHawkInput::m_IsInited
	bool ___m_IsInited_16;
	// System.Boolean Ximmerse.InputSystem.XHawkInput::m_IsRequestVR
	bool ___m_IsRequestVR_17;
	// Ximmerse.InputSystem.ControllerInput Ximmerse.InputSystem.XHawkInput::m_HmdInput
	ControllerInput_t1382602808 * ___m_HmdInput_18;

public:
	inline static int32_t get_offset_of_m_Controllers_14() { return static_cast<int32_t>(offsetof(XHawkInput_t3103630643, ___m_Controllers_14)); }
	inline StringIntPairU5BU5D_t3801962760* get_m_Controllers_14() const { return ___m_Controllers_14; }
	inline StringIntPairU5BU5D_t3801962760** get_address_of_m_Controllers_14() { return &___m_Controllers_14; }
	inline void set_m_Controllers_14(StringIntPairU5BU5D_t3801962760* value)
	{
		___m_Controllers_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_Controllers_14, value);
	}

	inline static int32_t get_offset_of_controllers_15() { return static_cast<int32_t>(offsetof(XHawkInput_t3103630643, ___controllers_15)); }
	inline TrackedControllerInputU5BU5D_t1311428531* get_controllers_15() const { return ___controllers_15; }
	inline TrackedControllerInputU5BU5D_t1311428531** get_address_of_controllers_15() { return &___controllers_15; }
	inline void set_controllers_15(TrackedControllerInputU5BU5D_t1311428531* value)
	{
		___controllers_15 = value;
		Il2CppCodeGenWriteBarrier(&___controllers_15, value);
	}

	inline static int32_t get_offset_of_m_IsInited_16() { return static_cast<int32_t>(offsetof(XHawkInput_t3103630643, ___m_IsInited_16)); }
	inline bool get_m_IsInited_16() const { return ___m_IsInited_16; }
	inline bool* get_address_of_m_IsInited_16() { return &___m_IsInited_16; }
	inline void set_m_IsInited_16(bool value)
	{
		___m_IsInited_16 = value;
	}

	inline static int32_t get_offset_of_m_IsRequestVR_17() { return static_cast<int32_t>(offsetof(XHawkInput_t3103630643, ___m_IsRequestVR_17)); }
	inline bool get_m_IsRequestVR_17() const { return ___m_IsRequestVR_17; }
	inline bool* get_address_of_m_IsRequestVR_17() { return &___m_IsRequestVR_17; }
	inline void set_m_IsRequestVR_17(bool value)
	{
		___m_IsRequestVR_17 = value;
	}

	inline static int32_t get_offset_of_m_HmdInput_18() { return static_cast<int32_t>(offsetof(XHawkInput_t3103630643, ___m_HmdInput_18)); }
	inline ControllerInput_t1382602808 * get_m_HmdInput_18() const { return ___m_HmdInput_18; }
	inline ControllerInput_t1382602808 ** get_address_of_m_HmdInput_18() { return &___m_HmdInput_18; }
	inline void set_m_HmdInput_18(ControllerInput_t1382602808 * value)
	{
		___m_HmdInput_18 = value;
		Il2CppCodeGenWriteBarrier(&___m_HmdInput_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
