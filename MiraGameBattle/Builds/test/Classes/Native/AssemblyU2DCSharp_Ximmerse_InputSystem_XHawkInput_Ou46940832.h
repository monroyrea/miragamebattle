﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XHawkInput3103630643.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// Ximmerse.VR.VRDevice
struct VRDevice_t903186564;
// PlayAreaHelper
struct PlayAreaHelper_t1795592975;
// System.Single[]
struct SingleU5BU5D_t577127397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.InputSystem.XHawkInput_OutsideIn
struct  XHawkInput_OutsideIn_t46940832  : public XHawkInput_t3103630643
{
public:
	// UnityEngine.GameObject Ximmerse.InputSystem.XHawkInput_OutsideIn::anchorPrefab
	GameObject_t1756533147 * ___anchorPrefab_19;
	// System.Boolean Ximmerse.InputSystem.XHawkInput_OutsideIn::makeTrackerForward
	bool ___makeTrackerForward_20;
	// UnityEngine.Transform Ximmerse.InputSystem.XHawkInput_OutsideIn::m_CenterEye
	Transform_t3275118058 * ___m_CenterEye_21;
	// Ximmerse.VR.VRDevice Ximmerse.InputSystem.XHawkInput_OutsideIn::m_VRDevice
	VRDevice_t903186564 * ___m_VRDevice_22;
	// PlayAreaHelper Ximmerse.InputSystem.XHawkInput_OutsideIn::m_PlayAreaHelper
	PlayAreaHelper_t1795592975 * ___m_PlayAreaHelper_23;
	// System.Single[] Ximmerse.InputSystem.XHawkInput_OutsideIn::m_UpdateAnchorFromPluginTRS
	SingleU5BU5D_t577127397* ___m_UpdateAnchorFromPluginTRS_24;

public:
	inline static int32_t get_offset_of_anchorPrefab_19() { return static_cast<int32_t>(offsetof(XHawkInput_OutsideIn_t46940832, ___anchorPrefab_19)); }
	inline GameObject_t1756533147 * get_anchorPrefab_19() const { return ___anchorPrefab_19; }
	inline GameObject_t1756533147 ** get_address_of_anchorPrefab_19() { return &___anchorPrefab_19; }
	inline void set_anchorPrefab_19(GameObject_t1756533147 * value)
	{
		___anchorPrefab_19 = value;
		Il2CppCodeGenWriteBarrier(&___anchorPrefab_19, value);
	}

	inline static int32_t get_offset_of_makeTrackerForward_20() { return static_cast<int32_t>(offsetof(XHawkInput_OutsideIn_t46940832, ___makeTrackerForward_20)); }
	inline bool get_makeTrackerForward_20() const { return ___makeTrackerForward_20; }
	inline bool* get_address_of_makeTrackerForward_20() { return &___makeTrackerForward_20; }
	inline void set_makeTrackerForward_20(bool value)
	{
		___makeTrackerForward_20 = value;
	}

	inline static int32_t get_offset_of_m_CenterEye_21() { return static_cast<int32_t>(offsetof(XHawkInput_OutsideIn_t46940832, ___m_CenterEye_21)); }
	inline Transform_t3275118058 * get_m_CenterEye_21() const { return ___m_CenterEye_21; }
	inline Transform_t3275118058 ** get_address_of_m_CenterEye_21() { return &___m_CenterEye_21; }
	inline void set_m_CenterEye_21(Transform_t3275118058 * value)
	{
		___m_CenterEye_21 = value;
		Il2CppCodeGenWriteBarrier(&___m_CenterEye_21, value);
	}

	inline static int32_t get_offset_of_m_VRDevice_22() { return static_cast<int32_t>(offsetof(XHawkInput_OutsideIn_t46940832, ___m_VRDevice_22)); }
	inline VRDevice_t903186564 * get_m_VRDevice_22() const { return ___m_VRDevice_22; }
	inline VRDevice_t903186564 ** get_address_of_m_VRDevice_22() { return &___m_VRDevice_22; }
	inline void set_m_VRDevice_22(VRDevice_t903186564 * value)
	{
		___m_VRDevice_22 = value;
		Il2CppCodeGenWriteBarrier(&___m_VRDevice_22, value);
	}

	inline static int32_t get_offset_of_m_PlayAreaHelper_23() { return static_cast<int32_t>(offsetof(XHawkInput_OutsideIn_t46940832, ___m_PlayAreaHelper_23)); }
	inline PlayAreaHelper_t1795592975 * get_m_PlayAreaHelper_23() const { return ___m_PlayAreaHelper_23; }
	inline PlayAreaHelper_t1795592975 ** get_address_of_m_PlayAreaHelper_23() { return &___m_PlayAreaHelper_23; }
	inline void set_m_PlayAreaHelper_23(PlayAreaHelper_t1795592975 * value)
	{
		___m_PlayAreaHelper_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_PlayAreaHelper_23, value);
	}

	inline static int32_t get_offset_of_m_UpdateAnchorFromPluginTRS_24() { return static_cast<int32_t>(offsetof(XHawkInput_OutsideIn_t46940832, ___m_UpdateAnchorFromPluginTRS_24)); }
	inline SingleU5BU5D_t577127397* get_m_UpdateAnchorFromPluginTRS_24() const { return ___m_UpdateAnchorFromPluginTRS_24; }
	inline SingleU5BU5D_t577127397** get_address_of_m_UpdateAnchorFromPluginTRS_24() { return &___m_UpdateAnchorFromPluginTRS_24; }
	inline void set_m_UpdateAnchorFromPluginTRS_24(SingleU5BU5D_t577127397* value)
	{
		___m_UpdateAnchorFromPluginTRS_24 = value;
		Il2CppCodeGenWriteBarrier(&___m_UpdateAnchorFromPluginTRS_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
