# Mira SDK for Unity
Enables easy development for the Mira Headset in Unity.

This SDK requires Unity 5.6. If you have an older version, changes to the code may be necessary.

Read our [Getting Started Guide here](https://developers.miralabs.io/alpha/mira-unity-sdk/wikis/getting-started).

You are more than welcome to open issues \^at\^the\^top\^of\^this\^page\^, and we will address them as soon as possible.
If you have any questions, don't hesitate to reach out on the [Mira Developers Slack](https://mira-developers.slack.com).