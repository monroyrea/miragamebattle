﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiraAnimation : ControllerTarget {
	[SerializeField]
	GameObject m;

	[SerializeField]
	GameObject i;

	[SerializeField]
	GameObject r;

	[SerializeField]
	GameObject a;
	
	Vector3 mPos;
	Vector3 iPos;
	Vector3 rPos;
	Vector3 aPos;

	[SerializeField]
	float amplitude = 1;
	[SerializeField]
	float lerpAmp = 0;
	[SerializeField]
	float letterOffset = 1;

	float timer;
	[SerializeField]
	float timeMult = 3;
	[SerializeField]
	float lerpAmpSet = 0.5f;

	bool activated = false;

	Coroutine currentCoroutine;


	IEnumerator ampUp ()
	{
		while (lerpAmp < 1)
		{
			lerpAmp += (Time.deltaTime/lerpAmpSet);
			sinAnim(lerpAmp * amplitude);
			yield return null;
		}

		yield return StartCoroutine(ampAnim());


     	
	}
	IEnumerator ampAnim ()
	{
		while (activated == true)
		{
			sinAnim(amplitude);
			yield return null;
		}

	}
	IEnumerator ampDown ()
	{
		while (lerpAmp > 0)
		{
			lerpAmp -= (Time.deltaTime/lerpAmpSet);
			sinAnim(lerpAmp * amplitude);
			yield return null;
		}
		
	}

	public override void ControllerOn()
	{
		
		//sinAnim();

	}
	public override void OnStartControllerOn()
	{
		//lerpAmp = 0;
		activated = true;
		StopAllCoroutines();
		StartCoroutine(ampUp());

	}
	public override void OnStopControllerOn()
	{	
		StopAllCoroutines();
		activated = false;
		StartCoroutine(ampDown());

	}
	void sinAnim(float currentAmp)
	{

		Vector3 mMove = new Vector3 (mPos.x, (mPos.y + Mathf.Sin(timer + letterOffset*3)*currentAmp), mPos.z);
		m.transform.position = mMove;
		Vector3 iMove = new Vector3 (iPos.x, (iPos.y + Mathf.Sin(timer + letterOffset*2)*currentAmp), iPos.z);
		i.transform.position = iMove;
		Vector3 rMove = new Vector3 (rPos.x, (rPos.y + Mathf.Sin(timer + letterOffset*1)*currentAmp), rPos.z);
		r.transform.position = rMove;
		Vector3 aMove = new Vector3 (aPos.x, (aPos.y + Mathf.Sin(timer)*currentAmp), aPos.z);
		a.transform.position = aMove;
		timer += Time.deltaTime * timeMult; 


	}
	void Start () 
	{

		mPos = m.transform.position;
		iPos = i.transform.position;
		rPos = r.transform.position;
		aPos = a.transform.position;
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		//sinAnim();
	}
}
