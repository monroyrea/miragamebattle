﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPointerLength : MonoBehaviour {

    /// <summary>
    /// The reticle which drives the laser length
    /// </summary>
	[SerializeField]
	MiraReticle reticle;

	Vector3 originalScale;
	void Start () 
	{
		originalScale = new Vector3(1.277715f, 1.277715f, 1.277715f);
		MatchReticle(reticle.reticuleOffsetZ);
	}
	
	// Update is called once per frame
	void Update () 
	{

		
	}

	public void MatchReticle(float zDistance)
	{
		transform.localScale = new Vector3(originalScale.x, originalScale.y, zDistance);
		transform.localPosition = new Vector3(0, 0, zDistance/2);
	}

}
