﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;


public class NotificationIcon : MonoBehaviour {

	public float iconBoundsX;
	public Quaternion OgRotationLU;

	Material mat;

	Color ogClr;
	[SerializeField]
	Color lerpClr;

	// Use this for initialization
	void Start () 
	{
		getNotificationBounds();
		OgRotationLU = transform.rotation;
		mat = gameObject.GetComponent<Renderer>().material;
		ogClr = mat.GetColor("_Color");
		lerpClr = ogClr;
		lerpClr.a = 0;
		//mat.SetColor("_Color", lerpClr);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void getNotificationBounds()
	{

		Assert.IsNotNull(gameObject.GetComponent<MeshFilter>(), "Notification Icon must have a MeshFilter");
		iconBoundsX =  gameObject.GetComponent<MeshFilter>().sharedMesh.bounds.size.x * transform.localScale.x;
	
	}

	public void FadeLUIIcon(float time)
	{
		// if (gameObject.GetComponent<gazeTest>().isGazedAt == false)
		// {
		// mat.SetColor("_Color", Color.Lerp(lerpClr, ogClr, time));
		// }
	}
	
}
