﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerHover : ControllerTarget{



	float scaleChange = 1.0f;

	Vector3 ogScale;

	Vector3 finalScaleUp;

	Vector3 finalScaleDown;

	Vector3 currentScale;

	Color ogColor;

	[SerializeField]
	[TooltipAttribute("Color of object when hovered over")]

	protected Color holverColor = Color.red;

	protected bool hovering = false;


	
	void Start () 
	{
		
		GetOgState();
		
	}

	protected void GetOgState()
	{
		ogColor = gameObject.GetComponent<Renderer>().material.GetColor("_Color");
		ogScale = transform.localScale;
		finalScaleUp = new Vector3(ogScale.x * scaleChange, ogScale.y * scaleChange, ogScale.z * scaleChange);
		finalScaleDown = ogScale;


	}
	

	public override void ControllerOn()
	{
	
	}
	public override void OnStartControllerOn()
	{
	
			Hover();
	
	
	}
	protected void Hover()
	{
		// currentScale = transform.localScale;
		gameObject.GetComponent<Renderer>().material.SetColor("_Color", holverColor);
		hovering = true;
	}
	protected void StopHover()
	{
		// currentScale = transform.localScale;
		gameObject.GetComponent<Renderer>().material.SetColor("_Color", ogColor);
		// transform.localScale = finalScaleDown;
		hovering = false;

	}
	public override void OnStopControllerOn()
	{
			
		StopHover();
	}

	
}