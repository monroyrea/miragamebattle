﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]

public class NotificationMenu : ControllerHover {
	

	private float maxAngle = 360;
	[SerializeField]
	float height = 100f;

	[SerializeField]
	GameObject centerCollider;
	
	[SerializeField]
	private float zRadiusLU = 10;
	private float padding;
	private List<NotificationIcon> icons = new List<NotificationIcon>();

	[SerializeField]
	float elevationAngle = 30;

	[SerializeField]
	float fadeTime = 0.5f;


	float currentFadeTimer;


	public override void ControllerOn()
	{

	}

	public override void OnStartControllerOn()
	{
		StopAllCoroutines();
		StartCoroutine(FadeIn());
		Debug.Log("Fading In LookUp UI");
	}

	public override void OnStopControllerOn()
	{
		StopAllCoroutines();
		StartCoroutine(FadeOut());
		Debug.Log("Fading Out LookUp UI");

	}



	void OnValidate()
     {
         UpdateNotificationMenu();
     }


	// Use this for initialization
	void Start () 
	{
		//height = transform.position.y;
		GetChildren();
		UpdateNotificationMenu();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void PlaceNotifications ()
	{

	}

	IEnumerator FadeIn ()
	{
		//for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / fadeTime)
		while (currentFadeTimer < 1.0f)
		{
		
			foreach(NotificationIcon icon in icons)
			{
				icon.FadeLUIIcon(currentFadeTimer);
			}
			Debug.Log("Fade Timer: " + currentFadeTimer);
			currentFadeTimer += (Time.deltaTime / fadeTime);

			yield return null;
		
		}
		
		
	}
	IEnumerator FadeOut ()
	{
		while (currentFadeTimer >= 0.0f)
		{
			currentFadeTimer -= (Time.deltaTime / fadeTime);
			Debug.Log("Fade Timer: " + currentFadeTimer);

			foreach(NotificationIcon icon in icons)
			{
				icon.FadeLUIIcon(currentFadeTimer);
			}
			
			yield return null;
		
		}

	}

	void RotateElevation()
	{
		
	}
	float AngleDist(float angle)
	{
		float dist = angle/360 * 2 * Mathf.PI * zRadiusLU;
		return dist;
	}

	// Calculates the position of objects on that circle
	Vector3 CirclePos(float xDist)
	{	
		float radPos = xDist/zRadiusLU;
		float xPos = zRadiusLU * Mathf.Sin(radPos);
		float zPos = zRadiusLU * Mathf.Cos(radPos);
		return new Vector3(xPos, height, zPos);
	}

	// Rotates the UI objects to face world center
	Quaternion FaceCenter(Vector3 pos)
	{
		Vector3 origin = new Vector3(0,0,0);
        Quaternion rot = Quaternion.LookRotation(pos - origin);
		return rot;
	}

	// Adds the children of UIManager to the UIObject list (on Start)
	void GetChildren()
	{
		icons.Clear();
		foreach(Transform child in transform)
		{
			if (child.gameObject.GetComponent<NotificationIcon>() != null && child.gameObject.activeSelf == true)
			{
				icons.Add(child.gameObject.GetComponent<NotificationIcon>());
			}
		}
	}

	// Update UI recalculates world position of UIObjects in UIObjList
	void UpdateNotificationMenu()
	{	
		Vector3 origin = new Vector3(0,0,0);
		Vector3 axis = new Vector3(1,0,0);
		centerCollider.transform.position = new Vector3(0, height, 0);
		centerCollider.transform.rotation = Quaternion.Euler(0, 0, 0);
		centerCollider.transform.RotateAround(origin, axis, elevationAngle);
		int num = icons.Count;
		float totalDist = 0f;

		if(num > 0)
		{
			for(int o = 1; o < num; o++)
			{
				NotificationIcon icon = icons[o];
				icon.getNotificationBounds();
				totalDist += icon.iconBoundsX;
				
				//icon.UIObjIndex = o;
			}
			float circ = 2 * Mathf.PI * zRadiusLU;
			padding = (circ - totalDist)/(num);
			totalDist += padding;
			float centerX = totalDist/2;

			if (AngleDist(maxAngle) > totalDist)
			{
				float currentDist = 0f;
				foreach(NotificationIcon icon in icons)
				{
					float xPos = currentDist + (icon.iconBoundsX/2) - centerX;
					Vector3 finalPos = CirclePos(xPos);
					Transform dispTrans = icon.gameObject.transform;
					dispTrans.position = finalPos;

					dispTrans.rotation = FaceCenter(finalPos);

					dispTrans.RotateAround(new Vector3(0,0,0), new Vector3(1, 0, 0), elevationAngle);
					//dispTrans.rotation = (FaceCenter(finalPos) * icon.OgRotationLU);


					//LerpSpacing(dispTrans, finalPos, FaceCenter(finalPos)* lookUp);
					
				
					
					currentDist += (icon.iconBoundsX) + padding;
				}
			}
			else
			{
				//TO DO: Manage object overflow
				Debug.Log("UI field of view too small to display all objects");

			}

		}
		

	}

}
