﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCamera : MonoBehaviour {

	[SerializeField]
	Transform stereoCamRig;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		//transform.rotation = Quaternion.LookRotation(transform.position - new Vector3(0,0,0));
		transform.LookAt(stereoCamRig);
		transform.Rotate(0, 180, 0);
	}
}
