﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapToController : ControllerHover {
	[HideInInspector]
	 public bool parentedToController = false;
	 GameObject controller;
	 float castMultiplier = 500f;


	 float defaultDistanceZ;

	 bool castStart = false;

	 float lastCast;
	 static bool triggerToggle = true;

	 Collider thisCollider;
	 


	void Start () 
	{
		ControllerEventManager controllerEvent = GameObject.FindObjectOfType<ControllerEventManager>();
		if(controllerEvent)
		controller = controllerEvent.gameObject;
		GetOgState();
		thisCollider = gameObject.GetComponent<Collider>();
	}
	
	void Update () 
	{

	}
	void OnEnable()
    {
		// CheckSnap will be triggered on TriggerStart
        ControllerEventManager.TriggerStart += CheckSnap;
		// CheckParent will be triggered on TriggerEnd
		ControllerEventManager.TriggerEnd += CheckParent;
    }

	void OnDestroy()
	{
		ControllerEventManager.TriggerStart -= CheckSnap;
		ControllerEventManager.TriggerEnd -= CheckParent;
		ControllerEventManager.SendTouchPadY -= Cast;

	}

	void CheckSnap()
	{
		if(controller!=null){

			// Trigger toggle mode isn't activated (only snapped when trigger held)
			if(triggerToggle == false)
			{
				if(hovering==true && parentedToController == false)
				{
					ParentToController();

				}

			}

			// Trigger toggle mode activated
			if(triggerToggle == true)
			{
				// if object is already parented
				if(parentedToController == true)
				{
					// unparent from controller and parent to world
					ParentToWorld();
				}
				// if object's being hovered over, and it's not already parented to the controller
				if(hovering == true && parentedToController == false)
				{
					// Parent it to the controller
					ParentToController();
				}

			}
		}
	}

	void CheckParent()
	{
		if(controller!=null)
		{
			// Check to see if the object was parented to the controller, and that triggerToggle is not activated
			if(parentedToController == true && triggerToggle == false)
			{
				ParentToWorld();

			}
		}
	}


	void ParentToController()
	{
		if(gameObject.GetComponent<UIObject>() != null)
		{
			gameObject.GetComponent<UIObject>().RemoveFromUI();
		}
		transform.SetParent(controller.transform);
		parentedToController = true;
		defaultDistanceZ = transform.localPosition.z;
		castStart = true;
		ControllerEventManager.SendTouchPadY += Cast;
		thisCollider.enabled = false;


	}
	void ParentToWorld()
	{
		transform.SetParent(null);
		parentedToController = false;
		ControllerEventManager.SendTouchPadY -= Cast;
		thisCollider.enabled = true;


	}
	void Cast(float value)
	{
		if(castStart == true)
		{
			lastCast = value;
			castStart = false;
		}
		
		if(value!=0)
		{
			transform.Translate(Vector3.back * (value - lastCast) * castMultiplier);
			lastCast = value;

		}
		else
		{
			castStart = true;
		}

	}

}