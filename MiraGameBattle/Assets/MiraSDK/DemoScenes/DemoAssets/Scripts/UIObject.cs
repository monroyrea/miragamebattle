﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class UIObject : TrackingInteractable {

	[HideInInspector]
	public int UIObjIndex;
	[HideInInspector]
	public float BoundsX;
	[HideInInspector]
	public bool inUI = false;
	[HideInInspector]

	public UIManager mainUIManager;
	[HideInInspector]

	public Quaternion UIOgRotation; 

	Vector3 currentPos;
	Vector3 lerpPos;
	Quaternion currentRot;
	Quaternion lerpRot;
	[HideInInspector]

	public bool dontLerp = false;

	float rotateSpeed = 1;


	public void RemoveFromUI()
	{
		if(inUI)
		{
			mainUIManager.UIObj.RemoveAt(UIObjIndex);
			mainUIManager.UpdateUI();
			inUI = false;
		}

	}

	public void SetLerp (Vector3 lerpPosition, Quaternion lerpRotation)
	{
		currentPos = transform.position;
		currentRot = transform.rotation;
		lerpPos = lerpPosition;
		lerpRot = lerpRotation;
	}

	public void lerpUIObj(float time)
	{	if (dontLerp == false)
		{
			transform.position = Vector3.Slerp(currentPos, lerpPos, time);
			transform.rotation = Quaternion.Slerp(currentRot, lerpRot, time);
		}
		else
		{
			transform.position = lerpPos;
			transform.rotation = lerpRot;
		}
	}
		
	public void getBoundsX()
	{	
		Assert.IsNotNull(gameObject.GetComponent<MeshFilter>(), "UI Element must have a MeshFilter");
		BoundsX =  gameObject.GetComponent<MeshFilter>().sharedMesh.bounds.size.x * transform.localScale.x;
	}
	void spin()
	{
		transform.Rotate(Time.deltaTime * rotateSpeed, 0, 0);
	}

	void Start () 
	{	
		TrackingInit();
		getBoundsX();

		UIOgRotation = transform.rotation;
	}


	
	

	void Update () 
	{
	
	}



}