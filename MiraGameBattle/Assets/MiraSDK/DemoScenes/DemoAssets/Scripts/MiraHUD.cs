﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiraHUD : MonoBehaviour {

	[SerializeField]
	Text trackingText;

	[SerializeField]
	Color trackingColor = Color.green;
	[SerializeField]
	Color notTrackingColor = Color.red;
	// Use this for initialization
	void Start () 
	{
		SetCanvasWorld();
		NotTrackingText();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SetCanvasWorld()
	{
		Canvas canvas = gameObject.GetComponent<Canvas>();
		canvas.renderMode = RenderMode.WorldSpace;

	}

	public void TrackingText()
	{
		trackingText.text = "Tracking";
		trackingText.color = trackingColor;
	}
	public void NotTrackingText()
	{
		trackingText.text = "Not Tracking";
		trackingText.color = notTrackingColor;
	}

	
}
