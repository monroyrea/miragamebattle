﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class UIManager : MonoBehaviour {

	
	[HeaderAttribute("UI Dimensions")]
	//The following attribute isn't currently super useful, but will be essential later when managing UIObject overflow
	// Max angle dimension for UI circle
	[HideInInspector]
	public float UIAngle = 360;

	[TooltipAttribute("Distance of UI from User")]
	[SerializeField]
	public float zRadius = 2;
	
	[TooltipAttribute("Height of UI")]
	[SerializeField]
	float height = -45;
	
	[TooltipAttribute("Spacing between UI Objects")]
	[SerializeField]
	private float padding = 0.3f;
	


	[SerializeField]
	[TooltipAttribute("Time (in seconds) for objects to lerp when removed")]

	float setLerpSpaceTime = 0.5f;
	float lerpSpaceTime = 0;


	[HideInInspector]
	public List<UIObject> UIObj;

	Coroutine lerpSpaceCo;

	bool firstLerp = true;

	

	
	
     void OnValidate()
     {
         UpdateUI();
     }
     
	void Start () 
	{
		//Zero Out position
		gameObject.transform.position = new Vector3(0, 0, 0);
		GetChildren();
		UpdateUI();

		firstLerp = true;
	}
	
	void Update () 
	{
		

		
	}

	IEnumerator lerpSpacing()
	{
		while (lerpSpaceTime < 1.0f)
		{
		
			foreach(UIObject obj in UIObj)
			{
				obj.lerpUIObj(lerpSpaceTime);
			}
			lerpSpaceTime += (Time.deltaTime / setLerpSpaceTime);

			yield return null;
		
		}
	
	}
	
	float AngleDist(float angle)
	{
		float dist = angle/360 * 2 * Mathf.PI * zRadius;
		return dist;
	}

	Vector3 CirclePos(float xDist)
	{	
		float radPos = xDist/zRadius;
		float xPos = zRadius * Mathf.Sin(radPos);
		float zPos = zRadius * Mathf.Cos(radPos);
		return new Vector3(xPos, height, zPos);
	}

	Quaternion FaceCenter(Vector3 pos)
	{
        Quaternion rot = Quaternion.LookRotation(-pos);
		return rot;
	}

	void GetChildren()
	{
		UIObj.Clear();
		foreach(Transform child in transform)
		{
			if (child.gameObject.GetComponent<UIObject>() != null && child.gameObject.activeSelf == true)
			{
				UIObj.Add(child.gameObject.GetComponent<UIObject>());
				child.GetComponent<UIObject>().mainUIManager = this;
			}
		}
	}

	public void UpdateUI()
	{	
		int num = UIObj.Count;
		float totalDist = 0f;

		if(num > 0)
		{
			for(int o = 0; o < UIObj.Count; o++)
			{
				UIObject obj = UIObj[o];
				obj.getBoundsX();
				totalDist += obj.BoundsX;
				obj.UIObjIndex = o;
			}
			
			totalDist += (padding * (num - 1));
			float centerX = totalDist/2;

			if (AngleDist(UIAngle) > totalDist)
			{
				float currentDist = 0f;
				foreach(UIObject dispObj in UIObj)
				{
				
					dispObj.inUI = true;

					float xPos = currentDist + (dispObj.BoundsX/2) - centerX;
					Vector3 finalPos = CirclePos(xPos);

				

					Transform dispTrans = dispObj.gameObject.transform;

					Quaternion finalRot = FaceCenter(finalPos);
		
					dispObj.SetLerp(finalPos, finalRot);
			
					currentDist += (dispObj.BoundsX) + padding;
				}

				lerpSpaceTime = 0;
				if(lerpSpaceCo != null)
				{
					StopCoroutine(lerpSpaceCo);
				}
				lerpSpaceCo = StartCoroutine(lerpSpacing());
			}
			else
			{
				Debug.Log("UI field of view too small to display all objects");

			}

		}
		

	}
	



	
}