﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ximmerse;
using Ximmerse.InputSystem;
using UnityEngine.UI;
public class ControllerEventManager : MonoBehaviour {

	private static bool created = false;
	public static Vector3 accelerometer;
	public static Vector3 gyroscope;
	private int count =0;
	public Text controlText;

	public static ControllerInput controller;
	ControllerButton trigger = ControllerButton.PrimaryTrigger;
	ControllerButton start = ControllerButton.Start;
	ControllerButton back = ControllerButton.Back;
	ControllerButton touchPadUp = ControllerButton.DpadUp;
	ControllerButton touchPadDown = ControllerButton.DpadDown;
	ControllerButton touchPadLeft = ControllerButton.DpadLeft;
	ControllerButton touchPadRight = ControllerButton.DpadRight;
	ControllerAxis touchPadX = ControllerAxis.PrimaryThumbX;
	ControllerAxis touchPadY = ControllerAxis.PrimaryThumbY;
	
	public delegate void ControllerClick();
    public static event ControllerClick TriggerOn;
	public static event ControllerClick TriggerStart;
	public static event ControllerClick TriggerEnd;
	public static event ControllerClick StartOn;
	public static event ControllerClick StartDown;
	public static event ControllerClick StartUp;
	public static event ControllerClick BackOn;
	public static event ControllerClick BackDown;
	public static event ControllerClick BackUp;
	public static event ControllerClick TouchPadUpClick;
	
	public delegate void SendControllerAxis(float value);
	public static event SendControllerAxis SendTouchPadY;
	public static event SendControllerAxis SendTouchPadX;

	void CheckButtonStates()
	{
		if(controller.GetButton(trigger))
		{
			if(TriggerOn != null)
			TriggerOn();
		}
		if(controller.GetButtonDown(trigger))
		{
			if(TriggerStart != null)
			TriggerStart();
		}
		if(controller.GetButtonUp(trigger))
		{
			if(TriggerEnd != null)
			TriggerEnd();
		}

		if (controller.GetButtonDown (back)) {
			if (BackDown != null)
				BackDown ();
		}

		if (controller.GetButtonDown (start)) {
			if (StartDown != null)
				StartDown ();
		}

		if(SendTouchPadY !=null)
		{
			SendTouchPadY(controller.GetAxis(touchPadY));
		}

		if(SendTouchPadX !=null)
		{
			SendTouchPadX(controller.GetAxis(touchPadX));
		}
	

	}

	void Awake() {
		if (!created) {
			// this is the first instance - make it persist
			DontDestroyOnLoad(this.gameObject);
			created = true;
		} else {
			// this must be a duplicate from a scene reload - DESTROY!
			Destroy(this.gameObject);
		} 
	}
	
	void Start () 
	{
		controller = ControllerInputManager.instance.GetControllerInput(ControllerType.LeftController);	
		accelerometer = new Vector3 (0.0f,0.0f,0.0f);
		gyroscope = new Vector3 (0.0f,0.0f,0.0f);
		
	}
	
	
	void Update () 
	{
		if (controller != null) {
			if(count ==0)
				controlText.text = "Controller CONECTED";
			
			controlText.color = Color.green;
			CheckButtonStates ();
			accelerometer = controller.GetAccelerometer ();
			gyroscope = controller.GetGyroscope ();
		}
		count++;

	}
	
}
