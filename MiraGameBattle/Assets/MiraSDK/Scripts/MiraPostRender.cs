﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiraPostRender : MonoBehaviour {
	public enum Eye {Left, Right};
	
	public Eye eye = Eye.Right;
    int xSize = 10;
	int ySize = 10;
	float zOffset = 35;
	float xScalar = 2.5985f;
	float yScalar = 2.922f;

	float screenSizeX = 25.985f;
	float screenSizeY = 29.22f;
	Mesh mesh;
	[SerializeField]
	public Material renderTextureMaterial;
	[SerializeField]
	public float IPD = 64;

	public void OnPostRender() {
        // set first shader pass of the material
        renderTextureMaterial.SetPass(0);
        // draw mesh
		if(eye == Eye.Left)
        {
			Graphics.DrawMeshNow(mesh, new Vector3(0,0,zOffset*2), Quaternion.Euler(0,180,0));
        }
        else
        {
            Graphics.DrawMeshNow(mesh, Vector3.zero, Quaternion.identity);
        }
    }
	void Start () 
	{
        DistortionMesh();
    }
	
	void Update () 
	{
		// Add a check to see if IPD has changed since last frame? - To redraw mesh
		
	}
	void Awake () 
	{
		
	}
	public void UpdateDistortionMesh()
	{
		DistortionMesh();
	}
	void DistortionMesh()
	{
		mesh = GetComponent<MeshFilter>().mesh = new Mesh();
		mesh.Clear();

		Vector3[] vertices = new Vector3[(xSize + 1)*(ySize + 1)];
		Vector3[] verticesDistort = new Vector3[vertices.Length];
		Vector2[] uv = new Vector2[vertices.Length];
		int i = 0;
		for(int y=0; y<=ySize; y++)
		{
			for(int x = 0; x<=xSize; x++)
			{
				vertices[i] = new Vector3((x * xScalar) - (xSize * xScalar * 0.5f), (y * yScalar) - (ySize * yScalar * 0.5f), zOffset);
				
				if(eye == Eye.Left)
				{
					verticesDistort[i] = DistortionL(vertices[i].x, vertices[i].y);
			
				}
				if(eye == Eye.Right)
				{
				
					verticesDistort[i] = DistortionR(vertices[i].x, vertices[i].y);

				}
                verticesDistort[i].z = zOffset;

				uv[i] = new Vector2((float)x / xSize, (float)y / ySize);

				i+=1;

			}
		}

		mesh.vertices = verticesDistort;
		mesh.uv = uv;
        

		int[] triangles = new int[xSize * ySize * 6];
		for (int ti = 0, vi = 0, y = 0; y < ySize; y++, vi++) 
        {
			for (int x = 0; x < xSize; x++, ti += 6, vi++) {
				triangles[ti] = vi;
				triangles[ti + 3] = triangles[ti + 2] = vi + 1;
				triangles[ti + 4] = triangles[ti + 1] = vi + xSize + 1;
				triangles[ti + 5] = vi + xSize + 2;
			}
		}
		mesh.triangles = triangles;
		mesh.RecalculateNormals();
		
     

	}

	Vector3 DistortionL(float theta, float phi)
	{
		double x = 	-0.035811 * IPD
                + 0.20792 * phi
                + -0.0029306 * phi * IPD
                + 0.0037335 * phi * phi
                + -0.000061791 * phi * phi * IPD
                + -1.233 * theta
                + 0.000032767 * theta * IPD
                + -0.0056208 * theta * phi
                + -0.0000016589 * theta * phi * IPD
                + 0.00006961 * theta * phi * phi
                + 0.00066835 * theta * theta
                + -0.000011878 * theta * theta * IPD
                + 0.0000040765 * theta * theta * phi
                + 0.29691 * 1
                + 0.0000017628 * theta * theta * theta
                + 0.000001726 * phi * phi * phi;

		double y =  0.004078 * IPD
                + 1.0447 * phi
                + 0.00013059 * phi * IPD
                + 0.00019842 * phi * phi
                + 0.00000096438 * phi * phi * IPD
                + 0.11986 * theta
                + -0.0011709 * theta * IPD
                + 0.0024096 * theta * phi
                + -0.000041343 * theta * phi * IPD
                + -0.000010215 * theta * phi * phi
                + -0.0062068 * theta * theta
                + -0.00001416 * theta * theta * IPD
                + -0.00010989 * theta * theta * phi
                + -0.48942 * 1
                + 0.00000011131 * theta * theta * theta
                + 0.000057112 * phi * phi * phi;

		return new Vector3((float)x, (float)y);


	}

	Vector3 DistortionR(float theta, float phi)
	{
		double x =   -0.035811 * IPD
                + 0.20792 * phi
                + -0.0029306 * phi * IPD
                + 0.0037335 * phi * phi
                + -0.000061791 * phi * phi * IPD
                + 1.233 * theta
                + -0.000032767 * theta * IPD
                + 0.0056208 * theta * phi
                + 0.0000016589 * theta * phi * IPD
                + -0.00006961 * theta * phi * phi
                + 0.00066835 * theta * theta
                + -0.000011878 * theta * theta * IPD
                + 0.0000040765 * theta * theta * phi
                + 0.29691 * 1
                + -0.0000017628 * theta * theta * theta
                + 0.000001726 * phi * phi * phi;

    double y =   0.004078 * IPD
                + 1.0447 * phi
                + 0.00013059 * phi * IPD
                + 0.00019842 * phi * phi
                + 0.00000096438 * phi * phi * IPD
                + -0.11986 * theta
                + 0.0011709 * theta * IPD
                + -0.0024096 * theta * phi
                + 0.000041343 * theta * phi * IPD
                + 0.000010215 * theta * phi * phi
                + -0.0062068 * theta * theta
                + -0.00001416 * theta * theta * IPD
                + -0.00010989 * theta * theta * phi
                + -0.48942 * 1
                + -0.00000011131 * theta * theta * theta
                + 0.000057112 * phi * phi * phi;

	return new Vector3((float)x, (float)y);
	
	}
	
}
