﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// reference ~> https://unity3d.com/learn/tutorials/topics/scripting/events-creating-simple-messaging-system

public class MiraEventManager : MonoBehaviour {

    private Dictionary<string, UnityEvent> eventDictionary;

    private static MiraEventManager eventManager;

    public static MiraEventManager instance
    {
        get
        {
            if (!eventManager)
            {
                eventManager = FindObjectOfType(typeof(MiraEventManager)) as MiraEventManager;

                if (!eventManager)
                {
                    Debug.LogError("There needs to be one active EventManger script on a GameObject in your scene.");
                }
                else
                {
                    eventManager.Init();
                }
            }

            return eventManager;
        }
    }

    void Init()
    {
        if (eventDictionary == null)
        {
            eventDictionary = new Dictionary<string, UnityEvent>();
        }
    }

    //public static void StartListening(string eventName, UnityAction listener)
    //{
    //    UnityEvent thisEvent = null;
    //    if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
    //    {
    //        thisEvent.AddListener(listener);
    //    }
    //    else
    //    {
    //        thisEvent = new UnityEvent();
    //        thisEvent.AddListener(listener);
    //        instance.eventDictionary.Add(eventName, thisEvent);
    //    }
    //}

    public static void StartListening(string eventName, UnityEvent newEvent)
    {
        instance.eventDictionary.Add(eventName, newEvent);
    }
    public static void StopListening(string eventName, UnityAction listener)
    {
        if (eventManager == null) return;
        UnityEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(string eventName)
    {
        UnityEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke();
        }
    }
}