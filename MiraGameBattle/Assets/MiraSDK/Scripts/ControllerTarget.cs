﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ControllerTarget : MonoBehaviour {

	/// <summary>
    /// Called every frame that this object is being gazed at.
    /// </summary>
    public abstract void ControllerOn();

    /// <summary>
    /// Called the first frame that this object is being gazed at.
    /// </summary>
    public abstract void OnStartControllerOn();

    /// <summary>
    /// Called the first frame after this object is no longer being gazed at.
    /// </summary>
    public abstract void OnStopControllerOn();

}
