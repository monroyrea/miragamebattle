﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingHandoff : MonoBehaviour {
	[SerializeField]
	GameObject currentObject;
	Vector3 defaultPos;
	Quaternion defaultRot;
	Transform tempPlacement;




	void Start () 
	{
		NewCurrentObject();
	}
	
	void Update () 
	{
		
	}

	// IEnumerator LostColorLerp()
	// {
	// 	while(lost == true)
	// 	{
	// 		lerpTime += (Time.deltaTime / lerpCycleTime);
	// 		currentObjRend.material.SetColor("_Color", Color.Lerp(defaultColor, Color.white, 0.5f * Mathf.Sin(lerpTime)));
	// 		Debug.Log("Lost Tracking Color LerpTime: " + (0.5f * Mathf.Sin(lerpTime)));
	// 		yield return null;
	// 	}
	// }


	public void TrackingLostEvent()
	{
		//Parent Tracked Object to the World
		if(currentObject)
		{
			currentObject.transform.SetParent(tempPlacement);
			currentObject.GetComponent<TrackingInteractable>().StartLostBeacon();
			//lerpTime = 0;
			//lost = true;
			//StartCoroutine(LostColorLerp());
		
		}
	}
	public void TrackingFoundEvent()
	{
		//Store child object to currentObject variable
		// To do: UIObject inherits from InteractableObj class, which should be here instead
		if (currentObject)
		{
			currentObject.GetComponent<TrackingInteractable>().StopLostBeacon();
			currentObject.transform.SetParent(transform);
			currentObject.transform.localPosition = defaultPos;
			currentObject.transform.localRotation = defaultRot;
		}

	}	
	public void NewCurrentObject()
	{	
		currentObject = transform.GetComponentInChildren<TrackingInteractable>().gameObject;
		defaultPos = currentObject.transform.localPosition;
		defaultRot = currentObject.transform.localRotation;
	}

}
