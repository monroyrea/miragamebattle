﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class MiraViewer : MonoBehaviour {

    public void Awake()
    {
        DontDestroyOnLoad(gameObject);
        gameObject.GetComponent<Transform>().hideFlags = HideFlags.HideInInspector;
    }
    public enum CameraNames
    {
        Right_Camera = 1,
        Left_Camera = 2,
    }

    public static MiraViewer Instance
    {
      get {

            if (instance == null && !Application.isPlaying)
            {
                instance = UnityEngine.Object.FindObjectOfType<MiraViewer>();   

            }
                if (instance == null)
                Debug.Log("Mira Viewer Found");

            return instance;
           }
    }
    private static MiraViewer instance = null;
    private static GyroController currentGyroController;
    private static Camera currentMainCamera;
    public static bool IsDebug = false;
    public static GameObject viewer = null;
    public void Create()
    {
        if (instance == null && UnityEngine.Object.FindObjectOfType<MiraViewer>() == null)
        {
           // Debug.Log("Creating MiraViewer object");
            GameObject go = new GameObject("MiraViewer", typeof(MiraViewer));
            go.transform.localPosition = Vector3.zero;
            viewer = go;
        }
    }



    public static GyroController Controller
    {
        get
        {
            Camera camera = Camera.main;
            if (camera != currentMainCamera || currentGyroController == null)
            {
                currentMainCamera = camera;
                currentGyroController = camera.GetComponent<GyroController>();
             }

            return currentGyroController;
        }
    }
    public GameObject CreateCameraforEachEye(CameraNames name)
    {

        Debug.Log("Creating "+ name);

       
        GameObject go = new GameObject(name.ToString(), typeof(Camera));
        Camera eyecamera = go.GetComponent<Camera>();
        eyecamera.clearFlags = CameraClearFlags.SolidColor;
        eyecamera.backgroundColor = new Color(0f, 0f, 0f, 0f);
        eyecamera.orthographic = false;
        eyecamera.fov = 49;
        eyecamera.transform.eulerAngles = new Vector3(-29f, 0f, 0f);
        eyecamera.transform.localPosition = new Vector3(-0.6f, 0.064f, 0f);
        eyecamera.depth = 99;
        eyecamera.far = 2000f;
        //eyecamera.cullingMask = 
        go.transform.SetParent(currentMainCamera.transform,false);
        //  var Eye = go.AddComponent<CameraDistort>();
        // Eye.DistortShader = Resources.Load("DistortionAnnotated3") as Shader;
        //  go.AddComponent<GUILayer>();
        //  go.AddComponent<FlareLayer>();
        if (name == CameraNames.Left_Camera)
        {
            eyecamera.rect = new Rect(0f, 0f, 1f, 1f);
            eyecamera.targetTexture = Resources.Load("LeftEye") as RenderTexture;
            eyecamera.transform.localPosition = new Vector3(-MiraArController.Instance.IPD/2, 0.064f, 0f);
        }
        else
        {
            eyecamera.rect = new Rect(0f, 0f, 1f, 1f);
            eyecamera.targetTexture = Resources.Load("RightEye") as RenderTexture;
            eyecamera.transform.localPosition = new Vector3(MiraArController.Instance.IPD/2, 0.064f, 0f);
        }
        return go;
           
    }
    

    public static GameObject backrenderer = null;

    public static GameObject BackgroundRenderer
    {
        get
        {
            if(backrenderer == null)
            {
                GameObject go = Instantiate(Resources.Load("BackgroundRender") as GameObject, currentMainCamera.transform);
                backrenderer = go;
            }
            return backrenderer;
        }
    }
   
    public static GameObject Left_Eye = null;
    public static GameObject Right_Eye = null;
    public void AddGyroControllerToCameras()
    {
        //Can be changed to addinng GyroController to more than one Cameras
        currentMainCamera.gameObject.AddComponent<GyroController>();
      Left_Eye =   CreateCameraforEachEye(CameraNames.Right_Camera);
      Right_Eye = CreateCameraforEachEye(CameraNames.Left_Camera);
    }

    public void AddReticle()
    {
        GameObject go = new GameObject("Reticle", typeof(SpriteRenderer));
        go.transform.SetParent(currentMainCamera.transform, false);
        SpriteRenderer reticlesprite = go.GetComponent<SpriteRenderer>();
        //reticlesprite.transform.position = new Vector3()
        reticlesprite.transform.localScale = new Vector3(2.8f, 2.8f, 2.8f);
        var reticle =  go.AddComponent<MiraReticle>();
      //  reticle.CameraFacing = Left_Eye.transform;
      //  reticle.camAngle = -30f;
        if (Resources.Load<Sprite>("Reticle_v1") != null)
            Debug.Log("FoundSprite");
        else
        Debug.Log("Sprite not founds");
        reticlesprite.sprite = Resources.Load<Sprite>("Reticle_v1");
        //reticlesprite.material.SetTexture(Resources.Load("Reticle_v1") as Texture2D);


    }


    public void AddPreRenderer()
    {
        GameObject go = new GameObject("PreRenderer");
        go.transform.SetParent(viewer.transform);
    }

    public void AddPostRenderer()
    {

        GameObject go = new GameObject("PostRenderer");
        go.transform.SetParent(viewer.transform);
    }
    
}
