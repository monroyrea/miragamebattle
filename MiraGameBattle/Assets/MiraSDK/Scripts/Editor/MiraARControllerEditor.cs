﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Kudan.AR;

[CustomEditor(typeof(MiraArController))]

public class MiraARControllerEditor : Editor
{
    int i = 0;
    void Awake()
    {
        //_target = (TrackingMethodMarker)target;
    }

    public override void OnInspectorGUI()
    {
        GUILayout.BeginVertical();
        this.DrawDefaultInspector();
        GUILayout.Space(16f);
        bool externalOperation = false;
       // GUILayout.TextField("Expected ID",);

        

        if (GUILayout.Button("Add KARMarker Asset"))
        {
            externalOperation = true;
            TrackableData.EditorCreateIssue();
        }
        if(GUILayout.Button("Add New Marker Component(Child of )"))
        {
            GameObject go = new GameObject("Marker_"+ i++, typeof(MarkerTransformDriver));
            go.transform.tag = "Marker";
            go.transform.SetParent(Camera.main.transform);
            go.transform.localPosition = new Vector3(0f, 0f, -180f);
        }
        if (GUILayout.Button("Get Support"))
        {
            Application.OpenURL("http://developers.miralabs.io/");
        }
        GUILayout.EndVertical();
        if (externalOperation)
        {
            // This has to be here otherwise we get strange GUI stack exceptions
            EditorGUIUtility.ExitGUI();
        }
    }

}
