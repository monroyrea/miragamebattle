﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudan.AR;

public class MiraMarkerEvents : MarkerEvents
{
    string id = "";

    protected override void Start()
    {
        base.Start();
        trackingMethodMarker = UnityEngine.Object.FindObjectOfType<TrackingMethodMarker>();
        for (int i = 0; i < MiraArController.Instance.ListOfMarkers.Length; i++)
        {
           
            MiraArController.Instance.ListOfMarkers[i].currentState = MiraArController.MarkerState.Passive;
             MiraEventManager.StartListening(MiraArController.Instance.ListOfMarkers[i].id+ "_found", MiraArController.Instance.ListOfMarkers[i].OnMarkerFound);
            MiraEventManager.StartListening(MiraArController.Instance.ListOfMarkers[i].id + "_update", MiraArController.Instance.ListOfMarkers[i].OnMarkerUpdate);
            MiraEventManager.StartListening(MiraArController.Instance.ListOfMarkers[i].id + "_lost", MiraArController.Instance.ListOfMarkers[i].OnMarkerLost);
           
        }
    }

    protected override   void Update()
    {
        base.Update();
     
    }
    public void ChangetoFoundState()
    {
        for (int i = 0; i < MiraArController.Instance.ListOfMarkers.Length; i++)
        {
            if (id == MiraArController.Instance.ListOfMarkers[i].id)
                MiraArController.Instance.ListOfMarkers[i].currentState = MiraArController.MarkerState.Found;

        }
     }
    public void ChangetoLostState()
    {
        for (int i = 0; i < MiraArController.Instance.ListOfMarkers.Length; i++)
        {
            if (id == MiraArController.Instance.ListOfMarkers[i].id)
                MiraArController.Instance.ListOfMarkers[i].currentState = MiraArController.MarkerState.Lost;

        }

    }
    public void ChangetoActiveState()
    {
        for (int i = 0; i < MiraArController.Instance.ListOfMarkers.Length; i++)
        {
            if (id == MiraArController.Instance.ListOfMarkers[i].id)
                MiraArController.Instance.ListOfMarkers[i].currentState = MiraArController.MarkerState.Active;

        }
    }
    public void ChangeToPassiveState()
    {
        for (int i = 0; i < MiraArController.Instance.ListOfMarkers.Length; i++)
        {
            if (id == MiraArController.Instance.ListOfMarkers[i].id)
                MiraArController.Instance.ListOfMarkers[i].currentState = MiraArController.MarkerState.Passive;

        }
    }
    public override void checkFound(markerStruct markerStruct)
    {
        
        if (markerStruct.isActive && !markerStruct.wasActive)
        {
            //name = marker.name;
            id = markerStruct.name;
            ChangetoFoundState();
            MiraEventManager.TriggerEvent(markerStruct.name + "_found");
            
        }

    }
    public override void checkTrack(markerStruct markerStruct)
    {
        // If the marker is currently active, it is being tracked
        if (markerStruct.marker.activeInHierarchy)
        {
            id = markerStruct.name;
            ChangetoActiveState();
            MiraEventManager.TriggerEvent(markerStruct.name + "_update");
            
        }
    
        // If the marker is no longer active, it must have been lost, so set the marker to null, allowing the marker lost event to fire
        else
            markerStruct.marker = null;
    }

    public override void checkLost(markerStruct markerStruct)
    {
        // If the marker is no longer active but was active last frame, it must have been lost this frame
        if (!markerStruct.isActive && markerStruct.wasActive)
        {
            id = markerStruct.name;
            ChangetoLostState();
            MiraEventManager.TriggerEvent(markerStruct.name + "_lost");
        }
        else
        {
            ChangeToPassiveState();
        }
    }
}