using UnityEngine;
using System.Collections;
public class MiraReticle : MonoBehaviour
{
    public Transform ParentFacing;
   
    /// <summary>
    /// ScaleMultiplier scales up the reticule when hovering over a ControllerTarget
    /// </summary>
    float scaleMultiplier = 1.3f;
    /// <summary>
    /// The Color the reticule is tinted when hovering over a ControllerTarget
    /// </summary>
    [SerializeField]
    Color reticleHoverColor = Color.red;
    SpriteRenderer reticleRenderer;
    private Vector3 reticleOriginalScale;
    private RaycastHit raycastHit;
    private GameObject gameObjectHit;
    private ControllerTarget ControllerTargetHit;
    public RaycastHit GetRaycastHit() { return raycastHit; }
    public GameObject GetGameObjectHit() { return gameObjectHit; }
    public ControllerTarget GetControllerTargetHit() { return ControllerTargetHit; }

    // Sets Controller Offset in Z
    public float reticuleOffsetZ = 600;

    float camAngle = 0;
    float camRadians;

    Vector3 nullPos;



    void Start()
    {
       
       ParentFacing = transform.parent;
        reticleRenderer = GetComponent<SpriteRenderer>();
        reticleOriginalScale = transform.localScale;

        nullPos = new Vector3(0, 0, reticuleOffsetZ);
        transform.localPosition = nullPos;
        transform.LookAt(ParentFacing.position);

    }


    void Update()
    {
        // Starting position of ray
        Vector3 position = ParentFacing.position;
        Vector3 target = ParentFacing.forward;

        Debug.DrawRay(position, target * 1000f, Color.red);

        if (Physics.Raycast(position, target, out raycastHit))
        {
            gameObjectHit = raycastHit.collider.gameObject;


            // Check if ray hit a ControllerTarget
            ControllerTarget ControllerTargetComp;
            ControllerTargetComp = gameObjectHit.GetComponent<ControllerTarget>();
            if (!ControllerTargetComp) ControllerTargetComp = gameObjectHit.GetComponentInParent<ControllerTarget>();
            else if (!ControllerTargetComp) ControllerTargetComp = gameObjectHit.GetComponentInChildren<ControllerTarget>();

            // Call appropriate ControllerTarget functions
            if (ControllerTargetComp)
            {   // ControllerTarget hit
                if (ControllerTargetComp != ControllerTargetHit)
                {
                    if (ControllerTargetHit)
                        ControllerTargetHit.OnStopControllerOn();

                    // Reticle hover mode
                    transform.localScale = reticleOriginalScale * scaleMultiplier;
                    reticleRenderer.color = reticleHoverColor;


                    ControllerTargetComp.OnStartControllerOn();
                    ControllerTargetHit = ControllerTargetComp;
                }

                //Debug.Log("Gaze hit this: " + ControllerTargetComp.ToString());
                ControllerTargetComp.ControllerOn();
            }
            else if (ControllerTargetHit)
            {   // Hit something without a ControllerTarget, but just was on a ControllerTarget
                ControllerTargetHit.OnStopControllerOn();
                ControllerTargetHit = null;

            }

        }
        else
        {   // Didn't hit anything
            if (ControllerTargetHit)
            {   // Just was on a ControllerTarget
                ControllerTargetHit.OnStopControllerOn();
                ControllerTargetHit = null;
                // Reset Reticle
                transform.localScale = reticleOriginalScale;
                reticleRenderer.color = Color.white;
            }




        }
    }
}