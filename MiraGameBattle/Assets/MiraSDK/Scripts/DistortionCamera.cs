﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistortionCamera : MonoBehaviour {


    public static DistortionCamera instance = null;
    public static DistortionCamera Instance
    {
        get
        {
            if(instance == null)
            {
                instance = UnityEngine.Object.FindObjectOfType<DistortionCamera>();
            }

            return instance;
        }
    }

    public void Create()
    {
        if (instance == null && UnityEngine.Object.FindObjectOfType<DistortionCamera>() == null)
        {
            GameObject go = new GameObject("MiraDistortionCamera", typeof(DistortionCamera));
            Camera cam = go.AddComponent<Camera>();
            go.AddComponent<MiraPreRender>();
            cam.clearFlags = CameraClearFlags.SolidColor;
            cam.backgroundColor = new Color(0f, 0f, 0f);
            cam.orthographic = false;
            cam.fov = 60;
            cam.cullingMask = 0;
            cam.depth = -100;
            cam.useOcclusionCulling = false;
            AddCamera('R');
            AddCamera('L');
        }
        Debug.Log("Already Exists");
    }

    public void AddCamera(char Camera)
    {

        GameObject go = new GameObject("DistortionCamera" + Camera, typeof(Camera));
        go.transform.SetParent(DistortionCamera.Instance.transform);
        Camera cam = go.GetComponent<Camera>();
        {
            cam.clearFlags = CameraClearFlags.SolidColor;
            cam.backgroundColor = new Color(0f, 0f, 0f, 0f);
            cam.orthographic = true;
            cam.orthographicSize = 15;
            cam.nearClipPlane = 0.3f;
            cam.farClipPlane = 99.21f;
            cam.depth = 999;
            if (Camera == 'L')
            {
                cam.rect = new Rect(0.5f, 0f, 0.5f, 1f);
                cam.cullingMask = 1<<8;
            }
            else
            {
                cam.rect = new Rect(0f, 0f, 0.5f, 1f);
                cam.cullingMask = 1<<9;
            }

        }
       
        go.AddComponent<MeshFilter>();
        //MiraPreRender mpre =  go.AddComponent<MiraPreRender>();
        MiraPostRender mpost =  go.AddComponent<MiraPostRender>();
        mpost.IPD = MiraArController.Instance.IPD;
       if(Camera == 'L')
        {
            mpost.eye = MiraPostRender.Eye.Left;
            mpost.renderTextureMaterial = Resources.Load("DistortionLeft") as Material;
        }
       else
        {
            mpost.eye = MiraPostRender.Eye.Right;
            mpost.renderTextureMaterial = Resources.Load("DistortionRight") as Material;
        }
        
    }
}