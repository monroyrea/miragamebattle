﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ximmerse;
using Ximmerse.InputSystem;

public class ControllerTransform : MonoBehaviour {

	public bool allowControllerClick = true;

	/// <summary>
    /// Button to recenter controller axes
    /// </summary>
	[SerializeField]
	ControllerButton recenterButton;

	/// <summary>
    /// Button to reset camera and controller direction
    /// </summary>
	[SerializeField]
	ControllerButton rotateToCamButton;
	
	/// <summary>
    /// Does it match the camera vertically and horizontally, or just horizontally
    /// </summary>
	[SerializeField]
	bool horizontalOnly = true;
	ControllerInput controller;
	
	Quaternion offsetRotation;

	/// <summary>
    /// Does the controller also track Position?
    /// </summary>
	[SerializeField]
	bool trackPosition;

	/// <summary>
    /// Scene Camera Gyro
    /// </summary>
	public GyroController gyro;

	float frameRate;

	/// <summary>
    /// Scale of current scene vs Unity default (to correctly map arm model)
    /// </summary>

	float sceneScaleMult = 100;

	/// <summary>
    /// Offset of Arm Model in X
    /// </summary>
	[SerializeField]
	float sceneOffsetX = 0f;

	/// <summary>
    /// Offset of Arm Model in Y
    /// </summary>
	[SerializeField]
	float sceneOffsetY = 0f;

	/// <summary>
    /// Offset of Arm Model in Z
    /// </summary>
	[SerializeField]
	float sceneOffsetZ = 0f;

	
	void Start () 
	{
        gyro = UnityEngine.Object.FindObjectOfType<GyroController>();
        offsetRotation = Quaternion.identity;
		controller = ControllerInputManager.instance.GetControllerInput(ControllerType.LeftController);	
	}
	void RotateController()
	{
		transform.rotation = offsetRotation * controller.GetRotation();
	}
	void TranslateController()
	{
		transform.localPosition = controller.GetPosition() * sceneScaleMult;
		transform.Translate(new Vector3(sceneOffsetX, sceneOffsetY, sceneOffsetZ), Space.World);
	}
	void GetControllerFPS()
	{
		frameRate = controller.frameCount;
	}
	void Update () 
	{
		if(controller != null)
		{
			if(trackPosition)
			{
				TranslateController();
			}

			RotateController();
			// GetControllerFPS();
			if (allowControllerClick == true) {
				if (controller.GetButton (recenterButton)) {
					//	controller.Recenter();
				}	
				if (controller.GetButton (rotateToCamButton)) {
	
					gyro.Recenter (true);
					Quaternion cameraRigRotation;
					if (horizontalOnly) {
						Vector3 fw = gyro.transform.forward;
						fw.y = 0;
						cameraRigRotation = Quaternion.FromToRotation (Vector3.forward, fw);
					} else {
						cameraRigRotation = gyro.transform.rotation;
					}
				
					offsetRotation = Quaternion.Inverse (Quaternion.Inverse (offsetRotation) * transform.rotation * cameraRigRotation);
	
				}
			}
		}
	}
}
