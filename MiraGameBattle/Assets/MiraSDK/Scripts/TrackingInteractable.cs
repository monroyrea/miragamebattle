﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingInteractable : MonoBehaviour {
	bool trackingLost = false;
	Material baseMaterial;

	Color defaultColor;

	Collider thisCollider;

	float lerpCycleTime = 0.3f;

	Color beaconLerpColor;

	UIObject thisUIObj;

	protected void TrackingInit () {
		baseMaterial = gameObject.GetComponent<Renderer>().material;
        //Debug.Log(baseMaterial.name);
        defaultColor = baseMaterial.GetColor("_Color");
        //Debug.Log(baseMaterial.name);
		beaconLerpColor = Color.red;
		thisCollider = gameObject.GetComponent<Collider>();
		thisUIObj = gameObject.GetComponent<UIObject>();
		
	}
	
	public void StartLostBeacon()
	{
		trackingLost = true;
		if(thisCollider)
		{
			thisCollider.enabled = false;
		}
		StartCoroutine(LostTrackingBeacon());
	}

	public void StopLostBeacon()
	{
		trackingLost = false;
		if(thisCollider)
		{
			thisCollider.enabled = true;

		}
		baseMaterial.SetColor("_Color", defaultColor);

	}

	IEnumerator LostTrackingBeacon()
	{
		float lerpTime = 0f;
		while(trackingLost == true)
		{
			lerpTime += (Time.deltaTime / lerpCycleTime);
			baseMaterial.SetColor("_Color", Color.Lerp(defaultColor, beaconLerpColor, 0.5f + (0.5f * Mathf.Sin(lerpTime))));
			yield return null;
		}
	}
}
