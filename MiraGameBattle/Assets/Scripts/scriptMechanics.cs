using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class ClassPlayerSensorsData
{
	public scriptSensor GetSensors{
		get{ return _sensors;}
	}
	public scriptMechanics.EnumAttackCollider GetAttackColliders{
		get { return _attackCollider;}
	}
	[SerializeField] scriptSensor _sensors;
	[SerializeField] scriptMechanics.EnumAttackCollider _attackCollider;
}

public class scriptMechanics : MonoBehaviour {
	

	// Use this for initialization
	public enum EnumPlayerType{
		player_1,
		player_2
	}
	void Start () {
		_currentHealth = _maximumHealth;
		
	}
	public enum EnumAttackCollider{
		leftHand,
		rightHand,
		leftLeg,
		rightLeg
	};
    public float GetSliderValue
    {
        get { return _value; }
        set { _value = value; }
    }
	public Animator GetCharacterAnimator{
		get{ return  anim;}
	}
	public List<ClassPlayerSensorsData> GetPlayerSensorList{
		get{ return _playerSensorList;}
	}
	public float GetSetCurrentHealth{
		get{ return _currentHealth;}
		set{ _currentHealth = value;}
	}
	public float GetSetMaximumHealth{
		get{ return _maximumHealth;}
		set{ _maximumHealth = value;}
	}
	public EnumPlayerType GetPlayerType{
		get{ return _instancePlayerType;}
		set{ _instancePlayerType = value;}
	}
	[SerializeField] List<ClassPlayerSensorsData> _playerSensorList;
	[SerializeField] Animator anim;
    [SerializeField] float _value;
	[SerializeField]float _maximumHealth;
	[SerializeField]float _currentHealth;
	[SerializeField]EnumPlayerType _instancePlayerType;


	
	
}
