﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	//Player Setup
	public Text gesture;
	private GameObject enemy;
	private float verticalVelocity;
	private float gravity = 650.0f;
	private float jumpForce =450.0f;
	private bool canJump = false;
	public bool defending = false;

	//Gameboard Limits
	private GameObject leftLimit;
	private GameObject rightLimit;

	Animator anim;

	//Controller Variables
	private CharacterController charController;
	private float playerMovementX;
	private Time time;



	// Use this for initialization
	void Start () {
		//
		playerMovementX = 0;
		//playerLife.value = 40;

		//Getting components
		//cem = GameObject.FindGameObjectWithTag("MiraController").GetComponent<ControllerEventManager>();
		enemy = GameObject.FindGameObjectWithTag("enemy");
		anim = gameObject.GetComponent<Animator> ();
		charController = gameObject.GetComponent<CharacterController> ();
		rightLimit = GameObject.FindGameObjectWithTag("RightLimit");
		leftLimit = GameObject.FindGameObjectWithTag("LeftLimit");
		gesture.text = "";

		ControllerEventManager controllerEvent = GameObject.FindObjectOfType<ControllerEventManager> ();
		if (controllerEvent) { 
			ControllerEventManager.SendTouchPadX += MovePlayerX;
			ControllerEventManager.TriggerStart += playerJump;
		}
		//Registering events for controller input
	}
	
	// Update is called once per frame
	void Update () {
		
		//Trigger player die animation
		//if(playerLife.value == 0)
		//	anim.SetTrigger ("monster_die");
		if(enemy == null){ enemy = GameObject.FindGameObjectWithTag("enemy");}
		//Rotate player to always look to enemy
		if (enemy != null) {
			Vector3 lookPos = enemy.transform.position - gameObject.transform.position;
			lookPos.y = 0;
			Quaternion rotation = Quaternion.LookRotation (lookPos);
			gameObject.transform.rotation = Quaternion.Slerp (transform.rotation, rotation, Time.deltaTime * 200.0f);
		}
		//Getting controller data to trigger animations
		if (ControllerEventManager.controller != null) {
			Vector3 giroscope = ControllerEventManager.gyroscope;
			Vector3 accelerometer = ControllerEventManager.accelerometer;
			if (accelerometer.x > 5 && accelerometer.x < 9 && accelerometer.y > -4 && accelerometer.y < 4) {
				anim.SetInteger ("monster_defend", 1);
				defending = true;
				gesture.text = "Defending";
			} else {
				anim.SetInteger ("monster_defend", 0);
				defending = false;
				if (giroscope.x > 10) {
					anim.SetTrigger ("monster_attack1");
					gesture.text = "Atack1";
				} else if (giroscope.x < -10) {
					anim.SetTrigger ("monster_attack2");
					gesture.text = "Atack2";
				} else if (giroscope.y > 10) {
					anim.SetTrigger ("monster_attack3");
					gesture.text = "Atack3";
				}
			}
		} 
	}
		
		void FixedUpdate(){
		//Player Movement Forward, Backwards and jump.
		float moveHorizontal = playerMovementX; //Input.GetAxis ("Horizontal");
		Debug.Log (moveHorizontal);
		if (charController.isGrounded) {
			verticalVelocity = -gravity * Time.deltaTime;
			if (canJump) {
				anim.SetTrigger ("monster_jump");
				verticalVelocity = jumpForce;
				canJump = false;
			}
		} else {
			verticalVelocity -= gravity * Time.deltaTime;
		}
		Vector3 MoveVector = Vector3.zero;

		if (transform.rotation.eulerAngles.y < 120) {
			anim.SetFloat ("monster_walkF", -moveHorizontal);
			anim.SetFloat ("monster_walkB", -moveHorizontal);
		} else {
			anim.SetFloat ("monster_walkF", moveHorizontal);
			anim.SetFloat ("monster_walkB", moveHorizontal);

		}


		MoveVector.x = -moveHorizontal * 200;
		MoveVector.y = verticalVelocity;

		charController.Move (MoveVector * Time.deltaTime);
		transform.position = new Vector3(Mathf.Clamp (transform.position.x, leftLimit.transform.position.x, rightLimit.transform.position.x),transform.position.y, transform.position.z);

	}

	//Getting controller data to trigger player attacks
	void MovePlayerX(float value){
		playerMovementX = value;
	}

	void playerJump(){
		canJump = true;
	}

}
