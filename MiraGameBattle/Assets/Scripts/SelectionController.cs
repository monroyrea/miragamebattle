﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SelectionController : MonoBehaviour {
	//static selected player
	public static int selectedPlayer = 0;
	public static int selectedEnemy = 0;

	public static Vector3 boardPosition;
	public static Quaternion boardRotation;

	//Activate Gameobjects
	public ControllerTransform ct;
	public GameObject laser;
	public GameObject startSetupUI;
	public GameObject characterSelectionUI;
	public GameObject characterSelection;
	public GameObject battleBoard;
	public MeshRenderer battleBoardRenderer;


	private int count = 0;
	// Use this for initialization
	void Start () {

		characterSelection.SetActive (false);
		characterSelectionUI.SetActive (false);

		ControllerEventManager controllerEvent = GameObject.FindObjectOfType<ControllerEventManager> ();
		if (controllerEvent) { 
			ControllerEventManager.BackDown += placeBoard;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown (KeyCode.Space))
		SceneManager.LoadScene ("BattleScene");

	}

	void placeBoard(){
		if (count == 0) {
			startSetupUI.SetActive (false);
			battleBoardRenderer.enabled = false;

			battleBoard.transform.parent = null;
			//moveBattle = false;
			battleBoard.transform.eulerAngles = new Vector3 (90f, 0f, 0f);
			battleBoardRenderer.enabled = false;
			ct.allowControllerClick = false;

			characterSelection.SetActive (true);
			characterSelectionUI.SetActive (true);
		} else {
			RaycastHit hit;
			Ray ray = new Ray (laser.transform.position, Vector3.forward);

			if(Physics.Raycast(ray, out hit)){
				if (hit.collider.gameObject.name == "default") {
					selectedPlayer = 0;
					LoadBattle ();
				} else if (hit.collider.gameObject.name == "troll") {
					selectedPlayer = 1;
					LoadBattle ();
				} else if (hit.collider.gameObject.name == "golem") {
					selectedPlayer = 2;
					LoadBattle ();
				} else if (hit.collider.gameObject.name == "skeleton") {
					selectedPlayer = 3;
					LoadBattle ();
				}
			}
				
		}

		count++;
	}

	private void LoadBattle(){
		SceneManager.LoadScene ("BattleScene");
		boardPosition = battleBoard.transform.position;
		boardRotation = battleBoard.transform.rotation;
	}
}
