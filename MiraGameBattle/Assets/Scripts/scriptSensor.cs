using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class scriptSensor : scriptSensorHitValues {
	public GameObject [] enemies;
	public Sprite [] sprites;
	public Image enemyImage;

	public Slider Enemy;
	public Slider Player;
	public Text gameOver;
	public GameObject battleBoard;
	public GameObject uiVS;
	public GameObject battleUI;
	private PlayerController pController;
	private EnemyAIController eAIController;
	// Use this for initialization
	public enum PlayerHittingCollider
	{
		NoCollision,
		BodyCollision,
		HitCollision

	};

	void Start () {
		pController = GameObject.FindGameObjectWithTag ("player").GetComponent<PlayerController> ();
		eAIController = GameObject.FindGameObjectWithTag ("enemy").GetComponent<EnemyAIController> ();
		Enemy.value = 30;
		gameOver.text = "";
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public PlayerHittingCollider GetPlayerHittingCollider {
		get { return _instance;}
		set{ _instance = value;}
	}

	void OnTriggerEnter(Collider other){
		Debug.Log ("1");

		if (_instance != PlayerHittingCollider.NoCollision) {
			Debug.Log ("2");

			var tempScript = other.GetComponent<scriptSensor> ();
			if(tempScript != null){
				if (_parent != tempScript.GetParent) {
					Debug.Log ("3");

					if (!tempScript.GetSensorHit) {
						Debug.Log ("4");

						if (tempScript.GetPlayerHittingCollider == PlayerHittingCollider.HitCollision) {
							tempScript.GetSensorHit = true;
							Debug.Log ("5");

							var myParent = _parent.GetComponent<scriptMechanics> ();
							myParent.GetCharacterAnimator.SetTrigger ("hitReaction");
							myParent.GetSetCurrentHealth -= tempScript.GetHitPower;

							if (myParent.GetPlayerType == scriptMechanics.EnumPlayerType.player_1 && pController.defending == false) {
								Enemy.value = myParent.GetSetCurrentHealth;
								Debug.Log ("6");

							} else if(myParent.GetPlayerType == scriptMechanics.EnumPlayerType.player_2 && pController.defending == false){
								Debug.Log ("7");
								Player.value = myParent.GetSetCurrentHealth;
							}
							if (myParent.GetSetCurrentHealth <= 0) {
								myParent.GetSetCurrentHealth = 0;
								if (myParent.GetPlayerType == scriptMechanics.EnumPlayerType.player_1) {
									if (eAIController == null) {
										eAIController = GameObject.FindGameObjectWithTag ("enemy").GetComponent<EnemyAIController> ();
									}
									myParent.GetCharacterAnimator.SetTrigger ("monster_die");
									eAIController.enabled = false;
									gameOver.text = "You WIN ";
									if (SelectionController.selectedEnemy < 3) {
										SelectionController.selectedEnemy++;
										GameObject enemy =  GameObject.FindGameObjectWithTag ("enemy");
										Destroy (enemy);
										enemies [SelectionController.selectedEnemy].SetActive (true);

										/*battleUI.SetActive (false);
										uiVS.SetActive (true);

										switch (SelectionController.selectedEnemy) {
										case(0):
											enemyImage.sprite = sprites [0];
											Invoke ("fight", .5f);
											break;
										case(1):
											enemyImage.sprite = sprites [1];
											Invoke ("fight", .5f);
											break;
										case(2):
											enemyImage.sprite = sprites [2];
											Invoke ("fight", .5f);
											break;
										case(3):
											enemyImage.sprite = sprites [3];
											Invoke ("fight", .5f);
											break;
										}*/
									}
								} else {
									pController.enabled = false;
									myParent.GetCharacterAnimator.SetTrigger ("monster_die");
									gameOver.text = "You LOSE";
								}
							}
						}
					}
				}
	   		 }
		}
	}
		

	void fight(){
		battleUI.SetActive (true);
		uiVS.SetActive (false);
	}

	public Transform GetParent{
		get{ return _parent; }
	}
	public bool GetSensorHit{
		get{ return _getHit;}
		set{ _getHit = value;}
	}
	[SerializeField] bool _getHit;
	[SerializeField] Transform _parent;//making the sensors aware about who is the parent//
	[SerializeField] PlayerHittingCollider _instance; 
}
