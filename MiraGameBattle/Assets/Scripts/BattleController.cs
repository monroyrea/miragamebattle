﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BattleController : MonoBehaviour {

	public Image playerImage;
	public Image enemyImage;
	public Sprite[] sprites;

	//FPS Variables
	public GameObject[] enemies;
	public GameObject[] players;

	public GameObject battleBoard;
	public GameObject battleUI;

	public GameObject VSUI;
	public MeshRenderer laser;

	//Test
	MeshRenderer rend;

	// Use this for initialization
	void Start () {
		battleBoard.SetActive (false);
		battleUI.SetActive (false);

		switch (SelectionController.selectedPlayer) {
		case(0):
			playerImage.sprite = sprites [0];
			break;
		case(1):
			playerImage.sprite = sprites [1];
			break;
		case(2):
			playerImage.sprite = sprites [2];
			break;
		case(3):
			playerImage.sprite = sprites [3];
			break;
		}

		switch (SelectionController.selectedEnemy) {
		case(0):
			enemyImage.sprite = sprites [0];
			break;
		case(1):
			enemyImage.sprite = sprites [1];
			break;
		case(2):
			enemyImage.sprite = sprites [2];
			break;
		case(3):
			enemyImage.sprite = sprites [3];
			break;
		}


		Invoke ("startBattle", 3.0f);
		//laser.enabled = false;

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown (KeyCode.Space))
			SceneManager.LoadScene ("BattleScene");
	}

	void startBattle(){
		VSUI.SetActive (false);
		battleUI.SetActive (true);
		battleBoard.SetActive (true);
		battleBoard.transform.position = new Vector3(SelectionController.boardPosition.x,SelectionController.boardPosition.y,-SelectionController.boardPosition.z);
		float rot = battleBoard.transform.rotation.eulerAngles.z + 180.0f;
		battleBoard.transform.rotation = Quaternion.Euler (new Vector3(battleBoard.transform.rotation.eulerAngles.x,battleBoard.transform.rotation.eulerAngles.y, rot ));

		switch (SelectionController.selectedPlayer) {
		case(0):
			players [0].SetActive (true);
			break;
		case(1):
			players [1].SetActive (true);
			break;
		case(2):
			players [2].SetActive (true);
			break;
		case(3):
			players [3].SetActive (true);
			break;
		}

		switch (SelectionController.selectedEnemy) {
		case(0):
			enemies [0].SetActive (true);
			break;
		case(1):
			enemies [1].SetActive (true);
			break;
		case(2):
			enemies [2].SetActive (true);
			break;
		case(3):
			enemies [3].SetActive (true);
			break;
		}

	}
}
