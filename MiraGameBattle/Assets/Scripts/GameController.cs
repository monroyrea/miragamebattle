﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	//GameOver
	public string playerToSpawn;

	//FPS Variables
	public GameObject[] enemies;
	public GameObject[] players;
	int m_frameCounter = 0;
	float m_timeCounter = 0.0f;
	//float m_lastFramerate = 0.0f;
	public float m_refreshTime = 0.5f;

	public Text FPS;
	public GameObject selectCharacter;
	public ControllerTransform ct;

	public GameObject laser;
	private GameObject setupUI;
	private GameObject battleUI;
	private GameObject boardObjects;
	private GameObject playerSelection;
	//Test
	private int count= 0;
	GameObject battleBoard;
	MeshRenderer rend;
	bool moveBattle = true;
	//Move TouchpadY

	float lastCast;
	// Use this for initialization
	void Start () {
		battleBoard = GameObject.FindGameObjectWithTag ("BattleBoard");
		ct = GameObject.FindObjectOfType<ControllerTransform> ();
		setupUI = GameObject.FindGameObjectWithTag ("Setup");
		battleUI = GameObject.FindGameObjectWithTag ("BattleUI");
		selectCharacter.SetActive (false);
		boardObjects = GameObject.FindGameObjectWithTag ("BoardObjects");
		playerSelection = GameObject.FindGameObjectWithTag ("playerSelection");


		boardObjects.SetActive (false);
		playerSelection.SetActive (false);
		rend = battleBoard.GetComponent<MeshRenderer> ();
		battleUI.SetActive (false);

		ControllerEventManager controllerEvent = GameObject.FindObjectOfType<ControllerEventManager> ();
		if (controllerEvent) { 
			ControllerEventManager.BackDown += placeBoard;
			ControllerEventManager.SendTouchPadY += Cast;

		}
	}
	
	// Update is called once per frame
	void Update () {

		/*if (pController.playerLife.value == 0) {
			gameOver.text = "You Lose";
		} else if (eController.enemyLife.value == 0) {
			gameOver.text ="You Win"
		}*/
		if( m_timeCounter < m_refreshTime )
		{
			m_timeCounter += Time.deltaTime;
			m_frameCounter++;
		}
		else
		{
			//This code will break if you set your m_refreshTime to 0, which makes no sense.
			//m_lastFramerate = (float)m_frameCounter/m_timeCounter;
			m_frameCounter = 0;
			m_timeCounter = 0.0f;
		}

		//FPS.text = "FPS: " + m_lastFramerate;
	}

	void placeBoard(){
		if (count == 0) {
			battleBoard.transform.parent = null;
			moveBattle = false;
			battleBoard.transform.eulerAngles = new Vector3 (90f, 0f, 0f);
			rend.enabled = false;
			ct.allowControllerClick = false;
			setupUI.SetActive (false);
			//battleUI.SetActive (true);
			playerSelection.SetActive (true);
			selectCharacter.SetActive (true);
		} else {
			RaycastHit hit;
			Ray ray = new Ray (laser.transform.position, Vector3.forward);

			if(Physics.Raycast(ray, out hit)){
				bool a = false;
				if (hit.collider.gameObject.name == "skeleton") {
					players [0].SetActive (true);
					a = true;
				} else if (hit.collider.gameObject.name == "default") {
					players [1].SetActive (true);
					a = true;
				} else if (hit.collider.gameObject.name == "golem") {
					players [2].SetActive (true);
					a = true;
				} else if (hit.collider.gameObject.name == "troll") {
					players [3].SetActive (true);
					a = true;
				}
				if (a) {
					boardObjects.SetActive (true);
					enemies [0].SetActive (true);
					battleUI.SetActive (true);
					playerSelection.SetActive (false);
					selectCharacter.SetActive (false);
				}
			}
				
		}

		count++;
	}

	void Cast(float value)
	{
		if (value != 0 && moveBattle == true) {
			
			float battleBoardX = battleBoard.transform.position.x;
			float battleBoardY = battleBoard.transform.position.y;
			float battleBoardZ = battleBoard.transform.position.z;

			if (battleBoardZ >= 600f || battleBoardZ <= 1700) {
				Vector3 tempPosition = battleBoard.transform.position;
				Vector3 tempScale = battleBoard.transform.localScale;
					
				battleBoard.transform.Translate (Vector3.up * (value * 10f));
				battleBoard.transform.localScale += new Vector3 (value * 15f, value * 15f, value * 15f );

				if (battleBoard.transform.position.z < 550 || battleBoard.transform.position.z > 1700) {
					battleBoard.transform.position = tempPosition;
					battleBoard.transform.localScale = tempScale;
				}
			}
		}
	}
}
