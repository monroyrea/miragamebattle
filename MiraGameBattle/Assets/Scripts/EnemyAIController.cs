﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EnemyAIController : MonoBehaviour {

	//Constants for enemy movement
	const int ATTACK1 = 0;
	const int ATTACK2 = 1;
	const int ATTACK3 = 2;
	const int WALKF = 3;
	const int WALKB = 4;

	//Enemy variables
	public float speed;
	private GameObject player;
	private GameObject leftLimit;
	private GameObject rightLimit;
	bool rotate;
	float walkSpeed;

	Animator anim;
	Rigidbody rb;
	int a;

	int startAI;
	float delay = 1;
	float nextUsage;
	// Use this for initialization
	void Start () {
		startAI = 0;
		rb = gameObject.GetComponent<Rigidbody> ();
		anim = gameObject.GetComponent<Animator> ();
		anim.SetInteger ("monster_idle", 1);

		//find Components
		rightLimit = GameObject.FindGameObjectWithTag("RightLimit");
		leftLimit = GameObject.FindGameObjectWithTag("LeftLimit");
		player = GameObject.FindGameObjectWithTag ("player");

	}
	
	// Update is called once per frame
	void Update () {
		if(!anim.GetCurrentAnimatorStateInfo(0).IsName("creature1Spawn")){
			startAI = 1;
		}
			
		float distance = Vector3.Distance (player.transform.position, gameObject.transform.position);

		Vector3 lookPos = player.transform.position - gameObject.transform.position;
		lookPos.y = 0;
		Quaternion rotation = Quaternion.LookRotation (lookPos);
		gameObject.transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 200.0f);

		if(startAI == 1)
		{

			if (distance > 200) {
				a = 3;
				approach ();
			}
			else {
				if (Time.time >nextUsage) {
					nextUsage = Time.time + delay;
					a = attack ();
					if (a == 0) {
						anim.SetTrigger ("monster_attack1");
					} else if (a == 1) {
						anim.SetTrigger ("monster_attack2");
					} else if (a == 2) {
						anim.SetTrigger ("monster_attack3");
					} else if (a == 3) {
						anim.SetTrigger ("monster_walkF");
					} else if (a == 4) {
						anim.SetTrigger ("monster_walkB");
					}
				}
			}
		}
	}

	void FixedUpdate(){
		if (a == 3) {
			Vector3 movement = transform.position + transform.forward * Time.deltaTime * walkSpeed;
			rb.MovePosition(new Vector3(Mathf.Clamp(movement.x, leftLimit.transform.position.x, rightLimit.transform.position.x),
				movement.y,
				movement.z));
		} else if(a==4){
			Vector3 movement = transform.position + transform.forward * Time.deltaTime * -60;
			rb.MovePosition(new Vector3(Mathf.Clamp(movement.x, leftLimit.transform.position.x, rightLimit.transform.position.x),
				movement.y,
				movement.z));
		}		
	}

	//AI for aproaching, attacking and defending 
	void approach(){
		if (a == 3) {
			anim.SetTrigger ("monster_walkF");
			walkSpeed = Random.Range (50, 120);
		}
	}

	int attack(){
		int val = Random.Range (0, 5);
		if (val == 0) {
			return ATTACK1;
		} else if (val == 1) {
			return ATTACK2;
		} else if (val == 2) {
			return ATTACK3;
		} else if (val == 3) {
			return WALKB;
		} else if (val == 4) {
			walkSpeed = 40;
			return WALKF;
		}return 0;
	}

	int defend(){
		int val = Random.Range (0, 2);
		if (val == 0) {
			return WALKB;
		} else if (val == 1) {
			return WALKF;
		} else {
			return ATTACK2;
		}
	}
		

}
