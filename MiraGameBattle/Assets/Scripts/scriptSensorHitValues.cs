using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptSensorHitValues : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	public float GetHitPower{
		get{ return _hitPower;}
		set{ _hitPower = value;}
	}
	public float GetSpeedPower{
		get { return _speed;}
		set{ _speed = value;}

	}
	[SerializeField]float _hitPower;
	[SerializeField]float _speed;

}
