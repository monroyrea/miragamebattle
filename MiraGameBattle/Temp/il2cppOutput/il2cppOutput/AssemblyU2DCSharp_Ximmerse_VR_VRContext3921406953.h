﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Ximmerse_VR_TrackingOrigin2567001781.h"

// Ximmerse.VR.VRContext
struct VRContext_t3921406953;
// Ximmerse.InputSystem.XDevicePlugin/ControllerStateDelegate
struct ControllerStateDelegate_t1957920311;
// Ximmerse.InputSystem.XDevicePlugin/SendMessageDelegate
struct SendMessageDelegate_t3864830023;
// Ximmerse.VR.VRDevice
struct VRDevice_t903186564;
// Ximmerse.VR.VRContext/NodeTransformPair[]
struct NodeTransformPairU5BU5D_t3177909563;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.Transform
struct Transform_t3275118058;
// Ximmerse.UI.UIFade
struct UIFade_t1631582502;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.VR.VRContext
struct  VRContext_t3921406953  : public MonoBehaviour_t1158329972
{
public:
	// Ximmerse.VR.TrackingOrigin Ximmerse.VR.VRContext::trackingOriginType
	int32_t ___trackingOriginType_6;
	// Ximmerse.VR.VRDevice Ximmerse.VR.VRContext::vrDevice
	VRDevice_t903186564 * ___vrDevice_7;
	// System.Single Ximmerse.VR.VRContext::m_HomeIsDownLastTime
	float ___m_HomeIsDownLastTime_8;
	// Ximmerse.VR.VRContext/NodeTransformPair[] Ximmerse.VR.VRContext::m_Anchors
	NodeTransformPairU5BU5D_t3177909563* ___m_Anchors_9;
	// System.Boolean Ximmerse.VR.VRContext::canRecenter
	bool ___canRecenter_10;
	// UnityEngine.Events.UnityAction Ximmerse.VR.VRContext::onRecenter
	UnityAction_t4025899511 * ___onRecenter_11;
	// System.Boolean Ximmerse.VR.VRContext::m_IsInited
	bool ___m_IsInited_12;
	// UnityEngine.Transform Ximmerse.VR.VRContext::m_UiRootVR
	Transform_t3275118058 * ___m_UiRootVR_13;
	// Ximmerse.UI.UIFade Ximmerse.VR.VRContext::m_UIFade
	UIFade_t1631582502 * ___m_UIFade_14;

public:
	inline static int32_t get_offset_of_trackingOriginType_6() { return static_cast<int32_t>(offsetof(VRContext_t3921406953, ___trackingOriginType_6)); }
	inline int32_t get_trackingOriginType_6() const { return ___trackingOriginType_6; }
	inline int32_t* get_address_of_trackingOriginType_6() { return &___trackingOriginType_6; }
	inline void set_trackingOriginType_6(int32_t value)
	{
		___trackingOriginType_6 = value;
	}

	inline static int32_t get_offset_of_vrDevice_7() { return static_cast<int32_t>(offsetof(VRContext_t3921406953, ___vrDevice_7)); }
	inline VRDevice_t903186564 * get_vrDevice_7() const { return ___vrDevice_7; }
	inline VRDevice_t903186564 ** get_address_of_vrDevice_7() { return &___vrDevice_7; }
	inline void set_vrDevice_7(VRDevice_t903186564 * value)
	{
		___vrDevice_7 = value;
		Il2CppCodeGenWriteBarrier(&___vrDevice_7, value);
	}

	inline static int32_t get_offset_of_m_HomeIsDownLastTime_8() { return static_cast<int32_t>(offsetof(VRContext_t3921406953, ___m_HomeIsDownLastTime_8)); }
	inline float get_m_HomeIsDownLastTime_8() const { return ___m_HomeIsDownLastTime_8; }
	inline float* get_address_of_m_HomeIsDownLastTime_8() { return &___m_HomeIsDownLastTime_8; }
	inline void set_m_HomeIsDownLastTime_8(float value)
	{
		___m_HomeIsDownLastTime_8 = value;
	}

	inline static int32_t get_offset_of_m_Anchors_9() { return static_cast<int32_t>(offsetof(VRContext_t3921406953, ___m_Anchors_9)); }
	inline NodeTransformPairU5BU5D_t3177909563* get_m_Anchors_9() const { return ___m_Anchors_9; }
	inline NodeTransformPairU5BU5D_t3177909563** get_address_of_m_Anchors_9() { return &___m_Anchors_9; }
	inline void set_m_Anchors_9(NodeTransformPairU5BU5D_t3177909563* value)
	{
		___m_Anchors_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_Anchors_9, value);
	}

	inline static int32_t get_offset_of_canRecenter_10() { return static_cast<int32_t>(offsetof(VRContext_t3921406953, ___canRecenter_10)); }
	inline bool get_canRecenter_10() const { return ___canRecenter_10; }
	inline bool* get_address_of_canRecenter_10() { return &___canRecenter_10; }
	inline void set_canRecenter_10(bool value)
	{
		___canRecenter_10 = value;
	}

	inline static int32_t get_offset_of_onRecenter_11() { return static_cast<int32_t>(offsetof(VRContext_t3921406953, ___onRecenter_11)); }
	inline UnityAction_t4025899511 * get_onRecenter_11() const { return ___onRecenter_11; }
	inline UnityAction_t4025899511 ** get_address_of_onRecenter_11() { return &___onRecenter_11; }
	inline void set_onRecenter_11(UnityAction_t4025899511 * value)
	{
		___onRecenter_11 = value;
		Il2CppCodeGenWriteBarrier(&___onRecenter_11, value);
	}

	inline static int32_t get_offset_of_m_IsInited_12() { return static_cast<int32_t>(offsetof(VRContext_t3921406953, ___m_IsInited_12)); }
	inline bool get_m_IsInited_12() const { return ___m_IsInited_12; }
	inline bool* get_address_of_m_IsInited_12() { return &___m_IsInited_12; }
	inline void set_m_IsInited_12(bool value)
	{
		___m_IsInited_12 = value;
	}

	inline static int32_t get_offset_of_m_UiRootVR_13() { return static_cast<int32_t>(offsetof(VRContext_t3921406953, ___m_UiRootVR_13)); }
	inline Transform_t3275118058 * get_m_UiRootVR_13() const { return ___m_UiRootVR_13; }
	inline Transform_t3275118058 ** get_address_of_m_UiRootVR_13() { return &___m_UiRootVR_13; }
	inline void set_m_UiRootVR_13(Transform_t3275118058 * value)
	{
		___m_UiRootVR_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_UiRootVR_13, value);
	}

	inline static int32_t get_offset_of_m_UIFade_14() { return static_cast<int32_t>(offsetof(VRContext_t3921406953, ___m_UIFade_14)); }
	inline UIFade_t1631582502 * get_m_UIFade_14() const { return ___m_UIFade_14; }
	inline UIFade_t1631582502 ** get_address_of_m_UIFade_14() { return &___m_UIFade_14; }
	inline void set_m_UIFade_14(UIFade_t1631582502 * value)
	{
		___m_UIFade_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_UIFade_14, value);
	}
};

struct VRContext_t3921406953_StaticFields
{
public:
	// Ximmerse.VR.VRContext Ximmerse.VR.VRContext::s_Main
	VRContext_t3921406953 * ___s_Main_2;
	// System.Boolean Ximmerse.VR.VRContext::s_MainCached
	bool ___s_MainCached_3;
	// Ximmerse.InputSystem.XDevicePlugin/ControllerStateDelegate Ximmerse.VR.VRContext::s_OnHmdUpdate
	ControllerStateDelegate_t1957920311 * ___s_OnHmdUpdate_4;
	// Ximmerse.InputSystem.XDevicePlugin/SendMessageDelegate Ximmerse.VR.VRContext::s_OnHmdMessage
	SendMessageDelegate_t3864830023 * ___s_OnHmdMessage_5;
	// Ximmerse.InputSystem.XDevicePlugin/ControllerStateDelegate Ximmerse.VR.VRContext::<>f__mg$cache0
	ControllerStateDelegate_t1957920311 * ___U3CU3Ef__mgU24cache0_15;
	// Ximmerse.InputSystem.XDevicePlugin/SendMessageDelegate Ximmerse.VR.VRContext::<>f__mg$cache1
	SendMessageDelegate_t3864830023 * ___U3CU3Ef__mgU24cache1_16;

public:
	inline static int32_t get_offset_of_s_Main_2() { return static_cast<int32_t>(offsetof(VRContext_t3921406953_StaticFields, ___s_Main_2)); }
	inline VRContext_t3921406953 * get_s_Main_2() const { return ___s_Main_2; }
	inline VRContext_t3921406953 ** get_address_of_s_Main_2() { return &___s_Main_2; }
	inline void set_s_Main_2(VRContext_t3921406953 * value)
	{
		___s_Main_2 = value;
		Il2CppCodeGenWriteBarrier(&___s_Main_2, value);
	}

	inline static int32_t get_offset_of_s_MainCached_3() { return static_cast<int32_t>(offsetof(VRContext_t3921406953_StaticFields, ___s_MainCached_3)); }
	inline bool get_s_MainCached_3() const { return ___s_MainCached_3; }
	inline bool* get_address_of_s_MainCached_3() { return &___s_MainCached_3; }
	inline void set_s_MainCached_3(bool value)
	{
		___s_MainCached_3 = value;
	}

	inline static int32_t get_offset_of_s_OnHmdUpdate_4() { return static_cast<int32_t>(offsetof(VRContext_t3921406953_StaticFields, ___s_OnHmdUpdate_4)); }
	inline ControllerStateDelegate_t1957920311 * get_s_OnHmdUpdate_4() const { return ___s_OnHmdUpdate_4; }
	inline ControllerStateDelegate_t1957920311 ** get_address_of_s_OnHmdUpdate_4() { return &___s_OnHmdUpdate_4; }
	inline void set_s_OnHmdUpdate_4(ControllerStateDelegate_t1957920311 * value)
	{
		___s_OnHmdUpdate_4 = value;
		Il2CppCodeGenWriteBarrier(&___s_OnHmdUpdate_4, value);
	}

	inline static int32_t get_offset_of_s_OnHmdMessage_5() { return static_cast<int32_t>(offsetof(VRContext_t3921406953_StaticFields, ___s_OnHmdMessage_5)); }
	inline SendMessageDelegate_t3864830023 * get_s_OnHmdMessage_5() const { return ___s_OnHmdMessage_5; }
	inline SendMessageDelegate_t3864830023 ** get_address_of_s_OnHmdMessage_5() { return &___s_OnHmdMessage_5; }
	inline void set_s_OnHmdMessage_5(SendMessageDelegate_t3864830023 * value)
	{
		___s_OnHmdMessage_5 = value;
		Il2CppCodeGenWriteBarrier(&___s_OnHmdMessage_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_15() { return static_cast<int32_t>(offsetof(VRContext_t3921406953_StaticFields, ___U3CU3Ef__mgU24cache0_15)); }
	inline ControllerStateDelegate_t1957920311 * get_U3CU3Ef__mgU24cache0_15() const { return ___U3CU3Ef__mgU24cache0_15; }
	inline ControllerStateDelegate_t1957920311 ** get_address_of_U3CU3Ef__mgU24cache0_15() { return &___U3CU3Ef__mgU24cache0_15; }
	inline void set_U3CU3Ef__mgU24cache0_15(ControllerStateDelegate_t1957920311 * value)
	{
		___U3CU3Ef__mgU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_16() { return static_cast<int32_t>(offsetof(VRContext_t3921406953_StaticFields, ___U3CU3Ef__mgU24cache1_16)); }
	inline SendMessageDelegate_t3864830023 * get_U3CU3Ef__mgU24cache1_16() const { return ___U3CU3Ef__mgU24cache1_16; }
	inline SendMessageDelegate_t3864830023 ** get_address_of_U3CU3Ef__mgU24cache1_16() { return &___U3CU3Ef__mgU24cache1_16; }
	inline void set_U3CU3Ef__mgU24cache1_16(SendMessageDelegate_t3864830023 * value)
	{
		___U3CU3Ef__mgU24cache1_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache1_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
