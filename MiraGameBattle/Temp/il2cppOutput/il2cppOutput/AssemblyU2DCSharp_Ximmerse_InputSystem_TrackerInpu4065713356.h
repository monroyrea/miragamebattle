﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XDevicePlug2122967042.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"

// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Action
struct Action_t3226471752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.InputSystem.TrackerInput
struct  TrackerInput_t4065713356  : public MonoBehaviour_t1158329972
{
public:
	// System.String Ximmerse.InputSystem.TrackerInput::deviceName
	String_t* ___deviceName_2;
	// UnityEngine.Vector3 Ximmerse.InputSystem.TrackerInput::sensitivity
	Vector3_t2243707580  ___sensitivity_3;
	// UnityEngine.Transform Ximmerse.InputSystem.TrackerInput::trackingSpace
	Transform_t3275118058 * ___trackingSpace_4;
	// UnityEngine.Transform Ximmerse.InputSystem.TrackerInput::anchor
	Transform_t3275118058 * ___anchor_5;
	// System.Action Ximmerse.InputSystem.TrackerInput::onRecenter
	Action_t3226471752 * ___onRecenter_6;
	// System.Int32 Ximmerse.InputSystem.TrackerInput::m_Handle
	int32_t ___m_Handle_7;
	// Ximmerse.InputSystem.XDevicePlugin/TrackerState Ximmerse.InputSystem.TrackerInput::m_State
	TrackerState_t2122967042  ___m_State_8;
	// System.Int32 Ximmerse.InputSystem.TrackerInput::m_PrevFrameCount
	int32_t ___m_PrevFrameCount_9;
	// System.Int32 Ximmerse.InputSystem.TrackerInput::m_PrevState_frameCount
	int32_t ___m_PrevState_frameCount_10;
	// System.Int32 Ximmerse.InputSystem.TrackerInput::m_KeepFrames
	int32_t ___m_KeepFrames_11;
	// System.Boolean Ximmerse.InputSystem.TrackerInput::m_UseAnchorProjection
	bool ___m_UseAnchorProjection_12;
	// UnityEngine.Matrix4x4 Ximmerse.InputSystem.TrackerInput::m_AnchorMatrix
	Matrix4x4_t2933234003  ___m_AnchorMatrix_13;

public:
	inline static int32_t get_offset_of_deviceName_2() { return static_cast<int32_t>(offsetof(TrackerInput_t4065713356, ___deviceName_2)); }
	inline String_t* get_deviceName_2() const { return ___deviceName_2; }
	inline String_t** get_address_of_deviceName_2() { return &___deviceName_2; }
	inline void set_deviceName_2(String_t* value)
	{
		___deviceName_2 = value;
		Il2CppCodeGenWriteBarrier(&___deviceName_2, value);
	}

	inline static int32_t get_offset_of_sensitivity_3() { return static_cast<int32_t>(offsetof(TrackerInput_t4065713356, ___sensitivity_3)); }
	inline Vector3_t2243707580  get_sensitivity_3() const { return ___sensitivity_3; }
	inline Vector3_t2243707580 * get_address_of_sensitivity_3() { return &___sensitivity_3; }
	inline void set_sensitivity_3(Vector3_t2243707580  value)
	{
		___sensitivity_3 = value;
	}

	inline static int32_t get_offset_of_trackingSpace_4() { return static_cast<int32_t>(offsetof(TrackerInput_t4065713356, ___trackingSpace_4)); }
	inline Transform_t3275118058 * get_trackingSpace_4() const { return ___trackingSpace_4; }
	inline Transform_t3275118058 ** get_address_of_trackingSpace_4() { return &___trackingSpace_4; }
	inline void set_trackingSpace_4(Transform_t3275118058 * value)
	{
		___trackingSpace_4 = value;
		Il2CppCodeGenWriteBarrier(&___trackingSpace_4, value);
	}

	inline static int32_t get_offset_of_anchor_5() { return static_cast<int32_t>(offsetof(TrackerInput_t4065713356, ___anchor_5)); }
	inline Transform_t3275118058 * get_anchor_5() const { return ___anchor_5; }
	inline Transform_t3275118058 ** get_address_of_anchor_5() { return &___anchor_5; }
	inline void set_anchor_5(Transform_t3275118058 * value)
	{
		___anchor_5 = value;
		Il2CppCodeGenWriteBarrier(&___anchor_5, value);
	}

	inline static int32_t get_offset_of_onRecenter_6() { return static_cast<int32_t>(offsetof(TrackerInput_t4065713356, ___onRecenter_6)); }
	inline Action_t3226471752 * get_onRecenter_6() const { return ___onRecenter_6; }
	inline Action_t3226471752 ** get_address_of_onRecenter_6() { return &___onRecenter_6; }
	inline void set_onRecenter_6(Action_t3226471752 * value)
	{
		___onRecenter_6 = value;
		Il2CppCodeGenWriteBarrier(&___onRecenter_6, value);
	}

	inline static int32_t get_offset_of_m_Handle_7() { return static_cast<int32_t>(offsetof(TrackerInput_t4065713356, ___m_Handle_7)); }
	inline int32_t get_m_Handle_7() const { return ___m_Handle_7; }
	inline int32_t* get_address_of_m_Handle_7() { return &___m_Handle_7; }
	inline void set_m_Handle_7(int32_t value)
	{
		___m_Handle_7 = value;
	}

	inline static int32_t get_offset_of_m_State_8() { return static_cast<int32_t>(offsetof(TrackerInput_t4065713356, ___m_State_8)); }
	inline TrackerState_t2122967042  get_m_State_8() const { return ___m_State_8; }
	inline TrackerState_t2122967042 * get_address_of_m_State_8() { return &___m_State_8; }
	inline void set_m_State_8(TrackerState_t2122967042  value)
	{
		___m_State_8 = value;
	}

	inline static int32_t get_offset_of_m_PrevFrameCount_9() { return static_cast<int32_t>(offsetof(TrackerInput_t4065713356, ___m_PrevFrameCount_9)); }
	inline int32_t get_m_PrevFrameCount_9() const { return ___m_PrevFrameCount_9; }
	inline int32_t* get_address_of_m_PrevFrameCount_9() { return &___m_PrevFrameCount_9; }
	inline void set_m_PrevFrameCount_9(int32_t value)
	{
		___m_PrevFrameCount_9 = value;
	}

	inline static int32_t get_offset_of_m_PrevState_frameCount_10() { return static_cast<int32_t>(offsetof(TrackerInput_t4065713356, ___m_PrevState_frameCount_10)); }
	inline int32_t get_m_PrevState_frameCount_10() const { return ___m_PrevState_frameCount_10; }
	inline int32_t* get_address_of_m_PrevState_frameCount_10() { return &___m_PrevState_frameCount_10; }
	inline void set_m_PrevState_frameCount_10(int32_t value)
	{
		___m_PrevState_frameCount_10 = value;
	}

	inline static int32_t get_offset_of_m_KeepFrames_11() { return static_cast<int32_t>(offsetof(TrackerInput_t4065713356, ___m_KeepFrames_11)); }
	inline int32_t get_m_KeepFrames_11() const { return ___m_KeepFrames_11; }
	inline int32_t* get_address_of_m_KeepFrames_11() { return &___m_KeepFrames_11; }
	inline void set_m_KeepFrames_11(int32_t value)
	{
		___m_KeepFrames_11 = value;
	}

	inline static int32_t get_offset_of_m_UseAnchorProjection_12() { return static_cast<int32_t>(offsetof(TrackerInput_t4065713356, ___m_UseAnchorProjection_12)); }
	inline bool get_m_UseAnchorProjection_12() const { return ___m_UseAnchorProjection_12; }
	inline bool* get_address_of_m_UseAnchorProjection_12() { return &___m_UseAnchorProjection_12; }
	inline void set_m_UseAnchorProjection_12(bool value)
	{
		___m_UseAnchorProjection_12 = value;
	}

	inline static int32_t get_offset_of_m_AnchorMatrix_13() { return static_cast<int32_t>(offsetof(TrackerInput_t4065713356, ___m_AnchorMatrix_13)); }
	inline Matrix4x4_t2933234003  get_m_AnchorMatrix_13() const { return ___m_AnchorMatrix_13; }
	inline Matrix4x4_t2933234003 * get_address_of_m_AnchorMatrix_13() { return &___m_AnchorMatrix_13; }
	inline void set_m_AnchorMatrix_13(Matrix4x4_t2933234003  value)
	{
		___m_AnchorMatrix_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
