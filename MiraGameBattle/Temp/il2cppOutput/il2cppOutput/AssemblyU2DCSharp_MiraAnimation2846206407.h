﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_ControllerTarget3193765035.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraAnimation
struct  MiraAnimation_t2846206407  : public ControllerTarget_t3193765035
{
public:
	// UnityEngine.GameObject MiraAnimation::m
	GameObject_t1756533147 * ___m_2;
	// UnityEngine.GameObject MiraAnimation::i
	GameObject_t1756533147 * ___i_3;
	// UnityEngine.GameObject MiraAnimation::r
	GameObject_t1756533147 * ___r_4;
	// UnityEngine.GameObject MiraAnimation::a
	GameObject_t1756533147 * ___a_5;
	// UnityEngine.Vector3 MiraAnimation::mPos
	Vector3_t2243707580  ___mPos_6;
	// UnityEngine.Vector3 MiraAnimation::iPos
	Vector3_t2243707580  ___iPos_7;
	// UnityEngine.Vector3 MiraAnimation::rPos
	Vector3_t2243707580  ___rPos_8;
	// UnityEngine.Vector3 MiraAnimation::aPos
	Vector3_t2243707580  ___aPos_9;
	// System.Single MiraAnimation::amplitude
	float ___amplitude_10;
	// System.Single MiraAnimation::lerpAmp
	float ___lerpAmp_11;
	// System.Single MiraAnimation::letterOffset
	float ___letterOffset_12;
	// System.Single MiraAnimation::timer
	float ___timer_13;
	// System.Single MiraAnimation::timeMult
	float ___timeMult_14;
	// System.Single MiraAnimation::lerpAmpSet
	float ___lerpAmpSet_15;
	// System.Boolean MiraAnimation::activated
	bool ___activated_16;
	// UnityEngine.Coroutine MiraAnimation::currentCoroutine
	Coroutine_t2299508840 * ___currentCoroutine_17;

public:
	inline static int32_t get_offset_of_m_2() { return static_cast<int32_t>(offsetof(MiraAnimation_t2846206407, ___m_2)); }
	inline GameObject_t1756533147 * get_m_2() const { return ___m_2; }
	inline GameObject_t1756533147 ** get_address_of_m_2() { return &___m_2; }
	inline void set_m_2(GameObject_t1756533147 * value)
	{
		___m_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_2, value);
	}

	inline static int32_t get_offset_of_i_3() { return static_cast<int32_t>(offsetof(MiraAnimation_t2846206407, ___i_3)); }
	inline GameObject_t1756533147 * get_i_3() const { return ___i_3; }
	inline GameObject_t1756533147 ** get_address_of_i_3() { return &___i_3; }
	inline void set_i_3(GameObject_t1756533147 * value)
	{
		___i_3 = value;
		Il2CppCodeGenWriteBarrier(&___i_3, value);
	}

	inline static int32_t get_offset_of_r_4() { return static_cast<int32_t>(offsetof(MiraAnimation_t2846206407, ___r_4)); }
	inline GameObject_t1756533147 * get_r_4() const { return ___r_4; }
	inline GameObject_t1756533147 ** get_address_of_r_4() { return &___r_4; }
	inline void set_r_4(GameObject_t1756533147 * value)
	{
		___r_4 = value;
		Il2CppCodeGenWriteBarrier(&___r_4, value);
	}

	inline static int32_t get_offset_of_a_5() { return static_cast<int32_t>(offsetof(MiraAnimation_t2846206407, ___a_5)); }
	inline GameObject_t1756533147 * get_a_5() const { return ___a_5; }
	inline GameObject_t1756533147 ** get_address_of_a_5() { return &___a_5; }
	inline void set_a_5(GameObject_t1756533147 * value)
	{
		___a_5 = value;
		Il2CppCodeGenWriteBarrier(&___a_5, value);
	}

	inline static int32_t get_offset_of_mPos_6() { return static_cast<int32_t>(offsetof(MiraAnimation_t2846206407, ___mPos_6)); }
	inline Vector3_t2243707580  get_mPos_6() const { return ___mPos_6; }
	inline Vector3_t2243707580 * get_address_of_mPos_6() { return &___mPos_6; }
	inline void set_mPos_6(Vector3_t2243707580  value)
	{
		___mPos_6 = value;
	}

	inline static int32_t get_offset_of_iPos_7() { return static_cast<int32_t>(offsetof(MiraAnimation_t2846206407, ___iPos_7)); }
	inline Vector3_t2243707580  get_iPos_7() const { return ___iPos_7; }
	inline Vector3_t2243707580 * get_address_of_iPos_7() { return &___iPos_7; }
	inline void set_iPos_7(Vector3_t2243707580  value)
	{
		___iPos_7 = value;
	}

	inline static int32_t get_offset_of_rPos_8() { return static_cast<int32_t>(offsetof(MiraAnimation_t2846206407, ___rPos_8)); }
	inline Vector3_t2243707580  get_rPos_8() const { return ___rPos_8; }
	inline Vector3_t2243707580 * get_address_of_rPos_8() { return &___rPos_8; }
	inline void set_rPos_8(Vector3_t2243707580  value)
	{
		___rPos_8 = value;
	}

	inline static int32_t get_offset_of_aPos_9() { return static_cast<int32_t>(offsetof(MiraAnimation_t2846206407, ___aPos_9)); }
	inline Vector3_t2243707580  get_aPos_9() const { return ___aPos_9; }
	inline Vector3_t2243707580 * get_address_of_aPos_9() { return &___aPos_9; }
	inline void set_aPos_9(Vector3_t2243707580  value)
	{
		___aPos_9 = value;
	}

	inline static int32_t get_offset_of_amplitude_10() { return static_cast<int32_t>(offsetof(MiraAnimation_t2846206407, ___amplitude_10)); }
	inline float get_amplitude_10() const { return ___amplitude_10; }
	inline float* get_address_of_amplitude_10() { return &___amplitude_10; }
	inline void set_amplitude_10(float value)
	{
		___amplitude_10 = value;
	}

	inline static int32_t get_offset_of_lerpAmp_11() { return static_cast<int32_t>(offsetof(MiraAnimation_t2846206407, ___lerpAmp_11)); }
	inline float get_lerpAmp_11() const { return ___lerpAmp_11; }
	inline float* get_address_of_lerpAmp_11() { return &___lerpAmp_11; }
	inline void set_lerpAmp_11(float value)
	{
		___lerpAmp_11 = value;
	}

	inline static int32_t get_offset_of_letterOffset_12() { return static_cast<int32_t>(offsetof(MiraAnimation_t2846206407, ___letterOffset_12)); }
	inline float get_letterOffset_12() const { return ___letterOffset_12; }
	inline float* get_address_of_letterOffset_12() { return &___letterOffset_12; }
	inline void set_letterOffset_12(float value)
	{
		___letterOffset_12 = value;
	}

	inline static int32_t get_offset_of_timer_13() { return static_cast<int32_t>(offsetof(MiraAnimation_t2846206407, ___timer_13)); }
	inline float get_timer_13() const { return ___timer_13; }
	inline float* get_address_of_timer_13() { return &___timer_13; }
	inline void set_timer_13(float value)
	{
		___timer_13 = value;
	}

	inline static int32_t get_offset_of_timeMult_14() { return static_cast<int32_t>(offsetof(MiraAnimation_t2846206407, ___timeMult_14)); }
	inline float get_timeMult_14() const { return ___timeMult_14; }
	inline float* get_address_of_timeMult_14() { return &___timeMult_14; }
	inline void set_timeMult_14(float value)
	{
		___timeMult_14 = value;
	}

	inline static int32_t get_offset_of_lerpAmpSet_15() { return static_cast<int32_t>(offsetof(MiraAnimation_t2846206407, ___lerpAmpSet_15)); }
	inline float get_lerpAmpSet_15() const { return ___lerpAmpSet_15; }
	inline float* get_address_of_lerpAmpSet_15() { return &___lerpAmpSet_15; }
	inline void set_lerpAmpSet_15(float value)
	{
		___lerpAmpSet_15 = value;
	}

	inline static int32_t get_offset_of_activated_16() { return static_cast<int32_t>(offsetof(MiraAnimation_t2846206407, ___activated_16)); }
	inline bool get_activated_16() const { return ___activated_16; }
	inline bool* get_address_of_activated_16() { return &___activated_16; }
	inline void set_activated_16(bool value)
	{
		___activated_16 = value;
	}

	inline static int32_t get_offset_of_currentCoroutine_17() { return static_cast<int32_t>(offsetof(MiraAnimation_t2846206407, ___currentCoroutine_17)); }
	inline Coroutine_t2299508840 * get_currentCoroutine_17() const { return ___currentCoroutine_17; }
	inline Coroutine_t2299508840 ** get_address_of_currentCoroutine_17() { return &___currentCoroutine_17; }
	inline void set_currentCoroutine_17(Coroutine_t2299508840 * value)
	{
		___currentCoroutine_17 = value;
		Il2CppCodeGenWriteBarrier(&___currentCoroutine_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
