﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// Ximmerse.InputSystem.ControllerButton[]
struct ControllerButtonU5BU5D_t160497009;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControllerVisual/ButtonListener
struct  ButtonListener_t2279732829  : public Il2CppObject
{
public:
	// System.String ControllerVisual/ButtonListener::name
	String_t* ___name_0;
	// Ximmerse.InputSystem.ControllerButton[] ControllerVisual/ButtonListener::unacceptables
	ControllerButtonU5BU5D_t160497009* ___unacceptables_1;
	// Ximmerse.InputSystem.ControllerButton[] ControllerVisual/ButtonListener::acceptables
	ControllerButtonU5BU5D_t160497009* ___acceptables_2;
	// UnityEngine.GameObject ControllerVisual/ButtonListener::target
	GameObject_t1756533147 * ___target_3;
	// UnityEngine.Events.UnityEvent ControllerVisual/ButtonListener::onPressed
	UnityEvent_t408735097 * ___onPressed_4;
	// UnityEngine.Events.UnityEvent ControllerVisual/ButtonListener::onReleased
	UnityEvent_t408735097 * ___onReleased_5;
	// System.Boolean ControllerVisual/ButtonListener::value
	bool ___value_6;
	// System.UInt32[] ControllerVisual/ButtonListener::buttonMasks
	UInt32U5BU5D_t59386216* ___buttonMasks_7;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ButtonListener_t2279732829, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_unacceptables_1() { return static_cast<int32_t>(offsetof(ButtonListener_t2279732829, ___unacceptables_1)); }
	inline ControllerButtonU5BU5D_t160497009* get_unacceptables_1() const { return ___unacceptables_1; }
	inline ControllerButtonU5BU5D_t160497009** get_address_of_unacceptables_1() { return &___unacceptables_1; }
	inline void set_unacceptables_1(ControllerButtonU5BU5D_t160497009* value)
	{
		___unacceptables_1 = value;
		Il2CppCodeGenWriteBarrier(&___unacceptables_1, value);
	}

	inline static int32_t get_offset_of_acceptables_2() { return static_cast<int32_t>(offsetof(ButtonListener_t2279732829, ___acceptables_2)); }
	inline ControllerButtonU5BU5D_t160497009* get_acceptables_2() const { return ___acceptables_2; }
	inline ControllerButtonU5BU5D_t160497009** get_address_of_acceptables_2() { return &___acceptables_2; }
	inline void set_acceptables_2(ControllerButtonU5BU5D_t160497009* value)
	{
		___acceptables_2 = value;
		Il2CppCodeGenWriteBarrier(&___acceptables_2, value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(ButtonListener_t2279732829, ___target_3)); }
	inline GameObject_t1756533147 * get_target_3() const { return ___target_3; }
	inline GameObject_t1756533147 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(GameObject_t1756533147 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier(&___target_3, value);
	}

	inline static int32_t get_offset_of_onPressed_4() { return static_cast<int32_t>(offsetof(ButtonListener_t2279732829, ___onPressed_4)); }
	inline UnityEvent_t408735097 * get_onPressed_4() const { return ___onPressed_4; }
	inline UnityEvent_t408735097 ** get_address_of_onPressed_4() { return &___onPressed_4; }
	inline void set_onPressed_4(UnityEvent_t408735097 * value)
	{
		___onPressed_4 = value;
		Il2CppCodeGenWriteBarrier(&___onPressed_4, value);
	}

	inline static int32_t get_offset_of_onReleased_5() { return static_cast<int32_t>(offsetof(ButtonListener_t2279732829, ___onReleased_5)); }
	inline UnityEvent_t408735097 * get_onReleased_5() const { return ___onReleased_5; }
	inline UnityEvent_t408735097 ** get_address_of_onReleased_5() { return &___onReleased_5; }
	inline void set_onReleased_5(UnityEvent_t408735097 * value)
	{
		___onReleased_5 = value;
		Il2CppCodeGenWriteBarrier(&___onReleased_5, value);
	}

	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(ButtonListener_t2279732829, ___value_6)); }
	inline bool get_value_6() const { return ___value_6; }
	inline bool* get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(bool value)
	{
		___value_6 = value;
	}

	inline static int32_t get_offset_of_buttonMasks_7() { return static_cast<int32_t>(offsetof(ButtonListener_t2279732829, ___buttonMasks_7)); }
	inline UInt32U5BU5D_t59386216* get_buttonMasks_7() const { return ___buttonMasks_7; }
	inline UInt32U5BU5D_t59386216** get_address_of_buttonMasks_7() { return &___buttonMasks_7; }
	inline void set_buttonMasks_7(UInt32U5BU5D_t59386216* value)
	{
		___buttonMasks_7 = value;
		Il2CppCodeGenWriteBarrier(&___buttonMasks_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
