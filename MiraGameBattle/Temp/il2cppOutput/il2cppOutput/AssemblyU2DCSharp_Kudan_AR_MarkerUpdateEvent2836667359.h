﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2053017820.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kudan.AR.MarkerUpdateEvent
struct  MarkerUpdateEvent_t2836667359  : public UnityEvent_1_t2053017820
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
