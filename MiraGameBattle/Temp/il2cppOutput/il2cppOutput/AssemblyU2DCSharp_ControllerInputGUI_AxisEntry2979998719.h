﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_ControllerInputGUI_AxisType1230240043.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// ControllerInputGUI/ControllerEntry
struct ControllerEntry_t1998352496;
// Ximmerse.InputSystem.ControllerAxis[]
struct ControllerAxisU5BU5D_t3016952124;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControllerInputGUI/AxisEntry
struct  AxisEntry_t2979998719  : public Il2CppObject
{
public:
	// ControllerInputGUI/ControllerEntry ControllerInputGUI/AxisEntry::context
	ControllerEntry_t1998352496 * ___context_0;
	// ControllerInputGUI/AxisType ControllerInputGUI/AxisEntry::type
	uint8_t ___type_1;
	// Ximmerse.InputSystem.ControllerAxis[] ControllerInputGUI/AxisEntry::keys
	ControllerAxisU5BU5D_t3016952124* ___keys_2;
	// UnityEngine.GameObject ControllerInputGUI/AxisEntry::m_GameObject
	GameObject_t1756533147 * ___m_GameObject_3;
	// UnityEngine.UI.Image ControllerInputGUI/AxisEntry::m_Image
	Image_t2042527209 * ___m_Image_4;
	// UnityEngine.RectTransform ControllerInputGUI/AxisEntry::m_Thumb
	RectTransform_t3349966182 * ___m_Thumb_5;
	// UnityEngine.Vector3 ControllerInputGUI/AxisEntry::m_Center
	Vector3_t2243707580  ___m_Center_6;
	// System.Single ControllerInputGUI/AxisEntry::m_Radius
	float ___m_Radius_7;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(AxisEntry_t2979998719, ___context_0)); }
	inline ControllerEntry_t1998352496 * get_context_0() const { return ___context_0; }
	inline ControllerEntry_t1998352496 ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(ControllerEntry_t1998352496 * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier(&___context_0, value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(AxisEntry_t2979998719, ___type_1)); }
	inline uint8_t get_type_1() const { return ___type_1; }
	inline uint8_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(uint8_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_keys_2() { return static_cast<int32_t>(offsetof(AxisEntry_t2979998719, ___keys_2)); }
	inline ControllerAxisU5BU5D_t3016952124* get_keys_2() const { return ___keys_2; }
	inline ControllerAxisU5BU5D_t3016952124** get_address_of_keys_2() { return &___keys_2; }
	inline void set_keys_2(ControllerAxisU5BU5D_t3016952124* value)
	{
		___keys_2 = value;
		Il2CppCodeGenWriteBarrier(&___keys_2, value);
	}

	inline static int32_t get_offset_of_m_GameObject_3() { return static_cast<int32_t>(offsetof(AxisEntry_t2979998719, ___m_GameObject_3)); }
	inline GameObject_t1756533147 * get_m_GameObject_3() const { return ___m_GameObject_3; }
	inline GameObject_t1756533147 ** get_address_of_m_GameObject_3() { return &___m_GameObject_3; }
	inline void set_m_GameObject_3(GameObject_t1756533147 * value)
	{
		___m_GameObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_GameObject_3, value);
	}

	inline static int32_t get_offset_of_m_Image_4() { return static_cast<int32_t>(offsetof(AxisEntry_t2979998719, ___m_Image_4)); }
	inline Image_t2042527209 * get_m_Image_4() const { return ___m_Image_4; }
	inline Image_t2042527209 ** get_address_of_m_Image_4() { return &___m_Image_4; }
	inline void set_m_Image_4(Image_t2042527209 * value)
	{
		___m_Image_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_Image_4, value);
	}

	inline static int32_t get_offset_of_m_Thumb_5() { return static_cast<int32_t>(offsetof(AxisEntry_t2979998719, ___m_Thumb_5)); }
	inline RectTransform_t3349966182 * get_m_Thumb_5() const { return ___m_Thumb_5; }
	inline RectTransform_t3349966182 ** get_address_of_m_Thumb_5() { return &___m_Thumb_5; }
	inline void set_m_Thumb_5(RectTransform_t3349966182 * value)
	{
		___m_Thumb_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_Thumb_5, value);
	}

	inline static int32_t get_offset_of_m_Center_6() { return static_cast<int32_t>(offsetof(AxisEntry_t2979998719, ___m_Center_6)); }
	inline Vector3_t2243707580  get_m_Center_6() const { return ___m_Center_6; }
	inline Vector3_t2243707580 * get_address_of_m_Center_6() { return &___m_Center_6; }
	inline void set_m_Center_6(Vector3_t2243707580  value)
	{
		___m_Center_6 = value;
	}

	inline static int32_t get_offset_of_m_Radius_7() { return static_cast<int32_t>(offsetof(AxisEntry_t2979998719, ___m_Radius_7)); }
	inline float get_m_Radius_7() const { return ___m_Radius_7; }
	inline float* get_address_of_m_Radius_7() { return &___m_Radius_7; }
	inline void set_m_Radius_7(float value)
	{
		___m_Radius_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
