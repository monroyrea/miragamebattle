﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// UnityEngine.Object
struct Object_t1021602117;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UKeyValuePair`2<System.String,UnityEngine.Object>
struct  UKeyValuePair_2_t4095796484  : public Il2CppObject
{
public:
	// TKey UKeyValuePair`2::key
	String_t* ___key_0;
	// TValue UKeyValuePair`2::value
	Object_t1021602117 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(UKeyValuePair_2_t4095796484, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(UKeyValuePair_2_t4095796484, ___value_1)); }
	inline Object_t1021602117 * get_value_1() const { return ___value_1; }
	inline Object_t1021602117 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Object_t1021602117 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
