﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_IntPtr2504060609.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.InputSystem.XDevicePlugin/TrackerState
struct  TrackerState_t2122967042 
{
public:
	// System.Int32 Ximmerse.InputSystem.XDevicePlugin/TrackerState::handle
	int32_t ___handle_1;
	// System.Int32 Ximmerse.InputSystem.XDevicePlugin/TrackerState::timestamp
	int32_t ___timestamp_2;
	// System.Int32 Ximmerse.InputSystem.XDevicePlugin/TrackerState::frameCount
	int32_t ___frameCount_3;
	// System.Int32 Ximmerse.InputSystem.XDevicePlugin/TrackerState::capacity
	int32_t ___capacity_4;
	// System.Int32 Ximmerse.InputSystem.XDevicePlugin/TrackerState::count
	int32_t ___count_5;
	// System.IntPtr Ximmerse.InputSystem.XDevicePlugin/TrackerState::id
	IntPtr_t ___id_6;
	// System.IntPtr Ximmerse.InputSystem.XDevicePlugin/TrackerState::data
	IntPtr_t ___data_7;

public:
	inline static int32_t get_offset_of_handle_1() { return static_cast<int32_t>(offsetof(TrackerState_t2122967042, ___handle_1)); }
	inline int32_t get_handle_1() const { return ___handle_1; }
	inline int32_t* get_address_of_handle_1() { return &___handle_1; }
	inline void set_handle_1(int32_t value)
	{
		___handle_1 = value;
	}

	inline static int32_t get_offset_of_timestamp_2() { return static_cast<int32_t>(offsetof(TrackerState_t2122967042, ___timestamp_2)); }
	inline int32_t get_timestamp_2() const { return ___timestamp_2; }
	inline int32_t* get_address_of_timestamp_2() { return &___timestamp_2; }
	inline void set_timestamp_2(int32_t value)
	{
		___timestamp_2 = value;
	}

	inline static int32_t get_offset_of_frameCount_3() { return static_cast<int32_t>(offsetof(TrackerState_t2122967042, ___frameCount_3)); }
	inline int32_t get_frameCount_3() const { return ___frameCount_3; }
	inline int32_t* get_address_of_frameCount_3() { return &___frameCount_3; }
	inline void set_frameCount_3(int32_t value)
	{
		___frameCount_3 = value;
	}

	inline static int32_t get_offset_of_capacity_4() { return static_cast<int32_t>(offsetof(TrackerState_t2122967042, ___capacity_4)); }
	inline int32_t get_capacity_4() const { return ___capacity_4; }
	inline int32_t* get_address_of_capacity_4() { return &___capacity_4; }
	inline void set_capacity_4(int32_t value)
	{
		___capacity_4 = value;
	}

	inline static int32_t get_offset_of_count_5() { return static_cast<int32_t>(offsetof(TrackerState_t2122967042, ___count_5)); }
	inline int32_t get_count_5() const { return ___count_5; }
	inline int32_t* get_address_of_count_5() { return &___count_5; }
	inline void set_count_5(int32_t value)
	{
		___count_5 = value;
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(TrackerState_t2122967042, ___id_6)); }
	inline IntPtr_t get_id_6() const { return ___id_6; }
	inline IntPtr_t* get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(IntPtr_t value)
	{
		___id_6 = value;
	}

	inline static int32_t get_offset_of_data_7() { return static_cast<int32_t>(offsetof(TrackerState_t2122967042, ___data_7)); }
	inline IntPtr_t get_data_7() const { return ___data_7; }
	inline IntPtr_t* get_address_of_data_7() { return &___data_7; }
	inline void set_data_7(IntPtr_t value)
	{
		___data_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
