﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_ControllerHover24802818.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Collider
struct Collider_t3497673348;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SnapToController
struct  SnapToController_t261557653  : public ControllerHover_t24802818
{
public:
	// System.Boolean SnapToController::parentedToController
	bool ___parentedToController_10;
	// UnityEngine.GameObject SnapToController::controller
	GameObject_t1756533147 * ___controller_11;
	// System.Single SnapToController::castMultiplier
	float ___castMultiplier_12;
	// System.Single SnapToController::defaultDistanceZ
	float ___defaultDistanceZ_13;
	// System.Boolean SnapToController::castStart
	bool ___castStart_14;
	// System.Single SnapToController::lastCast
	float ___lastCast_15;
	// UnityEngine.Collider SnapToController::thisCollider
	Collider_t3497673348 * ___thisCollider_17;

public:
	inline static int32_t get_offset_of_parentedToController_10() { return static_cast<int32_t>(offsetof(SnapToController_t261557653, ___parentedToController_10)); }
	inline bool get_parentedToController_10() const { return ___parentedToController_10; }
	inline bool* get_address_of_parentedToController_10() { return &___parentedToController_10; }
	inline void set_parentedToController_10(bool value)
	{
		___parentedToController_10 = value;
	}

	inline static int32_t get_offset_of_controller_11() { return static_cast<int32_t>(offsetof(SnapToController_t261557653, ___controller_11)); }
	inline GameObject_t1756533147 * get_controller_11() const { return ___controller_11; }
	inline GameObject_t1756533147 ** get_address_of_controller_11() { return &___controller_11; }
	inline void set_controller_11(GameObject_t1756533147 * value)
	{
		___controller_11 = value;
		Il2CppCodeGenWriteBarrier(&___controller_11, value);
	}

	inline static int32_t get_offset_of_castMultiplier_12() { return static_cast<int32_t>(offsetof(SnapToController_t261557653, ___castMultiplier_12)); }
	inline float get_castMultiplier_12() const { return ___castMultiplier_12; }
	inline float* get_address_of_castMultiplier_12() { return &___castMultiplier_12; }
	inline void set_castMultiplier_12(float value)
	{
		___castMultiplier_12 = value;
	}

	inline static int32_t get_offset_of_defaultDistanceZ_13() { return static_cast<int32_t>(offsetof(SnapToController_t261557653, ___defaultDistanceZ_13)); }
	inline float get_defaultDistanceZ_13() const { return ___defaultDistanceZ_13; }
	inline float* get_address_of_defaultDistanceZ_13() { return &___defaultDistanceZ_13; }
	inline void set_defaultDistanceZ_13(float value)
	{
		___defaultDistanceZ_13 = value;
	}

	inline static int32_t get_offset_of_castStart_14() { return static_cast<int32_t>(offsetof(SnapToController_t261557653, ___castStart_14)); }
	inline bool get_castStart_14() const { return ___castStart_14; }
	inline bool* get_address_of_castStart_14() { return &___castStart_14; }
	inline void set_castStart_14(bool value)
	{
		___castStart_14 = value;
	}

	inline static int32_t get_offset_of_lastCast_15() { return static_cast<int32_t>(offsetof(SnapToController_t261557653, ___lastCast_15)); }
	inline float get_lastCast_15() const { return ___lastCast_15; }
	inline float* get_address_of_lastCast_15() { return &___lastCast_15; }
	inline void set_lastCast_15(float value)
	{
		___lastCast_15 = value;
	}

	inline static int32_t get_offset_of_thisCollider_17() { return static_cast<int32_t>(offsetof(SnapToController_t261557653, ___thisCollider_17)); }
	inline Collider_t3497673348 * get_thisCollider_17() const { return ___thisCollider_17; }
	inline Collider_t3497673348 ** get_address_of_thisCollider_17() { return &___thisCollider_17; }
	inline void set_thisCollider_17(Collider_t3497673348 * value)
	{
		___thisCollider_17 = value;
		Il2CppCodeGenWriteBarrier(&___thisCollider_17, value);
	}
};

struct SnapToController_t261557653_StaticFields
{
public:
	// System.Boolean SnapToController::triggerToggle
	bool ___triggerToggle_16;

public:
	inline static int32_t get_offset_of_triggerToggle_16() { return static_cast<int32_t>(offsetof(SnapToController_t261557653_StaticFields, ___triggerToggle_16)); }
	inline bool get_triggerToggle_16() const { return ___triggerToggle_16; }
	inline bool* get_address_of_triggerToggle_16() { return &___triggerToggle_16; }
	inline void set_triggerToggle_16(bool value)
	{
		___triggerToggle_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
