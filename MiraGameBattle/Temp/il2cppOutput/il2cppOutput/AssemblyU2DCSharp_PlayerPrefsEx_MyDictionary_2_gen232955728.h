﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// PlayerPrefsEx/StringPlayerPrefsExPair[]
struct StringPlayerPrefsExPairU5BU5D_t2827973713;
// System.Collections.Generic.Dictionary`2<System.String,PlayerPrefsEx>
struct Dictionary_2_t3374429942;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerPrefsEx/MyDictionary`2<PlayerPrefsEx/StringPlayerPrefsExPair,PlayerPrefsEx>
struct  MyDictionary_2_t232955728  : public Il2CppObject
{
public:
	// KeyValuePair[] PlayerPrefsEx/MyDictionary`2::m_Data
	StringPlayerPrefsExPairU5BU5D_t2827973713* ___m_Data_0;
	// System.Collections.Generic.Dictionary`2<System.String,TValue> PlayerPrefsEx/MyDictionary`2::data
	Dictionary_2_t3374429942 * ___data_1;

public:
	inline static int32_t get_offset_of_m_Data_0() { return static_cast<int32_t>(offsetof(MyDictionary_2_t232955728, ___m_Data_0)); }
	inline StringPlayerPrefsExPairU5BU5D_t2827973713* get_m_Data_0() const { return ___m_Data_0; }
	inline StringPlayerPrefsExPairU5BU5D_t2827973713** get_address_of_m_Data_0() { return &___m_Data_0; }
	inline void set_m_Data_0(StringPlayerPrefsExPairU5BU5D_t2827973713* value)
	{
		___m_Data_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Data_0, value);
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(MyDictionary_2_t232955728, ___data_1)); }
	inline Dictionary_2_t3374429942 * get_data_1() const { return ___data_1; }
	inline Dictionary_2_t3374429942 ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(Dictionary_2_t3374429942 * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier(&___data_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
