﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// PlayAreaRenderer
struct PlayAreaRenderer_t3466907232;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayAreaHelper
struct  PlayAreaHelper_t1795592975  : public MonoBehaviour_t1158329972
{
public:
	// System.Single PlayAreaHelper::warningDistance
	float ___warningDistance_2;
	// UnityEngine.Transform[] PlayAreaHelper::controlPoints
	TransformU5BU5D_t3764228911* ___controlPoints_3;
	// System.Boolean PlayAreaHelper::showCameraModel
	bool ___showCameraModel_4;
	// UnityEngine.GameObject PlayAreaHelper::m_cameraModel
	GameObject_t1756533147 * ___m_cameraModel_5;
	// System.Boolean PlayAreaHelper::autoCreatePlayArea
	bool ___autoCreatePlayArea_6;
	// PlayAreaRenderer PlayAreaHelper::m_PlayArea
	PlayAreaRenderer_t3466907232 * ___m_PlayArea_7;
	// UnityEngine.Vector3[] PlayAreaHelper::m_Corners
	Vector3U5BU5D_t1172311765* ___m_Corners_8;
	// System.String PlayAreaHelper::trackerName
	String_t* ___trackerName_9;
	// System.Int32 PlayAreaHelper::m_TrackerHandle
	int32_t ___m_TrackerHandle_10;
	// System.Boolean PlayAreaHelper::showOnTrackingLost
	bool ___showOnTrackingLost_11;
	// System.Int32[] PlayAreaHelper::trackedNodes
	Int32U5BU5D_t3030399641* ___trackedNodes_12;
	// UnityEngine.Transform PlayAreaHelper::m_Transform
	Transform_t3275118058 * ___m_Transform_13;
	// System.Boolean PlayAreaHelper::m_ReadyForModel
	bool ___m_ReadyForModel_14;
	// System.Int32 PlayAreaHelper::m_GroundAlpha
	int32_t ___m_GroundAlpha_15;
	// System.Int32 PlayAreaHelper::m_WallAlpha
	int32_t ___m_WallAlpha_16;
	// UnityEngine.Vector3[] PlayAreaHelper::m_CachedCorners
	Vector3U5BU5D_t1172311765* ___m_CachedCorners_17;

public:
	inline static int32_t get_offset_of_warningDistance_2() { return static_cast<int32_t>(offsetof(PlayAreaHelper_t1795592975, ___warningDistance_2)); }
	inline float get_warningDistance_2() const { return ___warningDistance_2; }
	inline float* get_address_of_warningDistance_2() { return &___warningDistance_2; }
	inline void set_warningDistance_2(float value)
	{
		___warningDistance_2 = value;
	}

	inline static int32_t get_offset_of_controlPoints_3() { return static_cast<int32_t>(offsetof(PlayAreaHelper_t1795592975, ___controlPoints_3)); }
	inline TransformU5BU5D_t3764228911* get_controlPoints_3() const { return ___controlPoints_3; }
	inline TransformU5BU5D_t3764228911** get_address_of_controlPoints_3() { return &___controlPoints_3; }
	inline void set_controlPoints_3(TransformU5BU5D_t3764228911* value)
	{
		___controlPoints_3 = value;
		Il2CppCodeGenWriteBarrier(&___controlPoints_3, value);
	}

	inline static int32_t get_offset_of_showCameraModel_4() { return static_cast<int32_t>(offsetof(PlayAreaHelper_t1795592975, ___showCameraModel_4)); }
	inline bool get_showCameraModel_4() const { return ___showCameraModel_4; }
	inline bool* get_address_of_showCameraModel_4() { return &___showCameraModel_4; }
	inline void set_showCameraModel_4(bool value)
	{
		___showCameraModel_4 = value;
	}

	inline static int32_t get_offset_of_m_cameraModel_5() { return static_cast<int32_t>(offsetof(PlayAreaHelper_t1795592975, ___m_cameraModel_5)); }
	inline GameObject_t1756533147 * get_m_cameraModel_5() const { return ___m_cameraModel_5; }
	inline GameObject_t1756533147 ** get_address_of_m_cameraModel_5() { return &___m_cameraModel_5; }
	inline void set_m_cameraModel_5(GameObject_t1756533147 * value)
	{
		___m_cameraModel_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_cameraModel_5, value);
	}

	inline static int32_t get_offset_of_autoCreatePlayArea_6() { return static_cast<int32_t>(offsetof(PlayAreaHelper_t1795592975, ___autoCreatePlayArea_6)); }
	inline bool get_autoCreatePlayArea_6() const { return ___autoCreatePlayArea_6; }
	inline bool* get_address_of_autoCreatePlayArea_6() { return &___autoCreatePlayArea_6; }
	inline void set_autoCreatePlayArea_6(bool value)
	{
		___autoCreatePlayArea_6 = value;
	}

	inline static int32_t get_offset_of_m_PlayArea_7() { return static_cast<int32_t>(offsetof(PlayAreaHelper_t1795592975, ___m_PlayArea_7)); }
	inline PlayAreaRenderer_t3466907232 * get_m_PlayArea_7() const { return ___m_PlayArea_7; }
	inline PlayAreaRenderer_t3466907232 ** get_address_of_m_PlayArea_7() { return &___m_PlayArea_7; }
	inline void set_m_PlayArea_7(PlayAreaRenderer_t3466907232 * value)
	{
		___m_PlayArea_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_PlayArea_7, value);
	}

	inline static int32_t get_offset_of_m_Corners_8() { return static_cast<int32_t>(offsetof(PlayAreaHelper_t1795592975, ___m_Corners_8)); }
	inline Vector3U5BU5D_t1172311765* get_m_Corners_8() const { return ___m_Corners_8; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_Corners_8() { return &___m_Corners_8; }
	inline void set_m_Corners_8(Vector3U5BU5D_t1172311765* value)
	{
		___m_Corners_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_Corners_8, value);
	}

	inline static int32_t get_offset_of_trackerName_9() { return static_cast<int32_t>(offsetof(PlayAreaHelper_t1795592975, ___trackerName_9)); }
	inline String_t* get_trackerName_9() const { return ___trackerName_9; }
	inline String_t** get_address_of_trackerName_9() { return &___trackerName_9; }
	inline void set_trackerName_9(String_t* value)
	{
		___trackerName_9 = value;
		Il2CppCodeGenWriteBarrier(&___trackerName_9, value);
	}

	inline static int32_t get_offset_of_m_TrackerHandle_10() { return static_cast<int32_t>(offsetof(PlayAreaHelper_t1795592975, ___m_TrackerHandle_10)); }
	inline int32_t get_m_TrackerHandle_10() const { return ___m_TrackerHandle_10; }
	inline int32_t* get_address_of_m_TrackerHandle_10() { return &___m_TrackerHandle_10; }
	inline void set_m_TrackerHandle_10(int32_t value)
	{
		___m_TrackerHandle_10 = value;
	}

	inline static int32_t get_offset_of_showOnTrackingLost_11() { return static_cast<int32_t>(offsetof(PlayAreaHelper_t1795592975, ___showOnTrackingLost_11)); }
	inline bool get_showOnTrackingLost_11() const { return ___showOnTrackingLost_11; }
	inline bool* get_address_of_showOnTrackingLost_11() { return &___showOnTrackingLost_11; }
	inline void set_showOnTrackingLost_11(bool value)
	{
		___showOnTrackingLost_11 = value;
	}

	inline static int32_t get_offset_of_trackedNodes_12() { return static_cast<int32_t>(offsetof(PlayAreaHelper_t1795592975, ___trackedNodes_12)); }
	inline Int32U5BU5D_t3030399641* get_trackedNodes_12() const { return ___trackedNodes_12; }
	inline Int32U5BU5D_t3030399641** get_address_of_trackedNodes_12() { return &___trackedNodes_12; }
	inline void set_trackedNodes_12(Int32U5BU5D_t3030399641* value)
	{
		___trackedNodes_12 = value;
		Il2CppCodeGenWriteBarrier(&___trackedNodes_12, value);
	}

	inline static int32_t get_offset_of_m_Transform_13() { return static_cast<int32_t>(offsetof(PlayAreaHelper_t1795592975, ___m_Transform_13)); }
	inline Transform_t3275118058 * get_m_Transform_13() const { return ___m_Transform_13; }
	inline Transform_t3275118058 ** get_address_of_m_Transform_13() { return &___m_Transform_13; }
	inline void set_m_Transform_13(Transform_t3275118058 * value)
	{
		___m_Transform_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_Transform_13, value);
	}

	inline static int32_t get_offset_of_m_ReadyForModel_14() { return static_cast<int32_t>(offsetof(PlayAreaHelper_t1795592975, ___m_ReadyForModel_14)); }
	inline bool get_m_ReadyForModel_14() const { return ___m_ReadyForModel_14; }
	inline bool* get_address_of_m_ReadyForModel_14() { return &___m_ReadyForModel_14; }
	inline void set_m_ReadyForModel_14(bool value)
	{
		___m_ReadyForModel_14 = value;
	}

	inline static int32_t get_offset_of_m_GroundAlpha_15() { return static_cast<int32_t>(offsetof(PlayAreaHelper_t1795592975, ___m_GroundAlpha_15)); }
	inline int32_t get_m_GroundAlpha_15() const { return ___m_GroundAlpha_15; }
	inline int32_t* get_address_of_m_GroundAlpha_15() { return &___m_GroundAlpha_15; }
	inline void set_m_GroundAlpha_15(int32_t value)
	{
		___m_GroundAlpha_15 = value;
	}

	inline static int32_t get_offset_of_m_WallAlpha_16() { return static_cast<int32_t>(offsetof(PlayAreaHelper_t1795592975, ___m_WallAlpha_16)); }
	inline int32_t get_m_WallAlpha_16() const { return ___m_WallAlpha_16; }
	inline int32_t* get_address_of_m_WallAlpha_16() { return &___m_WallAlpha_16; }
	inline void set_m_WallAlpha_16(int32_t value)
	{
		___m_WallAlpha_16 = value;
	}

	inline static int32_t get_offset_of_m_CachedCorners_17() { return static_cast<int32_t>(offsetof(PlayAreaHelper_t1795592975, ___m_CachedCorners_17)); }
	inline Vector3U5BU5D_t1172311765* get_m_CachedCorners_17() const { return ___m_CachedCorners_17; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_CachedCorners_17() { return &___m_CachedCorners_17; }
	inline void set_m_CachedCorners_17(Vector3U5BU5D_t1172311765* value)
	{
		___m_CachedCorners_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_CachedCorners_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
