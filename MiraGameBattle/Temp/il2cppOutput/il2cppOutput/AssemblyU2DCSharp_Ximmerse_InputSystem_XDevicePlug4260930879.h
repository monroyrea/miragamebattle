﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// Ximmerse.InputSystem.XDevicePlugin/LogDelegate
struct LogDelegate_t4117204168;
// System.Single[]
struct SingleU5BU5D_t577127397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.InputSystem.XDevicePlugin
struct  XDevicePlugin_t4260930879  : public Il2CppObject
{
public:

public:
};

struct XDevicePlugin_t4260930879_StaticFields
{
public:
	// System.Boolean Ximmerse.InputSystem.XDevicePlugin::s_IsInited
	bool ___s_IsInited_23;
	// Ximmerse.InputSystem.XDevicePlugin/LogDelegate Ximmerse.InputSystem.XDevicePlugin::s_Logger
	LogDelegate_t4117204168 * ___s_Logger_24;
	// System.Single[] Ximmerse.InputSystem.XDevicePlugin::GetNodePosition_floats
	SingleU5BU5D_t577127397* ___GetNodePosition_floats_25;
	// System.Single[] Ximmerse.InputSystem.XDevicePlugin::UpdateNodeRotation_floats
	SingleU5BU5D_t577127397* ___UpdateNodeRotation_floats_26;

public:
	inline static int32_t get_offset_of_s_IsInited_23() { return static_cast<int32_t>(offsetof(XDevicePlugin_t4260930879_StaticFields, ___s_IsInited_23)); }
	inline bool get_s_IsInited_23() const { return ___s_IsInited_23; }
	inline bool* get_address_of_s_IsInited_23() { return &___s_IsInited_23; }
	inline void set_s_IsInited_23(bool value)
	{
		___s_IsInited_23 = value;
	}

	inline static int32_t get_offset_of_s_Logger_24() { return static_cast<int32_t>(offsetof(XDevicePlugin_t4260930879_StaticFields, ___s_Logger_24)); }
	inline LogDelegate_t4117204168 * get_s_Logger_24() const { return ___s_Logger_24; }
	inline LogDelegate_t4117204168 ** get_address_of_s_Logger_24() { return &___s_Logger_24; }
	inline void set_s_Logger_24(LogDelegate_t4117204168 * value)
	{
		___s_Logger_24 = value;
		Il2CppCodeGenWriteBarrier(&___s_Logger_24, value);
	}

	inline static int32_t get_offset_of_GetNodePosition_floats_25() { return static_cast<int32_t>(offsetof(XDevicePlugin_t4260930879_StaticFields, ___GetNodePosition_floats_25)); }
	inline SingleU5BU5D_t577127397* get_GetNodePosition_floats_25() const { return ___GetNodePosition_floats_25; }
	inline SingleU5BU5D_t577127397** get_address_of_GetNodePosition_floats_25() { return &___GetNodePosition_floats_25; }
	inline void set_GetNodePosition_floats_25(SingleU5BU5D_t577127397* value)
	{
		___GetNodePosition_floats_25 = value;
		Il2CppCodeGenWriteBarrier(&___GetNodePosition_floats_25, value);
	}

	inline static int32_t get_offset_of_UpdateNodeRotation_floats_26() { return static_cast<int32_t>(offsetof(XDevicePlugin_t4260930879_StaticFields, ___UpdateNodeRotation_floats_26)); }
	inline SingleU5BU5D_t577127397* get_UpdateNodeRotation_floats_26() const { return ___UpdateNodeRotation_floats_26; }
	inline SingleU5BU5D_t577127397** get_address_of_UpdateNodeRotation_floats_26() { return &___UpdateNodeRotation_floats_26; }
	inline void set_UpdateNodeRotation_floats_26(SingleU5BU5D_t577127397* value)
	{
		___UpdateNodeRotation_floats_26 = value;
		Il2CppCodeGenWriteBarrier(&___UpdateNodeRotation_floats_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
