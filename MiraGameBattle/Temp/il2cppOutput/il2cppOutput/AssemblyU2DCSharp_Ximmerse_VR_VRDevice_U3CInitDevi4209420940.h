﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.VR.VRDevice/<InitDevice>c__AnonStorey0
struct  U3CInitDeviceU3Ec__AnonStorey0_t4209420940  : public Il2CppObject
{
public:
	// System.Single Ximmerse.VR.VRDevice/<InitDevice>c__AnonStorey0::f
	float ___f_0;

public:
	inline static int32_t get_offset_of_f_0() { return static_cast<int32_t>(offsetof(U3CInitDeviceU3Ec__AnonStorey0_t4209420940, ___f_0)); }
	inline float get_f_0() const { return ___f_0; }
	inline float* get_address_of_f_0() { return &___f_0; }
	inline void set_f_0(float value)
	{
		___f_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
