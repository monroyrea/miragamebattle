﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Delegate
struct Delegate_t3022476291;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReflectUtil/MemberEntry
struct  MemberEntry_t200158728  : public Il2CppObject
{
public:
	// System.Type ReflectUtil/MemberEntry::type
	Type_t * ___type_0;
	// System.String ReflectUtil/MemberEntry::memberType
	String_t* ___memberType_1;
	// System.Object ReflectUtil/MemberEntry::objectValue
	Il2CppObject * ___objectValue_2;
	// System.Reflection.MethodInfo ReflectUtil/MemberEntry::methodValue
	MethodInfo_t * ___methodValue_3;
	// System.Reflection.PropertyInfo ReflectUtil/MemberEntry::propertyValue
	PropertyInfo_t * ___propertyValue_4;
	// System.Delegate ReflectUtil/MemberEntry::delegateValue
	Delegate_t3022476291 * ___delegateValue_5;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(MemberEntry_t200158728, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier(&___type_0, value);
	}

	inline static int32_t get_offset_of_memberType_1() { return static_cast<int32_t>(offsetof(MemberEntry_t200158728, ___memberType_1)); }
	inline String_t* get_memberType_1() const { return ___memberType_1; }
	inline String_t** get_address_of_memberType_1() { return &___memberType_1; }
	inline void set_memberType_1(String_t* value)
	{
		___memberType_1 = value;
		Il2CppCodeGenWriteBarrier(&___memberType_1, value);
	}

	inline static int32_t get_offset_of_objectValue_2() { return static_cast<int32_t>(offsetof(MemberEntry_t200158728, ___objectValue_2)); }
	inline Il2CppObject * get_objectValue_2() const { return ___objectValue_2; }
	inline Il2CppObject ** get_address_of_objectValue_2() { return &___objectValue_2; }
	inline void set_objectValue_2(Il2CppObject * value)
	{
		___objectValue_2 = value;
		Il2CppCodeGenWriteBarrier(&___objectValue_2, value);
	}

	inline static int32_t get_offset_of_methodValue_3() { return static_cast<int32_t>(offsetof(MemberEntry_t200158728, ___methodValue_3)); }
	inline MethodInfo_t * get_methodValue_3() const { return ___methodValue_3; }
	inline MethodInfo_t ** get_address_of_methodValue_3() { return &___methodValue_3; }
	inline void set_methodValue_3(MethodInfo_t * value)
	{
		___methodValue_3 = value;
		Il2CppCodeGenWriteBarrier(&___methodValue_3, value);
	}

	inline static int32_t get_offset_of_propertyValue_4() { return static_cast<int32_t>(offsetof(MemberEntry_t200158728, ___propertyValue_4)); }
	inline PropertyInfo_t * get_propertyValue_4() const { return ___propertyValue_4; }
	inline PropertyInfo_t ** get_address_of_propertyValue_4() { return &___propertyValue_4; }
	inline void set_propertyValue_4(PropertyInfo_t * value)
	{
		___propertyValue_4 = value;
		Il2CppCodeGenWriteBarrier(&___propertyValue_4, value);
	}

	inline static int32_t get_offset_of_delegateValue_5() { return static_cast<int32_t>(offsetof(MemberEntry_t200158728, ___delegateValue_5)); }
	inline Delegate_t3022476291 * get_delegateValue_5() const { return ___delegateValue_5; }
	inline Delegate_t3022476291 ** get_address_of_delegateValue_5() { return &___delegateValue_5; }
	inline void set_delegateValue_5(Delegate_t3022476291 * value)
	{
		___delegateValue_5 = value;
		Il2CppCodeGenWriteBarrier(&___delegateValue_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
