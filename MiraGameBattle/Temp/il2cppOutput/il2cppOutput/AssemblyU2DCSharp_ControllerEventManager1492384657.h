﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerB2189630288.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerA3753021729.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// Ximmerse.InputSystem.ControllerInput
struct ControllerInput_t1382602808;
// ControllerEventManager/ControllerClick
struct ControllerClick_t4149645536;
// ControllerEventManager/SendControllerAxis
struct SendControllerAxis_t1059044385;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControllerEventManager
struct  ControllerEventManager_t1492384657  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ControllerEventManager::count
	int32_t ___count_5;
	// UnityEngine.UI.Text ControllerEventManager::controlText
	Text_t356221433 * ___controlText_6;
	// Ximmerse.InputSystem.ControllerButton ControllerEventManager::trigger
	int32_t ___trigger_8;
	// Ximmerse.InputSystem.ControllerButton ControllerEventManager::start
	int32_t ___start_9;
	// Ximmerse.InputSystem.ControllerButton ControllerEventManager::back
	int32_t ___back_10;
	// Ximmerse.InputSystem.ControllerButton ControllerEventManager::touchPadUp
	int32_t ___touchPadUp_11;
	// Ximmerse.InputSystem.ControllerButton ControllerEventManager::touchPadDown
	int32_t ___touchPadDown_12;
	// Ximmerse.InputSystem.ControllerButton ControllerEventManager::touchPadLeft
	int32_t ___touchPadLeft_13;
	// Ximmerse.InputSystem.ControllerButton ControllerEventManager::touchPadRight
	int32_t ___touchPadRight_14;
	// Ximmerse.InputSystem.ControllerAxis ControllerEventManager::touchPadX
	int32_t ___touchPadX_15;
	// Ximmerse.InputSystem.ControllerAxis ControllerEventManager::touchPadY
	int32_t ___touchPadY_16;

public:
	inline static int32_t get_offset_of_count_5() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657, ___count_5)); }
	inline int32_t get_count_5() const { return ___count_5; }
	inline int32_t* get_address_of_count_5() { return &___count_5; }
	inline void set_count_5(int32_t value)
	{
		___count_5 = value;
	}

	inline static int32_t get_offset_of_controlText_6() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657, ___controlText_6)); }
	inline Text_t356221433 * get_controlText_6() const { return ___controlText_6; }
	inline Text_t356221433 ** get_address_of_controlText_6() { return &___controlText_6; }
	inline void set_controlText_6(Text_t356221433 * value)
	{
		___controlText_6 = value;
		Il2CppCodeGenWriteBarrier(&___controlText_6, value);
	}

	inline static int32_t get_offset_of_trigger_8() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657, ___trigger_8)); }
	inline int32_t get_trigger_8() const { return ___trigger_8; }
	inline int32_t* get_address_of_trigger_8() { return &___trigger_8; }
	inline void set_trigger_8(int32_t value)
	{
		___trigger_8 = value;
	}

	inline static int32_t get_offset_of_start_9() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657, ___start_9)); }
	inline int32_t get_start_9() const { return ___start_9; }
	inline int32_t* get_address_of_start_9() { return &___start_9; }
	inline void set_start_9(int32_t value)
	{
		___start_9 = value;
	}

	inline static int32_t get_offset_of_back_10() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657, ___back_10)); }
	inline int32_t get_back_10() const { return ___back_10; }
	inline int32_t* get_address_of_back_10() { return &___back_10; }
	inline void set_back_10(int32_t value)
	{
		___back_10 = value;
	}

	inline static int32_t get_offset_of_touchPadUp_11() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657, ___touchPadUp_11)); }
	inline int32_t get_touchPadUp_11() const { return ___touchPadUp_11; }
	inline int32_t* get_address_of_touchPadUp_11() { return &___touchPadUp_11; }
	inline void set_touchPadUp_11(int32_t value)
	{
		___touchPadUp_11 = value;
	}

	inline static int32_t get_offset_of_touchPadDown_12() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657, ___touchPadDown_12)); }
	inline int32_t get_touchPadDown_12() const { return ___touchPadDown_12; }
	inline int32_t* get_address_of_touchPadDown_12() { return &___touchPadDown_12; }
	inline void set_touchPadDown_12(int32_t value)
	{
		___touchPadDown_12 = value;
	}

	inline static int32_t get_offset_of_touchPadLeft_13() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657, ___touchPadLeft_13)); }
	inline int32_t get_touchPadLeft_13() const { return ___touchPadLeft_13; }
	inline int32_t* get_address_of_touchPadLeft_13() { return &___touchPadLeft_13; }
	inline void set_touchPadLeft_13(int32_t value)
	{
		___touchPadLeft_13 = value;
	}

	inline static int32_t get_offset_of_touchPadRight_14() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657, ___touchPadRight_14)); }
	inline int32_t get_touchPadRight_14() const { return ___touchPadRight_14; }
	inline int32_t* get_address_of_touchPadRight_14() { return &___touchPadRight_14; }
	inline void set_touchPadRight_14(int32_t value)
	{
		___touchPadRight_14 = value;
	}

	inline static int32_t get_offset_of_touchPadX_15() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657, ___touchPadX_15)); }
	inline int32_t get_touchPadX_15() const { return ___touchPadX_15; }
	inline int32_t* get_address_of_touchPadX_15() { return &___touchPadX_15; }
	inline void set_touchPadX_15(int32_t value)
	{
		___touchPadX_15 = value;
	}

	inline static int32_t get_offset_of_touchPadY_16() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657, ___touchPadY_16)); }
	inline int32_t get_touchPadY_16() const { return ___touchPadY_16; }
	inline int32_t* get_address_of_touchPadY_16() { return &___touchPadY_16; }
	inline void set_touchPadY_16(int32_t value)
	{
		___touchPadY_16 = value;
	}
};

struct ControllerEventManager_t1492384657_StaticFields
{
public:
	// System.Boolean ControllerEventManager::created
	bool ___created_2;
	// UnityEngine.Vector3 ControllerEventManager::accelerometer
	Vector3_t2243707580  ___accelerometer_3;
	// UnityEngine.Vector3 ControllerEventManager::gyroscope
	Vector3_t2243707580  ___gyroscope_4;
	// Ximmerse.InputSystem.ControllerInput ControllerEventManager::controller
	ControllerInput_t1382602808 * ___controller_7;
	// ControllerEventManager/ControllerClick ControllerEventManager::TriggerOn
	ControllerClick_t4149645536 * ___TriggerOn_17;
	// ControllerEventManager/ControllerClick ControllerEventManager::TriggerStart
	ControllerClick_t4149645536 * ___TriggerStart_18;
	// ControllerEventManager/ControllerClick ControllerEventManager::TriggerEnd
	ControllerClick_t4149645536 * ___TriggerEnd_19;
	// ControllerEventManager/ControllerClick ControllerEventManager::StartOn
	ControllerClick_t4149645536 * ___StartOn_20;
	// ControllerEventManager/ControllerClick ControllerEventManager::StartDown
	ControllerClick_t4149645536 * ___StartDown_21;
	// ControllerEventManager/ControllerClick ControllerEventManager::StartUp
	ControllerClick_t4149645536 * ___StartUp_22;
	// ControllerEventManager/ControllerClick ControllerEventManager::BackOn
	ControllerClick_t4149645536 * ___BackOn_23;
	// ControllerEventManager/ControllerClick ControllerEventManager::BackDown
	ControllerClick_t4149645536 * ___BackDown_24;
	// ControllerEventManager/ControllerClick ControllerEventManager::BackUp
	ControllerClick_t4149645536 * ___BackUp_25;
	// ControllerEventManager/ControllerClick ControllerEventManager::TouchPadUpClick
	ControllerClick_t4149645536 * ___TouchPadUpClick_26;
	// ControllerEventManager/SendControllerAxis ControllerEventManager::SendTouchPadY
	SendControllerAxis_t1059044385 * ___SendTouchPadY_27;
	// ControllerEventManager/SendControllerAxis ControllerEventManager::SendTouchPadX
	SendControllerAxis_t1059044385 * ___SendTouchPadX_28;

public:
	inline static int32_t get_offset_of_created_2() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657_StaticFields, ___created_2)); }
	inline bool get_created_2() const { return ___created_2; }
	inline bool* get_address_of_created_2() { return &___created_2; }
	inline void set_created_2(bool value)
	{
		___created_2 = value;
	}

	inline static int32_t get_offset_of_accelerometer_3() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657_StaticFields, ___accelerometer_3)); }
	inline Vector3_t2243707580  get_accelerometer_3() const { return ___accelerometer_3; }
	inline Vector3_t2243707580 * get_address_of_accelerometer_3() { return &___accelerometer_3; }
	inline void set_accelerometer_3(Vector3_t2243707580  value)
	{
		___accelerometer_3 = value;
	}

	inline static int32_t get_offset_of_gyroscope_4() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657_StaticFields, ___gyroscope_4)); }
	inline Vector3_t2243707580  get_gyroscope_4() const { return ___gyroscope_4; }
	inline Vector3_t2243707580 * get_address_of_gyroscope_4() { return &___gyroscope_4; }
	inline void set_gyroscope_4(Vector3_t2243707580  value)
	{
		___gyroscope_4 = value;
	}

	inline static int32_t get_offset_of_controller_7() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657_StaticFields, ___controller_7)); }
	inline ControllerInput_t1382602808 * get_controller_7() const { return ___controller_7; }
	inline ControllerInput_t1382602808 ** get_address_of_controller_7() { return &___controller_7; }
	inline void set_controller_7(ControllerInput_t1382602808 * value)
	{
		___controller_7 = value;
		Il2CppCodeGenWriteBarrier(&___controller_7, value);
	}

	inline static int32_t get_offset_of_TriggerOn_17() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657_StaticFields, ___TriggerOn_17)); }
	inline ControllerClick_t4149645536 * get_TriggerOn_17() const { return ___TriggerOn_17; }
	inline ControllerClick_t4149645536 ** get_address_of_TriggerOn_17() { return &___TriggerOn_17; }
	inline void set_TriggerOn_17(ControllerClick_t4149645536 * value)
	{
		___TriggerOn_17 = value;
		Il2CppCodeGenWriteBarrier(&___TriggerOn_17, value);
	}

	inline static int32_t get_offset_of_TriggerStart_18() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657_StaticFields, ___TriggerStart_18)); }
	inline ControllerClick_t4149645536 * get_TriggerStart_18() const { return ___TriggerStart_18; }
	inline ControllerClick_t4149645536 ** get_address_of_TriggerStart_18() { return &___TriggerStart_18; }
	inline void set_TriggerStart_18(ControllerClick_t4149645536 * value)
	{
		___TriggerStart_18 = value;
		Il2CppCodeGenWriteBarrier(&___TriggerStart_18, value);
	}

	inline static int32_t get_offset_of_TriggerEnd_19() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657_StaticFields, ___TriggerEnd_19)); }
	inline ControllerClick_t4149645536 * get_TriggerEnd_19() const { return ___TriggerEnd_19; }
	inline ControllerClick_t4149645536 ** get_address_of_TriggerEnd_19() { return &___TriggerEnd_19; }
	inline void set_TriggerEnd_19(ControllerClick_t4149645536 * value)
	{
		___TriggerEnd_19 = value;
		Il2CppCodeGenWriteBarrier(&___TriggerEnd_19, value);
	}

	inline static int32_t get_offset_of_StartOn_20() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657_StaticFields, ___StartOn_20)); }
	inline ControllerClick_t4149645536 * get_StartOn_20() const { return ___StartOn_20; }
	inline ControllerClick_t4149645536 ** get_address_of_StartOn_20() { return &___StartOn_20; }
	inline void set_StartOn_20(ControllerClick_t4149645536 * value)
	{
		___StartOn_20 = value;
		Il2CppCodeGenWriteBarrier(&___StartOn_20, value);
	}

	inline static int32_t get_offset_of_StartDown_21() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657_StaticFields, ___StartDown_21)); }
	inline ControllerClick_t4149645536 * get_StartDown_21() const { return ___StartDown_21; }
	inline ControllerClick_t4149645536 ** get_address_of_StartDown_21() { return &___StartDown_21; }
	inline void set_StartDown_21(ControllerClick_t4149645536 * value)
	{
		___StartDown_21 = value;
		Il2CppCodeGenWriteBarrier(&___StartDown_21, value);
	}

	inline static int32_t get_offset_of_StartUp_22() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657_StaticFields, ___StartUp_22)); }
	inline ControllerClick_t4149645536 * get_StartUp_22() const { return ___StartUp_22; }
	inline ControllerClick_t4149645536 ** get_address_of_StartUp_22() { return &___StartUp_22; }
	inline void set_StartUp_22(ControllerClick_t4149645536 * value)
	{
		___StartUp_22 = value;
		Il2CppCodeGenWriteBarrier(&___StartUp_22, value);
	}

	inline static int32_t get_offset_of_BackOn_23() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657_StaticFields, ___BackOn_23)); }
	inline ControllerClick_t4149645536 * get_BackOn_23() const { return ___BackOn_23; }
	inline ControllerClick_t4149645536 ** get_address_of_BackOn_23() { return &___BackOn_23; }
	inline void set_BackOn_23(ControllerClick_t4149645536 * value)
	{
		___BackOn_23 = value;
		Il2CppCodeGenWriteBarrier(&___BackOn_23, value);
	}

	inline static int32_t get_offset_of_BackDown_24() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657_StaticFields, ___BackDown_24)); }
	inline ControllerClick_t4149645536 * get_BackDown_24() const { return ___BackDown_24; }
	inline ControllerClick_t4149645536 ** get_address_of_BackDown_24() { return &___BackDown_24; }
	inline void set_BackDown_24(ControllerClick_t4149645536 * value)
	{
		___BackDown_24 = value;
		Il2CppCodeGenWriteBarrier(&___BackDown_24, value);
	}

	inline static int32_t get_offset_of_BackUp_25() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657_StaticFields, ___BackUp_25)); }
	inline ControllerClick_t4149645536 * get_BackUp_25() const { return ___BackUp_25; }
	inline ControllerClick_t4149645536 ** get_address_of_BackUp_25() { return &___BackUp_25; }
	inline void set_BackUp_25(ControllerClick_t4149645536 * value)
	{
		___BackUp_25 = value;
		Il2CppCodeGenWriteBarrier(&___BackUp_25, value);
	}

	inline static int32_t get_offset_of_TouchPadUpClick_26() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657_StaticFields, ___TouchPadUpClick_26)); }
	inline ControllerClick_t4149645536 * get_TouchPadUpClick_26() const { return ___TouchPadUpClick_26; }
	inline ControllerClick_t4149645536 ** get_address_of_TouchPadUpClick_26() { return &___TouchPadUpClick_26; }
	inline void set_TouchPadUpClick_26(ControllerClick_t4149645536 * value)
	{
		___TouchPadUpClick_26 = value;
		Il2CppCodeGenWriteBarrier(&___TouchPadUpClick_26, value);
	}

	inline static int32_t get_offset_of_SendTouchPadY_27() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657_StaticFields, ___SendTouchPadY_27)); }
	inline SendControllerAxis_t1059044385 * get_SendTouchPadY_27() const { return ___SendTouchPadY_27; }
	inline SendControllerAxis_t1059044385 ** get_address_of_SendTouchPadY_27() { return &___SendTouchPadY_27; }
	inline void set_SendTouchPadY_27(SendControllerAxis_t1059044385 * value)
	{
		___SendTouchPadY_27 = value;
		Il2CppCodeGenWriteBarrier(&___SendTouchPadY_27, value);
	}

	inline static int32_t get_offset_of_SendTouchPadX_28() { return static_cast<int32_t>(offsetof(ControllerEventManager_t1492384657_StaticFields, ___SendTouchPadX_28)); }
	inline SendControllerAxis_t1059044385 * get_SendTouchPadX_28() const { return ___SendTouchPadX_28; }
	inline SendControllerAxis_t1059044385 ** get_address_of_SendTouchPadX_28() { return &___SendTouchPadX_28; }
	inline void set_SendTouchPadX_28(SendControllerAxis_t1059044385 * value)
	{
		___SendTouchPadX_28 = value;
		Il2CppCodeGenWriteBarrier(&___SendTouchPadX_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
