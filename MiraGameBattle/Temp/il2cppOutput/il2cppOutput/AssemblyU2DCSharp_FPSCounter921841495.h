﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// System.Func`1<System.Int32>
struct Func_1_t4026270130;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPSCounter
struct  FPSCounter_t921841495  : public MonoBehaviour_t1158329972
{
public:
	// System.Single FPSCounter::fpsMeasurePeriod
	float ___fpsMeasurePeriod_2;
	// System.String FPSCounter::display
	String_t* ___display_3;
	// System.Func`1<System.Int32> FPSCounter::getFrameCount
	Func_1_t4026270130 * ___getFrameCount_4;
	// System.Int32 FPSCounter::m_FpsAccumulator
	int32_t ___m_FpsAccumulator_5;
	// System.Int32 FPSCounter::m_PrevFpsAccumulator
	int32_t ___m_PrevFpsAccumulator_6;
	// System.Single FPSCounter::m_FpsNextPeriod
	float ___m_FpsNextPeriod_7;
	// System.Single FPSCounter::m_CurrentFps
	float ___m_CurrentFps_8;
	// UnityEngine.UI.Text FPSCounter::m_Text
	Text_t356221433 * ___m_Text_9;

public:
	inline static int32_t get_offset_of_fpsMeasurePeriod_2() { return static_cast<int32_t>(offsetof(FPSCounter_t921841495, ___fpsMeasurePeriod_2)); }
	inline float get_fpsMeasurePeriod_2() const { return ___fpsMeasurePeriod_2; }
	inline float* get_address_of_fpsMeasurePeriod_2() { return &___fpsMeasurePeriod_2; }
	inline void set_fpsMeasurePeriod_2(float value)
	{
		___fpsMeasurePeriod_2 = value;
	}

	inline static int32_t get_offset_of_display_3() { return static_cast<int32_t>(offsetof(FPSCounter_t921841495, ___display_3)); }
	inline String_t* get_display_3() const { return ___display_3; }
	inline String_t** get_address_of_display_3() { return &___display_3; }
	inline void set_display_3(String_t* value)
	{
		___display_3 = value;
		Il2CppCodeGenWriteBarrier(&___display_3, value);
	}

	inline static int32_t get_offset_of_getFrameCount_4() { return static_cast<int32_t>(offsetof(FPSCounter_t921841495, ___getFrameCount_4)); }
	inline Func_1_t4026270130 * get_getFrameCount_4() const { return ___getFrameCount_4; }
	inline Func_1_t4026270130 ** get_address_of_getFrameCount_4() { return &___getFrameCount_4; }
	inline void set_getFrameCount_4(Func_1_t4026270130 * value)
	{
		___getFrameCount_4 = value;
		Il2CppCodeGenWriteBarrier(&___getFrameCount_4, value);
	}

	inline static int32_t get_offset_of_m_FpsAccumulator_5() { return static_cast<int32_t>(offsetof(FPSCounter_t921841495, ___m_FpsAccumulator_5)); }
	inline int32_t get_m_FpsAccumulator_5() const { return ___m_FpsAccumulator_5; }
	inline int32_t* get_address_of_m_FpsAccumulator_5() { return &___m_FpsAccumulator_5; }
	inline void set_m_FpsAccumulator_5(int32_t value)
	{
		___m_FpsAccumulator_5 = value;
	}

	inline static int32_t get_offset_of_m_PrevFpsAccumulator_6() { return static_cast<int32_t>(offsetof(FPSCounter_t921841495, ___m_PrevFpsAccumulator_6)); }
	inline int32_t get_m_PrevFpsAccumulator_6() const { return ___m_PrevFpsAccumulator_6; }
	inline int32_t* get_address_of_m_PrevFpsAccumulator_6() { return &___m_PrevFpsAccumulator_6; }
	inline void set_m_PrevFpsAccumulator_6(int32_t value)
	{
		___m_PrevFpsAccumulator_6 = value;
	}

	inline static int32_t get_offset_of_m_FpsNextPeriod_7() { return static_cast<int32_t>(offsetof(FPSCounter_t921841495, ___m_FpsNextPeriod_7)); }
	inline float get_m_FpsNextPeriod_7() const { return ___m_FpsNextPeriod_7; }
	inline float* get_address_of_m_FpsNextPeriod_7() { return &___m_FpsNextPeriod_7; }
	inline void set_m_FpsNextPeriod_7(float value)
	{
		___m_FpsNextPeriod_7 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFps_8() { return static_cast<int32_t>(offsetof(FPSCounter_t921841495, ___m_CurrentFps_8)); }
	inline float get_m_CurrentFps_8() const { return ___m_CurrentFps_8; }
	inline float* get_address_of_m_CurrentFps_8() { return &___m_CurrentFps_8; }
	inline void set_m_CurrentFps_8(float value)
	{
		___m_CurrentFps_8 = value;
	}

	inline static int32_t get_offset_of_m_Text_9() { return static_cast<int32_t>(offsetof(FPSCounter_t921841495, ___m_Text_9)); }
	inline Text_t356221433 * get_m_Text_9() const { return ___m_Text_9; }
	inline Text_t356221433 ** get_address_of_m_Text_9() { return &___m_Text_9; }
	inline void set_m_Text_9(Text_t356221433 * value)
	{
		___m_Text_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_Text_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
