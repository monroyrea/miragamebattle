﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Ximmerse_InputSystem_InputManage3388780835.h"

// System.Collections.Generic.Dictionary`2<System.String,Ximmerse.InputSystem.ControllerInputManager/ControllerInfo>
struct Dictionary_2_t657141779;
// Ximmerse.InputSystem.ControllerInput
struct ControllerInput_t1382602808;
// UnityEngine.TextAsset
struct TextAsset_t3973159845;
// System.Collections.Generic.Dictionary`2<System.String,Ximmerse.InputSystem.ControllerInput>
struct Dictionary_2_t3297382070;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// System.Action
struct Action_t3226471752;
// System.Collections.Generic.Dictionary`2<Ximmerse.InputSystem.ControllerType,Ximmerse.InputSystem.ControllerInput>
struct Dictionary_2_t2109596977;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.InputSystem.ControllerInputManager
struct  ControllerInputManager_t1382684775  : public InputManager_t3388780835
{
public:
	// System.Boolean Ximmerse.InputSystem.ControllerInputManager::dontDestroyOnLoad
	bool ___dontDestroyOnLoad_8;
	// UnityEngine.TextAsset Ximmerse.InputSystem.ControllerInputManager::m_ControllerDataTxt
	TextAsset_t3973159845 * ___m_ControllerDataTxt_9;
	// System.Collections.Generic.Dictionary`2<System.String,Ximmerse.InputSystem.ControllerInput> Ximmerse.InputSystem.ControllerInputManager::controllerInputs
	Dictionary_2_t3297382070 * ___controllerInputs_10;
	// UnityEngine.Events.UnityEvent Ximmerse.InputSystem.ControllerInputManager::m_OnInitialized
	UnityEvent_t408735097 * ___m_OnInitialized_11;
	// System.Action Ximmerse.InputSystem.ControllerInputManager::onUpdate
	Action_t3226471752 * ___onUpdate_12;
	// System.Action Ximmerse.InputSystem.ControllerInputManager::onDestroy
	Action_t3226471752 * ___onDestroy_13;
	// System.Collections.Generic.Dictionary`2<Ximmerse.InputSystem.ControllerType,Ximmerse.InputSystem.ControllerInput> Ximmerse.InputSystem.ControllerInputManager::m_GetInputByTypeCaches
	Dictionary_2_t2109596977 * ___m_GetInputByTypeCaches_14;

public:
	inline static int32_t get_offset_of_dontDestroyOnLoad_8() { return static_cast<int32_t>(offsetof(ControllerInputManager_t1382684775, ___dontDestroyOnLoad_8)); }
	inline bool get_dontDestroyOnLoad_8() const { return ___dontDestroyOnLoad_8; }
	inline bool* get_address_of_dontDestroyOnLoad_8() { return &___dontDestroyOnLoad_8; }
	inline void set_dontDestroyOnLoad_8(bool value)
	{
		___dontDestroyOnLoad_8 = value;
	}

	inline static int32_t get_offset_of_m_ControllerDataTxt_9() { return static_cast<int32_t>(offsetof(ControllerInputManager_t1382684775, ___m_ControllerDataTxt_9)); }
	inline TextAsset_t3973159845 * get_m_ControllerDataTxt_9() const { return ___m_ControllerDataTxt_9; }
	inline TextAsset_t3973159845 ** get_address_of_m_ControllerDataTxt_9() { return &___m_ControllerDataTxt_9; }
	inline void set_m_ControllerDataTxt_9(TextAsset_t3973159845 * value)
	{
		___m_ControllerDataTxt_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_ControllerDataTxt_9, value);
	}

	inline static int32_t get_offset_of_controllerInputs_10() { return static_cast<int32_t>(offsetof(ControllerInputManager_t1382684775, ___controllerInputs_10)); }
	inline Dictionary_2_t3297382070 * get_controllerInputs_10() const { return ___controllerInputs_10; }
	inline Dictionary_2_t3297382070 ** get_address_of_controllerInputs_10() { return &___controllerInputs_10; }
	inline void set_controllerInputs_10(Dictionary_2_t3297382070 * value)
	{
		___controllerInputs_10 = value;
		Il2CppCodeGenWriteBarrier(&___controllerInputs_10, value);
	}

	inline static int32_t get_offset_of_m_OnInitialized_11() { return static_cast<int32_t>(offsetof(ControllerInputManager_t1382684775, ___m_OnInitialized_11)); }
	inline UnityEvent_t408735097 * get_m_OnInitialized_11() const { return ___m_OnInitialized_11; }
	inline UnityEvent_t408735097 ** get_address_of_m_OnInitialized_11() { return &___m_OnInitialized_11; }
	inline void set_m_OnInitialized_11(UnityEvent_t408735097 * value)
	{
		___m_OnInitialized_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_OnInitialized_11, value);
	}

	inline static int32_t get_offset_of_onUpdate_12() { return static_cast<int32_t>(offsetof(ControllerInputManager_t1382684775, ___onUpdate_12)); }
	inline Action_t3226471752 * get_onUpdate_12() const { return ___onUpdate_12; }
	inline Action_t3226471752 ** get_address_of_onUpdate_12() { return &___onUpdate_12; }
	inline void set_onUpdate_12(Action_t3226471752 * value)
	{
		___onUpdate_12 = value;
		Il2CppCodeGenWriteBarrier(&___onUpdate_12, value);
	}

	inline static int32_t get_offset_of_onDestroy_13() { return static_cast<int32_t>(offsetof(ControllerInputManager_t1382684775, ___onDestroy_13)); }
	inline Action_t3226471752 * get_onDestroy_13() const { return ___onDestroy_13; }
	inline Action_t3226471752 ** get_address_of_onDestroy_13() { return &___onDestroy_13; }
	inline void set_onDestroy_13(Action_t3226471752 * value)
	{
		___onDestroy_13 = value;
		Il2CppCodeGenWriteBarrier(&___onDestroy_13, value);
	}

	inline static int32_t get_offset_of_m_GetInputByTypeCaches_14() { return static_cast<int32_t>(offsetof(ControllerInputManager_t1382684775, ___m_GetInputByTypeCaches_14)); }
	inline Dictionary_2_t2109596977 * get_m_GetInputByTypeCaches_14() const { return ___m_GetInputByTypeCaches_14; }
	inline Dictionary_2_t2109596977 ** get_address_of_m_GetInputByTypeCaches_14() { return &___m_GetInputByTypeCaches_14; }
	inline void set_m_GetInputByTypeCaches_14(Dictionary_2_t2109596977 * value)
	{
		___m_GetInputByTypeCaches_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_GetInputByTypeCaches_14, value);
	}
};

struct ControllerInputManager_t1382684775_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Ximmerse.InputSystem.ControllerInputManager/ControllerInfo> Ximmerse.InputSystem.ControllerInputManager::s_ControllerDatabase
	Dictionary_2_t657141779 * ___s_ControllerDatabase_6;
	// Ximmerse.InputSystem.ControllerInput Ximmerse.InputSystem.ControllerInputManager::s_EmptyInput
	ControllerInput_t1382602808 * ___s_EmptyInput_7;

public:
	inline static int32_t get_offset_of_s_ControllerDatabase_6() { return static_cast<int32_t>(offsetof(ControllerInputManager_t1382684775_StaticFields, ___s_ControllerDatabase_6)); }
	inline Dictionary_2_t657141779 * get_s_ControllerDatabase_6() const { return ___s_ControllerDatabase_6; }
	inline Dictionary_2_t657141779 ** get_address_of_s_ControllerDatabase_6() { return &___s_ControllerDatabase_6; }
	inline void set_s_ControllerDatabase_6(Dictionary_2_t657141779 * value)
	{
		___s_ControllerDatabase_6 = value;
		Il2CppCodeGenWriteBarrier(&___s_ControllerDatabase_6, value);
	}

	inline static int32_t get_offset_of_s_EmptyInput_7() { return static_cast<int32_t>(offsetof(ControllerInputManager_t1382684775_StaticFields, ___s_EmptyInput_7)); }
	inline ControllerInput_t1382602808 * get_s_EmptyInput_7() const { return ___s_EmptyInput_7; }
	inline ControllerInput_t1382602808 ** get_address_of_s_EmptyInput_7() { return &___s_EmptyInput_7; }
	inline void set_s_EmptyInput_7(ControllerInput_t1382602808 * value)
	{
		___s_EmptyInput_7 = value;
		Il2CppCodeGenWriteBarrier(&___s_EmptyInput_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
