﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Ximmerse.InputSystem.ControllerInput
struct ControllerInput_t1382602808;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// FPSCounter
struct FPSCounter_t921841495;
// ControllerInputGUI/AxisEntry[]
struct AxisEntryU5BU5D_t2282225958;
// ControllerInputGUI/ButtonEntry[]
struct ButtonEntryU5BU5D_t2502046281;
// ControllerInputGUI/VibrationEntry[]
struct VibrationEntryU5BU5D_t1778498175;
// Ximmerse.UI.UIVector3Field
struct UIVector3Field_t3536787880;
// ControllerInputGUI/SliderGroup
struct SliderGroup_t396385140;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControllerInputGUI/ControllerEntry
struct  ControllerEntry_t1998352496  : public Il2CppObject
{
public:
	// Ximmerse.InputSystem.ControllerInput ControllerInputGUI/ControllerEntry::controllerInput
	ControllerInput_t1382602808 * ___controllerInput_0;
	// UnityEngine.UI.Text ControllerInputGUI/ControllerEntry::m_NameText
	Text_t356221433 * ___m_NameText_1;
	// System.String ControllerInputGUI/ControllerEntry::m_Name
	String_t* ___m_Name_2;
	// UnityEngine.GameObject ControllerInputGUI/ControllerEntry::m_EnableGo
	GameObject_t1756533147 * ___m_EnableGo_3;
	// UnityEngine.GameObject ControllerInputGUI/ControllerEntry::m_DisableGo
	GameObject_t1756533147 * ___m_DisableGo_4;
	// FPSCounter ControllerInputGUI/ControllerEntry::m_FPSCounter
	FPSCounter_t921841495 * ___m_FPSCounter_5;
	// UnityEngine.UI.Text ControllerInputGUI/ControllerEntry::m_ConnectionText
	Text_t356221433 * ___m_ConnectionText_6;
	// ControllerInputGUI/AxisEntry[] ControllerInputGUI/ControllerEntry::m_Axes
	AxisEntryU5BU5D_t2282225958* ___m_Axes_7;
	// ControllerInputGUI/ButtonEntry[] ControllerInputGUI/ControllerEntry::m_Buttons
	ButtonEntryU5BU5D_t2502046281* ___m_Buttons_8;
	// ControllerInputGUI/VibrationEntry[] ControllerInputGUI/ControllerEntry::m_Vibrations
	VibrationEntryU5BU5D_t1778498175* ___m_Vibrations_9;
	// Ximmerse.UI.UIVector3Field ControllerInputGUI/ControllerEntry::m_Position
	UIVector3Field_t3536787880 * ___m_Position_10;
	// UnityEngine.GameObject ControllerInputGUI/ControllerEntry::m_PositionGo
	GameObject_t1756533147 * ___m_PositionGo_11;
	// Ximmerse.UI.UIVector3Field ControllerInputGUI/ControllerEntry::m_Rotation
	UIVector3Field_t3536787880 * ___m_Rotation_12;
	// Ximmerse.UI.UIVector3Field ControllerInputGUI/ControllerEntry::m_Accelerometer
	UIVector3Field_t3536787880 * ___m_Accelerometer_13;
	// Ximmerse.UI.UIVector3Field ControllerInputGUI/ControllerEntry::m_Gyroscope
	UIVector3Field_t3536787880 * ___m_Gyroscope_14;
	// ControllerInputGUI/SliderGroup ControllerInputGUI/ControllerEntry::m_AccelerometerMag
	SliderGroup_t396385140 * ___m_AccelerometerMag_15;
	// ControllerInputGUI/SliderGroup ControllerInputGUI/ControllerEntry::m_GyroscopeMag
	SliderGroup_t396385140 * ___m_GyroscopeMag_16;

public:
	inline static int32_t get_offset_of_controllerInput_0() { return static_cast<int32_t>(offsetof(ControllerEntry_t1998352496, ___controllerInput_0)); }
	inline ControllerInput_t1382602808 * get_controllerInput_0() const { return ___controllerInput_0; }
	inline ControllerInput_t1382602808 ** get_address_of_controllerInput_0() { return &___controllerInput_0; }
	inline void set_controllerInput_0(ControllerInput_t1382602808 * value)
	{
		___controllerInput_0 = value;
		Il2CppCodeGenWriteBarrier(&___controllerInput_0, value);
	}

	inline static int32_t get_offset_of_m_NameText_1() { return static_cast<int32_t>(offsetof(ControllerEntry_t1998352496, ___m_NameText_1)); }
	inline Text_t356221433 * get_m_NameText_1() const { return ___m_NameText_1; }
	inline Text_t356221433 ** get_address_of_m_NameText_1() { return &___m_NameText_1; }
	inline void set_m_NameText_1(Text_t356221433 * value)
	{
		___m_NameText_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_NameText_1, value);
	}

	inline static int32_t get_offset_of_m_Name_2() { return static_cast<int32_t>(offsetof(ControllerEntry_t1998352496, ___m_Name_2)); }
	inline String_t* get_m_Name_2() const { return ___m_Name_2; }
	inline String_t** get_address_of_m_Name_2() { return &___m_Name_2; }
	inline void set_m_Name_2(String_t* value)
	{
		___m_Name_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Name_2, value);
	}

	inline static int32_t get_offset_of_m_EnableGo_3() { return static_cast<int32_t>(offsetof(ControllerEntry_t1998352496, ___m_EnableGo_3)); }
	inline GameObject_t1756533147 * get_m_EnableGo_3() const { return ___m_EnableGo_3; }
	inline GameObject_t1756533147 ** get_address_of_m_EnableGo_3() { return &___m_EnableGo_3; }
	inline void set_m_EnableGo_3(GameObject_t1756533147 * value)
	{
		___m_EnableGo_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_EnableGo_3, value);
	}

	inline static int32_t get_offset_of_m_DisableGo_4() { return static_cast<int32_t>(offsetof(ControllerEntry_t1998352496, ___m_DisableGo_4)); }
	inline GameObject_t1756533147 * get_m_DisableGo_4() const { return ___m_DisableGo_4; }
	inline GameObject_t1756533147 ** get_address_of_m_DisableGo_4() { return &___m_DisableGo_4; }
	inline void set_m_DisableGo_4(GameObject_t1756533147 * value)
	{
		___m_DisableGo_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_DisableGo_4, value);
	}

	inline static int32_t get_offset_of_m_FPSCounter_5() { return static_cast<int32_t>(offsetof(ControllerEntry_t1998352496, ___m_FPSCounter_5)); }
	inline FPSCounter_t921841495 * get_m_FPSCounter_5() const { return ___m_FPSCounter_5; }
	inline FPSCounter_t921841495 ** get_address_of_m_FPSCounter_5() { return &___m_FPSCounter_5; }
	inline void set_m_FPSCounter_5(FPSCounter_t921841495 * value)
	{
		___m_FPSCounter_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_FPSCounter_5, value);
	}

	inline static int32_t get_offset_of_m_ConnectionText_6() { return static_cast<int32_t>(offsetof(ControllerEntry_t1998352496, ___m_ConnectionText_6)); }
	inline Text_t356221433 * get_m_ConnectionText_6() const { return ___m_ConnectionText_6; }
	inline Text_t356221433 ** get_address_of_m_ConnectionText_6() { return &___m_ConnectionText_6; }
	inline void set_m_ConnectionText_6(Text_t356221433 * value)
	{
		___m_ConnectionText_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_ConnectionText_6, value);
	}

	inline static int32_t get_offset_of_m_Axes_7() { return static_cast<int32_t>(offsetof(ControllerEntry_t1998352496, ___m_Axes_7)); }
	inline AxisEntryU5BU5D_t2282225958* get_m_Axes_7() const { return ___m_Axes_7; }
	inline AxisEntryU5BU5D_t2282225958** get_address_of_m_Axes_7() { return &___m_Axes_7; }
	inline void set_m_Axes_7(AxisEntryU5BU5D_t2282225958* value)
	{
		___m_Axes_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_Axes_7, value);
	}

	inline static int32_t get_offset_of_m_Buttons_8() { return static_cast<int32_t>(offsetof(ControllerEntry_t1998352496, ___m_Buttons_8)); }
	inline ButtonEntryU5BU5D_t2502046281* get_m_Buttons_8() const { return ___m_Buttons_8; }
	inline ButtonEntryU5BU5D_t2502046281** get_address_of_m_Buttons_8() { return &___m_Buttons_8; }
	inline void set_m_Buttons_8(ButtonEntryU5BU5D_t2502046281* value)
	{
		___m_Buttons_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_Buttons_8, value);
	}

	inline static int32_t get_offset_of_m_Vibrations_9() { return static_cast<int32_t>(offsetof(ControllerEntry_t1998352496, ___m_Vibrations_9)); }
	inline VibrationEntryU5BU5D_t1778498175* get_m_Vibrations_9() const { return ___m_Vibrations_9; }
	inline VibrationEntryU5BU5D_t1778498175** get_address_of_m_Vibrations_9() { return &___m_Vibrations_9; }
	inline void set_m_Vibrations_9(VibrationEntryU5BU5D_t1778498175* value)
	{
		___m_Vibrations_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_Vibrations_9, value);
	}

	inline static int32_t get_offset_of_m_Position_10() { return static_cast<int32_t>(offsetof(ControllerEntry_t1998352496, ___m_Position_10)); }
	inline UIVector3Field_t3536787880 * get_m_Position_10() const { return ___m_Position_10; }
	inline UIVector3Field_t3536787880 ** get_address_of_m_Position_10() { return &___m_Position_10; }
	inline void set_m_Position_10(UIVector3Field_t3536787880 * value)
	{
		___m_Position_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_Position_10, value);
	}

	inline static int32_t get_offset_of_m_PositionGo_11() { return static_cast<int32_t>(offsetof(ControllerEntry_t1998352496, ___m_PositionGo_11)); }
	inline GameObject_t1756533147 * get_m_PositionGo_11() const { return ___m_PositionGo_11; }
	inline GameObject_t1756533147 ** get_address_of_m_PositionGo_11() { return &___m_PositionGo_11; }
	inline void set_m_PositionGo_11(GameObject_t1756533147 * value)
	{
		___m_PositionGo_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_PositionGo_11, value);
	}

	inline static int32_t get_offset_of_m_Rotation_12() { return static_cast<int32_t>(offsetof(ControllerEntry_t1998352496, ___m_Rotation_12)); }
	inline UIVector3Field_t3536787880 * get_m_Rotation_12() const { return ___m_Rotation_12; }
	inline UIVector3Field_t3536787880 ** get_address_of_m_Rotation_12() { return &___m_Rotation_12; }
	inline void set_m_Rotation_12(UIVector3Field_t3536787880 * value)
	{
		___m_Rotation_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_Rotation_12, value);
	}

	inline static int32_t get_offset_of_m_Accelerometer_13() { return static_cast<int32_t>(offsetof(ControllerEntry_t1998352496, ___m_Accelerometer_13)); }
	inline UIVector3Field_t3536787880 * get_m_Accelerometer_13() const { return ___m_Accelerometer_13; }
	inline UIVector3Field_t3536787880 ** get_address_of_m_Accelerometer_13() { return &___m_Accelerometer_13; }
	inline void set_m_Accelerometer_13(UIVector3Field_t3536787880 * value)
	{
		___m_Accelerometer_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_Accelerometer_13, value);
	}

	inline static int32_t get_offset_of_m_Gyroscope_14() { return static_cast<int32_t>(offsetof(ControllerEntry_t1998352496, ___m_Gyroscope_14)); }
	inline UIVector3Field_t3536787880 * get_m_Gyroscope_14() const { return ___m_Gyroscope_14; }
	inline UIVector3Field_t3536787880 ** get_address_of_m_Gyroscope_14() { return &___m_Gyroscope_14; }
	inline void set_m_Gyroscope_14(UIVector3Field_t3536787880 * value)
	{
		___m_Gyroscope_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_Gyroscope_14, value);
	}

	inline static int32_t get_offset_of_m_AccelerometerMag_15() { return static_cast<int32_t>(offsetof(ControllerEntry_t1998352496, ___m_AccelerometerMag_15)); }
	inline SliderGroup_t396385140 * get_m_AccelerometerMag_15() const { return ___m_AccelerometerMag_15; }
	inline SliderGroup_t396385140 ** get_address_of_m_AccelerometerMag_15() { return &___m_AccelerometerMag_15; }
	inline void set_m_AccelerometerMag_15(SliderGroup_t396385140 * value)
	{
		___m_AccelerometerMag_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_AccelerometerMag_15, value);
	}

	inline static int32_t get_offset_of_m_GyroscopeMag_16() { return static_cast<int32_t>(offsetof(ControllerEntry_t1998352496, ___m_GyroscopeMag_16)); }
	inline SliderGroup_t396385140 * get_m_GyroscopeMag_16() const { return ___m_GyroscopeMag_16; }
	inline SliderGroup_t396385140 ** get_address_of_m_GyroscopeMag_16() { return &___m_GyroscopeMag_16; }
	inline void set_m_GyroscopeMag_16(SliderGroup_t396385140 * value)
	{
		___m_GyroscopeMag_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_GyroscopeMag_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
