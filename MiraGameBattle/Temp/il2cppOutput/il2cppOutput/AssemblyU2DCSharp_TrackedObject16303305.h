﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerTy123719274.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_TrackingRes3701089776.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// System.String
struct String_t;
// Ximmerse.InputSystem.ControllerInput
struct ControllerInput_t1382602808;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackedObject
struct  TrackedObject_t16303305  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform TrackedObject::target
	Transform_t3275118058 * ___target_2;
	// Ximmerse.InputSystem.ControllerType TrackedObject::source
	int32_t ___source_3;
	// System.String TrackedObject::sourceName
	String_t* ___sourceName_4;
	// System.Boolean TrackedObject::trackPosition
	bool ___trackPosition_5;
	// System.Boolean TrackedObject::trackRotation
	bool ___trackRotation_6;
	// System.Boolean TrackedObject::checkParent
	bool ___checkParent_7;
	// System.Boolean TrackedObject::canRecenter
	bool ___canRecenter_8;
	// Ximmerse.InputSystem.ControllerInput TrackedObject::m_ControllerInput
	ControllerInput_t1382602808 * ___m_ControllerInput_9;
	// Ximmerse.InputSystem.TrackingResult TrackedObject::m_PrevTrackingResult
	int32_t ___m_PrevTrackingResult_10;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(TrackedObject_t16303305, ___target_2)); }
	inline Transform_t3275118058 * get_target_2() const { return ___target_2; }
	inline Transform_t3275118058 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3275118058 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier(&___target_2, value);
	}

	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(TrackedObject_t16303305, ___source_3)); }
	inline int32_t get_source_3() const { return ___source_3; }
	inline int32_t* get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(int32_t value)
	{
		___source_3 = value;
	}

	inline static int32_t get_offset_of_sourceName_4() { return static_cast<int32_t>(offsetof(TrackedObject_t16303305, ___sourceName_4)); }
	inline String_t* get_sourceName_4() const { return ___sourceName_4; }
	inline String_t** get_address_of_sourceName_4() { return &___sourceName_4; }
	inline void set_sourceName_4(String_t* value)
	{
		___sourceName_4 = value;
		Il2CppCodeGenWriteBarrier(&___sourceName_4, value);
	}

	inline static int32_t get_offset_of_trackPosition_5() { return static_cast<int32_t>(offsetof(TrackedObject_t16303305, ___trackPosition_5)); }
	inline bool get_trackPosition_5() const { return ___trackPosition_5; }
	inline bool* get_address_of_trackPosition_5() { return &___trackPosition_5; }
	inline void set_trackPosition_5(bool value)
	{
		___trackPosition_5 = value;
	}

	inline static int32_t get_offset_of_trackRotation_6() { return static_cast<int32_t>(offsetof(TrackedObject_t16303305, ___trackRotation_6)); }
	inline bool get_trackRotation_6() const { return ___trackRotation_6; }
	inline bool* get_address_of_trackRotation_6() { return &___trackRotation_6; }
	inline void set_trackRotation_6(bool value)
	{
		___trackRotation_6 = value;
	}

	inline static int32_t get_offset_of_checkParent_7() { return static_cast<int32_t>(offsetof(TrackedObject_t16303305, ___checkParent_7)); }
	inline bool get_checkParent_7() const { return ___checkParent_7; }
	inline bool* get_address_of_checkParent_7() { return &___checkParent_7; }
	inline void set_checkParent_7(bool value)
	{
		___checkParent_7 = value;
	}

	inline static int32_t get_offset_of_canRecenter_8() { return static_cast<int32_t>(offsetof(TrackedObject_t16303305, ___canRecenter_8)); }
	inline bool get_canRecenter_8() const { return ___canRecenter_8; }
	inline bool* get_address_of_canRecenter_8() { return &___canRecenter_8; }
	inline void set_canRecenter_8(bool value)
	{
		___canRecenter_8 = value;
	}

	inline static int32_t get_offset_of_m_ControllerInput_9() { return static_cast<int32_t>(offsetof(TrackedObject_t16303305, ___m_ControllerInput_9)); }
	inline ControllerInput_t1382602808 * get_m_ControllerInput_9() const { return ___m_ControllerInput_9; }
	inline ControllerInput_t1382602808 ** get_address_of_m_ControllerInput_9() { return &___m_ControllerInput_9; }
	inline void set_m_ControllerInput_9(ControllerInput_t1382602808 * value)
	{
		___m_ControllerInput_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_ControllerInput_9, value);
	}

	inline static int32_t get_offset_of_m_PrevTrackingResult_10() { return static_cast<int32_t>(offsetof(TrackedObject_t16303305, ___m_PrevTrackingResult_10)); }
	inline int32_t get_m_PrevTrackingResult_10() const { return ___m_PrevTrackingResult_10; }
	inline int32_t* get_address_of_m_PrevTrackingResult_10() { return &___m_PrevTrackingResult_10; }
	inline void set_m_PrevTrackingResult_10(int32_t value)
	{
		___m_PrevTrackingResult_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
