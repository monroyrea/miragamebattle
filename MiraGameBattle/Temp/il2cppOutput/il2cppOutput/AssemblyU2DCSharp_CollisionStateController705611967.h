﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_StateMachineBehaviour2151245329.h"
#include "AssemblyU2DCSharp_scriptSensor_PlayerHittingCollid3394089924.h"
#include "AssemblyU2DCSharp_scriptMechanics_EnumAttackCollide537956954.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollisionStateController
struct  CollisionStateController_t705611967  : public StateMachineBehaviour_t2151245329
{
public:
	// System.Single CollisionStateController::_startTime
	float ____startTime_2;
	// System.Single CollisionStateController::_endTime
	float ____endTime_3;
	// scriptSensor/PlayerHittingCollider CollisionStateController::_tempCollider
	int32_t ____tempCollider_4;
	// scriptMechanics/EnumAttackCollider CollisionStateController::_attackType
	int32_t ____attackType_5;

public:
	inline static int32_t get_offset_of__startTime_2() { return static_cast<int32_t>(offsetof(CollisionStateController_t705611967, ____startTime_2)); }
	inline float get__startTime_2() const { return ____startTime_2; }
	inline float* get_address_of__startTime_2() { return &____startTime_2; }
	inline void set__startTime_2(float value)
	{
		____startTime_2 = value;
	}

	inline static int32_t get_offset_of__endTime_3() { return static_cast<int32_t>(offsetof(CollisionStateController_t705611967, ____endTime_3)); }
	inline float get__endTime_3() const { return ____endTime_3; }
	inline float* get_address_of__endTime_3() { return &____endTime_3; }
	inline void set__endTime_3(float value)
	{
		____endTime_3 = value;
	}

	inline static int32_t get_offset_of__tempCollider_4() { return static_cast<int32_t>(offsetof(CollisionStateController_t705611967, ____tempCollider_4)); }
	inline int32_t get__tempCollider_4() const { return ____tempCollider_4; }
	inline int32_t* get_address_of__tempCollider_4() { return &____tempCollider_4; }
	inline void set__tempCollider_4(int32_t value)
	{
		____tempCollider_4 = value;
	}

	inline static int32_t get_offset_of__attackType_5() { return static_cast<int32_t>(offsetof(CollisionStateController_t705611967, ____attackType_5)); }
	inline int32_t get__attackType_5() const { return ____attackType_5; }
	inline int32_t* get_address_of__attackType_5() { return &____attackType_5; }
	inline void set__attackType_5(int32_t value)
	{
		____attackType_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
