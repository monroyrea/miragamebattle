﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CubeGridCreator
struct  CubeGridCreator_t2967326991  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform CubeGridCreator::root
	Transform_t3275118058 * ___root_2;
	// UnityEngine.Transform CubeGridCreator::defaultPrefab
	Transform_t3275118058 * ___defaultPrefab_3;
	// System.Boolean CubeGridCreator::createOnAwake
	bool ___createOnAwake_4;
	// System.Int32 CubeGridCreator::count_x
	int32_t ___count_x_5;
	// System.Int32 CubeGridCreator::count_y
	int32_t ___count_y_6;
	// System.Int32 CubeGridCreator::count_z
	int32_t ___count_z_7;
	// UnityEngine.Vector3 CubeGridCreator::offset
	Vector3_t2243707580  ___offset_8;

public:
	inline static int32_t get_offset_of_root_2() { return static_cast<int32_t>(offsetof(CubeGridCreator_t2967326991, ___root_2)); }
	inline Transform_t3275118058 * get_root_2() const { return ___root_2; }
	inline Transform_t3275118058 ** get_address_of_root_2() { return &___root_2; }
	inline void set_root_2(Transform_t3275118058 * value)
	{
		___root_2 = value;
		Il2CppCodeGenWriteBarrier(&___root_2, value);
	}

	inline static int32_t get_offset_of_defaultPrefab_3() { return static_cast<int32_t>(offsetof(CubeGridCreator_t2967326991, ___defaultPrefab_3)); }
	inline Transform_t3275118058 * get_defaultPrefab_3() const { return ___defaultPrefab_3; }
	inline Transform_t3275118058 ** get_address_of_defaultPrefab_3() { return &___defaultPrefab_3; }
	inline void set_defaultPrefab_3(Transform_t3275118058 * value)
	{
		___defaultPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___defaultPrefab_3, value);
	}

	inline static int32_t get_offset_of_createOnAwake_4() { return static_cast<int32_t>(offsetof(CubeGridCreator_t2967326991, ___createOnAwake_4)); }
	inline bool get_createOnAwake_4() const { return ___createOnAwake_4; }
	inline bool* get_address_of_createOnAwake_4() { return &___createOnAwake_4; }
	inline void set_createOnAwake_4(bool value)
	{
		___createOnAwake_4 = value;
	}

	inline static int32_t get_offset_of_count_x_5() { return static_cast<int32_t>(offsetof(CubeGridCreator_t2967326991, ___count_x_5)); }
	inline int32_t get_count_x_5() const { return ___count_x_5; }
	inline int32_t* get_address_of_count_x_5() { return &___count_x_5; }
	inline void set_count_x_5(int32_t value)
	{
		___count_x_5 = value;
	}

	inline static int32_t get_offset_of_count_y_6() { return static_cast<int32_t>(offsetof(CubeGridCreator_t2967326991, ___count_y_6)); }
	inline int32_t get_count_y_6() const { return ___count_y_6; }
	inline int32_t* get_address_of_count_y_6() { return &___count_y_6; }
	inline void set_count_y_6(int32_t value)
	{
		___count_y_6 = value;
	}

	inline static int32_t get_offset_of_count_z_7() { return static_cast<int32_t>(offsetof(CubeGridCreator_t2967326991, ___count_z_7)); }
	inline int32_t get_count_z_7() const { return ___count_z_7; }
	inline int32_t* get_address_of_count_z_7() { return &___count_z_7; }
	inline void set_count_z_7(int32_t value)
	{
		___count_z_7 = value;
	}

	inline static int32_t get_offset_of_offset_8() { return static_cast<int32_t>(offsetof(CubeGridCreator_t2967326991, ___offset_8)); }
	inline Vector3_t2243707580  get_offset_8() const { return ___offset_8; }
	inline Vector3_t2243707580 * get_address_of_offset_8() { return &___offset_8; }
	inline void set_offset_8(Vector3_t2243707580  value)
	{
		___offset_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
