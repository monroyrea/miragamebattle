﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_scriptSensorHitValues1173922324.h"
#include "AssemblyU2DCSharp_scriptSensor_PlayerHittingCollid3394089924.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// PlayerController
struct PlayerController_t4148409433;
// EnemyAIController
struct EnemyAIController_t937801982;
// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// scriptSensor
struct  scriptSensor_t1009192331  : public scriptSensorHitValues_t1173922324
{
public:
	// UnityEngine.GameObject[] scriptSensor::enemies
	GameObjectU5BU5D_t3057952154* ___enemies_4;
	// UnityEngine.Sprite[] scriptSensor::sprites
	SpriteU5BU5D_t3359083662* ___sprites_5;
	// UnityEngine.UI.Image scriptSensor::enemyImage
	Image_t2042527209 * ___enemyImage_6;
	// UnityEngine.UI.Slider scriptSensor::Enemy
	Slider_t297367283 * ___Enemy_7;
	// UnityEngine.UI.Slider scriptSensor::Player
	Slider_t297367283 * ___Player_8;
	// UnityEngine.UI.Text scriptSensor::gameOver
	Text_t356221433 * ___gameOver_9;
	// UnityEngine.GameObject scriptSensor::battleBoard
	GameObject_t1756533147 * ___battleBoard_10;
	// UnityEngine.GameObject scriptSensor::uiVS
	GameObject_t1756533147 * ___uiVS_11;
	// UnityEngine.GameObject scriptSensor::battleUI
	GameObject_t1756533147 * ___battleUI_12;
	// PlayerController scriptSensor::pController
	PlayerController_t4148409433 * ___pController_13;
	// EnemyAIController scriptSensor::eAIController
	EnemyAIController_t937801982 * ___eAIController_14;
	// System.Boolean scriptSensor::_getHit
	bool ____getHit_15;
	// UnityEngine.Transform scriptSensor::_parent
	Transform_t3275118058 * ____parent_16;
	// scriptSensor/PlayerHittingCollider scriptSensor::_instance
	int32_t ____instance_17;

public:
	inline static int32_t get_offset_of_enemies_4() { return static_cast<int32_t>(offsetof(scriptSensor_t1009192331, ___enemies_4)); }
	inline GameObjectU5BU5D_t3057952154* get_enemies_4() const { return ___enemies_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_enemies_4() { return &___enemies_4; }
	inline void set_enemies_4(GameObjectU5BU5D_t3057952154* value)
	{
		___enemies_4 = value;
		Il2CppCodeGenWriteBarrier(&___enemies_4, value);
	}

	inline static int32_t get_offset_of_sprites_5() { return static_cast<int32_t>(offsetof(scriptSensor_t1009192331, ___sprites_5)); }
	inline SpriteU5BU5D_t3359083662* get_sprites_5() const { return ___sprites_5; }
	inline SpriteU5BU5D_t3359083662** get_address_of_sprites_5() { return &___sprites_5; }
	inline void set_sprites_5(SpriteU5BU5D_t3359083662* value)
	{
		___sprites_5 = value;
		Il2CppCodeGenWriteBarrier(&___sprites_5, value);
	}

	inline static int32_t get_offset_of_enemyImage_6() { return static_cast<int32_t>(offsetof(scriptSensor_t1009192331, ___enemyImage_6)); }
	inline Image_t2042527209 * get_enemyImage_6() const { return ___enemyImage_6; }
	inline Image_t2042527209 ** get_address_of_enemyImage_6() { return &___enemyImage_6; }
	inline void set_enemyImage_6(Image_t2042527209 * value)
	{
		___enemyImage_6 = value;
		Il2CppCodeGenWriteBarrier(&___enemyImage_6, value);
	}

	inline static int32_t get_offset_of_Enemy_7() { return static_cast<int32_t>(offsetof(scriptSensor_t1009192331, ___Enemy_7)); }
	inline Slider_t297367283 * get_Enemy_7() const { return ___Enemy_7; }
	inline Slider_t297367283 ** get_address_of_Enemy_7() { return &___Enemy_7; }
	inline void set_Enemy_7(Slider_t297367283 * value)
	{
		___Enemy_7 = value;
		Il2CppCodeGenWriteBarrier(&___Enemy_7, value);
	}

	inline static int32_t get_offset_of_Player_8() { return static_cast<int32_t>(offsetof(scriptSensor_t1009192331, ___Player_8)); }
	inline Slider_t297367283 * get_Player_8() const { return ___Player_8; }
	inline Slider_t297367283 ** get_address_of_Player_8() { return &___Player_8; }
	inline void set_Player_8(Slider_t297367283 * value)
	{
		___Player_8 = value;
		Il2CppCodeGenWriteBarrier(&___Player_8, value);
	}

	inline static int32_t get_offset_of_gameOver_9() { return static_cast<int32_t>(offsetof(scriptSensor_t1009192331, ___gameOver_9)); }
	inline Text_t356221433 * get_gameOver_9() const { return ___gameOver_9; }
	inline Text_t356221433 ** get_address_of_gameOver_9() { return &___gameOver_9; }
	inline void set_gameOver_9(Text_t356221433 * value)
	{
		___gameOver_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameOver_9, value);
	}

	inline static int32_t get_offset_of_battleBoard_10() { return static_cast<int32_t>(offsetof(scriptSensor_t1009192331, ___battleBoard_10)); }
	inline GameObject_t1756533147 * get_battleBoard_10() const { return ___battleBoard_10; }
	inline GameObject_t1756533147 ** get_address_of_battleBoard_10() { return &___battleBoard_10; }
	inline void set_battleBoard_10(GameObject_t1756533147 * value)
	{
		___battleBoard_10 = value;
		Il2CppCodeGenWriteBarrier(&___battleBoard_10, value);
	}

	inline static int32_t get_offset_of_uiVS_11() { return static_cast<int32_t>(offsetof(scriptSensor_t1009192331, ___uiVS_11)); }
	inline GameObject_t1756533147 * get_uiVS_11() const { return ___uiVS_11; }
	inline GameObject_t1756533147 ** get_address_of_uiVS_11() { return &___uiVS_11; }
	inline void set_uiVS_11(GameObject_t1756533147 * value)
	{
		___uiVS_11 = value;
		Il2CppCodeGenWriteBarrier(&___uiVS_11, value);
	}

	inline static int32_t get_offset_of_battleUI_12() { return static_cast<int32_t>(offsetof(scriptSensor_t1009192331, ___battleUI_12)); }
	inline GameObject_t1756533147 * get_battleUI_12() const { return ___battleUI_12; }
	inline GameObject_t1756533147 ** get_address_of_battleUI_12() { return &___battleUI_12; }
	inline void set_battleUI_12(GameObject_t1756533147 * value)
	{
		___battleUI_12 = value;
		Il2CppCodeGenWriteBarrier(&___battleUI_12, value);
	}

	inline static int32_t get_offset_of_pController_13() { return static_cast<int32_t>(offsetof(scriptSensor_t1009192331, ___pController_13)); }
	inline PlayerController_t4148409433 * get_pController_13() const { return ___pController_13; }
	inline PlayerController_t4148409433 ** get_address_of_pController_13() { return &___pController_13; }
	inline void set_pController_13(PlayerController_t4148409433 * value)
	{
		___pController_13 = value;
		Il2CppCodeGenWriteBarrier(&___pController_13, value);
	}

	inline static int32_t get_offset_of_eAIController_14() { return static_cast<int32_t>(offsetof(scriptSensor_t1009192331, ___eAIController_14)); }
	inline EnemyAIController_t937801982 * get_eAIController_14() const { return ___eAIController_14; }
	inline EnemyAIController_t937801982 ** get_address_of_eAIController_14() { return &___eAIController_14; }
	inline void set_eAIController_14(EnemyAIController_t937801982 * value)
	{
		___eAIController_14 = value;
		Il2CppCodeGenWriteBarrier(&___eAIController_14, value);
	}

	inline static int32_t get_offset_of__getHit_15() { return static_cast<int32_t>(offsetof(scriptSensor_t1009192331, ____getHit_15)); }
	inline bool get__getHit_15() const { return ____getHit_15; }
	inline bool* get_address_of__getHit_15() { return &____getHit_15; }
	inline void set__getHit_15(bool value)
	{
		____getHit_15 = value;
	}

	inline static int32_t get_offset_of__parent_16() { return static_cast<int32_t>(offsetof(scriptSensor_t1009192331, ____parent_16)); }
	inline Transform_t3275118058 * get__parent_16() const { return ____parent_16; }
	inline Transform_t3275118058 ** get_address_of__parent_16() { return &____parent_16; }
	inline void set__parent_16(Transform_t3275118058 * value)
	{
		____parent_16 = value;
		Il2CppCodeGenWriteBarrier(&____parent_16, value);
	}

	inline static int32_t get_offset_of__instance_17() { return static_cast<int32_t>(offsetof(scriptSensor_t1009192331, ____instance_17)); }
	inline int32_t get__instance_17() const { return ____instance_17; }
	inline int32_t* get_address_of__instance_17() { return &____instance_17; }
	inline void set__instance_17(int32_t value)
	{
		____instance_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
