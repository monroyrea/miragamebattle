﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Collider
struct Collider_t3497673348;
// UIObject
struct UIObject_t4279159643;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackingInteractable
struct  TrackingInteractable_t149194413  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TrackingInteractable::trackingLost
	bool ___trackingLost_2;
	// UnityEngine.Material TrackingInteractable::baseMaterial
	Material_t193706927 * ___baseMaterial_3;
	// UnityEngine.Color TrackingInteractable::defaultColor
	Color_t2020392075  ___defaultColor_4;
	// UnityEngine.Collider TrackingInteractable::thisCollider
	Collider_t3497673348 * ___thisCollider_5;
	// System.Single TrackingInteractable::lerpCycleTime
	float ___lerpCycleTime_6;
	// UnityEngine.Color TrackingInteractable::beaconLerpColor
	Color_t2020392075  ___beaconLerpColor_7;
	// UIObject TrackingInteractable::thisUIObj
	UIObject_t4279159643 * ___thisUIObj_8;

public:
	inline static int32_t get_offset_of_trackingLost_2() { return static_cast<int32_t>(offsetof(TrackingInteractable_t149194413, ___trackingLost_2)); }
	inline bool get_trackingLost_2() const { return ___trackingLost_2; }
	inline bool* get_address_of_trackingLost_2() { return &___trackingLost_2; }
	inline void set_trackingLost_2(bool value)
	{
		___trackingLost_2 = value;
	}

	inline static int32_t get_offset_of_baseMaterial_3() { return static_cast<int32_t>(offsetof(TrackingInteractable_t149194413, ___baseMaterial_3)); }
	inline Material_t193706927 * get_baseMaterial_3() const { return ___baseMaterial_3; }
	inline Material_t193706927 ** get_address_of_baseMaterial_3() { return &___baseMaterial_3; }
	inline void set_baseMaterial_3(Material_t193706927 * value)
	{
		___baseMaterial_3 = value;
		Il2CppCodeGenWriteBarrier(&___baseMaterial_3, value);
	}

	inline static int32_t get_offset_of_defaultColor_4() { return static_cast<int32_t>(offsetof(TrackingInteractable_t149194413, ___defaultColor_4)); }
	inline Color_t2020392075  get_defaultColor_4() const { return ___defaultColor_4; }
	inline Color_t2020392075 * get_address_of_defaultColor_4() { return &___defaultColor_4; }
	inline void set_defaultColor_4(Color_t2020392075  value)
	{
		___defaultColor_4 = value;
	}

	inline static int32_t get_offset_of_thisCollider_5() { return static_cast<int32_t>(offsetof(TrackingInteractable_t149194413, ___thisCollider_5)); }
	inline Collider_t3497673348 * get_thisCollider_5() const { return ___thisCollider_5; }
	inline Collider_t3497673348 ** get_address_of_thisCollider_5() { return &___thisCollider_5; }
	inline void set_thisCollider_5(Collider_t3497673348 * value)
	{
		___thisCollider_5 = value;
		Il2CppCodeGenWriteBarrier(&___thisCollider_5, value);
	}

	inline static int32_t get_offset_of_lerpCycleTime_6() { return static_cast<int32_t>(offsetof(TrackingInteractable_t149194413, ___lerpCycleTime_6)); }
	inline float get_lerpCycleTime_6() const { return ___lerpCycleTime_6; }
	inline float* get_address_of_lerpCycleTime_6() { return &___lerpCycleTime_6; }
	inline void set_lerpCycleTime_6(float value)
	{
		___lerpCycleTime_6 = value;
	}

	inline static int32_t get_offset_of_beaconLerpColor_7() { return static_cast<int32_t>(offsetof(TrackingInteractable_t149194413, ___beaconLerpColor_7)); }
	inline Color_t2020392075  get_beaconLerpColor_7() const { return ___beaconLerpColor_7; }
	inline Color_t2020392075 * get_address_of_beaconLerpColor_7() { return &___beaconLerpColor_7; }
	inline void set_beaconLerpColor_7(Color_t2020392075  value)
	{
		___beaconLerpColor_7 = value;
	}

	inline static int32_t get_offset_of_thisUIObj_8() { return static_cast<int32_t>(offsetof(TrackingInteractable_t149194413, ___thisUIObj_8)); }
	inline UIObject_t4279159643 * get_thisUIObj_8() const { return ___thisUIObj_8; }
	inline UIObject_t4279159643 ** get_address_of_thisUIObj_8() { return &___thisUIObj_8; }
	inline void set_thisUIObj_8(UIObject_t4279159643 * value)
	{
		___thisUIObj_8 = value;
		Il2CppCodeGenWriteBarrier(&___thisUIObj_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
