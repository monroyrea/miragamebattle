﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ArmModel_Ga1258145080.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ArmModelNod3478863847.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerTy123719274.h"

// Ximmerse.InputSystem.ArmModel/OnArmModelUpdateEvent
struct OnArmModelUpdateEvent_t3934451428;
// System.String
struct String_t;
// Ximmerse.InputSystem.TrackedControllerInput
struct TrackedControllerInput_t3249248438;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.InputSystem.ArmModel
struct  ArmModel_t1335532495  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 Ximmerse.InputSystem.ArmModel::elbowOffset
	Vector3_t2243707580  ___elbowOffset_17;
	// UnityEngine.Vector3 Ximmerse.InputSystem.ArmModel::torsoDirection
	Vector3_t2243707580  ___torsoDirection_18;
	// UnityEngine.Vector3 Ximmerse.InputSystem.ArmModel::filteredVelocity
	Vector3_t2243707580  ___filteredVelocity_19;
	// UnityEngine.Vector3 Ximmerse.InputSystem.ArmModel::filteredAccel
	Vector3_t2243707580  ___filteredAccel_20;
	// UnityEngine.Vector3 Ximmerse.InputSystem.ArmModel::zeroAccel
	Vector3_t2243707580  ___zeroAccel_21;
	// System.Boolean Ximmerse.InputSystem.ArmModel::firstUpdate
	bool ___firstUpdate_22;
	// UnityEngine.Vector3 Ximmerse.InputSystem.ArmModel::handedMultiplier
	Vector3_t2243707580  ___handedMultiplier_23;
	// System.Single Ximmerse.InputSystem.ArmModel::addedElbowHeight
	float ___addedElbowHeight_24;
	// System.Single Ximmerse.InputSystem.ArmModel::addedElbowDepth
	float ___addedElbowDepth_25;
	// System.Single Ximmerse.InputSystem.ArmModel::pointerTiltAngle
	float ___pointerTiltAngle_26;
	// System.Single Ximmerse.InputSystem.ArmModel::fadeDistanceFromFace
	float ___fadeDistanceFromFace_27;
	// System.Single Ximmerse.InputSystem.ArmModel::tooltipMinDistanceFromFace
	float ___tooltipMinDistanceFromFace_28;
	// Ximmerse.InputSystem.ArmModel/GazeBehavior Ximmerse.InputSystem.ArmModel::followGaze
	int32_t ___followGaze_29;
	// System.Boolean Ximmerse.InputSystem.ArmModel::useAccelerometer
	bool ___useAccelerometer_30;
	// UnityEngine.Vector3 Ximmerse.InputSystem.ArmModel::<pointerPosition>k__BackingField
	Vector3_t2243707580  ___U3CpointerPositionU3Ek__BackingField_31;
	// UnityEngine.Quaternion Ximmerse.InputSystem.ArmModel::<pointerRotation>k__BackingField
	Quaternion_t4030073918  ___U3CpointerRotationU3Ek__BackingField_32;
	// UnityEngine.Vector3 Ximmerse.InputSystem.ArmModel::<wristPosition>k__BackingField
	Vector3_t2243707580  ___U3CwristPositionU3Ek__BackingField_33;
	// UnityEngine.Quaternion Ximmerse.InputSystem.ArmModel::<wristRotation>k__BackingField
	Quaternion_t4030073918  ___U3CwristRotationU3Ek__BackingField_34;
	// UnityEngine.Vector3 Ximmerse.InputSystem.ArmModel::<elbowPosition>k__BackingField
	Vector3_t2243707580  ___U3CelbowPositionU3Ek__BackingField_35;
	// UnityEngine.Quaternion Ximmerse.InputSystem.ArmModel::<elbowRotation>k__BackingField
	Quaternion_t4030073918  ___U3CelbowRotationU3Ek__BackingField_36;
	// UnityEngine.Vector3 Ximmerse.InputSystem.ArmModel::<shoulderPosition>k__BackingField
	Vector3_t2243707580  ___U3CshoulderPositionU3Ek__BackingField_37;
	// UnityEngine.Quaternion Ximmerse.InputSystem.ArmModel::<shoulderRotation>k__BackingField
	Quaternion_t4030073918  ___U3CshoulderRotationU3Ek__BackingField_38;
	// System.Single Ximmerse.InputSystem.ArmModel::<alphaValue>k__BackingField
	float ___U3CalphaValueU3Ek__BackingField_39;
	// System.Single Ximmerse.InputSystem.ArmModel::<tooltipAlphaValue>k__BackingField
	float ___U3CtooltipAlphaValueU3Ek__BackingField_40;
	// Ximmerse.InputSystem.ArmModel/OnArmModelUpdateEvent Ximmerse.InputSystem.ArmModel::OnArmModelUpdate
	OnArmModelUpdateEvent_t3934451428 * ___OnArmModelUpdate_41;
	// System.String Ximmerse.InputSystem.ArmModel::name
	String_t* ___name_42;
	// Ximmerse.InputSystem.TrackedControllerInput Ximmerse.InputSystem.ArmModel::Controller
	TrackedControllerInput_t3249248438 * ___Controller_43;
	// Ximmerse.InputSystem.ArmModelNode Ximmerse.InputSystem.ArmModel::defaultNode
	int32_t ___defaultNode_44;
	// Ximmerse.InputSystem.ControllerType Ximmerse.InputSystem.ArmModel::handedness
	int32_t ___handedness_45;

public:
	inline static int32_t get_offset_of_elbowOffset_17() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___elbowOffset_17)); }
	inline Vector3_t2243707580  get_elbowOffset_17() const { return ___elbowOffset_17; }
	inline Vector3_t2243707580 * get_address_of_elbowOffset_17() { return &___elbowOffset_17; }
	inline void set_elbowOffset_17(Vector3_t2243707580  value)
	{
		___elbowOffset_17 = value;
	}

	inline static int32_t get_offset_of_torsoDirection_18() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___torsoDirection_18)); }
	inline Vector3_t2243707580  get_torsoDirection_18() const { return ___torsoDirection_18; }
	inline Vector3_t2243707580 * get_address_of_torsoDirection_18() { return &___torsoDirection_18; }
	inline void set_torsoDirection_18(Vector3_t2243707580  value)
	{
		___torsoDirection_18 = value;
	}

	inline static int32_t get_offset_of_filteredVelocity_19() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___filteredVelocity_19)); }
	inline Vector3_t2243707580  get_filteredVelocity_19() const { return ___filteredVelocity_19; }
	inline Vector3_t2243707580 * get_address_of_filteredVelocity_19() { return &___filteredVelocity_19; }
	inline void set_filteredVelocity_19(Vector3_t2243707580  value)
	{
		___filteredVelocity_19 = value;
	}

	inline static int32_t get_offset_of_filteredAccel_20() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___filteredAccel_20)); }
	inline Vector3_t2243707580  get_filteredAccel_20() const { return ___filteredAccel_20; }
	inline Vector3_t2243707580 * get_address_of_filteredAccel_20() { return &___filteredAccel_20; }
	inline void set_filteredAccel_20(Vector3_t2243707580  value)
	{
		___filteredAccel_20 = value;
	}

	inline static int32_t get_offset_of_zeroAccel_21() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___zeroAccel_21)); }
	inline Vector3_t2243707580  get_zeroAccel_21() const { return ___zeroAccel_21; }
	inline Vector3_t2243707580 * get_address_of_zeroAccel_21() { return &___zeroAccel_21; }
	inline void set_zeroAccel_21(Vector3_t2243707580  value)
	{
		___zeroAccel_21 = value;
	}

	inline static int32_t get_offset_of_firstUpdate_22() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___firstUpdate_22)); }
	inline bool get_firstUpdate_22() const { return ___firstUpdate_22; }
	inline bool* get_address_of_firstUpdate_22() { return &___firstUpdate_22; }
	inline void set_firstUpdate_22(bool value)
	{
		___firstUpdate_22 = value;
	}

	inline static int32_t get_offset_of_handedMultiplier_23() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___handedMultiplier_23)); }
	inline Vector3_t2243707580  get_handedMultiplier_23() const { return ___handedMultiplier_23; }
	inline Vector3_t2243707580 * get_address_of_handedMultiplier_23() { return &___handedMultiplier_23; }
	inline void set_handedMultiplier_23(Vector3_t2243707580  value)
	{
		___handedMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_addedElbowHeight_24() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___addedElbowHeight_24)); }
	inline float get_addedElbowHeight_24() const { return ___addedElbowHeight_24; }
	inline float* get_address_of_addedElbowHeight_24() { return &___addedElbowHeight_24; }
	inline void set_addedElbowHeight_24(float value)
	{
		___addedElbowHeight_24 = value;
	}

	inline static int32_t get_offset_of_addedElbowDepth_25() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___addedElbowDepth_25)); }
	inline float get_addedElbowDepth_25() const { return ___addedElbowDepth_25; }
	inline float* get_address_of_addedElbowDepth_25() { return &___addedElbowDepth_25; }
	inline void set_addedElbowDepth_25(float value)
	{
		___addedElbowDepth_25 = value;
	}

	inline static int32_t get_offset_of_pointerTiltAngle_26() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___pointerTiltAngle_26)); }
	inline float get_pointerTiltAngle_26() const { return ___pointerTiltAngle_26; }
	inline float* get_address_of_pointerTiltAngle_26() { return &___pointerTiltAngle_26; }
	inline void set_pointerTiltAngle_26(float value)
	{
		___pointerTiltAngle_26 = value;
	}

	inline static int32_t get_offset_of_fadeDistanceFromFace_27() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___fadeDistanceFromFace_27)); }
	inline float get_fadeDistanceFromFace_27() const { return ___fadeDistanceFromFace_27; }
	inline float* get_address_of_fadeDistanceFromFace_27() { return &___fadeDistanceFromFace_27; }
	inline void set_fadeDistanceFromFace_27(float value)
	{
		___fadeDistanceFromFace_27 = value;
	}

	inline static int32_t get_offset_of_tooltipMinDistanceFromFace_28() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___tooltipMinDistanceFromFace_28)); }
	inline float get_tooltipMinDistanceFromFace_28() const { return ___tooltipMinDistanceFromFace_28; }
	inline float* get_address_of_tooltipMinDistanceFromFace_28() { return &___tooltipMinDistanceFromFace_28; }
	inline void set_tooltipMinDistanceFromFace_28(float value)
	{
		___tooltipMinDistanceFromFace_28 = value;
	}

	inline static int32_t get_offset_of_followGaze_29() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___followGaze_29)); }
	inline int32_t get_followGaze_29() const { return ___followGaze_29; }
	inline int32_t* get_address_of_followGaze_29() { return &___followGaze_29; }
	inline void set_followGaze_29(int32_t value)
	{
		___followGaze_29 = value;
	}

	inline static int32_t get_offset_of_useAccelerometer_30() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___useAccelerometer_30)); }
	inline bool get_useAccelerometer_30() const { return ___useAccelerometer_30; }
	inline bool* get_address_of_useAccelerometer_30() { return &___useAccelerometer_30; }
	inline void set_useAccelerometer_30(bool value)
	{
		___useAccelerometer_30 = value;
	}

	inline static int32_t get_offset_of_U3CpointerPositionU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___U3CpointerPositionU3Ek__BackingField_31)); }
	inline Vector3_t2243707580  get_U3CpointerPositionU3Ek__BackingField_31() const { return ___U3CpointerPositionU3Ek__BackingField_31; }
	inline Vector3_t2243707580 * get_address_of_U3CpointerPositionU3Ek__BackingField_31() { return &___U3CpointerPositionU3Ek__BackingField_31; }
	inline void set_U3CpointerPositionU3Ek__BackingField_31(Vector3_t2243707580  value)
	{
		___U3CpointerPositionU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CpointerRotationU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___U3CpointerRotationU3Ek__BackingField_32)); }
	inline Quaternion_t4030073918  get_U3CpointerRotationU3Ek__BackingField_32() const { return ___U3CpointerRotationU3Ek__BackingField_32; }
	inline Quaternion_t4030073918 * get_address_of_U3CpointerRotationU3Ek__BackingField_32() { return &___U3CpointerRotationU3Ek__BackingField_32; }
	inline void set_U3CpointerRotationU3Ek__BackingField_32(Quaternion_t4030073918  value)
	{
		___U3CpointerRotationU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CwristPositionU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___U3CwristPositionU3Ek__BackingField_33)); }
	inline Vector3_t2243707580  get_U3CwristPositionU3Ek__BackingField_33() const { return ___U3CwristPositionU3Ek__BackingField_33; }
	inline Vector3_t2243707580 * get_address_of_U3CwristPositionU3Ek__BackingField_33() { return &___U3CwristPositionU3Ek__BackingField_33; }
	inline void set_U3CwristPositionU3Ek__BackingField_33(Vector3_t2243707580  value)
	{
		___U3CwristPositionU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CwristRotationU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___U3CwristRotationU3Ek__BackingField_34)); }
	inline Quaternion_t4030073918  get_U3CwristRotationU3Ek__BackingField_34() const { return ___U3CwristRotationU3Ek__BackingField_34; }
	inline Quaternion_t4030073918 * get_address_of_U3CwristRotationU3Ek__BackingField_34() { return &___U3CwristRotationU3Ek__BackingField_34; }
	inline void set_U3CwristRotationU3Ek__BackingField_34(Quaternion_t4030073918  value)
	{
		___U3CwristRotationU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CelbowPositionU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___U3CelbowPositionU3Ek__BackingField_35)); }
	inline Vector3_t2243707580  get_U3CelbowPositionU3Ek__BackingField_35() const { return ___U3CelbowPositionU3Ek__BackingField_35; }
	inline Vector3_t2243707580 * get_address_of_U3CelbowPositionU3Ek__BackingField_35() { return &___U3CelbowPositionU3Ek__BackingField_35; }
	inline void set_U3CelbowPositionU3Ek__BackingField_35(Vector3_t2243707580  value)
	{
		___U3CelbowPositionU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_U3CelbowRotationU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___U3CelbowRotationU3Ek__BackingField_36)); }
	inline Quaternion_t4030073918  get_U3CelbowRotationU3Ek__BackingField_36() const { return ___U3CelbowRotationU3Ek__BackingField_36; }
	inline Quaternion_t4030073918 * get_address_of_U3CelbowRotationU3Ek__BackingField_36() { return &___U3CelbowRotationU3Ek__BackingField_36; }
	inline void set_U3CelbowRotationU3Ek__BackingField_36(Quaternion_t4030073918  value)
	{
		___U3CelbowRotationU3Ek__BackingField_36 = value;
	}

	inline static int32_t get_offset_of_U3CshoulderPositionU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___U3CshoulderPositionU3Ek__BackingField_37)); }
	inline Vector3_t2243707580  get_U3CshoulderPositionU3Ek__BackingField_37() const { return ___U3CshoulderPositionU3Ek__BackingField_37; }
	inline Vector3_t2243707580 * get_address_of_U3CshoulderPositionU3Ek__BackingField_37() { return &___U3CshoulderPositionU3Ek__BackingField_37; }
	inline void set_U3CshoulderPositionU3Ek__BackingField_37(Vector3_t2243707580  value)
	{
		___U3CshoulderPositionU3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of_U3CshoulderRotationU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___U3CshoulderRotationU3Ek__BackingField_38)); }
	inline Quaternion_t4030073918  get_U3CshoulderRotationU3Ek__BackingField_38() const { return ___U3CshoulderRotationU3Ek__BackingField_38; }
	inline Quaternion_t4030073918 * get_address_of_U3CshoulderRotationU3Ek__BackingField_38() { return &___U3CshoulderRotationU3Ek__BackingField_38; }
	inline void set_U3CshoulderRotationU3Ek__BackingField_38(Quaternion_t4030073918  value)
	{
		___U3CshoulderRotationU3Ek__BackingField_38 = value;
	}

	inline static int32_t get_offset_of_U3CalphaValueU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___U3CalphaValueU3Ek__BackingField_39)); }
	inline float get_U3CalphaValueU3Ek__BackingField_39() const { return ___U3CalphaValueU3Ek__BackingField_39; }
	inline float* get_address_of_U3CalphaValueU3Ek__BackingField_39() { return &___U3CalphaValueU3Ek__BackingField_39; }
	inline void set_U3CalphaValueU3Ek__BackingField_39(float value)
	{
		___U3CalphaValueU3Ek__BackingField_39 = value;
	}

	inline static int32_t get_offset_of_U3CtooltipAlphaValueU3Ek__BackingField_40() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___U3CtooltipAlphaValueU3Ek__BackingField_40)); }
	inline float get_U3CtooltipAlphaValueU3Ek__BackingField_40() const { return ___U3CtooltipAlphaValueU3Ek__BackingField_40; }
	inline float* get_address_of_U3CtooltipAlphaValueU3Ek__BackingField_40() { return &___U3CtooltipAlphaValueU3Ek__BackingField_40; }
	inline void set_U3CtooltipAlphaValueU3Ek__BackingField_40(float value)
	{
		___U3CtooltipAlphaValueU3Ek__BackingField_40 = value;
	}

	inline static int32_t get_offset_of_OnArmModelUpdate_41() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___OnArmModelUpdate_41)); }
	inline OnArmModelUpdateEvent_t3934451428 * get_OnArmModelUpdate_41() const { return ___OnArmModelUpdate_41; }
	inline OnArmModelUpdateEvent_t3934451428 ** get_address_of_OnArmModelUpdate_41() { return &___OnArmModelUpdate_41; }
	inline void set_OnArmModelUpdate_41(OnArmModelUpdateEvent_t3934451428 * value)
	{
		___OnArmModelUpdate_41 = value;
		Il2CppCodeGenWriteBarrier(&___OnArmModelUpdate_41, value);
	}

	inline static int32_t get_offset_of_name_42() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___name_42)); }
	inline String_t* get_name_42() const { return ___name_42; }
	inline String_t** get_address_of_name_42() { return &___name_42; }
	inline void set_name_42(String_t* value)
	{
		___name_42 = value;
		Il2CppCodeGenWriteBarrier(&___name_42, value);
	}

	inline static int32_t get_offset_of_Controller_43() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___Controller_43)); }
	inline TrackedControllerInput_t3249248438 * get_Controller_43() const { return ___Controller_43; }
	inline TrackedControllerInput_t3249248438 ** get_address_of_Controller_43() { return &___Controller_43; }
	inline void set_Controller_43(TrackedControllerInput_t3249248438 * value)
	{
		___Controller_43 = value;
		Il2CppCodeGenWriteBarrier(&___Controller_43, value);
	}

	inline static int32_t get_offset_of_defaultNode_44() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___defaultNode_44)); }
	inline int32_t get_defaultNode_44() const { return ___defaultNode_44; }
	inline int32_t* get_address_of_defaultNode_44() { return &___defaultNode_44; }
	inline void set_defaultNode_44(int32_t value)
	{
		___defaultNode_44 = value;
	}

	inline static int32_t get_offset_of_handedness_45() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495, ___handedness_45)); }
	inline int32_t get_handedness_45() const { return ___handedness_45; }
	inline int32_t* get_address_of_handedness_45() { return &___handedness_45; }
	inline void set_handedness_45(int32_t value)
	{
		___handedness_45 = value;
	}
};

struct ArmModel_t1335532495_StaticFields
{
public:
	// UnityEngine.Vector3 Ximmerse.InputSystem.ArmModel::DEFAULT_SHOULDER_RIGHT
	Vector3_t2243707580  ___DEFAULT_SHOULDER_RIGHT_0;
	// UnityEngine.Vector3 Ximmerse.InputSystem.ArmModel::ELBOW_MIN_RANGE
	Vector3_t2243707580  ___ELBOW_MIN_RANGE_1;
	// UnityEngine.Vector3 Ximmerse.InputSystem.ArmModel::ELBOW_MAX_RANGE
	Vector3_t2243707580  ___ELBOW_MAX_RANGE_2;
	// UnityEngine.Vector3 Ximmerse.InputSystem.ArmModel::POINTER_OFFSET
	Vector3_t2243707580  ___POINTER_OFFSET_3;
	// UnityEngine.Vector3 Ximmerse.InputSystem.ArmModel::ELBOW_POSITION
	Vector3_t2243707580  ___ELBOW_POSITION_4;
	// UnityEngine.Vector3 Ximmerse.InputSystem.ArmModel::WRIST_POSITION
	Vector3_t2243707580  ___WRIST_POSITION_5;
	// UnityEngine.Vector3 Ximmerse.InputSystem.ArmModel::ARM_EXTENSION_OFFSET
	Vector3_t2243707580  ___ARM_EXTENSION_OFFSET_6;

public:
	inline static int32_t get_offset_of_DEFAULT_SHOULDER_RIGHT_0() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495_StaticFields, ___DEFAULT_SHOULDER_RIGHT_0)); }
	inline Vector3_t2243707580  get_DEFAULT_SHOULDER_RIGHT_0() const { return ___DEFAULT_SHOULDER_RIGHT_0; }
	inline Vector3_t2243707580 * get_address_of_DEFAULT_SHOULDER_RIGHT_0() { return &___DEFAULT_SHOULDER_RIGHT_0; }
	inline void set_DEFAULT_SHOULDER_RIGHT_0(Vector3_t2243707580  value)
	{
		___DEFAULT_SHOULDER_RIGHT_0 = value;
	}

	inline static int32_t get_offset_of_ELBOW_MIN_RANGE_1() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495_StaticFields, ___ELBOW_MIN_RANGE_1)); }
	inline Vector3_t2243707580  get_ELBOW_MIN_RANGE_1() const { return ___ELBOW_MIN_RANGE_1; }
	inline Vector3_t2243707580 * get_address_of_ELBOW_MIN_RANGE_1() { return &___ELBOW_MIN_RANGE_1; }
	inline void set_ELBOW_MIN_RANGE_1(Vector3_t2243707580  value)
	{
		___ELBOW_MIN_RANGE_1 = value;
	}

	inline static int32_t get_offset_of_ELBOW_MAX_RANGE_2() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495_StaticFields, ___ELBOW_MAX_RANGE_2)); }
	inline Vector3_t2243707580  get_ELBOW_MAX_RANGE_2() const { return ___ELBOW_MAX_RANGE_2; }
	inline Vector3_t2243707580 * get_address_of_ELBOW_MAX_RANGE_2() { return &___ELBOW_MAX_RANGE_2; }
	inline void set_ELBOW_MAX_RANGE_2(Vector3_t2243707580  value)
	{
		___ELBOW_MAX_RANGE_2 = value;
	}

	inline static int32_t get_offset_of_POINTER_OFFSET_3() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495_StaticFields, ___POINTER_OFFSET_3)); }
	inline Vector3_t2243707580  get_POINTER_OFFSET_3() const { return ___POINTER_OFFSET_3; }
	inline Vector3_t2243707580 * get_address_of_POINTER_OFFSET_3() { return &___POINTER_OFFSET_3; }
	inline void set_POINTER_OFFSET_3(Vector3_t2243707580  value)
	{
		___POINTER_OFFSET_3 = value;
	}

	inline static int32_t get_offset_of_ELBOW_POSITION_4() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495_StaticFields, ___ELBOW_POSITION_4)); }
	inline Vector3_t2243707580  get_ELBOW_POSITION_4() const { return ___ELBOW_POSITION_4; }
	inline Vector3_t2243707580 * get_address_of_ELBOW_POSITION_4() { return &___ELBOW_POSITION_4; }
	inline void set_ELBOW_POSITION_4(Vector3_t2243707580  value)
	{
		___ELBOW_POSITION_4 = value;
	}

	inline static int32_t get_offset_of_WRIST_POSITION_5() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495_StaticFields, ___WRIST_POSITION_5)); }
	inline Vector3_t2243707580  get_WRIST_POSITION_5() const { return ___WRIST_POSITION_5; }
	inline Vector3_t2243707580 * get_address_of_WRIST_POSITION_5() { return &___WRIST_POSITION_5; }
	inline void set_WRIST_POSITION_5(Vector3_t2243707580  value)
	{
		___WRIST_POSITION_5 = value;
	}

	inline static int32_t get_offset_of_ARM_EXTENSION_OFFSET_6() { return static_cast<int32_t>(offsetof(ArmModel_t1335532495_StaticFields, ___ARM_EXTENSION_OFFSET_6)); }
	inline Vector3_t2243707580  get_ARM_EXTENSION_OFFSET_6() const { return ___ARM_EXTENSION_OFFSET_6; }
	inline Vector3_t2243707580 * get_address_of_ARM_EXTENSION_OFFSET_6() { return &___ARM_EXTENSION_OFFSET_6; }
	inline void set_ARM_EXTENSION_OFFSET_6(Vector3_t2243707580  value)
	{
		___ARM_EXTENSION_OFFSET_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
