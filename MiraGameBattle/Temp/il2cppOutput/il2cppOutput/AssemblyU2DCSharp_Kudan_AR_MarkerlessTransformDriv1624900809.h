﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Kudan_AR_TransformDriverBase2557076579.h"

// Kudan.AR.TrackingMethodMarkerless
struct TrackingMethodMarkerless_t432022491;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kudan.AR.MarkerlessTransformDriver
struct  MarkerlessTransformDriver_t1624900809  : public TransformDriverBase_t2557076579
{
public:
	// Kudan.AR.TrackingMethodMarkerless Kudan.AR.MarkerlessTransformDriver::_tracker
	TrackingMethodMarkerless_t432022491 * ____tracker_3;

public:
	inline static int32_t get_offset_of__tracker_3() { return static_cast<int32_t>(offsetof(MarkerlessTransformDriver_t1624900809, ____tracker_3)); }
	inline TrackingMethodMarkerless_t432022491 * get__tracker_3() const { return ____tracker_3; }
	inline TrackingMethodMarkerless_t432022491 ** get_address_of__tracker_3() { return &____tracker_3; }
	inline void set__tracker_3(TrackingMethodMarkerless_t432022491 * value)
	{
		____tracker_3 = value;
		Il2CppCodeGenWriteBarrier(&____tracker_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
