﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_MiraPostRender_Eye273978457.h"

// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraPostRender
struct  MiraPostRender_t3202756259  : public MonoBehaviour_t1158329972
{
public:
	// MiraPostRender/Eye MiraPostRender::eye
	int32_t ___eye_2;
	// System.Int32 MiraPostRender::xSize
	int32_t ___xSize_3;
	// System.Int32 MiraPostRender::ySize
	int32_t ___ySize_4;
	// System.Single MiraPostRender::zOffset
	float ___zOffset_5;
	// System.Single MiraPostRender::xScalar
	float ___xScalar_6;
	// System.Single MiraPostRender::yScalar
	float ___yScalar_7;
	// System.Single MiraPostRender::screenSizeX
	float ___screenSizeX_8;
	// System.Single MiraPostRender::screenSizeY
	float ___screenSizeY_9;
	// UnityEngine.Mesh MiraPostRender::mesh
	Mesh_t1356156583 * ___mesh_10;
	// UnityEngine.Material MiraPostRender::renderTextureMaterial
	Material_t193706927 * ___renderTextureMaterial_11;
	// System.Single MiraPostRender::IPD
	float ___IPD_12;

public:
	inline static int32_t get_offset_of_eye_2() { return static_cast<int32_t>(offsetof(MiraPostRender_t3202756259, ___eye_2)); }
	inline int32_t get_eye_2() const { return ___eye_2; }
	inline int32_t* get_address_of_eye_2() { return &___eye_2; }
	inline void set_eye_2(int32_t value)
	{
		___eye_2 = value;
	}

	inline static int32_t get_offset_of_xSize_3() { return static_cast<int32_t>(offsetof(MiraPostRender_t3202756259, ___xSize_3)); }
	inline int32_t get_xSize_3() const { return ___xSize_3; }
	inline int32_t* get_address_of_xSize_3() { return &___xSize_3; }
	inline void set_xSize_3(int32_t value)
	{
		___xSize_3 = value;
	}

	inline static int32_t get_offset_of_ySize_4() { return static_cast<int32_t>(offsetof(MiraPostRender_t3202756259, ___ySize_4)); }
	inline int32_t get_ySize_4() const { return ___ySize_4; }
	inline int32_t* get_address_of_ySize_4() { return &___ySize_4; }
	inline void set_ySize_4(int32_t value)
	{
		___ySize_4 = value;
	}

	inline static int32_t get_offset_of_zOffset_5() { return static_cast<int32_t>(offsetof(MiraPostRender_t3202756259, ___zOffset_5)); }
	inline float get_zOffset_5() const { return ___zOffset_5; }
	inline float* get_address_of_zOffset_5() { return &___zOffset_5; }
	inline void set_zOffset_5(float value)
	{
		___zOffset_5 = value;
	}

	inline static int32_t get_offset_of_xScalar_6() { return static_cast<int32_t>(offsetof(MiraPostRender_t3202756259, ___xScalar_6)); }
	inline float get_xScalar_6() const { return ___xScalar_6; }
	inline float* get_address_of_xScalar_6() { return &___xScalar_6; }
	inline void set_xScalar_6(float value)
	{
		___xScalar_6 = value;
	}

	inline static int32_t get_offset_of_yScalar_7() { return static_cast<int32_t>(offsetof(MiraPostRender_t3202756259, ___yScalar_7)); }
	inline float get_yScalar_7() const { return ___yScalar_7; }
	inline float* get_address_of_yScalar_7() { return &___yScalar_7; }
	inline void set_yScalar_7(float value)
	{
		___yScalar_7 = value;
	}

	inline static int32_t get_offset_of_screenSizeX_8() { return static_cast<int32_t>(offsetof(MiraPostRender_t3202756259, ___screenSizeX_8)); }
	inline float get_screenSizeX_8() const { return ___screenSizeX_8; }
	inline float* get_address_of_screenSizeX_8() { return &___screenSizeX_8; }
	inline void set_screenSizeX_8(float value)
	{
		___screenSizeX_8 = value;
	}

	inline static int32_t get_offset_of_screenSizeY_9() { return static_cast<int32_t>(offsetof(MiraPostRender_t3202756259, ___screenSizeY_9)); }
	inline float get_screenSizeY_9() const { return ___screenSizeY_9; }
	inline float* get_address_of_screenSizeY_9() { return &___screenSizeY_9; }
	inline void set_screenSizeY_9(float value)
	{
		___screenSizeY_9 = value;
	}

	inline static int32_t get_offset_of_mesh_10() { return static_cast<int32_t>(offsetof(MiraPostRender_t3202756259, ___mesh_10)); }
	inline Mesh_t1356156583 * get_mesh_10() const { return ___mesh_10; }
	inline Mesh_t1356156583 ** get_address_of_mesh_10() { return &___mesh_10; }
	inline void set_mesh_10(Mesh_t1356156583 * value)
	{
		___mesh_10 = value;
		Il2CppCodeGenWriteBarrier(&___mesh_10, value);
	}

	inline static int32_t get_offset_of_renderTextureMaterial_11() { return static_cast<int32_t>(offsetof(MiraPostRender_t3202756259, ___renderTextureMaterial_11)); }
	inline Material_t193706927 * get_renderTextureMaterial_11() const { return ___renderTextureMaterial_11; }
	inline Material_t193706927 ** get_address_of_renderTextureMaterial_11() { return &___renderTextureMaterial_11; }
	inline void set_renderTextureMaterial_11(Material_t193706927 * value)
	{
		___renderTextureMaterial_11 = value;
		Il2CppCodeGenWriteBarrier(&___renderTextureMaterial_11, value);
	}

	inline static int32_t get_offset_of_IPD_12() { return static_cast<int32_t>(offsetof(MiraPostRender_t3202756259, ___IPD_12)); }
	inline float get_IPD_12() const { return ___IPD_12; }
	inline float* get_address_of_IPD_12() { return &___IPD_12; }
	inline void set_IPD_12(float value)
	{
		___IPD_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
