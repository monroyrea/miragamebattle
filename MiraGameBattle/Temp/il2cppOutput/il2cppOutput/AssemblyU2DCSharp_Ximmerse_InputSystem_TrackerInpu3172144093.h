﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.InputSystem.TrackerInput/<FindAll>c__AnonStorey0
struct  U3CFindAllU3Ec__AnonStorey0_t3172144093  : public Il2CppObject
{
public:
	// System.String Ximmerse.InputSystem.TrackerInput/<FindAll>c__AnonStorey0::deviceName
	String_t* ___deviceName_0;

public:
	inline static int32_t get_offset_of_deviceName_0() { return static_cast<int32_t>(offsetof(U3CFindAllU3Ec__AnonStorey0_t3172144093, ___deviceName_0)); }
	inline String_t* get_deviceName_0() const { return ___deviceName_0; }
	inline String_t** get_address_of_deviceName_0() { return &___deviceName_0; }
	inline void set_deviceName_0(String_t* value)
	{
		___deviceName_0 = value;
		Il2CppCodeGenWriteBarrier(&___deviceName_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
