﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerTy123719274.h"

// TrackedEventListener
struct TrackedEventListener_t3475880246;
// Ximmerse.InputSystem.ControllerInput
struct ControllerInput_t1382602808;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Ximmerse.UI.UIFade
struct UIFade_t1631582502;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackedEventListener
struct  TrackedEventListener_t3475880246  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 TrackedEventListener::m_LastResult
	int32_t ___m_LastResult_8;
	// Ximmerse.InputSystem.ControllerType TrackedEventListener::controller
	int32_t ___controller_9;
	// Ximmerse.InputSystem.ControllerInput TrackedEventListener::m_ControllerInput
	ControllerInput_t1382602808 * ___m_ControllerInput_10;
	// UnityEngine.GameObject TrackedEventListener::uiOutOfRange
	GameObject_t1756533147 * ___uiOutOfRange_11;
	// Ximmerse.UI.UIFade TrackedEventListener::m_FadeOutOfRange
	UIFade_t1631582502 * ___m_FadeOutOfRange_12;
	// UnityEngine.Events.UnityEvent TrackedEventListener::onBecameVisible
	UnityEvent_t408735097 * ___onBecameVisible_13;
	// UnityEngine.Events.UnityEvent TrackedEventListener::onBecameInvisible
	UnityEvent_t408735097 * ___onBecameInvisible_14;

public:
	inline static int32_t get_offset_of_m_LastResult_8() { return static_cast<int32_t>(offsetof(TrackedEventListener_t3475880246, ___m_LastResult_8)); }
	inline int32_t get_m_LastResult_8() const { return ___m_LastResult_8; }
	inline int32_t* get_address_of_m_LastResult_8() { return &___m_LastResult_8; }
	inline void set_m_LastResult_8(int32_t value)
	{
		___m_LastResult_8 = value;
	}

	inline static int32_t get_offset_of_controller_9() { return static_cast<int32_t>(offsetof(TrackedEventListener_t3475880246, ___controller_9)); }
	inline int32_t get_controller_9() const { return ___controller_9; }
	inline int32_t* get_address_of_controller_9() { return &___controller_9; }
	inline void set_controller_9(int32_t value)
	{
		___controller_9 = value;
	}

	inline static int32_t get_offset_of_m_ControllerInput_10() { return static_cast<int32_t>(offsetof(TrackedEventListener_t3475880246, ___m_ControllerInput_10)); }
	inline ControllerInput_t1382602808 * get_m_ControllerInput_10() const { return ___m_ControllerInput_10; }
	inline ControllerInput_t1382602808 ** get_address_of_m_ControllerInput_10() { return &___m_ControllerInput_10; }
	inline void set_m_ControllerInput_10(ControllerInput_t1382602808 * value)
	{
		___m_ControllerInput_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_ControllerInput_10, value);
	}

	inline static int32_t get_offset_of_uiOutOfRange_11() { return static_cast<int32_t>(offsetof(TrackedEventListener_t3475880246, ___uiOutOfRange_11)); }
	inline GameObject_t1756533147 * get_uiOutOfRange_11() const { return ___uiOutOfRange_11; }
	inline GameObject_t1756533147 ** get_address_of_uiOutOfRange_11() { return &___uiOutOfRange_11; }
	inline void set_uiOutOfRange_11(GameObject_t1756533147 * value)
	{
		___uiOutOfRange_11 = value;
		Il2CppCodeGenWriteBarrier(&___uiOutOfRange_11, value);
	}

	inline static int32_t get_offset_of_m_FadeOutOfRange_12() { return static_cast<int32_t>(offsetof(TrackedEventListener_t3475880246, ___m_FadeOutOfRange_12)); }
	inline UIFade_t1631582502 * get_m_FadeOutOfRange_12() const { return ___m_FadeOutOfRange_12; }
	inline UIFade_t1631582502 ** get_address_of_m_FadeOutOfRange_12() { return &___m_FadeOutOfRange_12; }
	inline void set_m_FadeOutOfRange_12(UIFade_t1631582502 * value)
	{
		___m_FadeOutOfRange_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_FadeOutOfRange_12, value);
	}

	inline static int32_t get_offset_of_onBecameVisible_13() { return static_cast<int32_t>(offsetof(TrackedEventListener_t3475880246, ___onBecameVisible_13)); }
	inline UnityEvent_t408735097 * get_onBecameVisible_13() const { return ___onBecameVisible_13; }
	inline UnityEvent_t408735097 ** get_address_of_onBecameVisible_13() { return &___onBecameVisible_13; }
	inline void set_onBecameVisible_13(UnityEvent_t408735097 * value)
	{
		___onBecameVisible_13 = value;
		Il2CppCodeGenWriteBarrier(&___onBecameVisible_13, value);
	}

	inline static int32_t get_offset_of_onBecameInvisible_14() { return static_cast<int32_t>(offsetof(TrackedEventListener_t3475880246, ___onBecameInvisible_14)); }
	inline UnityEvent_t408735097 * get_onBecameInvisible_14() const { return ___onBecameInvisible_14; }
	inline UnityEvent_t408735097 ** get_address_of_onBecameInvisible_14() { return &___onBecameInvisible_14; }
	inline void set_onBecameInvisible_14(UnityEvent_t408735097 * value)
	{
		___onBecameInvisible_14 = value;
		Il2CppCodeGenWriteBarrier(&___onBecameInvisible_14, value);
	}
};

struct TrackedEventListener_t3475880246_StaticFields
{
public:
	// TrackedEventListener TrackedEventListener::current
	TrackedEventListener_t3475880246 * ___current_7;

public:
	inline static int32_t get_offset_of_current_7() { return static_cast<int32_t>(offsetof(TrackedEventListener_t3475880246_StaticFields, ___current_7)); }
	inline TrackedEventListener_t3475880246 * get_current_7() const { return ___current_7; }
	inline TrackedEventListener_t3475880246 ** get_address_of_current_7() { return &___current_7; }
	inline void set_current_7(TrackedEventListener_t3475880246 * value)
	{
		___current_7 = value;
		Il2CppCodeGenWriteBarrier(&___current_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
