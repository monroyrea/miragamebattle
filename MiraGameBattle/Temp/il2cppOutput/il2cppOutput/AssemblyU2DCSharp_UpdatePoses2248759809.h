﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UpdatePoses
struct UpdatePoses_t2248759809;
// System.Action
struct Action_t3226471752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdatePoses
struct  UpdatePoses_t2248759809  : public MonoBehaviour_t1158329972
{
public:
	// System.Action UpdatePoses::onUpdatePoses
	Action_t3226471752 * ___onUpdatePoses_4;
	// System.Int32 UpdatePoses::m_PrevFrameCount
	int32_t ___m_PrevFrameCount_5;

public:
	inline static int32_t get_offset_of_onUpdatePoses_4() { return static_cast<int32_t>(offsetof(UpdatePoses_t2248759809, ___onUpdatePoses_4)); }
	inline Action_t3226471752 * get_onUpdatePoses_4() const { return ___onUpdatePoses_4; }
	inline Action_t3226471752 ** get_address_of_onUpdatePoses_4() { return &___onUpdatePoses_4; }
	inline void set_onUpdatePoses_4(Action_t3226471752 * value)
	{
		___onUpdatePoses_4 = value;
		Il2CppCodeGenWriteBarrier(&___onUpdatePoses_4, value);
	}

	inline static int32_t get_offset_of_m_PrevFrameCount_5() { return static_cast<int32_t>(offsetof(UpdatePoses_t2248759809, ___m_PrevFrameCount_5)); }
	inline int32_t get_m_PrevFrameCount_5() const { return ___m_PrevFrameCount_5; }
	inline int32_t* get_address_of_m_PrevFrameCount_5() { return &___m_PrevFrameCount_5; }
	inline void set_m_PrevFrameCount_5(int32_t value)
	{
		___m_PrevFrameCount_5 = value;
	}
};

struct UpdatePoses_t2248759809_StaticFields
{
public:
	// UpdatePoses UpdatePoses::s_Instance
	UpdatePoses_t2248759809 * ___s_Instance_2;
	// System.Boolean UpdatePoses::s_InstanceCached
	bool ___s_InstanceCached_3;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(UpdatePoses_t2248759809_StaticFields, ___s_Instance_2)); }
	inline UpdatePoses_t2248759809 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline UpdatePoses_t2248759809 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(UpdatePoses_t2248759809 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___s_Instance_2, value);
	}

	inline static int32_t get_offset_of_s_InstanceCached_3() { return static_cast<int32_t>(offsetof(UpdatePoses_t2248759809_StaticFields, ___s_InstanceCached_3)); }
	inline bool get_s_InstanceCached_3() const { return ___s_InstanceCached_3; }
	inline bool* get_address_of_s_InstanceCached_3() { return &___s_InstanceCached_3; }
	inline void set_s_InstanceCached_3(bool value)
	{
		___s_InstanceCached_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
