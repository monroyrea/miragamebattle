﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// FPSCounter
struct FPSCounter_t921841495;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// Ximmerse.InputSystem.TrackerInput
struct TrackerInput_t4065713356;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackerInputTest
struct  TrackerInputTest_t1154877504  : public MonoBehaviour_t1158329972
{
public:
	// FPSCounter TrackerInputTest::m_FPSCounter
	FPSCounter_t921841495 * ___m_FPSCounter_2;
	// UnityEngine.UI.Text TrackerInputTest::m_Text
	Text_t356221433 * ___m_Text_3;
	// System.Int32[] TrackerInputTest::m_Nodes
	Int32U5BU5D_t3030399641* ___m_Nodes_4;
	// Ximmerse.InputSystem.TrackerInput TrackerInputTest::m_TrackerInput
	TrackerInput_t4065713356 * ___m_TrackerInput_5;

public:
	inline static int32_t get_offset_of_m_FPSCounter_2() { return static_cast<int32_t>(offsetof(TrackerInputTest_t1154877504, ___m_FPSCounter_2)); }
	inline FPSCounter_t921841495 * get_m_FPSCounter_2() const { return ___m_FPSCounter_2; }
	inline FPSCounter_t921841495 ** get_address_of_m_FPSCounter_2() { return &___m_FPSCounter_2; }
	inline void set_m_FPSCounter_2(FPSCounter_t921841495 * value)
	{
		___m_FPSCounter_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_FPSCounter_2, value);
	}

	inline static int32_t get_offset_of_m_Text_3() { return static_cast<int32_t>(offsetof(TrackerInputTest_t1154877504, ___m_Text_3)); }
	inline Text_t356221433 * get_m_Text_3() const { return ___m_Text_3; }
	inline Text_t356221433 ** get_address_of_m_Text_3() { return &___m_Text_3; }
	inline void set_m_Text_3(Text_t356221433 * value)
	{
		___m_Text_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_Text_3, value);
	}

	inline static int32_t get_offset_of_m_Nodes_4() { return static_cast<int32_t>(offsetof(TrackerInputTest_t1154877504, ___m_Nodes_4)); }
	inline Int32U5BU5D_t3030399641* get_m_Nodes_4() const { return ___m_Nodes_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_Nodes_4() { return &___m_Nodes_4; }
	inline void set_m_Nodes_4(Int32U5BU5D_t3030399641* value)
	{
		___m_Nodes_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_Nodes_4, value);
	}

	inline static int32_t get_offset_of_m_TrackerInput_5() { return static_cast<int32_t>(offsetof(TrackerInputTest_t1154877504, ___m_TrackerInput_5)); }
	inline TrackerInput_t4065713356 * get_m_TrackerInput_5() const { return ___m_TrackerInput_5; }
	inline TrackerInput_t4065713356 ** get_address_of_m_TrackerInput_5() { return &___m_TrackerInput_5; }
	inline void set_m_TrackerInput_5(TrackerInput_t4065713356 * value)
	{
		___m_TrackerInput_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_TrackerInput_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
