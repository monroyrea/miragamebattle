﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Kudan_AR_MarkerEvents4250814921.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraMarkerEvents
struct  MiraMarkerEvents_t1700133524  : public MarkerEvents_t4250814921
{
public:
	// System.String MiraMarkerEvents::id
	String_t* ___id_7;

public:
	inline static int32_t get_offset_of_id_7() { return static_cast<int32_t>(offsetof(MiraMarkerEvents_t1700133524, ___id_7)); }
	inline String_t* get_id_7() const { return ___id_7; }
	inline String_t** get_address_of_id_7() { return &___id_7; }
	inline void set_id_7(String_t* value)
	{
		___id_7 = value;
		Il2CppCodeGenWriteBarrier(&___id_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
