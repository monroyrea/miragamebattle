﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerTy123719274.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_XDevicePlugi472304880.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.InputSystem.ControllerInput
struct  ControllerInput_t1382602808  : public Il2CppObject
{
public:
	// Ximmerse.InputSystem.ControllerType Ximmerse.InputSystem.ControllerInput::type
	int32_t ___type_0;
	// System.Int32 Ximmerse.InputSystem.ControllerInput::handle
	int32_t ___handle_1;
	// System.String Ximmerse.InputSystem.ControllerInput::name
	String_t* ___name_2;
	// System.Int32 Ximmerse.InputSystem.ControllerInput::priority
	int32_t ___priority_3;
	// System.Int32 Ximmerse.InputSystem.ControllerInput::m_PrevFrameCount
	int32_t ___m_PrevFrameCount_4;
	// Ximmerse.InputSystem.XDevicePlugin/ControllerState Ximmerse.InputSystem.ControllerInput::m_State
	ControllerState_t472304880  ___m_State_5;
	// Ximmerse.InputSystem.XDevicePlugin/ControllerState Ximmerse.InputSystem.ControllerInput::m_PrevState
	ControllerState_t472304880  ___m_PrevState_6;
	// UnityEngine.Vector2 Ximmerse.InputSystem.ControllerInput::m_TouchPos
	Vector2_t2243707579  ___m_TouchPos_7;
	// System.Int32 Ximmerse.InputSystem.ControllerInput::m_HapticsDC
	int32_t ___m_HapticsDC_8;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(ControllerInput_t1382602808, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_handle_1() { return static_cast<int32_t>(offsetof(ControllerInput_t1382602808, ___handle_1)); }
	inline int32_t get_handle_1() const { return ___handle_1; }
	inline int32_t* get_address_of_handle_1() { return &___handle_1; }
	inline void set_handle_1(int32_t value)
	{
		___handle_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(ControllerInput_t1382602808, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_priority_3() { return static_cast<int32_t>(offsetof(ControllerInput_t1382602808, ___priority_3)); }
	inline int32_t get_priority_3() const { return ___priority_3; }
	inline int32_t* get_address_of_priority_3() { return &___priority_3; }
	inline void set_priority_3(int32_t value)
	{
		___priority_3 = value;
	}

	inline static int32_t get_offset_of_m_PrevFrameCount_4() { return static_cast<int32_t>(offsetof(ControllerInput_t1382602808, ___m_PrevFrameCount_4)); }
	inline int32_t get_m_PrevFrameCount_4() const { return ___m_PrevFrameCount_4; }
	inline int32_t* get_address_of_m_PrevFrameCount_4() { return &___m_PrevFrameCount_4; }
	inline void set_m_PrevFrameCount_4(int32_t value)
	{
		___m_PrevFrameCount_4 = value;
	}

	inline static int32_t get_offset_of_m_State_5() { return static_cast<int32_t>(offsetof(ControllerInput_t1382602808, ___m_State_5)); }
	inline ControllerState_t472304880  get_m_State_5() const { return ___m_State_5; }
	inline ControllerState_t472304880 * get_address_of_m_State_5() { return &___m_State_5; }
	inline void set_m_State_5(ControllerState_t472304880  value)
	{
		___m_State_5 = value;
	}

	inline static int32_t get_offset_of_m_PrevState_6() { return static_cast<int32_t>(offsetof(ControllerInput_t1382602808, ___m_PrevState_6)); }
	inline ControllerState_t472304880  get_m_PrevState_6() const { return ___m_PrevState_6; }
	inline ControllerState_t472304880 * get_address_of_m_PrevState_6() { return &___m_PrevState_6; }
	inline void set_m_PrevState_6(ControllerState_t472304880  value)
	{
		___m_PrevState_6 = value;
	}

	inline static int32_t get_offset_of_m_TouchPos_7() { return static_cast<int32_t>(offsetof(ControllerInput_t1382602808, ___m_TouchPos_7)); }
	inline Vector2_t2243707579  get_m_TouchPos_7() const { return ___m_TouchPos_7; }
	inline Vector2_t2243707579 * get_address_of_m_TouchPos_7() { return &___m_TouchPos_7; }
	inline void set_m_TouchPos_7(Vector2_t2243707579  value)
	{
		___m_TouchPos_7 = value;
	}

	inline static int32_t get_offset_of_m_HapticsDC_8() { return static_cast<int32_t>(offsetof(ControllerInput_t1382602808, ___m_HapticsDC_8)); }
	inline int32_t get_m_HapticsDC_8() const { return ___m_HapticsDC_8; }
	inline int32_t* get_address_of_m_HapticsDC_8() { return &___m_HapticsDC_8; }
	inline void set_m_HapticsDC_8(int32_t value)
	{
		___m_HapticsDC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
