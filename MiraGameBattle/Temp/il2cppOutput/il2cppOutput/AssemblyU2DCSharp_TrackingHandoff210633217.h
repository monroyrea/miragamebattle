﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackingHandoff
struct  TrackingHandoff_t210633217  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject TrackingHandoff::currentObject
	GameObject_t1756533147 * ___currentObject_2;
	// UnityEngine.Vector3 TrackingHandoff::defaultPos
	Vector3_t2243707580  ___defaultPos_3;
	// UnityEngine.Quaternion TrackingHandoff::defaultRot
	Quaternion_t4030073918  ___defaultRot_4;
	// UnityEngine.Transform TrackingHandoff::tempPlacement
	Transform_t3275118058 * ___tempPlacement_5;

public:
	inline static int32_t get_offset_of_currentObject_2() { return static_cast<int32_t>(offsetof(TrackingHandoff_t210633217, ___currentObject_2)); }
	inline GameObject_t1756533147 * get_currentObject_2() const { return ___currentObject_2; }
	inline GameObject_t1756533147 ** get_address_of_currentObject_2() { return &___currentObject_2; }
	inline void set_currentObject_2(GameObject_t1756533147 * value)
	{
		___currentObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___currentObject_2, value);
	}

	inline static int32_t get_offset_of_defaultPos_3() { return static_cast<int32_t>(offsetof(TrackingHandoff_t210633217, ___defaultPos_3)); }
	inline Vector3_t2243707580  get_defaultPos_3() const { return ___defaultPos_3; }
	inline Vector3_t2243707580 * get_address_of_defaultPos_3() { return &___defaultPos_3; }
	inline void set_defaultPos_3(Vector3_t2243707580  value)
	{
		___defaultPos_3 = value;
	}

	inline static int32_t get_offset_of_defaultRot_4() { return static_cast<int32_t>(offsetof(TrackingHandoff_t210633217, ___defaultRot_4)); }
	inline Quaternion_t4030073918  get_defaultRot_4() const { return ___defaultRot_4; }
	inline Quaternion_t4030073918 * get_address_of_defaultRot_4() { return &___defaultRot_4; }
	inline void set_defaultRot_4(Quaternion_t4030073918  value)
	{
		___defaultRot_4 = value;
	}

	inline static int32_t get_offset_of_tempPlacement_5() { return static_cast<int32_t>(offsetof(TrackingHandoff_t210633217, ___tempPlacement_5)); }
	inline Transform_t3275118058 * get_tempPlacement_5() const { return ___tempPlacement_5; }
	inline Transform_t3275118058 ** get_address_of_tempPlacement_5() { return &___tempPlacement_5; }
	inline void set_tempPlacement_5(Transform_t3275118058 * value)
	{
		___tempPlacement_5 = value;
		Il2CppCodeGenWriteBarrier(&___tempPlacement_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
