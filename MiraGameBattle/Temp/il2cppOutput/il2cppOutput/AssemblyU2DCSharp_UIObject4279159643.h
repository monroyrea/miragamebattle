﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_TrackingInteractable149194413.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UIManager
struct UIManager_t2519183485;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIObject
struct  UIObject_t4279159643  : public TrackingInteractable_t149194413
{
public:
	// System.Int32 UIObject::UIObjIndex
	int32_t ___UIObjIndex_9;
	// System.Single UIObject::BoundsX
	float ___BoundsX_10;
	// System.Boolean UIObject::inUI
	bool ___inUI_11;
	// UIManager UIObject::mainUIManager
	UIManager_t2519183485 * ___mainUIManager_12;
	// UnityEngine.Quaternion UIObject::UIOgRotation
	Quaternion_t4030073918  ___UIOgRotation_13;
	// UnityEngine.Vector3 UIObject::currentPos
	Vector3_t2243707580  ___currentPos_14;
	// UnityEngine.Vector3 UIObject::lerpPos
	Vector3_t2243707580  ___lerpPos_15;
	// UnityEngine.Quaternion UIObject::currentRot
	Quaternion_t4030073918  ___currentRot_16;
	// UnityEngine.Quaternion UIObject::lerpRot
	Quaternion_t4030073918  ___lerpRot_17;
	// System.Boolean UIObject::dontLerp
	bool ___dontLerp_18;
	// System.Single UIObject::rotateSpeed
	float ___rotateSpeed_19;

public:
	inline static int32_t get_offset_of_UIObjIndex_9() { return static_cast<int32_t>(offsetof(UIObject_t4279159643, ___UIObjIndex_9)); }
	inline int32_t get_UIObjIndex_9() const { return ___UIObjIndex_9; }
	inline int32_t* get_address_of_UIObjIndex_9() { return &___UIObjIndex_9; }
	inline void set_UIObjIndex_9(int32_t value)
	{
		___UIObjIndex_9 = value;
	}

	inline static int32_t get_offset_of_BoundsX_10() { return static_cast<int32_t>(offsetof(UIObject_t4279159643, ___BoundsX_10)); }
	inline float get_BoundsX_10() const { return ___BoundsX_10; }
	inline float* get_address_of_BoundsX_10() { return &___BoundsX_10; }
	inline void set_BoundsX_10(float value)
	{
		___BoundsX_10 = value;
	}

	inline static int32_t get_offset_of_inUI_11() { return static_cast<int32_t>(offsetof(UIObject_t4279159643, ___inUI_11)); }
	inline bool get_inUI_11() const { return ___inUI_11; }
	inline bool* get_address_of_inUI_11() { return &___inUI_11; }
	inline void set_inUI_11(bool value)
	{
		___inUI_11 = value;
	}

	inline static int32_t get_offset_of_mainUIManager_12() { return static_cast<int32_t>(offsetof(UIObject_t4279159643, ___mainUIManager_12)); }
	inline UIManager_t2519183485 * get_mainUIManager_12() const { return ___mainUIManager_12; }
	inline UIManager_t2519183485 ** get_address_of_mainUIManager_12() { return &___mainUIManager_12; }
	inline void set_mainUIManager_12(UIManager_t2519183485 * value)
	{
		___mainUIManager_12 = value;
		Il2CppCodeGenWriteBarrier(&___mainUIManager_12, value);
	}

	inline static int32_t get_offset_of_UIOgRotation_13() { return static_cast<int32_t>(offsetof(UIObject_t4279159643, ___UIOgRotation_13)); }
	inline Quaternion_t4030073918  get_UIOgRotation_13() const { return ___UIOgRotation_13; }
	inline Quaternion_t4030073918 * get_address_of_UIOgRotation_13() { return &___UIOgRotation_13; }
	inline void set_UIOgRotation_13(Quaternion_t4030073918  value)
	{
		___UIOgRotation_13 = value;
	}

	inline static int32_t get_offset_of_currentPos_14() { return static_cast<int32_t>(offsetof(UIObject_t4279159643, ___currentPos_14)); }
	inline Vector3_t2243707580  get_currentPos_14() const { return ___currentPos_14; }
	inline Vector3_t2243707580 * get_address_of_currentPos_14() { return &___currentPos_14; }
	inline void set_currentPos_14(Vector3_t2243707580  value)
	{
		___currentPos_14 = value;
	}

	inline static int32_t get_offset_of_lerpPos_15() { return static_cast<int32_t>(offsetof(UIObject_t4279159643, ___lerpPos_15)); }
	inline Vector3_t2243707580  get_lerpPos_15() const { return ___lerpPos_15; }
	inline Vector3_t2243707580 * get_address_of_lerpPos_15() { return &___lerpPos_15; }
	inline void set_lerpPos_15(Vector3_t2243707580  value)
	{
		___lerpPos_15 = value;
	}

	inline static int32_t get_offset_of_currentRot_16() { return static_cast<int32_t>(offsetof(UIObject_t4279159643, ___currentRot_16)); }
	inline Quaternion_t4030073918  get_currentRot_16() const { return ___currentRot_16; }
	inline Quaternion_t4030073918 * get_address_of_currentRot_16() { return &___currentRot_16; }
	inline void set_currentRot_16(Quaternion_t4030073918  value)
	{
		___currentRot_16 = value;
	}

	inline static int32_t get_offset_of_lerpRot_17() { return static_cast<int32_t>(offsetof(UIObject_t4279159643, ___lerpRot_17)); }
	inline Quaternion_t4030073918  get_lerpRot_17() const { return ___lerpRot_17; }
	inline Quaternion_t4030073918 * get_address_of_lerpRot_17() { return &___lerpRot_17; }
	inline void set_lerpRot_17(Quaternion_t4030073918  value)
	{
		___lerpRot_17 = value;
	}

	inline static int32_t get_offset_of_dontLerp_18() { return static_cast<int32_t>(offsetof(UIObject_t4279159643, ___dontLerp_18)); }
	inline bool get_dontLerp_18() const { return ___dontLerp_18; }
	inline bool* get_address_of_dontLerp_18() { return &___dontLerp_18; }
	inline void set_dontLerp_18(bool value)
	{
		___dontLerp_18 = value;
	}

	inline static int32_t get_offset_of_rotateSpeed_19() { return static_cast<int32_t>(offsetof(UIObject_t4279159643, ___rotateSpeed_19)); }
	inline float get_rotateSpeed_19() const { return ___rotateSpeed_19; }
	inline float* get_address_of_rotateSpeed_19() { return &___rotateSpeed_19; }
	inline void set_rotateSpeed_19(float value)
	{
		___rotateSpeed_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
