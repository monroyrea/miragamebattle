﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_scriptMechanics_EnumAttackCollide537956954.h"

// scriptSensor
struct scriptSensor_t1009192331;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClassPlayerSensorsData
struct  ClassPlayerSensorsData_t400201692  : public Il2CppObject
{
public:
	// scriptSensor ClassPlayerSensorsData::_sensors
	scriptSensor_t1009192331 * ____sensors_0;
	// scriptMechanics/EnumAttackCollider ClassPlayerSensorsData::_attackCollider
	int32_t ____attackCollider_1;

public:
	inline static int32_t get_offset_of__sensors_0() { return static_cast<int32_t>(offsetof(ClassPlayerSensorsData_t400201692, ____sensors_0)); }
	inline scriptSensor_t1009192331 * get__sensors_0() const { return ____sensors_0; }
	inline scriptSensor_t1009192331 ** get_address_of__sensors_0() { return &____sensors_0; }
	inline void set__sensors_0(scriptSensor_t1009192331 * value)
	{
		____sensors_0 = value;
		Il2CppCodeGenWriteBarrier(&____sensors_0, value);
	}

	inline static int32_t get_offset_of__attackCollider_1() { return static_cast<int32_t>(offsetof(ClassPlayerSensorsData_t400201692, ____attackCollider_1)); }
	inline int32_t get__attackCollider_1() const { return ____attackCollider_1; }
	inline int32_t* get_address_of__attackCollider_1() { return &____attackCollider_1; }
	inline void set__attackCollider_1(int32_t value)
	{
		____attackCollider_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
