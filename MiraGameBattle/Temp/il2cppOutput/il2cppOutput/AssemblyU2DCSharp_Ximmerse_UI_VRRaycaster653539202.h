﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster410733016.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerTy123719274.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"

// System.Collections.Generic.Dictionary`2<System.String,Ximmerse.UI.VRRaycaster>
struct Dictionary_2_t2568318464;
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t3873494194;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Camera
struct Camera_t189460977;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Predicate`1<UnityEngine.Canvas>
struct Predicate_1_t2947343177;
// System.Comparison`1<UnityEngine.RaycastHit>
struct Comparison_1_t1348919171;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.UI.VRRaycaster
struct  VRRaycaster_t653539202  : public GraphicRaycaster_t410733016
{
public:
	// Ximmerse.InputSystem.ControllerType Ximmerse.UI.VRRaycaster::controller
	int32_t ___controller_12;
	// UnityEngine.Transform Ximmerse.UI.VRRaycaster::laserLine
	Transform_t3275118058 * ___laserLine_13;
	// System.Boolean Ximmerse.UI.VRRaycaster::usePhysics
	bool ___usePhysics_14;
	// UnityEngine.LayerMask Ximmerse.UI.VRRaycaster::eventMask
	LayerMask_t3188175821  ___eventMask_15;
	// UnityEngine.Camera Ximmerse.UI.VRRaycaster::m_EventCamera
	Camera_t189460977 * ___m_EventCamera_16;
	// UnityEngine.Transform Ximmerse.UI.VRRaycaster::m_Transform
	Transform_t3275118058 * ___m_Transform_17;
	// System.Reflection.FieldInfo Ximmerse.UI.VRRaycaster::m_CanvasRef
	FieldInfo_t * ___m_CanvasRef_18;

public:
	inline static int32_t get_offset_of_controller_12() { return static_cast<int32_t>(offsetof(VRRaycaster_t653539202, ___controller_12)); }
	inline int32_t get_controller_12() const { return ___controller_12; }
	inline int32_t* get_address_of_controller_12() { return &___controller_12; }
	inline void set_controller_12(int32_t value)
	{
		___controller_12 = value;
	}

	inline static int32_t get_offset_of_laserLine_13() { return static_cast<int32_t>(offsetof(VRRaycaster_t653539202, ___laserLine_13)); }
	inline Transform_t3275118058 * get_laserLine_13() const { return ___laserLine_13; }
	inline Transform_t3275118058 ** get_address_of_laserLine_13() { return &___laserLine_13; }
	inline void set_laserLine_13(Transform_t3275118058 * value)
	{
		___laserLine_13 = value;
		Il2CppCodeGenWriteBarrier(&___laserLine_13, value);
	}

	inline static int32_t get_offset_of_usePhysics_14() { return static_cast<int32_t>(offsetof(VRRaycaster_t653539202, ___usePhysics_14)); }
	inline bool get_usePhysics_14() const { return ___usePhysics_14; }
	inline bool* get_address_of_usePhysics_14() { return &___usePhysics_14; }
	inline void set_usePhysics_14(bool value)
	{
		___usePhysics_14 = value;
	}

	inline static int32_t get_offset_of_eventMask_15() { return static_cast<int32_t>(offsetof(VRRaycaster_t653539202, ___eventMask_15)); }
	inline LayerMask_t3188175821  get_eventMask_15() const { return ___eventMask_15; }
	inline LayerMask_t3188175821 * get_address_of_eventMask_15() { return &___eventMask_15; }
	inline void set_eventMask_15(LayerMask_t3188175821  value)
	{
		___eventMask_15 = value;
	}

	inline static int32_t get_offset_of_m_EventCamera_16() { return static_cast<int32_t>(offsetof(VRRaycaster_t653539202, ___m_EventCamera_16)); }
	inline Camera_t189460977 * get_m_EventCamera_16() const { return ___m_EventCamera_16; }
	inline Camera_t189460977 ** get_address_of_m_EventCamera_16() { return &___m_EventCamera_16; }
	inline void set_m_EventCamera_16(Camera_t189460977 * value)
	{
		___m_EventCamera_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_EventCamera_16, value);
	}

	inline static int32_t get_offset_of_m_Transform_17() { return static_cast<int32_t>(offsetof(VRRaycaster_t653539202, ___m_Transform_17)); }
	inline Transform_t3275118058 * get_m_Transform_17() const { return ___m_Transform_17; }
	inline Transform_t3275118058 ** get_address_of_m_Transform_17() { return &___m_Transform_17; }
	inline void set_m_Transform_17(Transform_t3275118058 * value)
	{
		___m_Transform_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_Transform_17, value);
	}

	inline static int32_t get_offset_of_m_CanvasRef_18() { return static_cast<int32_t>(offsetof(VRRaycaster_t653539202, ___m_CanvasRef_18)); }
	inline FieldInfo_t * get_m_CanvasRef_18() const { return ___m_CanvasRef_18; }
	inline FieldInfo_t ** get_address_of_m_CanvasRef_18() { return &___m_CanvasRef_18; }
	inline void set_m_CanvasRef_18(FieldInfo_t * value)
	{
		___m_CanvasRef_18 = value;
		Il2CppCodeGenWriteBarrier(&___m_CanvasRef_18, value);
	}
};

struct VRRaycaster_t653539202_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Ximmerse.UI.VRRaycaster> Ximmerse.UI.VRRaycaster::s_InstanceMap
	Dictionary_2_t2568318464 * ___s_InstanceMap_10;
	// System.Collections.Generic.List`1<UnityEngine.Canvas> Ximmerse.UI.VRRaycaster::s_AllCanvases
	List_1_t3873494194 * ___s_AllCanvases_11;
	// System.Predicate`1<UnityEngine.Canvas> Ximmerse.UI.VRRaycaster::<>f__am$cache0
	Predicate_1_t2947343177 * ___U3CU3Ef__amU24cache0_19;
	// System.Comparison`1<UnityEngine.RaycastHit> Ximmerse.UI.VRRaycaster::<>f__am$cache1
	Comparison_1_t1348919171 * ___U3CU3Ef__amU24cache1_20;

public:
	inline static int32_t get_offset_of_s_InstanceMap_10() { return static_cast<int32_t>(offsetof(VRRaycaster_t653539202_StaticFields, ___s_InstanceMap_10)); }
	inline Dictionary_2_t2568318464 * get_s_InstanceMap_10() const { return ___s_InstanceMap_10; }
	inline Dictionary_2_t2568318464 ** get_address_of_s_InstanceMap_10() { return &___s_InstanceMap_10; }
	inline void set_s_InstanceMap_10(Dictionary_2_t2568318464 * value)
	{
		___s_InstanceMap_10 = value;
		Il2CppCodeGenWriteBarrier(&___s_InstanceMap_10, value);
	}

	inline static int32_t get_offset_of_s_AllCanvases_11() { return static_cast<int32_t>(offsetof(VRRaycaster_t653539202_StaticFields, ___s_AllCanvases_11)); }
	inline List_1_t3873494194 * get_s_AllCanvases_11() const { return ___s_AllCanvases_11; }
	inline List_1_t3873494194 ** get_address_of_s_AllCanvases_11() { return &___s_AllCanvases_11; }
	inline void set_s_AllCanvases_11(List_1_t3873494194 * value)
	{
		___s_AllCanvases_11 = value;
		Il2CppCodeGenWriteBarrier(&___s_AllCanvases_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_19() { return static_cast<int32_t>(offsetof(VRRaycaster_t653539202_StaticFields, ___U3CU3Ef__amU24cache0_19)); }
	inline Predicate_1_t2947343177 * get_U3CU3Ef__amU24cache0_19() const { return ___U3CU3Ef__amU24cache0_19; }
	inline Predicate_1_t2947343177 ** get_address_of_U3CU3Ef__amU24cache0_19() { return &___U3CU3Ef__amU24cache0_19; }
	inline void set_U3CU3Ef__amU24cache0_19(Predicate_1_t2947343177 * value)
	{
		___U3CU3Ef__amU24cache0_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_20() { return static_cast<int32_t>(offsetof(VRRaycaster_t653539202_StaticFields, ___U3CU3Ef__amU24cache1_20)); }
	inline Comparison_1_t1348919171 * get_U3CU3Ef__amU24cache1_20() const { return ___U3CU3Ef__amU24cache1_20; }
	inline Comparison_1_t1348919171 ** get_address_of_U3CU3Ef__amU24cache1_20() { return &___U3CU3Ef__amU24cache1_20; }
	inline void set_U3CU3Ef__amU24cache1_20(Comparison_1_t1348919171 * value)
	{
		___U3CU3Ef__amU24cache1_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
