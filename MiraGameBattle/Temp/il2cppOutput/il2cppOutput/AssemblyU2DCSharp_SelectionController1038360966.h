﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// ControllerTransform
struct ControllerTransform_t2066672932;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectionController
struct  SelectionController_t1038360966  : public MonoBehaviour_t1158329972
{
public:
	// ControllerTransform SelectionController::ct
	ControllerTransform_t2066672932 * ___ct_6;
	// UnityEngine.GameObject SelectionController::laser
	GameObject_t1756533147 * ___laser_7;
	// UnityEngine.GameObject SelectionController::startSetupUI
	GameObject_t1756533147 * ___startSetupUI_8;
	// UnityEngine.GameObject SelectionController::characterSelectionUI
	GameObject_t1756533147 * ___characterSelectionUI_9;
	// UnityEngine.GameObject SelectionController::characterSelection
	GameObject_t1756533147 * ___characterSelection_10;
	// UnityEngine.GameObject SelectionController::battleBoard
	GameObject_t1756533147 * ___battleBoard_11;
	// UnityEngine.MeshRenderer SelectionController::battleBoardRenderer
	MeshRenderer_t1268241104 * ___battleBoardRenderer_12;
	// System.Int32 SelectionController::count
	int32_t ___count_13;

public:
	inline static int32_t get_offset_of_ct_6() { return static_cast<int32_t>(offsetof(SelectionController_t1038360966, ___ct_6)); }
	inline ControllerTransform_t2066672932 * get_ct_6() const { return ___ct_6; }
	inline ControllerTransform_t2066672932 ** get_address_of_ct_6() { return &___ct_6; }
	inline void set_ct_6(ControllerTransform_t2066672932 * value)
	{
		___ct_6 = value;
		Il2CppCodeGenWriteBarrier(&___ct_6, value);
	}

	inline static int32_t get_offset_of_laser_7() { return static_cast<int32_t>(offsetof(SelectionController_t1038360966, ___laser_7)); }
	inline GameObject_t1756533147 * get_laser_7() const { return ___laser_7; }
	inline GameObject_t1756533147 ** get_address_of_laser_7() { return &___laser_7; }
	inline void set_laser_7(GameObject_t1756533147 * value)
	{
		___laser_7 = value;
		Il2CppCodeGenWriteBarrier(&___laser_7, value);
	}

	inline static int32_t get_offset_of_startSetupUI_8() { return static_cast<int32_t>(offsetof(SelectionController_t1038360966, ___startSetupUI_8)); }
	inline GameObject_t1756533147 * get_startSetupUI_8() const { return ___startSetupUI_8; }
	inline GameObject_t1756533147 ** get_address_of_startSetupUI_8() { return &___startSetupUI_8; }
	inline void set_startSetupUI_8(GameObject_t1756533147 * value)
	{
		___startSetupUI_8 = value;
		Il2CppCodeGenWriteBarrier(&___startSetupUI_8, value);
	}

	inline static int32_t get_offset_of_characterSelectionUI_9() { return static_cast<int32_t>(offsetof(SelectionController_t1038360966, ___characterSelectionUI_9)); }
	inline GameObject_t1756533147 * get_characterSelectionUI_9() const { return ___characterSelectionUI_9; }
	inline GameObject_t1756533147 ** get_address_of_characterSelectionUI_9() { return &___characterSelectionUI_9; }
	inline void set_characterSelectionUI_9(GameObject_t1756533147 * value)
	{
		___characterSelectionUI_9 = value;
		Il2CppCodeGenWriteBarrier(&___characterSelectionUI_9, value);
	}

	inline static int32_t get_offset_of_characterSelection_10() { return static_cast<int32_t>(offsetof(SelectionController_t1038360966, ___characterSelection_10)); }
	inline GameObject_t1756533147 * get_characterSelection_10() const { return ___characterSelection_10; }
	inline GameObject_t1756533147 ** get_address_of_characterSelection_10() { return &___characterSelection_10; }
	inline void set_characterSelection_10(GameObject_t1756533147 * value)
	{
		___characterSelection_10 = value;
		Il2CppCodeGenWriteBarrier(&___characterSelection_10, value);
	}

	inline static int32_t get_offset_of_battleBoard_11() { return static_cast<int32_t>(offsetof(SelectionController_t1038360966, ___battleBoard_11)); }
	inline GameObject_t1756533147 * get_battleBoard_11() const { return ___battleBoard_11; }
	inline GameObject_t1756533147 ** get_address_of_battleBoard_11() { return &___battleBoard_11; }
	inline void set_battleBoard_11(GameObject_t1756533147 * value)
	{
		___battleBoard_11 = value;
		Il2CppCodeGenWriteBarrier(&___battleBoard_11, value);
	}

	inline static int32_t get_offset_of_battleBoardRenderer_12() { return static_cast<int32_t>(offsetof(SelectionController_t1038360966, ___battleBoardRenderer_12)); }
	inline MeshRenderer_t1268241104 * get_battleBoardRenderer_12() const { return ___battleBoardRenderer_12; }
	inline MeshRenderer_t1268241104 ** get_address_of_battleBoardRenderer_12() { return &___battleBoardRenderer_12; }
	inline void set_battleBoardRenderer_12(MeshRenderer_t1268241104 * value)
	{
		___battleBoardRenderer_12 = value;
		Il2CppCodeGenWriteBarrier(&___battleBoardRenderer_12, value);
	}

	inline static int32_t get_offset_of_count_13() { return static_cast<int32_t>(offsetof(SelectionController_t1038360966, ___count_13)); }
	inline int32_t get_count_13() const { return ___count_13; }
	inline int32_t* get_address_of_count_13() { return &___count_13; }
	inline void set_count_13(int32_t value)
	{
		___count_13 = value;
	}
};

struct SelectionController_t1038360966_StaticFields
{
public:
	// System.Int32 SelectionController::selectedPlayer
	int32_t ___selectedPlayer_2;
	// System.Int32 SelectionController::selectedEnemy
	int32_t ___selectedEnemy_3;
	// UnityEngine.Vector3 SelectionController::boardPosition
	Vector3_t2243707580  ___boardPosition_4;
	// UnityEngine.Quaternion SelectionController::boardRotation
	Quaternion_t4030073918  ___boardRotation_5;

public:
	inline static int32_t get_offset_of_selectedPlayer_2() { return static_cast<int32_t>(offsetof(SelectionController_t1038360966_StaticFields, ___selectedPlayer_2)); }
	inline int32_t get_selectedPlayer_2() const { return ___selectedPlayer_2; }
	inline int32_t* get_address_of_selectedPlayer_2() { return &___selectedPlayer_2; }
	inline void set_selectedPlayer_2(int32_t value)
	{
		___selectedPlayer_2 = value;
	}

	inline static int32_t get_offset_of_selectedEnemy_3() { return static_cast<int32_t>(offsetof(SelectionController_t1038360966_StaticFields, ___selectedEnemy_3)); }
	inline int32_t get_selectedEnemy_3() const { return ___selectedEnemy_3; }
	inline int32_t* get_address_of_selectedEnemy_3() { return &___selectedEnemy_3; }
	inline void set_selectedEnemy_3(int32_t value)
	{
		___selectedEnemy_3 = value;
	}

	inline static int32_t get_offset_of_boardPosition_4() { return static_cast<int32_t>(offsetof(SelectionController_t1038360966_StaticFields, ___boardPosition_4)); }
	inline Vector3_t2243707580  get_boardPosition_4() const { return ___boardPosition_4; }
	inline Vector3_t2243707580 * get_address_of_boardPosition_4() { return &___boardPosition_4; }
	inline void set_boardPosition_4(Vector3_t2243707580  value)
	{
		___boardPosition_4 = value;
	}

	inline static int32_t get_offset_of_boardRotation_5() { return static_cast<int32_t>(offsetof(SelectionController_t1038360966_StaticFields, ___boardRotation_5)); }
	inline Quaternion_t4030073918  get_boardRotation_5() const { return ___boardRotation_5; }
	inline Quaternion_t4030073918 * get_address_of_boardRotation_5() { return &___boardRotation_5; }
	inline void set_boardRotation_5(Quaternion_t4030073918  value)
	{
		___boardRotation_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
