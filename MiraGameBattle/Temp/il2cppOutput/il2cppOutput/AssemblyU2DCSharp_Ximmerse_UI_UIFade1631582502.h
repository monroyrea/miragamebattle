﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
struct List_1_t1795346708;
// System.Single[]
struct SingleU5BU5D_t577127397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.UI.UIFade
struct  UIFade_t1631582502  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Ximmerse.UI.UIFade::autoPlay
	bool ___autoPlay_2;
	// System.Single Ximmerse.UI.UIFade::delay
	float ___delay_3;
	// System.Single Ximmerse.UI.UIFade::durationIn
	float ___durationIn_4;
	// System.Single Ximmerse.UI.UIFade::durationKeep
	float ___durationKeep_5;
	// System.Single Ximmerse.UI.UIFade::durationOut
	float ___durationOut_6;
	// UnityEngine.Events.UnityEvent Ximmerse.UI.UIFade::onBecameVisible
	UnityEvent_t408735097 * ___onBecameVisible_7;
	// UnityEngine.Events.UnityEvent Ximmerse.UI.UIFade::onBecameInvisible
	UnityEvent_t408735097 * ___onBecameInvisible_8;
	// UnityEngine.GameObject Ximmerse.UI.UIFade::m_GameObject
	GameObject_t1756533147 * ___m_GameObject_9;
	// System.Boolean Ximmerse.UI.UIFade::m_IsPlaying
	bool ___m_IsPlaying_10;
	// System.Boolean Ximmerse.UI.UIFade::m_IsFadeOut
	bool ___m_IsFadeOut_11;
	// System.Single Ximmerse.UI.UIFade::m_Duration
	float ___m_Duration_12;
	// System.Single Ximmerse.UI.UIFade::m_Alpha
	float ___m_Alpha_13;
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> Ximmerse.UI.UIFade::m_Graphics
	List_1_t1795346708 * ___m_Graphics_14;
	// System.Single[] Ximmerse.UI.UIFade::m_Alphas
	SingleU5BU5D_t577127397* ___m_Alphas_15;
	// System.Int32 Ximmerse.UI.UIFade::m_DelayCount
	int32_t ___m_DelayCount_16;

public:
	inline static int32_t get_offset_of_autoPlay_2() { return static_cast<int32_t>(offsetof(UIFade_t1631582502, ___autoPlay_2)); }
	inline bool get_autoPlay_2() const { return ___autoPlay_2; }
	inline bool* get_address_of_autoPlay_2() { return &___autoPlay_2; }
	inline void set_autoPlay_2(bool value)
	{
		___autoPlay_2 = value;
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(UIFade_t1631582502, ___delay_3)); }
	inline float get_delay_3() const { return ___delay_3; }
	inline float* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(float value)
	{
		___delay_3 = value;
	}

	inline static int32_t get_offset_of_durationIn_4() { return static_cast<int32_t>(offsetof(UIFade_t1631582502, ___durationIn_4)); }
	inline float get_durationIn_4() const { return ___durationIn_4; }
	inline float* get_address_of_durationIn_4() { return &___durationIn_4; }
	inline void set_durationIn_4(float value)
	{
		___durationIn_4 = value;
	}

	inline static int32_t get_offset_of_durationKeep_5() { return static_cast<int32_t>(offsetof(UIFade_t1631582502, ___durationKeep_5)); }
	inline float get_durationKeep_5() const { return ___durationKeep_5; }
	inline float* get_address_of_durationKeep_5() { return &___durationKeep_5; }
	inline void set_durationKeep_5(float value)
	{
		___durationKeep_5 = value;
	}

	inline static int32_t get_offset_of_durationOut_6() { return static_cast<int32_t>(offsetof(UIFade_t1631582502, ___durationOut_6)); }
	inline float get_durationOut_6() const { return ___durationOut_6; }
	inline float* get_address_of_durationOut_6() { return &___durationOut_6; }
	inline void set_durationOut_6(float value)
	{
		___durationOut_6 = value;
	}

	inline static int32_t get_offset_of_onBecameVisible_7() { return static_cast<int32_t>(offsetof(UIFade_t1631582502, ___onBecameVisible_7)); }
	inline UnityEvent_t408735097 * get_onBecameVisible_7() const { return ___onBecameVisible_7; }
	inline UnityEvent_t408735097 ** get_address_of_onBecameVisible_7() { return &___onBecameVisible_7; }
	inline void set_onBecameVisible_7(UnityEvent_t408735097 * value)
	{
		___onBecameVisible_7 = value;
		Il2CppCodeGenWriteBarrier(&___onBecameVisible_7, value);
	}

	inline static int32_t get_offset_of_onBecameInvisible_8() { return static_cast<int32_t>(offsetof(UIFade_t1631582502, ___onBecameInvisible_8)); }
	inline UnityEvent_t408735097 * get_onBecameInvisible_8() const { return ___onBecameInvisible_8; }
	inline UnityEvent_t408735097 ** get_address_of_onBecameInvisible_8() { return &___onBecameInvisible_8; }
	inline void set_onBecameInvisible_8(UnityEvent_t408735097 * value)
	{
		___onBecameInvisible_8 = value;
		Il2CppCodeGenWriteBarrier(&___onBecameInvisible_8, value);
	}

	inline static int32_t get_offset_of_m_GameObject_9() { return static_cast<int32_t>(offsetof(UIFade_t1631582502, ___m_GameObject_9)); }
	inline GameObject_t1756533147 * get_m_GameObject_9() const { return ___m_GameObject_9; }
	inline GameObject_t1756533147 ** get_address_of_m_GameObject_9() { return &___m_GameObject_9; }
	inline void set_m_GameObject_9(GameObject_t1756533147 * value)
	{
		___m_GameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_GameObject_9, value);
	}

	inline static int32_t get_offset_of_m_IsPlaying_10() { return static_cast<int32_t>(offsetof(UIFade_t1631582502, ___m_IsPlaying_10)); }
	inline bool get_m_IsPlaying_10() const { return ___m_IsPlaying_10; }
	inline bool* get_address_of_m_IsPlaying_10() { return &___m_IsPlaying_10; }
	inline void set_m_IsPlaying_10(bool value)
	{
		___m_IsPlaying_10 = value;
	}

	inline static int32_t get_offset_of_m_IsFadeOut_11() { return static_cast<int32_t>(offsetof(UIFade_t1631582502, ___m_IsFadeOut_11)); }
	inline bool get_m_IsFadeOut_11() const { return ___m_IsFadeOut_11; }
	inline bool* get_address_of_m_IsFadeOut_11() { return &___m_IsFadeOut_11; }
	inline void set_m_IsFadeOut_11(bool value)
	{
		___m_IsFadeOut_11 = value;
	}

	inline static int32_t get_offset_of_m_Duration_12() { return static_cast<int32_t>(offsetof(UIFade_t1631582502, ___m_Duration_12)); }
	inline float get_m_Duration_12() const { return ___m_Duration_12; }
	inline float* get_address_of_m_Duration_12() { return &___m_Duration_12; }
	inline void set_m_Duration_12(float value)
	{
		___m_Duration_12 = value;
	}

	inline static int32_t get_offset_of_m_Alpha_13() { return static_cast<int32_t>(offsetof(UIFade_t1631582502, ___m_Alpha_13)); }
	inline float get_m_Alpha_13() const { return ___m_Alpha_13; }
	inline float* get_address_of_m_Alpha_13() { return &___m_Alpha_13; }
	inline void set_m_Alpha_13(float value)
	{
		___m_Alpha_13 = value;
	}

	inline static int32_t get_offset_of_m_Graphics_14() { return static_cast<int32_t>(offsetof(UIFade_t1631582502, ___m_Graphics_14)); }
	inline List_1_t1795346708 * get_m_Graphics_14() const { return ___m_Graphics_14; }
	inline List_1_t1795346708 ** get_address_of_m_Graphics_14() { return &___m_Graphics_14; }
	inline void set_m_Graphics_14(List_1_t1795346708 * value)
	{
		___m_Graphics_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_Graphics_14, value);
	}

	inline static int32_t get_offset_of_m_Alphas_15() { return static_cast<int32_t>(offsetof(UIFade_t1631582502, ___m_Alphas_15)); }
	inline SingleU5BU5D_t577127397* get_m_Alphas_15() const { return ___m_Alphas_15; }
	inline SingleU5BU5D_t577127397** get_address_of_m_Alphas_15() { return &___m_Alphas_15; }
	inline void set_m_Alphas_15(SingleU5BU5D_t577127397* value)
	{
		___m_Alphas_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_Alphas_15, value);
	}

	inline static int32_t get_offset_of_m_DelayCount_16() { return static_cast<int32_t>(offsetof(UIFade_t1631582502, ___m_DelayCount_16)); }
	inline int32_t get_m_DelayCount_16() const { return ___m_DelayCount_16; }
	inline int32_t* get_address_of_m_DelayCount_16() { return &___m_DelayCount_16; }
	inline void set_m_DelayCount_16(int32_t value)
	{
		___m_DelayCount_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
