﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_ControllerB2189630288.h"

// ControllerInputGUI/ControllerEntry
struct ControllerEntry_t1998352496;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControllerInputGUI/ButtonEntry
struct  ButtonEntry_t1083102552  : public Il2CppObject
{
public:
	// ControllerInputGUI/ControllerEntry ControllerInputGUI/ButtonEntry::context
	ControllerEntry_t1998352496 * ___context_0;
	// Ximmerse.InputSystem.ControllerButton ControllerInputGUI/ButtonEntry::key
	int32_t ___key_1;
	// UnityEngine.GameObject ControllerInputGUI/ButtonEntry::m_GameObject
	GameObject_t1756533147 * ___m_GameObject_2;
	// UnityEngine.Transform ControllerInputGUI/ButtonEntry::m_Transform
	Transform_t3275118058 * ___m_Transform_3;
	// UnityEngine.UI.Graphic ControllerInputGUI/ButtonEntry::m_Graphic
	Graphic_t2426225576 * ___m_Graphic_4;
	// System.Single ControllerInputGUI/ButtonEntry::m_DefaultScale
	float ___m_DefaultScale_5;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(ButtonEntry_t1083102552, ___context_0)); }
	inline ControllerEntry_t1998352496 * get_context_0() const { return ___context_0; }
	inline ControllerEntry_t1998352496 ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(ControllerEntry_t1998352496 * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier(&___context_0, value);
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(ButtonEntry_t1083102552, ___key_1)); }
	inline int32_t get_key_1() const { return ___key_1; }
	inline int32_t* get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(int32_t value)
	{
		___key_1 = value;
	}

	inline static int32_t get_offset_of_m_GameObject_2() { return static_cast<int32_t>(offsetof(ButtonEntry_t1083102552, ___m_GameObject_2)); }
	inline GameObject_t1756533147 * get_m_GameObject_2() const { return ___m_GameObject_2; }
	inline GameObject_t1756533147 ** get_address_of_m_GameObject_2() { return &___m_GameObject_2; }
	inline void set_m_GameObject_2(GameObject_t1756533147 * value)
	{
		___m_GameObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_GameObject_2, value);
	}

	inline static int32_t get_offset_of_m_Transform_3() { return static_cast<int32_t>(offsetof(ButtonEntry_t1083102552, ___m_Transform_3)); }
	inline Transform_t3275118058 * get_m_Transform_3() const { return ___m_Transform_3; }
	inline Transform_t3275118058 ** get_address_of_m_Transform_3() { return &___m_Transform_3; }
	inline void set_m_Transform_3(Transform_t3275118058 * value)
	{
		___m_Transform_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_Transform_3, value);
	}

	inline static int32_t get_offset_of_m_Graphic_4() { return static_cast<int32_t>(offsetof(ButtonEntry_t1083102552, ___m_Graphic_4)); }
	inline Graphic_t2426225576 * get_m_Graphic_4() const { return ___m_Graphic_4; }
	inline Graphic_t2426225576 ** get_address_of_m_Graphic_4() { return &___m_Graphic_4; }
	inline void set_m_Graphic_4(Graphic_t2426225576 * value)
	{
		___m_Graphic_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_Graphic_4, value);
	}

	inline static int32_t get_offset_of_m_DefaultScale_5() { return static_cast<int32_t>(offsetof(ButtonEntry_t1083102552, ___m_DefaultScale_5)); }
	inline float get_m_DefaultScale_5() const { return ___m_DefaultScale_5; }
	inline float* get_address_of_m_DefaultScale_5() { return &___m_DefaultScale_5; }
	inline void set_m_DefaultScale_5(float value)
	{
		___m_DefaultScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
