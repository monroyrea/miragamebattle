﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Ximmerse_InputSystem_DeviceConne3617794665.h"

// System.String
struct String_t;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceStatus/DeviceUI
struct  DeviceUI_t3797029069  : public Il2CppObject
{
public:
	// System.String DeviceStatus/DeviceUI::deviceName
	String_t* ___deviceName_0;
	// UnityEngine.GameObject[] DeviceStatus/DeviceUI::connectUI
	GameObjectU5BU5D_t3057952154* ___connectUI_1;
	// UnityEngine.UI.Image DeviceStatus/DeviceUI::battImage
	Image_t2042527209 * ___battImage_2;
	// UnityEngine.UI.Text DeviceStatus/DeviceUI::battText
	Text_t356221433 * ___battText_3;
	// UnityEngine.Sprite[] DeviceStatus/DeviceUI::battSprites
	SpriteU5BU5D_t3359083662* ___battSprites_4;
	// System.Int32 DeviceStatus/DeviceUI::handle
	int32_t ___handle_5;
	// Ximmerse.InputSystem.DeviceConnectionState DeviceStatus/DeviceUI::connectionState
	int32_t ___connectionState_6;

public:
	inline static int32_t get_offset_of_deviceName_0() { return static_cast<int32_t>(offsetof(DeviceUI_t3797029069, ___deviceName_0)); }
	inline String_t* get_deviceName_0() const { return ___deviceName_0; }
	inline String_t** get_address_of_deviceName_0() { return &___deviceName_0; }
	inline void set_deviceName_0(String_t* value)
	{
		___deviceName_0 = value;
		Il2CppCodeGenWriteBarrier(&___deviceName_0, value);
	}

	inline static int32_t get_offset_of_connectUI_1() { return static_cast<int32_t>(offsetof(DeviceUI_t3797029069, ___connectUI_1)); }
	inline GameObjectU5BU5D_t3057952154* get_connectUI_1() const { return ___connectUI_1; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_connectUI_1() { return &___connectUI_1; }
	inline void set_connectUI_1(GameObjectU5BU5D_t3057952154* value)
	{
		___connectUI_1 = value;
		Il2CppCodeGenWriteBarrier(&___connectUI_1, value);
	}

	inline static int32_t get_offset_of_battImage_2() { return static_cast<int32_t>(offsetof(DeviceUI_t3797029069, ___battImage_2)); }
	inline Image_t2042527209 * get_battImage_2() const { return ___battImage_2; }
	inline Image_t2042527209 ** get_address_of_battImage_2() { return &___battImage_2; }
	inline void set_battImage_2(Image_t2042527209 * value)
	{
		___battImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___battImage_2, value);
	}

	inline static int32_t get_offset_of_battText_3() { return static_cast<int32_t>(offsetof(DeviceUI_t3797029069, ___battText_3)); }
	inline Text_t356221433 * get_battText_3() const { return ___battText_3; }
	inline Text_t356221433 ** get_address_of_battText_3() { return &___battText_3; }
	inline void set_battText_3(Text_t356221433 * value)
	{
		___battText_3 = value;
		Il2CppCodeGenWriteBarrier(&___battText_3, value);
	}

	inline static int32_t get_offset_of_battSprites_4() { return static_cast<int32_t>(offsetof(DeviceUI_t3797029069, ___battSprites_4)); }
	inline SpriteU5BU5D_t3359083662* get_battSprites_4() const { return ___battSprites_4; }
	inline SpriteU5BU5D_t3359083662** get_address_of_battSprites_4() { return &___battSprites_4; }
	inline void set_battSprites_4(SpriteU5BU5D_t3359083662* value)
	{
		___battSprites_4 = value;
		Il2CppCodeGenWriteBarrier(&___battSprites_4, value);
	}

	inline static int32_t get_offset_of_handle_5() { return static_cast<int32_t>(offsetof(DeviceUI_t3797029069, ___handle_5)); }
	inline int32_t get_handle_5() const { return ___handle_5; }
	inline int32_t* get_address_of_handle_5() { return &___handle_5; }
	inline void set_handle_5(int32_t value)
	{
		___handle_5 = value;
	}

	inline static int32_t get_offset_of_connectionState_6() { return static_cast<int32_t>(offsetof(DeviceUI_t3797029069, ___connectionState_6)); }
	inline int32_t get_connectionState_6() const { return ___connectionState_6; }
	inline int32_t* get_address_of_connectionState_6() { return &___connectionState_6; }
	inline void set_connectionState_6(int32_t value)
	{
		___connectionState_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
