﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3228926346.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_ControllerEventManager1492384657.h"
#include "AssemblyU2DCSharp_ControllerEventManager_Controlle4149645536.h"
#include "AssemblyU2DCSharp_ControllerEventManager_SendContr1059044385.h"
#include "AssemblyU2DCSharp_ControllerHover24802818.h"
#include "AssemblyU2DCSharp_FaceCamera2774504826.h"
#include "AssemblyU2DCSharp_LaserPointerLength1503249384.h"
#include "AssemblyU2DCSharp_MiraAnimation2846206407.h"
#include "AssemblyU2DCSharp_MiraAnimation_U3CampUpU3Ec__Iter4073898544.h"
#include "AssemblyU2DCSharp_MiraAnimation_U3CampAnimU3Ec__It3607870765.h"
#include "AssemblyU2DCSharp_MiraAnimation_U3CampDownU3Ec__It1031483199.h"
#include "AssemblyU2DCSharp_MiraHUD2762852056.h"
#include "AssemblyU2DCSharp_NotificationIcon332970646.h"
#include "AssemblyU2DCSharp_NotificationMenu1031876728.h"
#include "AssemblyU2DCSharp_NotificationMenu_U3CFadeInU3Ec__4261807277.h"
#include "AssemblyU2DCSharp_NotificationMenu_U3CFadeOutU3Ec_2399530081.h"
#include "AssemblyU2DCSharp_SnapToController261557653.h"
#include "AssemblyU2DCSharp_UIManager2519183485.h"
#include "AssemblyU2DCSharp_UIManager_U3ClerpSpacingU3Ec__Ite211701475.h"
#include "AssemblyU2DCSharp_UIObject4279159643.h"
#include "AssemblyU2DCSharp_FlyBehaviour_RigidbodyVelocity2153512425.h"
#include "AssemblyU2DCSharp_FlyBehaviour_TargetTransform2083404636.h"
#include "AssemblyU2DCSharp_Kudan_AR_MarkerlessTouch2306729296.h"
#include "AssemblyU2DCSharp_Kudan_AR_Samples_SampleApp2905726073.h"
#include "AssemblyU2DCSharp_Kudan_AR_Samples_SampleScripting4166834163.h"
#include "AssemblyU2DCSharp_ActivatedScript2824515540.h"
#include "AssemblyU2DCSharp_TouchControl1205959460.h"
#include "AssemblyU2DCSharp_Kudan_AR_MarkerEvents4250814921.h"
#include "AssemblyU2DCSharp_Kudan_AR_MarkerEvents_markerStru3406019119.h"
#include "AssemblyU2DCSharp_MarkererlessEvents3908334101.h"
#include "AssemblyU2DCSharp_MarkererlessEvents_markerStruct742852367.h"
#include "AssemblyU2DCSharp_Kudan_AR_NativeInterface3394340732.h"
#include "AssemblyU2DCSharp_Kudan_AR_MarkerLostEvent859612542.h"
#include "AssemblyU2DCSharp_Kudan_AR_MarkerFoundEvent1034540096.h"
#include "AssemblyU2DCSharp_Kudan_AR_MarkerUpdateEvent2836667359.h"
#include "AssemblyU2DCSharp_Kudan_AR_Trackable2014667805.h"
#include "AssemblyU2DCSharp_Kudan_AR_Tracker3774580070.h"
#include "AssemblyU2DCSharp_Kudan_AR_TrackerBase1411547817.h"
#include "AssemblyU2DCSharp_Kudan_AR_KudanTracker2134143885.h"
#include "AssemblyU2DCSharp_Kudan_AR_KudanTracker_U3CScreens1813530525.h"
#include "AssemblyU2DCSharp_Kudan_AR_TargetNode1452737769.h"
#include "AssemblyU2DCSharp_Kudan_AR_TrackableData3748763085.h"
#include "AssemblyU2DCSharp_Kudan_AR_TrackingMethodBase2810511357.h"
#include "AssemblyU2DCSharp_Kudan_AR_TrackingMethodMarker3831798236.h"
#include "AssemblyU2DCSharp_Kudan_AR_TrackingMethodMarkerless432022491.h"
#include "AssemblyU2DCSharp_Kudan_AR_MarkerTransformDriver337329646.h"
#include "AssemblyU2DCSharp_Kudan_AR_MarkerlessTransformDriv1624900809.h"
#include "AssemblyU2DCSharp_Kudan_AR_TransformDriverBase2557076579.h"
#include "AssemblyU2DCSharp_ControllerTarget3193765035.h"
#include "AssemblyU2DCSharp_ControllerTransform2066672932.h"
#include "AssemblyU2DCSharp_DistortionCamera2025071212.h"
#include "AssemblyU2DCSharp_GyroController3857203917.h"
#include "AssemblyU2DCSharp_Line2729441502.h"
#include "AssemblyU2DCSharp_MiraArController3896789088.h"
#include "AssemblyU2DCSharp_MiraArController_MarkerState2127725244.h"
#include "AssemblyU2DCSharp_MiraArController_MarkerList3622290753.h"
#include "AssemblyU2DCSharp_MiraArController_CommonMarkerList285872970.h"
#include "AssemblyU2DCSharp_MiraEventManager364232676.h"
#include "AssemblyU2DCSharp_MiraMarkerEvents1700133524.h"
#include "AssemblyU2DCSharp_MiraPostRender3202756259.h"
#include "AssemblyU2DCSharp_MiraPostRender_Eye273978457.h"
#include "AssemblyU2DCSharp_MiraPreRender2810281508.h"
#include "AssemblyU2DCSharp_MiraReticle2195475589.h"
#include "AssemblyU2DCSharp_MiraViewer256795227.h"
#include "AssemblyU2DCSharp_MiraViewer_CameraNames2647135469.h"
#include "AssemblyU2DCSharp_TrackingHandoff210633217.h"
#include "AssemblyU2DCSharp_TrackingInteractable149194413.h"
#include "AssemblyU2DCSharp_TrackingInteractable_U3CLostTrac2706263904.h"
#include "AssemblyU2DCSharp_ApplicationManager2110631419.h"
#include "AssemblyU2DCSharp_BattleController3050984398.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1704[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1705[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1706[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1707[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1708[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1711[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1712[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1713[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1714[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1719[11] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Uv2S_4(),
	VertexHelper_t385374196::get_offset_of_m_Uv3S_5(),
	VertexHelper_t385374196::get_offset_of_m_Normals_6(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_7(),
	VertexHelper_t385374196::get_offset_of_m_Indices_8(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1721[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1726[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1727[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (ControllerEventManager_t1492384657), -1, sizeof(ControllerEventManager_t1492384657_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1730[27] = 
{
	ControllerEventManager_t1492384657_StaticFields::get_offset_of_created_2(),
	ControllerEventManager_t1492384657_StaticFields::get_offset_of_accelerometer_3(),
	ControllerEventManager_t1492384657_StaticFields::get_offset_of_gyroscope_4(),
	ControllerEventManager_t1492384657::get_offset_of_count_5(),
	ControllerEventManager_t1492384657::get_offset_of_controlText_6(),
	ControllerEventManager_t1492384657_StaticFields::get_offset_of_controller_7(),
	ControllerEventManager_t1492384657::get_offset_of_trigger_8(),
	ControllerEventManager_t1492384657::get_offset_of_start_9(),
	ControllerEventManager_t1492384657::get_offset_of_back_10(),
	ControllerEventManager_t1492384657::get_offset_of_touchPadUp_11(),
	ControllerEventManager_t1492384657::get_offset_of_touchPadDown_12(),
	ControllerEventManager_t1492384657::get_offset_of_touchPadLeft_13(),
	ControllerEventManager_t1492384657::get_offset_of_touchPadRight_14(),
	ControllerEventManager_t1492384657::get_offset_of_touchPadX_15(),
	ControllerEventManager_t1492384657::get_offset_of_touchPadY_16(),
	ControllerEventManager_t1492384657_StaticFields::get_offset_of_TriggerOn_17(),
	ControllerEventManager_t1492384657_StaticFields::get_offset_of_TriggerStart_18(),
	ControllerEventManager_t1492384657_StaticFields::get_offset_of_TriggerEnd_19(),
	ControllerEventManager_t1492384657_StaticFields::get_offset_of_StartOn_20(),
	ControllerEventManager_t1492384657_StaticFields::get_offset_of_StartDown_21(),
	ControllerEventManager_t1492384657_StaticFields::get_offset_of_StartUp_22(),
	ControllerEventManager_t1492384657_StaticFields::get_offset_of_BackOn_23(),
	ControllerEventManager_t1492384657_StaticFields::get_offset_of_BackDown_24(),
	ControllerEventManager_t1492384657_StaticFields::get_offset_of_BackUp_25(),
	ControllerEventManager_t1492384657_StaticFields::get_offset_of_TouchPadUpClick_26(),
	ControllerEventManager_t1492384657_StaticFields::get_offset_of_SendTouchPadY_27(),
	ControllerEventManager_t1492384657_StaticFields::get_offset_of_SendTouchPadX_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (ControllerClick_t4149645536), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (SendControllerAxis_t1059044385), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (ControllerHover_t24802818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1733[8] = 
{
	ControllerHover_t24802818::get_offset_of_scaleChange_2(),
	ControllerHover_t24802818::get_offset_of_ogScale_3(),
	ControllerHover_t24802818::get_offset_of_finalScaleUp_4(),
	ControllerHover_t24802818::get_offset_of_finalScaleDown_5(),
	ControllerHover_t24802818::get_offset_of_currentScale_6(),
	ControllerHover_t24802818::get_offset_of_ogColor_7(),
	ControllerHover_t24802818::get_offset_of_holverColor_8(),
	ControllerHover_t24802818::get_offset_of_hovering_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (FaceCamera_t2774504826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1734[1] = 
{
	FaceCamera_t2774504826::get_offset_of_stereoCamRig_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (LaserPointerLength_t1503249384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1735[2] = 
{
	LaserPointerLength_t1503249384::get_offset_of_reticle_2(),
	LaserPointerLength_t1503249384::get_offset_of_originalScale_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (MiraAnimation_t2846206407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1736[16] = 
{
	MiraAnimation_t2846206407::get_offset_of_m_2(),
	MiraAnimation_t2846206407::get_offset_of_i_3(),
	MiraAnimation_t2846206407::get_offset_of_r_4(),
	MiraAnimation_t2846206407::get_offset_of_a_5(),
	MiraAnimation_t2846206407::get_offset_of_mPos_6(),
	MiraAnimation_t2846206407::get_offset_of_iPos_7(),
	MiraAnimation_t2846206407::get_offset_of_rPos_8(),
	MiraAnimation_t2846206407::get_offset_of_aPos_9(),
	MiraAnimation_t2846206407::get_offset_of_amplitude_10(),
	MiraAnimation_t2846206407::get_offset_of_lerpAmp_11(),
	MiraAnimation_t2846206407::get_offset_of_letterOffset_12(),
	MiraAnimation_t2846206407::get_offset_of_timer_13(),
	MiraAnimation_t2846206407::get_offset_of_timeMult_14(),
	MiraAnimation_t2846206407::get_offset_of_lerpAmpSet_15(),
	MiraAnimation_t2846206407::get_offset_of_activated_16(),
	MiraAnimation_t2846206407::get_offset_of_currentCoroutine_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (U3CampUpU3Ec__Iterator0_t4073898544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[4] = 
{
	U3CampUpU3Ec__Iterator0_t4073898544::get_offset_of_U24this_0(),
	U3CampUpU3Ec__Iterator0_t4073898544::get_offset_of_U24current_1(),
	U3CampUpU3Ec__Iterator0_t4073898544::get_offset_of_U24disposing_2(),
	U3CampUpU3Ec__Iterator0_t4073898544::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (U3CampAnimU3Ec__Iterator1_t3607870765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1738[4] = 
{
	U3CampAnimU3Ec__Iterator1_t3607870765::get_offset_of_U24this_0(),
	U3CampAnimU3Ec__Iterator1_t3607870765::get_offset_of_U24current_1(),
	U3CampAnimU3Ec__Iterator1_t3607870765::get_offset_of_U24disposing_2(),
	U3CampAnimU3Ec__Iterator1_t3607870765::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (U3CampDownU3Ec__Iterator2_t1031483199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1739[4] = 
{
	U3CampDownU3Ec__Iterator2_t1031483199::get_offset_of_U24this_0(),
	U3CampDownU3Ec__Iterator2_t1031483199::get_offset_of_U24current_1(),
	U3CampDownU3Ec__Iterator2_t1031483199::get_offset_of_U24disposing_2(),
	U3CampDownU3Ec__Iterator2_t1031483199::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (MiraHUD_t2762852056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1740[3] = 
{
	MiraHUD_t2762852056::get_offset_of_trackingText_2(),
	MiraHUD_t2762852056::get_offset_of_trackingColor_3(),
	MiraHUD_t2762852056::get_offset_of_notTrackingColor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (NotificationIcon_t332970646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1741[5] = 
{
	NotificationIcon_t332970646::get_offset_of_iconBoundsX_2(),
	NotificationIcon_t332970646::get_offset_of_OgRotationLU_3(),
	NotificationIcon_t332970646::get_offset_of_mat_4(),
	NotificationIcon_t332970646::get_offset_of_ogClr_5(),
	NotificationIcon_t332970646::get_offset_of_lerpClr_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (NotificationMenu_t1031876728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1742[9] = 
{
	NotificationMenu_t1031876728::get_offset_of_maxAngle_10(),
	NotificationMenu_t1031876728::get_offset_of_height_11(),
	NotificationMenu_t1031876728::get_offset_of_centerCollider_12(),
	NotificationMenu_t1031876728::get_offset_of_zRadiusLU_13(),
	NotificationMenu_t1031876728::get_offset_of_padding_14(),
	NotificationMenu_t1031876728::get_offset_of_icons_15(),
	NotificationMenu_t1031876728::get_offset_of_elevationAngle_16(),
	NotificationMenu_t1031876728::get_offset_of_fadeTime_17(),
	NotificationMenu_t1031876728::get_offset_of_currentFadeTimer_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (U3CFadeInU3Ec__Iterator0_t4261807277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1743[5] = 
{
	U3CFadeInU3Ec__Iterator0_t4261807277::get_offset_of_U24locvar0_0(),
	U3CFadeInU3Ec__Iterator0_t4261807277::get_offset_of_U24this_1(),
	U3CFadeInU3Ec__Iterator0_t4261807277::get_offset_of_U24current_2(),
	U3CFadeInU3Ec__Iterator0_t4261807277::get_offset_of_U24disposing_3(),
	U3CFadeInU3Ec__Iterator0_t4261807277::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (U3CFadeOutU3Ec__Iterator1_t2399530081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1744[5] = 
{
	U3CFadeOutU3Ec__Iterator1_t2399530081::get_offset_of_U24locvar0_0(),
	U3CFadeOutU3Ec__Iterator1_t2399530081::get_offset_of_U24this_1(),
	U3CFadeOutU3Ec__Iterator1_t2399530081::get_offset_of_U24current_2(),
	U3CFadeOutU3Ec__Iterator1_t2399530081::get_offset_of_U24disposing_3(),
	U3CFadeOutU3Ec__Iterator1_t2399530081::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (SnapToController_t261557653), -1, sizeof(SnapToController_t261557653_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1745[8] = 
{
	SnapToController_t261557653::get_offset_of_parentedToController_10(),
	SnapToController_t261557653::get_offset_of_controller_11(),
	SnapToController_t261557653::get_offset_of_castMultiplier_12(),
	SnapToController_t261557653::get_offset_of_defaultDistanceZ_13(),
	SnapToController_t261557653::get_offset_of_castStart_14(),
	SnapToController_t261557653::get_offset_of_lastCast_15(),
	SnapToController_t261557653_StaticFields::get_offset_of_triggerToggle_16(),
	SnapToController_t261557653::get_offset_of_thisCollider_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (UIManager_t2519183485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1746[9] = 
{
	UIManager_t2519183485::get_offset_of_UIAngle_2(),
	UIManager_t2519183485::get_offset_of_zRadius_3(),
	UIManager_t2519183485::get_offset_of_height_4(),
	UIManager_t2519183485::get_offset_of_padding_5(),
	UIManager_t2519183485::get_offset_of_setLerpSpaceTime_6(),
	UIManager_t2519183485::get_offset_of_lerpSpaceTime_7(),
	UIManager_t2519183485::get_offset_of_UIObj_8(),
	UIManager_t2519183485::get_offset_of_lerpSpaceCo_9(),
	UIManager_t2519183485::get_offset_of_firstLerp_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (U3ClerpSpacingU3Ec__Iterator0_t211701475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1747[5] = 
{
	U3ClerpSpacingU3Ec__Iterator0_t211701475::get_offset_of_U24locvar0_0(),
	U3ClerpSpacingU3Ec__Iterator0_t211701475::get_offset_of_U24this_1(),
	U3ClerpSpacingU3Ec__Iterator0_t211701475::get_offset_of_U24current_2(),
	U3ClerpSpacingU3Ec__Iterator0_t211701475::get_offset_of_U24disposing_3(),
	U3ClerpSpacingU3Ec__Iterator0_t211701475::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (UIObject_t4279159643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1748[11] = 
{
	UIObject_t4279159643::get_offset_of_UIObjIndex_9(),
	UIObject_t4279159643::get_offset_of_BoundsX_10(),
	UIObject_t4279159643::get_offset_of_inUI_11(),
	UIObject_t4279159643::get_offset_of_mainUIManager_12(),
	UIObject_t4279159643::get_offset_of_UIOgRotation_13(),
	UIObject_t4279159643::get_offset_of_currentPos_14(),
	UIObject_t4279159643::get_offset_of_lerpPos_15(),
	UIObject_t4279159643::get_offset_of_currentRot_16(),
	UIObject_t4279159643::get_offset_of_lerpRot_17(),
	UIObject_t4279159643::get_offset_of_dontLerp_18(),
	UIObject_t4279159643::get_offset_of_rotateSpeed_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (FlyBehaviour_RigidbodyVelocity_t2153512425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1749[8] = 
{
	FlyBehaviour_RigidbodyVelocity_t2153512425::get_offset_of_distance_2(),
	FlyBehaviour_RigidbodyVelocity_t2153512425::get_offset_of_minThresh_3(),
	FlyBehaviour_RigidbodyVelocity_t2153512425::get_offset_of_maxThresh_4(),
	FlyBehaviour_RigidbodyVelocity_t2153512425::get_offset_of_currentTime_5(),
	FlyBehaviour_RigidbodyVelocity_t2153512425::get_offset_of_switchTime_6(),
	FlyBehaviour_RigidbodyVelocity_t2153512425::get_offset_of_currentVel_7(),
	FlyBehaviour_RigidbodyVelocity_t2153512425::get_offset_of_topSpeed_8(),
	FlyBehaviour_RigidbodyVelocity_t2153512425::get_offset_of_brother_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (FlyBehaviour_TargetTransform_t2083404636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1750[8] = 
{
	FlyBehaviour_TargetTransform_t2083404636::get_offset_of_distance_2(),
	FlyBehaviour_TargetTransform_t2083404636::get_offset_of_minThresh_3(),
	FlyBehaviour_TargetTransform_t2083404636::get_offset_of_maxThresh_4(),
	FlyBehaviour_TargetTransform_t2083404636::get_offset_of_moveThresh_5(),
	FlyBehaviour_TargetTransform_t2083404636::get_offset_of_targetVec_6(),
	FlyBehaviour_TargetTransform_t2083404636::get_offset_of_moveStep_7(),
	FlyBehaviour_TargetTransform_t2083404636::get_offset_of_maxMove_8(),
	FlyBehaviour_TargetTransform_t2083404636::get_offset_of_brother_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (MarkerlessTouch_t2306729296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1751[7] = 
{
	MarkerlessTouch_t2306729296::get_offset_of_tracker_2(),
	MarkerlessTouch_t2306729296::get_offset_of_interactableObject_3(),
	MarkerlessTouch_t2306729296::get_offset_of_moveSpeed_4(),
	MarkerlessTouch_t2306729296::get_offset_of_roughDiff_5(),
	MarkerlessTouch_t2306729296::get_offset_of_tap_6(),
	MarkerlessTouch_t2306729296::get_offset_of_startPos_7(),
	MarkerlessTouch_t2306729296::get_offset_of_endPos_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (SampleApp_t2905726073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1752[4] = 
{
	SampleApp_t2905726073::get_offset_of__kudanTracker_2(),
	SampleApp_t2905726073::get_offset_of__markerTracking_3(),
	SampleApp_t2905726073::get_offset_of__markerlessTracking_4(),
	SampleApp_t2905726073::get_offset_of_buttonText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (SampleScripting_t4166834163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1753[3] = 
{
	SampleScripting_t4166834163::get_offset_of__kudanTracker_2(),
	SampleScripting_t4166834163::get_offset_of__markerTracking_3(),
	SampleScripting_t4166834163::get_offset_of__markerlessTracking_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (ActivatedScript_t2824515540), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (TouchControl_t1205959460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1755[6] = 
{
	TouchControl_t1205959460::get_offset_of_zoomSpeed_2(),
	TouchControl_t1205959460::get_offset_of_moveSpeed_3(),
	TouchControl_t1205959460::get_offset_of_roughDiff_4(),
	TouchControl_t1205959460::get_offset_of_startPos_5(),
	TouchControl_t1205959460::get_offset_of_endPos_6(),
	TouchControl_t1205959460::get_offset_of_activate_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (MarkerEvents_t4250814921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1757[5] = 
{
	MarkerEvents_t4250814921::get_offset_of_numMaxEventTracking_2(),
	MarkerEvents_t4250814921::get_offset_of_trackingMethodMarker_3(),
	MarkerEvents_t4250814921::get_offset_of_trackables_4(),
	MarkerEvents_t4250814921::get_offset_of_markerArray_5(),
	MarkerEvents_t4250814921::get_offset_of_markerStructs_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (markerStruct_t3406019119)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1758[4] = 
{
	markerStruct_t3406019119::get_offset_of_marker_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	markerStruct_t3406019119::get_offset_of_name_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	markerStruct_t3406019119::get_offset_of_isActive_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	markerStruct_t3406019119::get_offset_of_wasActive_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (MarkererlessEvents_t3908334101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1759[3] = 
{
	MarkererlessEvents_t3908334101::get_offset_of_markerlessArray_2(),
	MarkererlessEvents_t3908334101::get_offset_of_markerlessObjs_3(),
	MarkererlessEvents_t3908334101::get_offset_of_numMaxEventTracking_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (markerStruct_t742852367)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1760[4] = 
{
	markerStruct_t742852367::get_offset_of_markerless_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	markerStruct_t742852367::get_offset_of_name_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	markerStruct_t742852367::get_offset_of_isActive_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	markerStruct_t742852367::get_offset_of_wasActive_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (NativeInterface_t3394340732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1761[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (MarkerLostEvent_t859612542), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (MarkerFoundEvent_t1034540096), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (MarkerUpdateEvent_t2836667359), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (Trackable_t2014667805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[7] = 
{
	Trackable_t2014667805::get_offset_of_name_0(),
	Trackable_t2014667805::get_offset_of_width_1(),
	Trackable_t2014667805::get_offset_of_height_2(),
	Trackable_t2014667805::get_offset_of_isDetected_3(),
	Trackable_t2014667805::get_offset_of_position_4(),
	Trackable_t2014667805::get_offset_of_orientation_5(),
	Trackable_t2014667805::get_offset_of_trackingMethod_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (Tracker_t3774580070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1766[16] = 
{
	Tracker_t3774580070::get_offset_of__background_14(),
	Tracker_t3774580070::get_offset_of__cameraBackgroundMeshFilter_15(),
	Tracker_t3774580070::get_offset_of__textureYp_16(),
	Tracker_t3774580070::get_offset_of__textureYpID_17(),
	Tracker_t3774580070::get_offset_of__textureCbCr_18(),
	Tracker_t3774580070::get_offset_of__textureCbCrID_19(),
	Tracker_t3774580070::get_offset_of__cameraAspect_20(),
	Tracker_t3774580070::get_offset_of__prevScreenOrientation_21(),
	Tracker_t3774580070::get_offset_of__projectionRotation_22(),
	0,
	Tracker_t3774580070::get_offset_of__numFramesGrabbed_24(),
	Tracker_t3774580070::get_offset_of__numFramesTracked_25(),
	Tracker_t3774580070::get_offset_of__numFramesProcessed_26(),
	Tracker_t3774580070::get_offset_of__numFramesRendered_27(),
	Tracker_t3774580070::get_offset_of__rateTimer_28(),
	Tracker_t3774580070::get_offset_of_receivingInput_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (TrackerBase_t1411547817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[14] = 
{
	0,
	TrackerBase_t1411547817::get_offset_of__trackables_1(),
	TrackerBase_t1411547817::get_offset_of__cameraNearPlane_2(),
	TrackerBase_t1411547817::get_offset_of__cameraFarPlane_3(),
	TrackerBase_t1411547817::get_offset_of__floorHeight_4(),
	TrackerBase_t1411547817::get_offset_of__trackingThread_5(),
	TrackerBase_t1411547817::get_offset_of__isTrackingRunning_6(),
	TrackerBase_t1411547817::get_offset_of__finalTexture_7(),
	TrackerBase_t1411547817::get_offset_of__projectionMatrix_8(),
	TrackerBase_t1411547817::get_offset_of__detected_9(),
	TrackerBase_t1411547817::get_offset_of__cameraRate_10(),
	TrackerBase_t1411547817::get_offset_of__trackerRate_11(),
	TrackerBase_t1411547817::get_offset_of__appRate_12(),
	TrackerBase_t1411547817::get_offset_of__clonedTexture_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (KudanTracker_t2134143885), -1, sizeof(KudanTracker_t2134143885_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1768[23] = 
{
	KudanTracker_t2134143885_StaticFields::get_offset_of_kudanTracker_2(),
	0,
	0,
	KudanTracker_t2134143885::get_offset_of__EditorAPIKey_5(),
	KudanTracker_t2134143885::get_offset_of__trackerPlugin_6(),
	KudanTracker_t2134143885::get_offset_of__lastDetectedTrackables_7(),
	KudanTracker_t2134143885::get_offset_of__APIKey_8(),
	KudanTracker_t2134143885::get_offset_of__defaultTrackingMethod_9(),
	KudanTracker_t2134143885::get_offset_of__trackingMethods_10(),
	KudanTracker_t2134143885::get_offset_of__useFrontFacingCameraOnMobile_11(),
	KudanTracker_t2134143885::get_offset_of__markerRecoveryMode_12(),
	KudanTracker_t2134143885::get_offset_of__markerAutoCrop_13(),
	KudanTracker_t2134143885::get_offset_of__markerExtensibility_14(),
	KudanTracker_t2134143885::get_offset_of__makePersistent_15(),
	KudanTracker_t2134143885::get_offset_of__startOnEnable_16(),
	KudanTracker_t2134143885::get_offset_of__applyProjection_17(),
	KudanTracker_t2134143885::get_offset_of__renderingCamera_18(),
	KudanTracker_t2134143885::get_offset_of__background_19(),
	KudanTracker_t2134143885::get_offset_of__displayDebugGUI_20(),
	KudanTracker_t2134143885::get_offset_of__debugGUIScale_21(),
	KudanTracker_t2134143885::get_offset_of__debugFlatShader_22(),
	KudanTracker_t2134143885::get_offset_of__currentTrackingMethod_23(),
	KudanTracker_t2134143885::get_offset_of__lineMaterial_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (U3CScreenshotU3Ec__Iterator0_t1813530525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1769[10] = 
{
	U3CScreenshotU3Ec__Iterator0_t1813530525::get_offset_of_U3CuiObjectsU3E__0_0(),
	U3CScreenshotU3Ec__Iterator0_t1813530525::get_offset_of_U3CwasDebugU3E__0_1(),
	U3CScreenshotU3Ec__Iterator0_t1813530525::get_offset_of_U3CRTU3E__0_2(),
	U3CScreenshotU3Ec__Iterator0_t1813530525::get_offset_of_U3CscreenU3E__0_3(),
	U3CScreenshotU3Ec__Iterator0_t1813530525::get_offset_of_U3CbytesU3E__0_4(),
	U3CScreenshotU3Ec__Iterator0_t1813530525::get_offset_of_U3CfilePathU3E__0_5(),
	U3CScreenshotU3Ec__Iterator0_t1813530525::get_offset_of_U24this_6(),
	U3CScreenshotU3Ec__Iterator0_t1813530525::get_offset_of_U24current_7(),
	U3CScreenshotU3Ec__Iterator0_t1813530525::get_offset_of_U24disposing_8(),
	U3CScreenshotU3Ec__Iterator0_t1813530525::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (TargetNode_t1452737769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1770[4] = 
{
	TargetNode_t1452737769::get_offset_of_markerless_2(),
	TargetNode_t1452737769::get_offset_of_tracker_3(),
	TargetNode_t1452737769::get_offset_of_activeObj_4(),
	TargetNode_t1452737769::get_offset_of_target_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (TrackableData_t3748763085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1771[3] = 
{
	TrackableData_t3748763085::get_offset_of_id_2(),
	TrackableData_t3748763085::get_offset_of_image_3(),
	TrackableData_t3748763085::get_offset_of_data_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (TrackingMethodBase_t2810511357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1772[2] = 
{
	TrackingMethodBase_t2810511357::get_offset_of__kudanTracker_2(),
	TrackingMethodBase_t2810511357::get_offset_of__isTrackingEnabled_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (TrackingMethodMarker_t3831798236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1773[5] = 
{
	TrackingMethodMarker_t3831798236::get_offset_of__markers_4(),
	TrackingMethodMarker_t3831798236::get_offset_of__foundMarkerEvent_5(),
	TrackingMethodMarker_t3831798236::get_offset_of__lostMarkerEvent_6(),
	TrackingMethodMarker_t3831798236::get_offset_of__updateMarkerEvent_7(),
	TrackingMethodMarker_t3831798236::get_offset_of__lastDetectedTrackables_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (TrackingMethodMarkerless_t432022491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[2] = 
{
	TrackingMethodMarkerless_t432022491::get_offset_of__updateMarkerEvent_4(),
	TrackingMethodMarkerless_t432022491::get_offset_of__floorDepth_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (MarkerTransformDriver_t337329646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1775[8] = 
{
	0,
	MarkerTransformDriver_t337329646::get_offset_of__expectedId_4(),
	MarkerTransformDriver_t337329646::get_offset_of__applyMarkerScale_5(),
	MarkerTransformDriver_t337329646::get_offset_of__alwaysDrawMarkerPlane_6(),
	MarkerTransformDriver_t337329646::get_offset_of__markerPlaneWidth_7(),
	MarkerTransformDriver_t337329646::get_offset_of__markerPlaneHeight_8(),
	MarkerTransformDriver_t337329646::get_offset_of__trackableId_9(),
	MarkerTransformDriver_t337329646::get_offset_of__tracker_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (MarkerlessTransformDriver_t1624900809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1776[1] = 
{
	MarkerlessTransformDriver_t1624900809::get_offset_of__tracker_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (TransformDriverBase_t2557076579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1777[1] = 
{
	TransformDriverBase_t2557076579::get_offset_of__trackerBase_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (ControllerTarget_t3193765035), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (ControllerTransform_t2066672932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1779[13] = 
{
	ControllerTransform_t2066672932::get_offset_of_allowControllerClick_2(),
	ControllerTransform_t2066672932::get_offset_of_recenterButton_3(),
	ControllerTransform_t2066672932::get_offset_of_rotateToCamButton_4(),
	ControllerTransform_t2066672932::get_offset_of_horizontalOnly_5(),
	ControllerTransform_t2066672932::get_offset_of_controller_6(),
	ControllerTransform_t2066672932::get_offset_of_offsetRotation_7(),
	ControllerTransform_t2066672932::get_offset_of_trackPosition_8(),
	ControllerTransform_t2066672932::get_offset_of_gyro_9(),
	ControllerTransform_t2066672932::get_offset_of_frameRate_10(),
	ControllerTransform_t2066672932::get_offset_of_sceneScaleMult_11(),
	ControllerTransform_t2066672932::get_offset_of_sceneOffsetX_12(),
	ControllerTransform_t2066672932::get_offset_of_sceneOffsetY_13(),
	ControllerTransform_t2066672932::get_offset_of_sceneOffsetZ_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (DistortionCamera_t2025071212), -1, sizeof(DistortionCamera_t2025071212_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1780[1] = 
{
	DistortionCamera_t2025071212_StaticFields::get_offset_of_instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (GyroController_t3857203917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1781[14] = 
{
	GyroController_t3857203917::get_offset_of_gyroEnabled_2(),
	GyroController_t3857203917::get_offset_of_lowPassFilterFactor_3(),
	GyroController_t3857203917::get_offset_of_baseIdentity_4(),
	GyroController_t3857203917::get_offset_of_landscapeRight_5(),
	GyroController_t3857203917::get_offset_of_landscapeLeft_6(),
	GyroController_t3857203917::get_offset_of_upsideDown_7(),
	GyroController_t3857203917::get_offset_of_frontCamera_8(),
	GyroController_t3857203917::get_offset_of_cameraBase_9(),
	GyroController_t3857203917::get_offset_of_calibration_10(),
	GyroController_t3857203917::get_offset_of_baseOrientation_11(),
	GyroController_t3857203917::get_offset_of_baseOrientationRotationFix_12(),
	GyroController_t3857203917::get_offset_of_referenceRotation_13(),
	GyroController_t3857203917::get_offset_of_debugGUI_14(),
	GyroController_t3857203917::get_offset_of_useFrontCamera_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (Line_t2729441502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1782[2] = 
{
	Line_t2729441502::get_offset_of_p0_2(),
	Line_t2729441502::get_offset_of_p1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (MiraArController_t3896789088), -1, sizeof(MiraArController_t3896789088_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1783[7] = 
{
	MiraArController_t3896789088::get_offset_of_CommonMarkerEvents_2(),
	MiraArController_t3896789088::get_offset_of_ListOfMarkers_3(),
	MiraArController_t3896789088::get_offset_of_IPD_4(),
	MiraArController_t3896789088::get_offset_of_Display_Debug_Menu_5(),
	MiraArController_t3896789088_StaticFields::get_offset_of_instance_6(),
	MiraArController_t3896789088_StaticFields::get_offset_of_MiraTracking_7(),
	MiraArController_t3896789088_StaticFields::get_offset_of_currentMiraMarkers_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (MarkerState_t2127725244)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1784[5] = 
{
	MarkerState_t2127725244::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (MarkerList_t3622290753)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1785[6] = 
{
	MarkerList_t3622290753::get_offset_of_currentState_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MarkerList_t3622290753::get_offset_of_id_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MarkerList_t3622290753::get_offset_of_tracker_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MarkerList_t3622290753::get_offset_of_OnMarkerFound_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MarkerList_t3622290753::get_offset_of_OnMarkerLost_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MarkerList_t3622290753::get_offset_of_OnMarkerUpdate_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (CommonMarkerList_t285872970)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1786[3] = 
{
	CommonMarkerList_t285872970::get_offset_of_foundMarkerEvent_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CommonMarkerList_t285872970::get_offset_of__lostMarkerEent_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CommonMarkerList_t285872970::get_offset_of__updateMarkerEvent_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (MiraEventManager_t364232676), -1, sizeof(MiraEventManager_t364232676_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1787[2] = 
{
	MiraEventManager_t364232676::get_offset_of_eventDictionary_2(),
	MiraEventManager_t364232676_StaticFields::get_offset_of_eventManager_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (MiraMarkerEvents_t1700133524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1788[1] = 
{
	MiraMarkerEvents_t1700133524::get_offset_of_id_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (MiraPostRender_t3202756259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1789[11] = 
{
	MiraPostRender_t3202756259::get_offset_of_eye_2(),
	MiraPostRender_t3202756259::get_offset_of_xSize_3(),
	MiraPostRender_t3202756259::get_offset_of_ySize_4(),
	MiraPostRender_t3202756259::get_offset_of_zOffset_5(),
	MiraPostRender_t3202756259::get_offset_of_xScalar_6(),
	MiraPostRender_t3202756259::get_offset_of_yScalar_7(),
	MiraPostRender_t3202756259::get_offset_of_screenSizeX_8(),
	MiraPostRender_t3202756259::get_offset_of_screenSizeY_9(),
	MiraPostRender_t3202756259::get_offset_of_mesh_10(),
	MiraPostRender_t3202756259::get_offset_of_renderTextureMaterial_11(),
	MiraPostRender_t3202756259::get_offset_of_IPD_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (Eye_t273978457)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1790[3] = 
{
	Eye_t273978457::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (MiraPreRender_t2810281508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1791[1] = 
{
	MiraPreRender_t2810281508::get_offset_of_U3CcamU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (MiraReticle_t2195475589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1792[12] = 
{
	MiraReticle_t2195475589::get_offset_of_ParentFacing_2(),
	MiraReticle_t2195475589::get_offset_of_scaleMultiplier_3(),
	MiraReticle_t2195475589::get_offset_of_reticleHoverColor_4(),
	MiraReticle_t2195475589::get_offset_of_reticleRenderer_5(),
	MiraReticle_t2195475589::get_offset_of_reticleOriginalScale_6(),
	MiraReticle_t2195475589::get_offset_of_raycastHit_7(),
	MiraReticle_t2195475589::get_offset_of_gameObjectHit_8(),
	MiraReticle_t2195475589::get_offset_of_ControllerTargetHit_9(),
	MiraReticle_t2195475589::get_offset_of_reticuleOffsetZ_10(),
	MiraReticle_t2195475589::get_offset_of_camAngle_11(),
	MiraReticle_t2195475589::get_offset_of_camRadians_12(),
	MiraReticle_t2195475589::get_offset_of_nullPos_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (MiraViewer_t256795227), -1, sizeof(MiraViewer_t256795227_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1793[8] = 
{
	MiraViewer_t256795227_StaticFields::get_offset_of_instance_2(),
	MiraViewer_t256795227_StaticFields::get_offset_of_currentGyroController_3(),
	MiraViewer_t256795227_StaticFields::get_offset_of_currentMainCamera_4(),
	MiraViewer_t256795227_StaticFields::get_offset_of_IsDebug_5(),
	MiraViewer_t256795227_StaticFields::get_offset_of_viewer_6(),
	MiraViewer_t256795227_StaticFields::get_offset_of_backrenderer_7(),
	MiraViewer_t256795227_StaticFields::get_offset_of_Left_Eye_8(),
	MiraViewer_t256795227_StaticFields::get_offset_of_Right_Eye_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (CameraNames_t2647135469)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1794[3] = 
{
	CameraNames_t2647135469::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (TrackingHandoff_t210633217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1795[4] = 
{
	TrackingHandoff_t210633217::get_offset_of_currentObject_2(),
	TrackingHandoff_t210633217::get_offset_of_defaultPos_3(),
	TrackingHandoff_t210633217::get_offset_of_defaultRot_4(),
	TrackingHandoff_t210633217::get_offset_of_tempPlacement_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (TrackingInteractable_t149194413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[7] = 
{
	TrackingInteractable_t149194413::get_offset_of_trackingLost_2(),
	TrackingInteractable_t149194413::get_offset_of_baseMaterial_3(),
	TrackingInteractable_t149194413::get_offset_of_defaultColor_4(),
	TrackingInteractable_t149194413::get_offset_of_thisCollider_5(),
	TrackingInteractable_t149194413::get_offset_of_lerpCycleTime_6(),
	TrackingInteractable_t149194413::get_offset_of_beaconLerpColor_7(),
	TrackingInteractable_t149194413::get_offset_of_thisUIObj_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (U3CLostTrackingBeaconU3Ec__Iterator0_t2706263904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1797[5] = 
{
	U3CLostTrackingBeaconU3Ec__Iterator0_t2706263904::get_offset_of_U3ClerpTimeU3E__0_0(),
	U3CLostTrackingBeaconU3Ec__Iterator0_t2706263904::get_offset_of_U24this_1(),
	U3CLostTrackingBeaconU3Ec__Iterator0_t2706263904::get_offset_of_U24current_2(),
	U3CLostTrackingBeaconU3Ec__Iterator0_t2706263904::get_offset_of_U24disposing_3(),
	U3CLostTrackingBeaconU3Ec__Iterator0_t2706263904::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (ApplicationManager_t2110631419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (BattleController_t3050984398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1799[10] = 
{
	BattleController_t3050984398::get_offset_of_playerImage_2(),
	BattleController_t3050984398::get_offset_of_enemyImage_3(),
	BattleController_t3050984398::get_offset_of_sprites_4(),
	BattleController_t3050984398::get_offset_of_enemies_5(),
	BattleController_t3050984398::get_offset_of_players_6(),
	BattleController_t3050984398::get_offset_of_battleBoard_7(),
	BattleController_t3050984398::get_offset_of_battleUI_8(),
	BattleController_t3050984398::get_offset_of_VSUI_9(),
	BattleController_t3050984398::get_offset_of_laser_10(),
	BattleController_t3050984398::get_offset_of_rend_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
