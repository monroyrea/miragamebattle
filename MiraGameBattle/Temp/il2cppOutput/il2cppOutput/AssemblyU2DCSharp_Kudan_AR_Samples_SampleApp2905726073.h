﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Kudan.AR.KudanTracker
struct KudanTracker_t2134143885;
// Kudan.AR.TrackingMethodMarker
struct TrackingMethodMarker_t3831798236;
// Kudan.AR.TrackingMethodMarkerless
struct TrackingMethodMarkerless_t432022491;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kudan.AR.Samples.SampleApp
struct  SampleApp_t2905726073  : public MonoBehaviour_t1158329972
{
public:
	// Kudan.AR.KudanTracker Kudan.AR.Samples.SampleApp::_kudanTracker
	KudanTracker_t2134143885 * ____kudanTracker_2;
	// Kudan.AR.TrackingMethodMarker Kudan.AR.Samples.SampleApp::_markerTracking
	TrackingMethodMarker_t3831798236 * ____markerTracking_3;
	// Kudan.AR.TrackingMethodMarkerless Kudan.AR.Samples.SampleApp::_markerlessTracking
	TrackingMethodMarkerless_t432022491 * ____markerlessTracking_4;
	// UnityEngine.UI.Text Kudan.AR.Samples.SampleApp::buttonText
	Text_t356221433 * ___buttonText_5;

public:
	inline static int32_t get_offset_of__kudanTracker_2() { return static_cast<int32_t>(offsetof(SampleApp_t2905726073, ____kudanTracker_2)); }
	inline KudanTracker_t2134143885 * get__kudanTracker_2() const { return ____kudanTracker_2; }
	inline KudanTracker_t2134143885 ** get_address_of__kudanTracker_2() { return &____kudanTracker_2; }
	inline void set__kudanTracker_2(KudanTracker_t2134143885 * value)
	{
		____kudanTracker_2 = value;
		Il2CppCodeGenWriteBarrier(&____kudanTracker_2, value);
	}

	inline static int32_t get_offset_of__markerTracking_3() { return static_cast<int32_t>(offsetof(SampleApp_t2905726073, ____markerTracking_3)); }
	inline TrackingMethodMarker_t3831798236 * get__markerTracking_3() const { return ____markerTracking_3; }
	inline TrackingMethodMarker_t3831798236 ** get_address_of__markerTracking_3() { return &____markerTracking_3; }
	inline void set__markerTracking_3(TrackingMethodMarker_t3831798236 * value)
	{
		____markerTracking_3 = value;
		Il2CppCodeGenWriteBarrier(&____markerTracking_3, value);
	}

	inline static int32_t get_offset_of__markerlessTracking_4() { return static_cast<int32_t>(offsetof(SampleApp_t2905726073, ____markerlessTracking_4)); }
	inline TrackingMethodMarkerless_t432022491 * get__markerlessTracking_4() const { return ____markerlessTracking_4; }
	inline TrackingMethodMarkerless_t432022491 ** get_address_of__markerlessTracking_4() { return &____markerlessTracking_4; }
	inline void set__markerlessTracking_4(TrackingMethodMarkerless_t432022491 * value)
	{
		____markerlessTracking_4 = value;
		Il2CppCodeGenWriteBarrier(&____markerlessTracking_4, value);
	}

	inline static int32_t get_offset_of_buttonText_5() { return static_cast<int32_t>(offsetof(SampleApp_t2905726073, ___buttonText_5)); }
	inline Text_t356221433 * get_buttonText_5() const { return ___buttonText_5; }
	inline Text_t356221433 ** get_address_of_buttonText_5() { return &___buttonText_5; }
	inline void set_buttonText_5(Text_t356221433 * value)
	{
		___buttonText_5 = value;
		Il2CppCodeGenWriteBarrier(&___buttonText_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
