﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationIcon
struct  NotificationIcon_t332970646  : public MonoBehaviour_t1158329972
{
public:
	// System.Single NotificationIcon::iconBoundsX
	float ___iconBoundsX_2;
	// UnityEngine.Quaternion NotificationIcon::OgRotationLU
	Quaternion_t4030073918  ___OgRotationLU_3;
	// UnityEngine.Material NotificationIcon::mat
	Material_t193706927 * ___mat_4;
	// UnityEngine.Color NotificationIcon::ogClr
	Color_t2020392075  ___ogClr_5;
	// UnityEngine.Color NotificationIcon::lerpClr
	Color_t2020392075  ___lerpClr_6;

public:
	inline static int32_t get_offset_of_iconBoundsX_2() { return static_cast<int32_t>(offsetof(NotificationIcon_t332970646, ___iconBoundsX_2)); }
	inline float get_iconBoundsX_2() const { return ___iconBoundsX_2; }
	inline float* get_address_of_iconBoundsX_2() { return &___iconBoundsX_2; }
	inline void set_iconBoundsX_2(float value)
	{
		___iconBoundsX_2 = value;
	}

	inline static int32_t get_offset_of_OgRotationLU_3() { return static_cast<int32_t>(offsetof(NotificationIcon_t332970646, ___OgRotationLU_3)); }
	inline Quaternion_t4030073918  get_OgRotationLU_3() const { return ___OgRotationLU_3; }
	inline Quaternion_t4030073918 * get_address_of_OgRotationLU_3() { return &___OgRotationLU_3; }
	inline void set_OgRotationLU_3(Quaternion_t4030073918  value)
	{
		___OgRotationLU_3 = value;
	}

	inline static int32_t get_offset_of_mat_4() { return static_cast<int32_t>(offsetof(NotificationIcon_t332970646, ___mat_4)); }
	inline Material_t193706927 * get_mat_4() const { return ___mat_4; }
	inline Material_t193706927 ** get_address_of_mat_4() { return &___mat_4; }
	inline void set_mat_4(Material_t193706927 * value)
	{
		___mat_4 = value;
		Il2CppCodeGenWriteBarrier(&___mat_4, value);
	}

	inline static int32_t get_offset_of_ogClr_5() { return static_cast<int32_t>(offsetof(NotificationIcon_t332970646, ___ogClr_5)); }
	inline Color_t2020392075  get_ogClr_5() const { return ___ogClr_5; }
	inline Color_t2020392075 * get_address_of_ogClr_5() { return &___ogClr_5; }
	inline void set_ogClr_5(Color_t2020392075  value)
	{
		___ogClr_5 = value;
	}

	inline static int32_t get_offset_of_lerpClr_6() { return static_cast<int32_t>(offsetof(NotificationIcon_t332970646, ___lerpClr_6)); }
	inline Color_t2020392075  get_lerpClr_6() const { return ___lerpClr_6; }
	inline Color_t2020392075 * get_address_of_lerpClr_6() { return &___lerpClr_6; }
	inline void set_lerpClr_6(Color_t2020392075  value)
	{
		___lerpClr_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
