﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Animator
struct Animator_t69676727;
// UnityEngine.CharacterController
struct CharacterController_t4094781467;
// UnityEngine.Time
struct Time_t31991979;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerController
struct  PlayerController_t4148409433  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text PlayerController::gesture
	Text_t356221433 * ___gesture_2;
	// UnityEngine.GameObject PlayerController::enemy
	GameObject_t1756533147 * ___enemy_3;
	// System.Single PlayerController::verticalVelocity
	float ___verticalVelocity_4;
	// System.Single PlayerController::gravity
	float ___gravity_5;
	// System.Single PlayerController::jumpForce
	float ___jumpForce_6;
	// System.Boolean PlayerController::canJump
	bool ___canJump_7;
	// System.Boolean PlayerController::defending
	bool ___defending_8;
	// UnityEngine.GameObject PlayerController::leftLimit
	GameObject_t1756533147 * ___leftLimit_9;
	// UnityEngine.GameObject PlayerController::rightLimit
	GameObject_t1756533147 * ___rightLimit_10;
	// UnityEngine.Animator PlayerController::anim
	Animator_t69676727 * ___anim_11;
	// UnityEngine.CharacterController PlayerController::charController
	CharacterController_t4094781467 * ___charController_12;
	// System.Single PlayerController::playerMovementX
	float ___playerMovementX_13;
	// UnityEngine.Time PlayerController::time
	Time_t31991979 * ___time_14;

public:
	inline static int32_t get_offset_of_gesture_2() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___gesture_2)); }
	inline Text_t356221433 * get_gesture_2() const { return ___gesture_2; }
	inline Text_t356221433 ** get_address_of_gesture_2() { return &___gesture_2; }
	inline void set_gesture_2(Text_t356221433 * value)
	{
		___gesture_2 = value;
		Il2CppCodeGenWriteBarrier(&___gesture_2, value);
	}

	inline static int32_t get_offset_of_enemy_3() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___enemy_3)); }
	inline GameObject_t1756533147 * get_enemy_3() const { return ___enemy_3; }
	inline GameObject_t1756533147 ** get_address_of_enemy_3() { return &___enemy_3; }
	inline void set_enemy_3(GameObject_t1756533147 * value)
	{
		___enemy_3 = value;
		Il2CppCodeGenWriteBarrier(&___enemy_3, value);
	}

	inline static int32_t get_offset_of_verticalVelocity_4() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___verticalVelocity_4)); }
	inline float get_verticalVelocity_4() const { return ___verticalVelocity_4; }
	inline float* get_address_of_verticalVelocity_4() { return &___verticalVelocity_4; }
	inline void set_verticalVelocity_4(float value)
	{
		___verticalVelocity_4 = value;
	}

	inline static int32_t get_offset_of_gravity_5() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___gravity_5)); }
	inline float get_gravity_5() const { return ___gravity_5; }
	inline float* get_address_of_gravity_5() { return &___gravity_5; }
	inline void set_gravity_5(float value)
	{
		___gravity_5 = value;
	}

	inline static int32_t get_offset_of_jumpForce_6() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___jumpForce_6)); }
	inline float get_jumpForce_6() const { return ___jumpForce_6; }
	inline float* get_address_of_jumpForce_6() { return &___jumpForce_6; }
	inline void set_jumpForce_6(float value)
	{
		___jumpForce_6 = value;
	}

	inline static int32_t get_offset_of_canJump_7() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___canJump_7)); }
	inline bool get_canJump_7() const { return ___canJump_7; }
	inline bool* get_address_of_canJump_7() { return &___canJump_7; }
	inline void set_canJump_7(bool value)
	{
		___canJump_7 = value;
	}

	inline static int32_t get_offset_of_defending_8() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___defending_8)); }
	inline bool get_defending_8() const { return ___defending_8; }
	inline bool* get_address_of_defending_8() { return &___defending_8; }
	inline void set_defending_8(bool value)
	{
		___defending_8 = value;
	}

	inline static int32_t get_offset_of_leftLimit_9() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___leftLimit_9)); }
	inline GameObject_t1756533147 * get_leftLimit_9() const { return ___leftLimit_9; }
	inline GameObject_t1756533147 ** get_address_of_leftLimit_9() { return &___leftLimit_9; }
	inline void set_leftLimit_9(GameObject_t1756533147 * value)
	{
		___leftLimit_9 = value;
		Il2CppCodeGenWriteBarrier(&___leftLimit_9, value);
	}

	inline static int32_t get_offset_of_rightLimit_10() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___rightLimit_10)); }
	inline GameObject_t1756533147 * get_rightLimit_10() const { return ___rightLimit_10; }
	inline GameObject_t1756533147 ** get_address_of_rightLimit_10() { return &___rightLimit_10; }
	inline void set_rightLimit_10(GameObject_t1756533147 * value)
	{
		___rightLimit_10 = value;
		Il2CppCodeGenWriteBarrier(&___rightLimit_10, value);
	}

	inline static int32_t get_offset_of_anim_11() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___anim_11)); }
	inline Animator_t69676727 * get_anim_11() const { return ___anim_11; }
	inline Animator_t69676727 ** get_address_of_anim_11() { return &___anim_11; }
	inline void set_anim_11(Animator_t69676727 * value)
	{
		___anim_11 = value;
		Il2CppCodeGenWriteBarrier(&___anim_11, value);
	}

	inline static int32_t get_offset_of_charController_12() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___charController_12)); }
	inline CharacterController_t4094781467 * get_charController_12() const { return ___charController_12; }
	inline CharacterController_t4094781467 ** get_address_of_charController_12() { return &___charController_12; }
	inline void set_charController_12(CharacterController_t4094781467 * value)
	{
		___charController_12 = value;
		Il2CppCodeGenWriteBarrier(&___charController_12, value);
	}

	inline static int32_t get_offset_of_playerMovementX_13() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___playerMovementX_13)); }
	inline float get_playerMovementX_13() const { return ___playerMovementX_13; }
	inline float* get_address_of_playerMovementX_13() { return &___playerMovementX_13; }
	inline void set_playerMovementX_13(float value)
	{
		___playerMovementX_13 = value;
	}

	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___time_14)); }
	inline Time_t31991979 * get_time_14() const { return ___time_14; }
	inline Time_t31991979 ** get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(Time_t31991979 * value)
	{
		___time_14 = value;
		Il2CppCodeGenWriteBarrier(&___time_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
