﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// MiraReticle
struct MiraReticle_t2195475589;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LaserPointerLength
struct  LaserPointerLength_t1503249384  : public MonoBehaviour_t1158329972
{
public:
	// MiraReticle LaserPointerLength::reticle
	MiraReticle_t2195475589 * ___reticle_2;
	// UnityEngine.Vector3 LaserPointerLength::originalScale
	Vector3_t2243707580  ___originalScale_3;

public:
	inline static int32_t get_offset_of_reticle_2() { return static_cast<int32_t>(offsetof(LaserPointerLength_t1503249384, ___reticle_2)); }
	inline MiraReticle_t2195475589 * get_reticle_2() const { return ___reticle_2; }
	inline MiraReticle_t2195475589 ** get_address_of_reticle_2() { return &___reticle_2; }
	inline void set_reticle_2(MiraReticle_t2195475589 * value)
	{
		___reticle_2 = value;
		Il2CppCodeGenWriteBarrier(&___reticle_2, value);
	}

	inline static int32_t get_offset_of_originalScale_3() { return static_cast<int32_t>(offsetof(LaserPointerLength_t1503249384, ___originalScale_3)); }
	inline Vector3_t2243707580  get_originalScale_3() const { return ___originalScale_3; }
	inline Vector3_t2243707580 * get_address_of_originalScale_3() { return &___originalScale_3; }
	inline void set_originalScale_3(Vector3_t2243707580  value)
	{
		___originalScale_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
