﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3531788748.h"

// NotificationMenu
struct NotificationMenu_t1031876728;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationMenu/<FadeIn>c__Iterator0
struct  U3CFadeInU3Ec__Iterator0_t4261807277  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<NotificationIcon> NotificationMenu/<FadeIn>c__Iterator0::$locvar0
	Enumerator_t3531788748  ___U24locvar0_0;
	// NotificationMenu NotificationMenu/<FadeIn>c__Iterator0::$this
	NotificationMenu_t1031876728 * ___U24this_1;
	// System.Object NotificationMenu/<FadeIn>c__Iterator0::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean NotificationMenu/<FadeIn>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 NotificationMenu/<FadeIn>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__Iterator0_t4261807277, ___U24locvar0_0)); }
	inline Enumerator_t3531788748  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t3531788748 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t3531788748  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__Iterator0_t4261807277, ___U24this_1)); }
	inline NotificationMenu_t1031876728 * get_U24this_1() const { return ___U24this_1; }
	inline NotificationMenu_t1031876728 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(NotificationMenu_t1031876728 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__Iterator0_t4261807277, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__Iterator0_t4261807277, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__Iterator0_t4261807277, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
