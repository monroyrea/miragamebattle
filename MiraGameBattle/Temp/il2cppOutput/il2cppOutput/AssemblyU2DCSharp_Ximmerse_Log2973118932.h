﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Ximmerse.Log/ILogger
struct ILogger_t1838748630;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ximmerse.Log
struct  Log_t2973118932  : public Il2CppObject
{
public:

public:
};

struct Log_t2973118932_StaticFields
{
public:
	// Ximmerse.Log/ILogger Ximmerse.Log::s_Logger
	Il2CppObject * ___s_Logger_0;
	// System.Int32 Ximmerse.Log::s_Filter
	int32_t ___s_Filter_1;

public:
	inline static int32_t get_offset_of_s_Logger_0() { return static_cast<int32_t>(offsetof(Log_t2973118932_StaticFields, ___s_Logger_0)); }
	inline Il2CppObject * get_s_Logger_0() const { return ___s_Logger_0; }
	inline Il2CppObject ** get_address_of_s_Logger_0() { return &___s_Logger_0; }
	inline void set_s_Logger_0(Il2CppObject * value)
	{
		___s_Logger_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_Logger_0, value);
	}

	inline static int32_t get_offset_of_s_Filter_1() { return static_cast<int32_t>(offsetof(Log_t2973118932_StaticFields, ___s_Filter_1)); }
	inline int32_t get_s_Filter_1() const { return ___s_Filter_1; }
	inline int32_t* get_address_of_s_Filter_1() { return &___s_Filter_1; }
	inline void set_s_Filter_1(int32_t value)
	{
		___s_Filter_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
