﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// ControllerInputGUI/ControllerEntry[]
struct ControllerEntryU5BU5D_t4057665489;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControllerInputGUI
struct  ControllerInputGUI_t2571972535  : public MonoBehaviour_t1158329972
{
public:
	// ControllerInputGUI/ControllerEntry[] ControllerInputGUI::m_Controllers
	ControllerEntryU5BU5D_t4057665489* ___m_Controllers_2;

public:
	inline static int32_t get_offset_of_m_Controllers_2() { return static_cast<int32_t>(offsetof(ControllerInputGUI_t2571972535, ___m_Controllers_2)); }
	inline ControllerEntryU5BU5D_t4057665489* get_m_Controllers_2() const { return ___m_Controllers_2; }
	inline ControllerEntryU5BU5D_t4057665489** get_address_of_m_Controllers_2() { return &___m_Controllers_2; }
	inline void set_m_Controllers_2(ControllerEntryU5BU5D_t4057665489* value)
	{
		___m_Controllers_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Controllers_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
