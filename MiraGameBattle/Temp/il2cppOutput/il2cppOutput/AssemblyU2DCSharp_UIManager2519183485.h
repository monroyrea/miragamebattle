﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Collections.Generic.List`1<UIObject>
struct List_1_t3648280775;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIManager
struct  UIManager_t2519183485  : public MonoBehaviour_t1158329972
{
public:
	// System.Single UIManager::UIAngle
	float ___UIAngle_2;
	// System.Single UIManager::zRadius
	float ___zRadius_3;
	// System.Single UIManager::height
	float ___height_4;
	// System.Single UIManager::padding
	float ___padding_5;
	// System.Single UIManager::setLerpSpaceTime
	float ___setLerpSpaceTime_6;
	// System.Single UIManager::lerpSpaceTime
	float ___lerpSpaceTime_7;
	// System.Collections.Generic.List`1<UIObject> UIManager::UIObj
	List_1_t3648280775 * ___UIObj_8;
	// UnityEngine.Coroutine UIManager::lerpSpaceCo
	Coroutine_t2299508840 * ___lerpSpaceCo_9;
	// System.Boolean UIManager::firstLerp
	bool ___firstLerp_10;

public:
	inline static int32_t get_offset_of_UIAngle_2() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___UIAngle_2)); }
	inline float get_UIAngle_2() const { return ___UIAngle_2; }
	inline float* get_address_of_UIAngle_2() { return &___UIAngle_2; }
	inline void set_UIAngle_2(float value)
	{
		___UIAngle_2 = value;
	}

	inline static int32_t get_offset_of_zRadius_3() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___zRadius_3)); }
	inline float get_zRadius_3() const { return ___zRadius_3; }
	inline float* get_address_of_zRadius_3() { return &___zRadius_3; }
	inline void set_zRadius_3(float value)
	{
		___zRadius_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_padding_5() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___padding_5)); }
	inline float get_padding_5() const { return ___padding_5; }
	inline float* get_address_of_padding_5() { return &___padding_5; }
	inline void set_padding_5(float value)
	{
		___padding_5 = value;
	}

	inline static int32_t get_offset_of_setLerpSpaceTime_6() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___setLerpSpaceTime_6)); }
	inline float get_setLerpSpaceTime_6() const { return ___setLerpSpaceTime_6; }
	inline float* get_address_of_setLerpSpaceTime_6() { return &___setLerpSpaceTime_6; }
	inline void set_setLerpSpaceTime_6(float value)
	{
		___setLerpSpaceTime_6 = value;
	}

	inline static int32_t get_offset_of_lerpSpaceTime_7() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___lerpSpaceTime_7)); }
	inline float get_lerpSpaceTime_7() const { return ___lerpSpaceTime_7; }
	inline float* get_address_of_lerpSpaceTime_7() { return &___lerpSpaceTime_7; }
	inline void set_lerpSpaceTime_7(float value)
	{
		___lerpSpaceTime_7 = value;
	}

	inline static int32_t get_offset_of_UIObj_8() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___UIObj_8)); }
	inline List_1_t3648280775 * get_UIObj_8() const { return ___UIObj_8; }
	inline List_1_t3648280775 ** get_address_of_UIObj_8() { return &___UIObj_8; }
	inline void set_UIObj_8(List_1_t3648280775 * value)
	{
		___UIObj_8 = value;
		Il2CppCodeGenWriteBarrier(&___UIObj_8, value);
	}

	inline static int32_t get_offset_of_lerpSpaceCo_9() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___lerpSpaceCo_9)); }
	inline Coroutine_t2299508840 * get_lerpSpaceCo_9() const { return ___lerpSpaceCo_9; }
	inline Coroutine_t2299508840 ** get_address_of_lerpSpaceCo_9() { return &___lerpSpaceCo_9; }
	inline void set_lerpSpaceCo_9(Coroutine_t2299508840 * value)
	{
		___lerpSpaceCo_9 = value;
		Il2CppCodeGenWriteBarrier(&___lerpSpaceCo_9, value);
	}

	inline static int32_t get_offset_of_firstLerp_10() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___firstLerp_10)); }
	inline bool get_firstLerp_10() const { return ___firstLerp_10; }
	inline bool* get_address_of_firstLerp_10() { return &___firstLerp_10; }
	inline void set_firstLerp_10(bool value)
	{
		___firstLerp_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
