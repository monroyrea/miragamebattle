﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// Kudan.AR.MarkerFoundEvent
struct MarkerFoundEvent_t1034540096;
// Kudan.AR.MarkerLostEvent
struct MarkerLostEvent_t859612542;
// Kudan.AR.MarkerUpdateEvent
struct MarkerUpdateEvent_t2836667359;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiraArController/CommonMarkerList
struct  CommonMarkerList_t285872970 
{
public:
	// Kudan.AR.MarkerFoundEvent MiraArController/CommonMarkerList::foundMarkerEvent
	MarkerFoundEvent_t1034540096 * ___foundMarkerEvent_0;
	// Kudan.AR.MarkerLostEvent MiraArController/CommonMarkerList::_lostMarkerEent
	MarkerLostEvent_t859612542 * ____lostMarkerEent_1;
	// Kudan.AR.MarkerUpdateEvent MiraArController/CommonMarkerList::_updateMarkerEvent
	MarkerUpdateEvent_t2836667359 * ____updateMarkerEvent_2;

public:
	inline static int32_t get_offset_of_foundMarkerEvent_0() { return static_cast<int32_t>(offsetof(CommonMarkerList_t285872970, ___foundMarkerEvent_0)); }
	inline MarkerFoundEvent_t1034540096 * get_foundMarkerEvent_0() const { return ___foundMarkerEvent_0; }
	inline MarkerFoundEvent_t1034540096 ** get_address_of_foundMarkerEvent_0() { return &___foundMarkerEvent_0; }
	inline void set_foundMarkerEvent_0(MarkerFoundEvent_t1034540096 * value)
	{
		___foundMarkerEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&___foundMarkerEvent_0, value);
	}

	inline static int32_t get_offset_of__lostMarkerEent_1() { return static_cast<int32_t>(offsetof(CommonMarkerList_t285872970, ____lostMarkerEent_1)); }
	inline MarkerLostEvent_t859612542 * get__lostMarkerEent_1() const { return ____lostMarkerEent_1; }
	inline MarkerLostEvent_t859612542 ** get_address_of__lostMarkerEent_1() { return &____lostMarkerEent_1; }
	inline void set__lostMarkerEent_1(MarkerLostEvent_t859612542 * value)
	{
		____lostMarkerEent_1 = value;
		Il2CppCodeGenWriteBarrier(&____lostMarkerEent_1, value);
	}

	inline static int32_t get_offset_of__updateMarkerEvent_2() { return static_cast<int32_t>(offsetof(CommonMarkerList_t285872970, ____updateMarkerEvent_2)); }
	inline MarkerUpdateEvent_t2836667359 * get__updateMarkerEvent_2() const { return ____updateMarkerEvent_2; }
	inline MarkerUpdateEvent_t2836667359 ** get_address_of__updateMarkerEvent_2() { return &____updateMarkerEvent_2; }
	inline void set__updateMarkerEvent_2(MarkerUpdateEvent_t2836667359 * value)
	{
		____updateMarkerEvent_2 = value;
		Il2CppCodeGenWriteBarrier(&____updateMarkerEvent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MiraArController/CommonMarkerList
struct CommonMarkerList_t285872970_marshaled_pinvoke
{
	MarkerFoundEvent_t1034540096 * ___foundMarkerEvent_0;
	MarkerLostEvent_t859612542 * ____lostMarkerEent_1;
	MarkerUpdateEvent_t2836667359 * ____updateMarkerEvent_2;
};
// Native definition for COM marshalling of MiraArController/CommonMarkerList
struct CommonMarkerList_t285872970_marshaled_com
{
	MarkerFoundEvent_t1034540096 * ___foundMarkerEvent_0;
	MarkerLostEvent_t859612542 * ____lostMarkerEent_1;
	MarkerUpdateEvent_t2836667359 * ____updateMarkerEvent_2;
};
