﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// HapticsLibrary
struct HapticsLibrary_t1701333201;
// HapticsLibrary/HapticsDesc[]
struct HapticsDescU5BU5D_t3943052438;
// System.Collections.Generic.Dictionary`2<System.String,HapticsLibrary/HapticsDesc>
struct Dictionary_2_t1116864461;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HapticsLibrary
struct  HapticsLibrary_t1701333201  : public MonoBehaviour_t1158329972
{
public:
	// HapticsLibrary/HapticsDesc[] HapticsLibrary::m_HapticsDescs
	HapticsDescU5BU5D_t3943052438* ___m_HapticsDescs_4;
	// System.Collections.Generic.Dictionary`2<System.String,HapticsLibrary/HapticsDesc> HapticsLibrary::hapticsDescs
	Dictionary_2_t1116864461 * ___hapticsDescs_5;

public:
	inline static int32_t get_offset_of_m_HapticsDescs_4() { return static_cast<int32_t>(offsetof(HapticsLibrary_t1701333201, ___m_HapticsDescs_4)); }
	inline HapticsDescU5BU5D_t3943052438* get_m_HapticsDescs_4() const { return ___m_HapticsDescs_4; }
	inline HapticsDescU5BU5D_t3943052438** get_address_of_m_HapticsDescs_4() { return &___m_HapticsDescs_4; }
	inline void set_m_HapticsDescs_4(HapticsDescU5BU5D_t3943052438* value)
	{
		___m_HapticsDescs_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_HapticsDescs_4, value);
	}

	inline static int32_t get_offset_of_hapticsDescs_5() { return static_cast<int32_t>(offsetof(HapticsLibrary_t1701333201, ___hapticsDescs_5)); }
	inline Dictionary_2_t1116864461 * get_hapticsDescs_5() const { return ___hapticsDescs_5; }
	inline Dictionary_2_t1116864461 ** get_address_of_hapticsDescs_5() { return &___hapticsDescs_5; }
	inline void set_hapticsDescs_5(Dictionary_2_t1116864461 * value)
	{
		___hapticsDescs_5 = value;
		Il2CppCodeGenWriteBarrier(&___hapticsDescs_5, value);
	}
};

struct HapticsLibrary_t1701333201_StaticFields
{
public:
	// HapticsLibrary HapticsLibrary::s_Instance
	HapticsLibrary_t1701333201 * ___s_Instance_2;
	// System.Boolean HapticsLibrary::s_InstanceCached
	bool ___s_InstanceCached_3;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(HapticsLibrary_t1701333201_StaticFields, ___s_Instance_2)); }
	inline HapticsLibrary_t1701333201 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline HapticsLibrary_t1701333201 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(HapticsLibrary_t1701333201 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___s_Instance_2, value);
	}

	inline static int32_t get_offset_of_s_InstanceCached_3() { return static_cast<int32_t>(offsetof(HapticsLibrary_t1701333201_StaticFields, ___s_InstanceCached_3)); }
	inline bool get_s_InstanceCached_3() const { return ___s_InstanceCached_3; }
	inline bool* get_address_of_s_InstanceCached_3() { return &___s_InstanceCached_3; }
	inline void set_s_InstanceCached_3(bool value)
	{
		___s_InstanceCached_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
