2O                         SPOT   SHADOWS_DEPTH      SHADOWS_SOFT�     xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;
struct Globals_Type
{
    float3 _WorldSpaceCameraPos;
    float4 _WorldSpaceLightPos0;
    half4 _LightShadowData;
    float4 _ShadowOffsets[4];
    float4 _LightColor0;
    float4 _normalmap_ST;
    float _gloss;
    float4 _specmap_ST;
    float4 _diffusemap_ST;
};

struct Mtl_FragmentIn
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float4 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float3 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    float3 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float3 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    float4 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
    float4 TEXCOORD6 [[ user(TEXCOORD6) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(0) ]];
};

constexpr sampler _mtl_xl_shadow_sampler(address::clamp_to_edge, filter::linear, compare_func::greater_equal);
fragment Mtl_FragmentOut xlatMtlMain(
    constant Globals_Type& Globals [[ buffer(0) ]],
    texture2d<half, access::sample > _normalmap [[ texture (0) ]] ,
    sampler sampler_normalmap [[ sampler (0) ]] ,
    texture2d<half, access::sample > _LightTexture0 [[ texture (1) ]] ,
    sampler sampler_LightTexture0 [[ sampler (1) ]] ,
    texture2d<half, access::sample > _LightTextureB0 [[ texture (2) ]] ,
    sampler sampler_LightTextureB0 [[ sampler (2) ]] ,
    texture2d<half, access::sample > _specmap [[ texture (3) ]] ,
    sampler sampler_specmap [[ sampler (3) ]] ,
    texture2d<half, access::sample > _diffusemap [[ texture (4) ]] ,
    sampler sampler_diffusemap [[ sampler (4) ]] ,
    depth2d<float, access::sample > _ShadowMapTexture [[ texture (5) ]] ,
    sampler sampler_ShadowMapTexture [[ sampler (5) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    half4 u_xlat16_0;
    float3 u_xlat1;
    half u_xlat16_1;
    float4 u_xlat2;
    half3 u_xlat16_2;
    float3 u_xlat3;
    half3 u_xlat16_3;
    float3 u_xlat4;
    float2 u_xlat6;
    half u_xlat16_6;
    bool u_xlatb11;
    float u_xlat16;
    u_xlat16_0.x = (-Globals._LightShadowData.x) + half(1.0);
    u_xlat1.xyz = input.TEXCOORD6.xyz / input.TEXCOORD6.www;
    u_xlat2.xyz = u_xlat1.xyz + Globals._ShadowOffsets[0].xyz;
    u_xlat2.x = float(_ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat2.xy, saturate(u_xlat2.z), level(0.0)));
    u_xlat3.xyz = u_xlat1.xyz + Globals._ShadowOffsets[1].xyz;
    u_xlat2.y = float(_ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat3.xy, saturate(u_xlat3.z), level(0.0)));
    u_xlat3.xyz = u_xlat1.xyz + Globals._ShadowOffsets[2].xyz;
    u_xlat1.xyz = u_xlat1.xyz + Globals._ShadowOffsets[3].xyz;
    u_xlat2.w = float(_ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat1.xy, saturate(u_xlat1.z), level(0.0)));
    u_xlat2.z = float(_ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat3.xy, saturate(u_xlat3.z), level(0.0)));
    u_xlat16_0 = half4(u_xlat2 * float4(u_xlat16_0.xxxx) + float4(Globals._LightShadowData.xxxx));
    u_xlat16_1 = dot(u_xlat16_0, half4(0.25, 0.25, 0.25, 0.25));
    u_xlat6.xy = input.TEXCOORD5.xy / input.TEXCOORD5.ww;
    u_xlat6.xy = u_xlat6.xy + float2(0.5, 0.5);
    u_xlat16_6 = _LightTexture0.sample(sampler_LightTexture0, u_xlat6.xy).w;
    u_xlatb11 = 0.0<input.TEXCOORD5.z;
    u_xlat16_0.x = (u_xlatb11) ? half(1.0) : half(0.0);
    u_xlat16_0.x = u_xlat16_6 * u_xlat16_0.x;
    u_xlat6.x = dot(input.TEXCOORD5.xyz, input.TEXCOORD5.xyz);
    u_xlat16_6 = _LightTextureB0.sample(sampler_LightTextureB0, u_xlat6.xx).w;
    u_xlat16_0.x = u_xlat16_0.x * u_xlat16_6;
    u_xlat16_0.x = u_xlat16_1 * u_xlat16_0.x;
    u_xlat1.xyz = float3(u_xlat16_0.xxx) * Globals._LightColor0.xyz;
    u_xlat16 = dot(input.TEXCOORD2.xyz, input.TEXCOORD2.xyz);
    u_xlat16 = rsqrt(u_xlat16);
    u_xlat2.xyz = float3(u_xlat16) * input.TEXCOORD2.xyz;
    u_xlat3.xy = input.TEXCOORD0.xy * Globals._normalmap_ST.xy + Globals._normalmap_ST.zw;
    u_xlat16_3.xyz = _normalmap.sample(sampler_normalmap, u_xlat3.xy).xyz;
    u_xlat16_0.xyz = u_xlat16_3.xyz * half3(2.0, 2.0, 2.0) + half3(-1.0, -1.0, -1.0);
    u_xlat3.xyz = float3(u_xlat16_0.yyy) * input.TEXCOORD4.xyz;
    u_xlat3.xyz = float3(u_xlat16_0.xxx) * input.TEXCOORD3.xyz + u_xlat3.xyz;
    u_xlat2.xyz = float3(u_xlat16_0.zzz) * u_xlat2.xyz + u_xlat3.xyz;
    u_xlat16 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat16 = rsqrt(u_xlat16);
    u_xlat2.xyz = float3(u_xlat16) * u_xlat2.xyz;
    u_xlat3.xyz = Globals._WorldSpaceLightPos0.www * (-input.TEXCOORD1.xyz) + Globals._WorldSpaceLightPos0.xyz;
    u_xlat16 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat16 = rsqrt(u_xlat16);
    u_xlat3.xyz = float3(u_xlat16) * u_xlat3.xyz;
    u_xlat4.xyz = (-input.TEXCOORD1.xyz) + Globals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat16 = dot(u_xlat4.xyz, u_xlat4.xyz);
    u_xlat16 = rsqrt(u_xlat16);
    u_xlat4.xyz = u_xlat4.xyz * float3(u_xlat16) + u_xlat3.xyz;
    u_xlat16 = dot(u_xlat2.xyz, u_xlat3.xyz);
    u_xlat16 = max(u_xlat16, 0.0);
    u_xlat3.xyz = u_xlat1.xyz * float3(u_xlat16);
    u_xlat16 = dot(u_xlat4.xyz, u_xlat4.xyz);
    u_xlat16 = rsqrt(u_xlat16);
    u_xlat4.xyz = float3(u_xlat16) * u_xlat4.xyz;
    u_xlat16 = dot(u_xlat4.xyz, u_xlat2.xyz);
    u_xlat16 = max(u_xlat16, 0.0);
    u_xlat16 = log2(u_xlat16);
    u_xlat2.x = Globals._gloss * 10.0 + 1.0;
    u_xlat2.x = exp2(u_xlat2.x);
    u_xlat16 = u_xlat16 * u_xlat2.x;
    u_xlat16 = exp2(u_xlat16);
    u_xlat1.xyz = float3(u_xlat16) * u_xlat1.xyz;
    u_xlat2.xy = input.TEXCOORD0.xy * Globals._specmap_ST.xy + Globals._specmap_ST.zw;
    u_xlat16_2.xyz = _specmap.sample(sampler_specmap, u_xlat2.xy).xyz;
    u_xlat1.xyz = u_xlat1.xyz * float3(u_xlat16_2.xyz);
    u_xlat2.xy = input.TEXCOORD0.xy * Globals._diffusemap_ST.xy + Globals._diffusemap_ST.zw;
    u_xlat16_2.xyz = _diffusemap.sample(sampler_diffusemap, u_xlat2.xy).xyz;
    u_xlat1.xyz = u_xlat3.xyz * float3(u_xlat16_2.xyz) + u_xlat1.xyz;
    output.SV_Target0.xyz = half3(u_xlat1.xyz);
    output.SV_Target0.w = half(0.0);
    return output;
}
                             Globals �   	      _WorldSpaceCameraPos                         _WorldSpaceLightPos0                        _LightShadowData                        _ShadowOffsets                   0      _LightColor0                  p      _normalmap_ST                     �      _gloss                    �      _specmap_ST                   �      _diffusemap_ST                    �      
   _normalmap                _LightTexture0              _LightTextureB0             _specmap            _diffusemap             _ShadowMapTexture               Globals            