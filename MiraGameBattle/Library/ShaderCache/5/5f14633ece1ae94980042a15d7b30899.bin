2O                         DIRECTIONAL    SHADOWS_SCREEN     _PARALLAXMAP   _SPECGLOSSMAP   4     xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;
struct Globals_Type
{
    float3 _WorldSpaceCameraPos;
    half4 _WorldSpaceLightPos0;
    half4 unity_SHAr;
    half4 unity_SHAg;
    half4 unity_SHAb;
    half4 unity_OcclusionMaskSelector;
    float4 hlslcc_mtx4x4unity_WorldToShadow[16];
    half4 _LightShadowData;
    float4 unity_ShadowFadeCenterAndType;
    float4 hlslcc_mtx4x4unity_MatrixV[4];
    half4 unity_SpecCube0_HDR;
    float4 unity_ProbeVolumeParams;
    float4 hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[4];
    float3 unity_ProbeVolumeSizeInv;
    float3 unity_ProbeVolumeMin;
    half4 _LightColor0;
    half4 _Color;
    half _GlossMapScale;
    half _OcclusionStrength;
    half _Parallax;
};

struct Mtl_FragmentIn
{
    float4 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    half3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    half4 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    half4 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    half4 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    half4 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
    float3 TEXCOORD8 [[ user(TEXCOORD8) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(0) ]];
};

constexpr sampler _mtl_xl_shadow_sampler(address::clamp_to_edge, filter::linear, compare_func::greater_equal);
fragment Mtl_FragmentOut xlatMtlMain(
    constant Globals_Type& Globals [[ buffer(0) ]],
    texture2d<half, access::sample > _ParallaxMap [[ texture (0) ]] ,
    sampler sampler_ParallaxMap [[ sampler (0) ]] ,
    texture2d<half, access::sample > _MainTex [[ texture (1) ]] ,
    sampler sampler_MainTex [[ sampler (1) ]] ,
    texture2d<half, access::sample > _SpecGlossMap [[ texture (2) ]] ,
    sampler sampler_SpecGlossMap [[ sampler (2) ]] ,
    texture2d<half, access::sample > _OcclusionMap [[ texture (3) ]] ,
    sampler sampler_OcclusionMap [[ sampler (3) ]] ,
    texturecube<half, access::sample > unity_SpecCube0 [[ texture (4) ]] ,
    sampler samplerunity_SpecCube0 [[ sampler (4) ]] ,
    texture3d<float, access::sample > unity_ProbeVolumeSH [[ texture (5) ]] ,
    sampler samplerunity_ProbeVolumeSH [[ sampler (5) ]] ,
    depth2d<float, access::sample > _ShadowMapTexture [[ texture (6) ]] ,
    sampler sampler_ShadowMapTexture [[ sampler (6) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    half4 u_xlat16_0;
    float2 u_xlat1;
    half4 u_xlat16_1;
    half u_xlat16_2;
    half4 u_xlat16_3;
    float4 u_xlat4;
    half4 u_xlat16_4;
    bool u_xlatb4;
    float4 u_xlat5;
    half4 u_xlat16_5;
    half4 u_xlat16_6;
    float4 u_xlat7;
    half3 u_xlat16_8;
    half3 u_xlat16_9;
    half3 u_xlat16_10;
    half3 u_xlat16_13;
    half3 u_xlat16_14;
    float3 u_xlat15;
    half u_xlat16_15;
    bool u_xlatb15;
    half u_xlat16_17;
    float u_xlat18;
    half u_xlat16_22;
    half u_xlat16_25;
    float u_xlat26;
    half u_xlat16_26;
    bool u_xlatb26;
    half u_xlat16_28;
    half u_xlat16_33;
    half u_xlat16_36;
    float u_xlat37;
    half u_xlat16_39;
    half u_xlat16_41;
    u_xlat16_0.x = input.TEXCOORD2.w;
    u_xlat16_0.y = input.TEXCOORD3.w;
    u_xlat16_0.z = input.TEXCOORD4.w;
    u_xlat16_33 = dot(u_xlat16_0.xyz, u_xlat16_0.xyz);
    u_xlat16_33 = rsqrt(u_xlat16_33);
    u_xlat16_0.xy = half2(u_xlat16_33) * u_xlat16_0.xy;
    u_xlat16_1.x = _ParallaxMap.sample(sampler_ParallaxMap, input.TEXCOORD0.xy).y;
    u_xlat16_2 = Globals._Parallax * half(0.5);
    u_xlat16_2 = u_xlat16_1.x * Globals._Parallax + (-u_xlat16_2);
    u_xlat16_22 = u_xlat16_0.z * u_xlat16_33 + half(0.419999987);
    u_xlat16_0.xy = u_xlat16_0.xy / half2(u_xlat16_22);
    u_xlat1.xy = float2(u_xlat16_2) * float2(u_xlat16_0.xy) + input.TEXCOORD0.xy;
    u_xlat16_0 = _SpecGlossMap.sample(sampler_SpecGlossMap, u_xlat1.xy);
    u_xlat16_1.xyz = _MainTex.sample(sampler_MainTex, u_xlat1.xy).xyz;
    u_xlat16_1.xyz = u_xlat16_1.xyz * Globals._Color.xyz;
    u_xlat16_2 = max(u_xlat16_0.y, u_xlat16_0.x);
    u_xlat16_2 = max(u_xlat16_0.z, u_xlat16_2);
    u_xlat16_2 = (-u_xlat16_2) + half(1.0);
    u_xlat16_13.xyz = u_xlat16_1.xyz * half3(u_xlat16_2);
    u_xlat16_3.x = dot(input.TEXCOORD4.xyz, input.TEXCOORD4.xyz);
    u_xlat16_3.x = rsqrt(u_xlat16_3.x);
    u_xlat16_1.xyz = u_xlat16_3.xxx * input.TEXCOORD4.xyz;
    u_xlat16_3.x = dot(input.TEXCOORD1.xyz, input.TEXCOORD1.xyz);
    u_xlat16_3.x = rsqrt(u_xlat16_3.x);
    u_xlat16_14.xyz = u_xlat16_3.xxx * input.TEXCOORD1.xyz;
    u_xlat4.xyz = (-input.TEXCOORD8.xyz) + Globals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat5.x = Globals.hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat5.y = Globals.hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat5.z = Globals.hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat4.x = dot(u_xlat4.xyz, u_xlat5.xyz);
    u_xlat15.xyz = input.TEXCOORD8.xyz + (-Globals.unity_ShadowFadeCenterAndType.xyz);
    u_xlat15.x = dot(u_xlat15.xyz, u_xlat15.xyz);
    u_xlat15.x = sqrt(u_xlat15.x);
    u_xlat15.x = (-u_xlat4.x) + u_xlat15.x;
    u_xlat4.x = Globals.unity_ShadowFadeCenterAndType.w * u_xlat15.x + u_xlat4.x;
    u_xlat4.x = u_xlat4.x * float(Globals._LightShadowData.z) + float(Globals._LightShadowData.w);
    u_xlat4.x = clamp(u_xlat4.x, 0.0f, 1.0f);
    u_xlatb15 = Globals.unity_ProbeVolumeParams.x==1.0;
    if(u_xlatb15){
        u_xlatb26 = Globals.unity_ProbeVolumeParams.y==1.0;
        u_xlat5.xyz = input.TEXCOORD8.yyy * Globals.hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[1].xyz;
        u_xlat5.xyz = Globals.hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[0].xyz * input.TEXCOORD8.xxx + u_xlat5.xyz;
        u_xlat5.xyz = Globals.hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[2].xyz * input.TEXCOORD8.zzz + u_xlat5.xyz;
        u_xlat5.xyz = u_xlat5.xyz + Globals.hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[3].xyz;
        u_xlat5.xyz = (bool(u_xlatb26)) ? u_xlat5.xyz : input.TEXCOORD8.xyz;
        u_xlat5.xyz = u_xlat5.xyz + (-Globals.unity_ProbeVolumeMin.xyzx.xyz);
        u_xlat5.yzw = u_xlat5.xyz * Globals.unity_ProbeVolumeSizeInv.xyzx.xyz;
        u_xlat26 = u_xlat5.y * 0.25 + 0.75;
        u_xlat37 = Globals.unity_ProbeVolumeParams.z * 0.5 + 0.75;
        u_xlat5.x = max(u_xlat37, u_xlat26);
        u_xlat5 = unity_ProbeVolumeSH.sample(samplerunity_ProbeVolumeSH, u_xlat5.xzw);
        u_xlat16_5 = half4(u_xlat5);
    } else {
        u_xlat16_5.x = half(1.0);
        u_xlat16_5.y = half(1.0);
        u_xlat16_5.z = half(1.0);
        u_xlat16_5.w = half(1.0);
    }
    u_xlat16_6.x = dot(u_xlat16_5, Globals.unity_OcclusionMaskSelector);
    u_xlat16_6.x = clamp(u_xlat16_6.x, 0.0h, 1.0h);
    u_xlat7.xyz = input.TEXCOORD8.yyy * Globals.hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat7.xyz = Globals.hlslcc_mtx4x4unity_WorldToShadow[0].xyz * input.TEXCOORD8.xxx + u_xlat7.xyz;
    u_xlat7.xyz = Globals.hlslcc_mtx4x4unity_WorldToShadow[2].xyz * input.TEXCOORD8.zzz + u_xlat7.xyz;
    u_xlat7.xyz = u_xlat7.xyz + Globals.hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    u_xlat16_26 = _ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat7.xy, saturate(u_xlat7.z), level(0.0));
    u_xlat16_17 = (-Globals._LightShadowData.x) + half(1.0);
    u_xlat16_17 = u_xlat16_26 * u_xlat16_17 + Globals._LightShadowData.x;
    u_xlat16_17 = half(u_xlat4.x + float(u_xlat16_17));
    u_xlat16_17 = clamp(u_xlat16_17, 0.0h, 1.0h);
    u_xlat16_6.x = min(u_xlat16_17, u_xlat16_6.x);
    u_xlat16_6.x = (u_xlatb15) ? u_xlat16_6.x : u_xlat16_17;
    u_xlat16_4.x = _OcclusionMap.sample(sampler_OcclusionMap, input.TEXCOORD0.xy).y;
    u_xlat16_17 = (-Globals._OcclusionStrength) + half(1.0);
    u_xlat16_17 = u_xlat16_4.x * Globals._OcclusionStrength + u_xlat16_17;
    u_xlat16_28 = (-u_xlat16_0.w) * Globals._GlossMapScale + half(1.0);
    u_xlat16_39 = dot(u_xlat16_14.xyz, u_xlat16_1.xyz);
    u_xlat16_39 = u_xlat16_39 + u_xlat16_39;
    u_xlat16_8.xyz = u_xlat16_1.xyz * (-half3(u_xlat16_39)) + u_xlat16_14.xyz;
    u_xlat16_9.xyz = u_xlat16_6.xxx * Globals._LightColor0.xyz;
    if(u_xlatb15){
        u_xlatb4 = Globals.unity_ProbeVolumeParams.y==1.0;
        u_xlat15.xyz = input.TEXCOORD8.yyy * Globals.hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[1].xyz;
        u_xlat15.xyz = Globals.hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[0].xyz * input.TEXCOORD8.xxx + u_xlat15.xyz;
        u_xlat15.xyz = Globals.hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[2].xyz * input.TEXCOORD8.zzz + u_xlat15.xyz;
        u_xlat15.xyz = u_xlat15.xyz + Globals.hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[3].xyz;
        u_xlat4.xyz = (bool(u_xlatb4)) ? u_xlat15.xyz : input.TEXCOORD8.xyz;
        u_xlat4.xyz = u_xlat4.xyz + (-Globals.unity_ProbeVolumeMin.xyzx.xyz);
        u_xlat4.yzw = u_xlat4.xyz * Globals.unity_ProbeVolumeSizeInv.xyzx.xyz;
        u_xlat15.x = u_xlat4.y * 0.25;
        u_xlat7.x = Globals.unity_ProbeVolumeParams.z * 0.5;
        u_xlat18 = (-Globals.unity_ProbeVolumeParams.z) * 0.5 + 0.25;
        u_xlat15.x = max(u_xlat15.x, u_xlat7.x);
        u_xlat4.x = min(u_xlat18, u_xlat15.x);
        u_xlat5 = unity_ProbeVolumeSH.sample(samplerunity_ProbeVolumeSH, u_xlat4.xzw);
        u_xlat7.xyz = u_xlat4.xzw + float3(0.25, 0.0, 0.0);
        u_xlat7 = unity_ProbeVolumeSH.sample(samplerunity_ProbeVolumeSH, u_xlat7.xyz);
        u_xlat4.xyz = u_xlat4.xzw + float3(0.5, 0.0, 0.0);
        u_xlat4 = unity_ProbeVolumeSH.sample(samplerunity_ProbeVolumeSH, u_xlat4.xyz);
        u_xlat16_1.w = half(1.0);
        u_xlat16_10.x = half(dot(u_xlat5, float4(u_xlat16_1)));
        u_xlat16_10.y = half(dot(u_xlat7, float4(u_xlat16_1)));
        u_xlat16_10.z = half(dot(u_xlat4, float4(u_xlat16_1)));
    } else {
        u_xlat16_1.w = half(1.0);
        u_xlat16_10.x = dot(Globals.unity_SHAr, u_xlat16_1);
        u_xlat16_10.y = dot(Globals.unity_SHAg, u_xlat16_1);
        u_xlat16_10.z = dot(Globals.unity_SHAb, u_xlat16_1);
    }
    u_xlat16_10.xyz = u_xlat16_10.xyz + input.TEXCOORD5.xyz;
    u_xlat16_10.xyz = max(u_xlat16_10.xyz, half3(0.0, 0.0, 0.0));
    u_xlat16_4.xyz = log2(u_xlat16_10.xyz);
    u_xlat16_4.xyz = u_xlat16_4.xyz * half3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_4.xyz = exp2(u_xlat16_4.xyz);
    u_xlat16_4.xyz = u_xlat16_4.xyz * half3(1.05499995, 1.05499995, 1.05499995) + half3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_4.xyz = max(u_xlat16_4.xyz, half3(0.0, 0.0, 0.0));
    u_xlat16_10.xyz = half3(u_xlat16_17) * u_xlat16_4.xyz;
    u_xlat16_6.x = (-u_xlat16_28) * half(0.699999988) + half(1.70000005);
    u_xlat16_6.x = u_xlat16_6.x * u_xlat16_28;
    u_xlat16_6.x = u_xlat16_6.x * half(6.0);
    u_xlat16_4 = unity_SpecCube0.sample(samplerunity_SpecCube0, float3(u_xlat16_8.xyz), level(float(u_xlat16_6.x)));
    u_xlat16_6.x = u_xlat16_4.w + half(-1.0);
    u_xlat16_6.x = Globals.unity_SpecCube0_HDR.w * u_xlat16_6.x + half(1.0);
    u_xlat16_6.x = u_xlat16_6.x * Globals.unity_SpecCube0_HDR.x;
    u_xlat16_8.xyz = u_xlat16_4.xyz * u_xlat16_6.xxx;
    u_xlat16_6.xyw = half3(u_xlat16_17) * u_xlat16_8.xyz;
    u_xlat16_8.xyz = (-input.TEXCOORD1.xyz) * u_xlat16_3.xxx + Globals._WorldSpaceLightPos0.xyz;
    u_xlat16_3.x = dot(u_xlat16_8.xyz, u_xlat16_8.xyz);
    u_xlat16_4.x = max(u_xlat16_3.x, half(0.00100000005));
    u_xlat16_3.x = rsqrt(u_xlat16_4.x);
    u_xlat16_8.xyz = u_xlat16_3.xxx * u_xlat16_8.xyz;
    u_xlat16_3.x = dot(u_xlat16_1.xyz, Globals._WorldSpaceLightPos0.xyz);
    u_xlat16_3.x = clamp(u_xlat16_3.x, 0.0h, 1.0h);
    u_xlat16_41 = dot(u_xlat16_1.xyz, u_xlat16_8.xyz);
    u_xlat16_41 = clamp(u_xlat16_41, 0.0h, 1.0h);
    u_xlat16_14.x = dot(u_xlat16_1.xyz, (-u_xlat16_14.xyz));
    u_xlat16_14.x = clamp(u_xlat16_14.x, 0.0h, 1.0h);
    u_xlat16_25 = dot(Globals._WorldSpaceLightPos0.xyz, u_xlat16_8.xyz);
    u_xlat16_25 = clamp(u_xlat16_25, 0.0h, 1.0h);
    u_xlat16_36 = u_xlat16_28 * u_xlat16_28;
    u_xlat16_8.x = u_xlat16_41 * u_xlat16_41;
    u_xlat16_4.x = u_xlat16_36 * u_xlat16_36 + half(-1.0);
    u_xlat16_4.x = u_xlat16_8.x * u_xlat16_4.x + half(1.00001001);
    u_xlat16_15 = max(u_xlat16_25, half(0.319999993));
    u_xlat16_26 = u_xlat16_28 * u_xlat16_28 + half(1.5);
    u_xlat16_15 = u_xlat16_26 * u_xlat16_15;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_15;
    u_xlat16_4.x = u_xlat16_36 / u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_4.x + half(-9.99999975e-05);
    u_xlat16_25 = max(u_xlat16_4.x, half(0.0));
    u_xlat16_25 = min(u_xlat16_25, half(100.0));
    u_xlat16_36 = u_xlat16_28 * u_xlat16_36;
    u_xlat16_36 = (-u_xlat16_36) * half(0.280000001) + half(1.0);
    u_xlat16_2 = u_xlat16_0.w * Globals._GlossMapScale + (-u_xlat16_2);
    u_xlat16_2 = u_xlat16_2 + half(1.0);
    u_xlat16_2 = clamp(u_xlat16_2, 0.0h, 1.0h);
    u_xlat16_8.xyz = half3(u_xlat16_25) * u_xlat16_0.xyz + u_xlat16_13.xyz;
    u_xlat16_8.xyz = u_xlat16_9.xyz * u_xlat16_8.xyz;
    u_xlat16_13.xyz = u_xlat16_13.xyz * u_xlat16_10.xyz;
    u_xlat16_13.xyz = u_xlat16_8.xyz * u_xlat16_3.xxx + u_xlat16_13.xyz;
    u_xlat16_3.xzw = u_xlat16_6.xyw * half3(u_xlat16_36);
    u_xlat16_14.x = (-u_xlat16_14.x) + half(1.0);
    u_xlat16_14.x = u_xlat16_14.x * u_xlat16_14.x;
    u_xlat16_14.x = u_xlat16_14.x * u_xlat16_14.x;
    u_xlat16_6.xyz = (-u_xlat16_0.xyz) + half3(u_xlat16_2);
    u_xlat16_6.xyz = u_xlat16_14.xxx * u_xlat16_6.xyz + u_xlat16_0.xyz;
    output.SV_Target0.xyz = u_xlat16_3.xzw * u_xlat16_6.xyz + u_xlat16_13.xyz;
    output.SV_Target0.w = half(1.0);
    return output;
}
                           Globals 6        _WorldSpaceCameraPos                         _WorldSpaceLightPos0                    
   unity_SHAr                      
   unity_SHAg                       
   unity_SHAb                   (      unity_OcclusionMaskSelector                  0      _LightShadowData                 @     unity_ShadowFadeCenterAndType                     P     unity_SpecCube0_HDR                  �     unity_ProbeVolumeParams                   �     unity_ProbeVolumeSizeInv                        unity_ProbeVolumeMin                       _LightColor0                       _Color                   (     _GlossMapScale                   0     _OcclusionStrength                   2  	   _Parallax                    4     unity_WorldToShadow                 @      unity_MatrixV                    `     unity_ProbeVolumeWorldToObject                   �        _ParallaxMap              _MainTex            _SpecGlossMap               _OcclusionMap               unity_SpecCube0             unity_ProbeVolumeSH             _ShadowMapTexture               Globals            